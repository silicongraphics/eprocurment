<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', function () {
    if(Auth::guard('client')->check())
        return redirect('client');
    else if(Auth::guard('vendor')->check())
        return redirect('vendor');
    else if(Auth::check())
        return redirect('admin');
    else
        return view('auth.login-type');
})->name('logintype');    


Route::get('removehypen', function(){
    $cat = DB::table('categories')->get();
    foreach($cat as $c){
        $name =  $c->name;
        $str = str_replace('_', ' ', $name);
        DB::table('categories')->where('id',$c->id)->update(['name' => $str]);
    }
});

Route::get('santizeProduct', function(){
    $cat = DB::table('products')->get();
    foreach($cat as $c){
        $name =  $c->attributes;
        $str = str_replace(',"}', '}', $name);
        DB::table('products')->where('id',$c->id)->update(['attributes' => $str]);
    }
});

Route::get('/login/client', 'Auth\LoginController@showClientLogin');
Route::get('/login/vendor', 'Auth\LoginController@showVendorLogin');
Route::get('/register/client', 'Auth\RegisterController@showRegistrationForm');
Route::post('/register/client', 'Auth\RegisterController@clientRegister');
Route::post('/login/client', 'Auth\LoginController@clientLogin');
Route::post('/login/vendor', 'Auth\LoginController@vendorLogin');
Route::get('/register/vendor', 'Auth\RegisterController@showRegistrationVendorForm');
Route::post('/register/vendor', 'Auth\RegisterController@vendorRegister');

// Generic Rest Api Routes

Route::get('/products/searchByPart','ProductController@searchByPart');
Route::get('/products/searchByCategories', 'ProductController@searchByCats');
Route::get('/products/link-to-supplier','ProductController@linkProductsToSupplier');
Route::get('/products/link-all-manuf-product-to-supplier','ProductController@linkProductsByManufactureToSupplier');
Route::get('/products/get-stored-search','ProductController@getStoredSearch');
Route::get('/products/get-rfq-selections','ProductController@getRFQselections');
Route::get('/products/get-general-terms','ProductController@getGeneralTerms');
Route::get('/products/delete-stored-search','ProductController@removeStoredSearch');
Route::get('/products/get-part-no-lists','ProductController@getPartNoList');
Route::get('/product/remove-selection/{id}', 'ProductController@removeSelection');
Route::get('/product/remove-rfq-selection/{partno}/{name?}', 'ProductController@removeRfqSelection');
Route::get('/product/get-categories', 'ProductController@getCategoryList');
Route::get('/product/get-categories-dp', 'ProductController@getCategoryDpList');
Route::get('/product/get-categories-level-list', 'ProductController@getCatLevelList');
Route::post('product/save-rfq-selections', 'ProductController@saveRfqSelections');
Route::post('/products/search','ProductController@search');
Route::get('/products/search','ProductController@search');
Route::post('/product/remove-rfq-selection', 'ProductController@removeRfqSelection_v1');

//  Client Routes

Route::group(['prefix' => 'client'], function(){
    Route::get('/', 'ClientController@index')->name('clienthome');    
    Route::get('create-rfq', 'ClientController@createRfq')->name('createrfq');
    Route::get('/get-my-general-terms','ClientController@getMyGeneralTerms');
    Route::get('my-rfqs', 'ClientController@myRfq')->name('client-my-rfq');
    Route::get('my-saved-rfqs', 'ClientController@mysavedRfq')->name('client-my-saved-rfq');
    Route::get('my-estimates', 'ClientController@myEstimates')->name('client-my-estimates');
    Route::get('my-purchase-orders', 'ClientController@myPos')->name('client-po');
    Route::get('purchase-order-detail/{id}', 'ClientController@poDetail');
    Route::get('my-rfq/{id}', 'ClientController@rfqDetail');
    Route::get('rfq/delete-product/{id}/{supp}', 'ClientController@deleteProductSupp');
    Route::get('my-estimate/{id}', 'ClientController@estimateDetail');
    Route::post('send-po-to-supplier', 'ClientController@sendPotoSupplier');
    Route::post('estimate/change-of-request', 'ClientController@changeOfRequest');
    Route::post('submit/create-my-rfq', 'ClientController@submitRFQ');
    Route::post('submit/save-my-rfq', 'ClientController@saveRFQ');
    Route::post('send-rfq-to-supplier', 'ClientController@submitRFQtoSuppInd');
    Route::post('submit-purchase-order','ClientController@submitPurchaseOrder'); 
    Route::get('/password/reset', 'Auth\Client\ForgotPasswordController@showLinkRequestForm')->name('client.password.request');
    Route::post('/password/email', 'Auth\Client\ForgotPasswordController@sendResetLinkEmail')->name('client.password.email');
    Route::get('/password/reset/{token}', 'Auth\Client\ResetPasswordController@showResetForm')->name('client.password.reset');
    Route::post('/password/reset', 'Auth\Client\ResetPasswordController@reset');    
});

// For Country and City DropDown
Route::post('cities/getCitiesByCountryId', 'ClientController@getcities');

//  Admin Routes
   Route::group(['prefix' => 'admin'], function(){
   /* Route::get('/', function(){
        return view('admin.home');
    }); */

    Route::get('getroducts', 'Admin\Select2Controller@getroducts');
    Route::get('/', 'AdminController@index')->name('admin'); 
    /* Product */
    Route::get('product.create', 'Admin\ProductController@create')->name('addproduct');  
    Route::get('products', 'Admin\ProductController@index')->name('products'); 
    Route::post('getproducts', 'Admin\ProductController@getproducts')->name('getproducts');
    Route::post('product.save', 'Admin\ProductController@store')->name('product.save');
    Route::delete('product.delete/{id}', 'Admin\ProductController@destroy')->name('product.delete');
    Route::get('product.show/{product}', 'Admin\ProductController@show')->name('product.show');
    Route::get('product.edit/{id}', 'Admin\ProductController@edit')->name('product.edit');
    Route::post('product.update/{id}', 'Admin\ProductController@update')->name('product.update');;  
    /* Product */
    /* Attributes */
    Route::get('productattributes', 'Admin\AttributeController@index')->name('productattributes');
    Route::get('attribute.create', 'Admin\AttributeController@create')->name('addattribute');
    Route::post('attribute.store', 'Admin\AttributeController@store')->name('saveattribute');  
    Route::delete('attribute.delete/{id}', 'Admin\AttributeController@destroy')->name('deleteattribute');
    Route::get('attribute.edit/{id}', 'Admin\AttributeController@edit')->name('editattribute'); 
    Route::post('attribute.update/{id}', 'Admin\AttributeController@update')->name('attribute.update'); 
    Route::post('getattributeoptions', 'Admin\AttributeController@getattributeoptions'); 
    Route::get('attribute.show/{id}', 'Admin\AttributeController@show')->name('attribute.show'); 
    Route::post('getattributes', 'Admin\AttributeController@getattributes')->name('getattributes');  
    /* Attributes */
    /* Categories */  
    Route::get('categories', 'Admin\CategoryController@index')->name('categories'); 
    Route::get('category/get-list', 'Admin\CategoryController@getCategoryList');
    Route::get('categories/create', 'Admin\CategoryController@create')->name('addcategory');
    Route::post('savecategory', 'Admin\CategoryController@store')->name('savecategory');  
    Route::get('category.edit/{id}', 'Admin\CategoryController@edit')->name('editcategory'); 
    Route::post('category.update/{id}', 'Admin\CategoryController@update')->name('category.update'); 
    Route::delete('category.delete/{id}', 'Admin\CategoryController@destroy')->name('deletecategory');
    Route::get('category.show/{id}', 'Admin\CategoryController@show')->name('category.show'); 
    Route::post('getcategories', 'Admin\CategoryController@getcategories')->name('getcategories');       
    /* Categories */  
    /* Brands */
    Route::get('brands', 'Admin\BrandController@index')->name('brands');
    Route::get('brand.create', 'Admin\BrandController@create')->name('addbrand');  
    Route::post('getbrands', 'Admin\BrandController@getbrands')->name('getbrands');
    Route::post('brand.store', 'Admin\BrandController@store')->name('savebrand'); 
    Route::delete('brand.delete/{id}', 'Admin\BrandController@destroy')->name('deletebrand');
    Route::get('brand.edit/{id}', 'Admin\BrandController@edit')->name('editbrand');  
    Route::post('brand.update/{id}', 'Admin\BrandController@update')->name('brand.update');  
    Route::get('brand.show/{id}', 'Admin\BrandController@show')->name('brand.show'); 
    Route::delete('brand.delete/{id}', 'Admin\BrandController@destroy')->name('brand.delete');     
    /* Brands */ 
    /* Applications */ 
    Route::get('applications', 'Admin\ApplicationController@index')->name('applications'); 
    Route::get('application.create', 'Admin\ApplicationController@create')->name('addapplication'); 
    Route::get('application.edit/{id}', 'Admin\ApplicationController@edit')->name('editapplication');   
    Route::post('getapplications', 'Admin\ApplicationController@getapplications')->name('getapplications'); 
    Route::post('application.store', 'Admin\ApplicationController@store')->name('saveapplication'); 
    Route::delete('application.delete/{id}', 'Admin\ApplicationController@destroy')->name('deleteapplication');
    Route::post('application.update/{id}', 'Admin\ApplicationController@update')->name('application.update'); 
    Route::get('application.show/{id}', 'Admin\ApplicationController@show')->name('application.show');    
     /* Applications */
    /* Client */  
    Route::get('addclient', 'Admin\ClientController@create')->name('addclient'); 
    Route::get('companies', 'Admin\ClientController@index')->name('companies'); 
    Route::get('pendingclients', 'Admin\ClientController@pendingclients')->name('pendingclients');  
    Route::post('getclients', 'Admin\ClientController@getclients')->name('getclients');
    Route::post('users.get', 'Admin\ClientController@getusers')->name('users.get');
    Route::post('client.save', 'Admin\ClientController@store')->name('client.save');
    Route::delete('client.delete/{id}', 'Admin\ClientController@destroy')->name('client.delete');
    Route::get('client.show/{id}', 'Admin\ClientController@show')->name('client.show');  
    Route::get('client.edit/{id}', 'Admin\ClientController@edit')->name('client.edit'); 
    Route::post('client.update/{id}', 'Admin\ClientController@update')->name('client.update');  
    /* Client */  
    /* Supplier */
    Route::get('supplier.create', 'Admin\SupplierController@create')->name('addsupplier'); 
    Route::get('suppliercompanies', 'Admin\SupplierController@index')->name('suppliercompanies');  
    Route::post('getsuppliers', 'Admin\SupplierController@getsuppliers')->name('getsuppliers'); 
    Route::post('supplier.save', 'Admin\SupplierController@store')->name('supplier.save'); 
    Route::delete('supplier.delete/{id}', 'Admin\SupplierController@destroy')->name('supplier.delete');
    Route::get('supplier.show/{id}', 'Admin\SupplierController@show')->name('supplier.show');  
    Route::get('supplier.edit/{id}', 'Admin\SupplierController@edit')->name('supplier.edit'); 
    Route::post('supplier.update/{id}', 'Admin\SupplierController@update')->name('supplier.update');
    Route::post('usersupplier.get', 'Admin\SupplierController@getusers')->name('usersupplier.get');   
    /* Supplier */ 
    /* Supplier Users */
    Route::post('getsupplierusers', 'Admin\SupplierUserController@getsupplierusers')->name('getsupplierusers');
    Route::get('supplierusers', 'Admin\SupplierUserController@index')->name('supplierusers'); 
    Route::get('supplieruser.create', 'Admin\SupplierUserController@create')->name('supplieruser');
    Route::post('supplieruser.store', 'Admin\SupplierUserController@store')->name('savesupplieruser');
    Route::delete('supplieruser.delete/{id}', 'Admin\SupplierUserController@destroy')->name('deletesupplieruser'); 
    Route::get('supplieruser.edit/{id}', 'Admin\SupplierUserController@edit')->name('supplieruser.edit'); 
    Route::post('supplieruser.update/{id}', 'Admin\SupplierUserController@update')->name('supplieruser.update'); 
    Route::get('supplieruser.show/{id}', 'Admin\SupplierUserController@show')->name('supplieruser.show');  
    /* Supplier Users */
    /* Client User */
    Route::get('clientusers', 'Admin\ClientUserController@index')->name('clientusers');
    Route::post('getclientusers', 'Admin\ClientUserController@getclientusers')->name('getclientusers');
    Route::get('clientuser.create', 'Admin\ClientUserController@create')->name('clientuser');
    Route::post('clientuser.store', 'Admin\ClientUserController@store')->name('saveclientuser');
    Route::delete('clientuser.delete/{id}', 'Admin\ClientUserController@destroy')->name('deleteclientuser'); 
    Route::get('clientuser.edit/{id}', 'Admin\ClientUserController@edit')->name('editclientuser'); 
    Route::post('clientuser.update/{id}', 'Admin\ClientUserController@update')->name('clientuser.update');
    Route::get('clientuser.show/{id}', 'Admin\ClientUserController@show')->name('clientuser.show');    
    /* Client User */
    /* Importer */
    Route::get('import', 'ImportController@index')->name('import'); 
    Route::post('uploadFile', 'ImportController@uploadFile')->name('uploadFile'); 
    Route::post('importImages', 'ImportController@importImages')->name('uploadImgFile'); 
});

// Vendor Routes

Route::group(['prefix' => 'vendor'], function(){
    Route::get('/', 'VendorController@index')->name('vendorHome');    
    Route::get('my-rfqs', 'VendorController@myRfq')->name('vendor-my-rfq');
    Route::get('my-products', 'VendorController@vendorProducts')->name('vendor-my-products');
    Route::get('my-estimates', 'VendorController@myEstimates')->name('vendor-my-estimated');
    Route::get('estimate/{id}', 'VendorController@estimateDetail');
    Route::get('/rfq/{id}','VendorController@rfqDetails');
    Route::get('my-purchase-orders', 'VendorController@myPos')->name('vendor-po');
    Route::get('purchase-order-detail/{id}', 'VendorController@poDetail');
    Route::post('/estimate/update-rfq-terms','VendorController@submitEstimate');
    Route::post('submit-rfq','VendorController@submitRfq');
    Route::post('approve-po-of-client','VendorController@approvePurchaseOrder');
    Route::get('/password/reset', 'Auth\Vendor\ForgotPasswordController@showLinkRequestForm')->name('vendor.password.request');
    Route::post('/password/email', 'Auth\Vendor\ForgotPasswordController@sendResetLinkEmail')->name('vendor.password.email');
    Route::get('/password/reset/{token}', 'Auth\Vendor\ResetPasswordController@showResetForm')->name('vendor.password.reset');
    Route::post('/password/reset', 'Auth\Vendor\ResetPasswordController@reset');    
});





