function clickAdd(text, key)
{
    var textWithSpaces = text;
    text =	text.replace(/\s/g, '');
    var firstKeyValue =	index =	0;
    var url =  base_url + "admin/getattributeoptions";
    $.ajax({
         url: url,
         type: 'post',
         data: {
            attr_name: text,
            attr_id: key
         },
         success: function(data) {
            $('#showattributes').append(data.html);
            $('#showattributes').find('select:last').select2({
                placeholder: 'Select Attribute Value',
            });
        },
        error: function(error){
            throw error;
        }
    });
}

            
    function optionExists(key)
    {
      var existing_options = [];
      $(("#showattributes")).find("input:text,select").each(function() {
      var input_type = $(this).data('type');
      if(input_type == 'key') {
         var input_val = $(this).val();
         existing_options.push(input_val);
       }
      });
        return existing_options;
    }

  //For same address
$(".same_addresses").change(function() {
        var checked = $(this).is(':checked');
        if (checked)
        {
            $('.shipping_address').val($('.billing_address').val()).attr("readonly", "readonly");
            $('.shipping_street').val($('.billing_street').val()).attr("readonly", "readonly");
            $('.shipping_pobox').val($('.billing_pobox').val()).attr("readonly", "readonly");
            $('.shipping_country_id').val($('.billing_country_id').val()).trigger('change.select2').attr("readonly", "readonly");
            $('.shipping_city_id').html($('.billing_city_id').html());
            $('.shipping_city_id').val($('.billing_city_id').val()).trigger('change.select2').attr("readonly", "readonly");
        }
        else
        {
            $('.shipping_address').val('').removeAttr("readonly");
            $('.shipping_street').val('').removeAttr("readonly");
            $('.shipping_pobox').val('').removeAttr("readonly");
            $('.shipping_country_id').val('').trigger('change').trigger('change.select2').removeAttr("readonly");
            $('.shipping_city_id').val('').trigger('change.select2').removeAttr("readonly");
        }
}); // same_addresses onchange :: END

$('#next-tab').click(function(e){
//Initialize the validation object which will be called on form submit.
var validobj = $('#ClientAddForm').validate({
onkeyup: false,
errorClass: 'error',
// wrapper: 'div',
messages:{
    "name": {
        required: 'Name of the company is mandatory.'
    },
    "first_name": {
        required: 'Please type in First name.'
    },
    "email": {
        required: 'Enter email address.'
    },
    "password": {
        required: 'Enter password.'
    },
},

//put error message behind each form element
errorPlacement: function (error, element) {
    var elem = $(element);
    error.insertAfter(element);
},

}); // validobj :: END

if($('#ClientAddForm').valid()){ // Checks if form is valid with all validations pass.
    // e.preventDefault();
    $('#company-info').removeClass('active');
    $('#company-info').removeClass('show');
    $('#admin-info').addClass('show');
    $('#admin-info').addClass('active');
    $('#base-tab1').removeClass('active');
    $('#base-tab2').addClass('active in');
    return false;
} else {
   console.log("I'm not valid");
}
}); // next-tab :: END

$('#next-tab-supplier').click(function(e){
//Initialize the validation object which will be called on form submit.
var validobj = $('#SupplierAddForm').validate({
onkeyup: false,
errorClass: 'error',
// wrapper: 'div',

messages:{
    "name": {
        required: 'Name of the company is mandatory.'
    },
    "first_name": {
        required: 'Please type in First name.'
    },
    "email": {
        required: 'Enter email address.'
    },
    "password": {
        required: 'Enter password.'
    },
},

//put error message behind each form element
errorPlacement: function (error, element) {
    var elem = $(element);
    error.insertAfter(element);
},

}); // validobj :: END

if($('#SupplierAddForm').valid()){ // Checks if form is valid with all validations pass.
    // e.preventDefault();
    $('#tab1').removeClass('active');
    $('#tab1').removeClass('show');
    $('#tab2').addClass('show');
    $('#tab2').addClass('active');
    $('#base-tab1').removeClass('active');
    $('#base-tab2').addClass('active in');
    return false;
} else {
   console.log("I'm not valid");
}
}); // next-tab :: END

$(".shipping_country_id").change(function(){
        var parent_id   = $(this).val();
        $.ajax({
            url: base_url + "cities/getCitiesByCountryId",
            type: 'post',
            data: {country_id: parent_id},
            dataType: 'json',
            cache: false,
            success: function (response) {
              if(response.status == 200){
                var citiesDropDown  =   $('.shipping_city_id');
                citiesDropDown.empty();
                citiesDropDown.append('<option> Select your City </option>');
                $.each(response.cities, function(key, value){
                        citiesDropDown.append('<option value="'+value.id+'">'+value.name+'</option>');
                });
                citiesDropDown.trigger('change.select2')
              }
              else 
                console.log('no results found');
              // var data = $.parseJSON(json);
              //  console.log(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // alert(textStatus + " - " + errorThrown + " -> Unable to fetch data.");
                console.log('no country id found something get went wrong');
            }
        });
    });



    $(".billing_country_id").change(function(){
         var parent_id   = $(this).val();
         $.ajax({
            url: base_url + "cities/getCitiesByCountryId",
            type: 'post',
            data: {country_id: parent_id},
            dataType: 'json',
            cache: false,
            success: function (response) {
               if(response.status == 200){
                var citiesDropDown  =   $('.billing_city_id');
                citiesDropDown.empty();
                citiesDropDown.append('<option> Select your City </option>');
                $.each(response.cities, function(key, value){
                        citiesDropDown.append('<option value="'+value.id+'">'+value.name+'</option>');
                });
                citiesDropDown.trigger('change.select2')
              }
              else 
                console.log('no results found');
              // var data = $.parseJSON(json);
              //  console.log(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // alert(textStatus + " - " + errorThrown + " -> Unable to fetch data.");
                console.log('no country id found something get went wrong');
            }
        });
    });