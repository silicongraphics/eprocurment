$('.cor').on('submit', function (e) {
    $('.loading').show();
    e.preventDefault(e);
    var data = $(this).serialize();
    var action = $(this).attr('action');
    $.ajax({
        url: action,
        data: data,
        type: 'post',
        success: function(response){
            $('.loading').hide();
            if(response.status == 200)
                toastr.success(response.msg,'Success');
            setTimeout(function(){
                //location.reload();
                window.location.href = response.url; 
            },2000)
        },
        error: function(error){
            $('.loading').hide();
            throw error;
        }
    })   
})