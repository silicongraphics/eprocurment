$(document).on('click', '.approvePo' ,function(){
    var supp = $(this).data('supp');
    var poid = $(this).data('id');
    $.ajax({
        url: base_url + '/vendor/approve-po-of-client',
        data: {
            supp: supp,
            poid: poid
        },
        type: 'post',
        success: function(response){
            if(response.status == 200){
                toastr.success(response.msg, 'Success');
                setTimeout(function(){
                    location.reload();
                },500);                
            }
            else
                toastr.error(response.msg, 'Error');
        },
        error: function(error){
            throw error;
        }
    })
})