$(function(){
    $('.navfirst').trigger('click');
})
function showSupp(code, supp){
    console.log(supp);
    $('.sup_code').text(code);
    $('.sup_name').text(supp.company_name);
    $('.sup_add').text(supp.billing_address);
    $('.sup_contper').text(supp.approval_name);
    $('.sup_fax').text(supp.fax);
    $('.sup_phone').text(supp.phone);
    $('.supp_email').text(supp.email);
}

$(document).on('click','.sendToIndivSupp', function(){
    $('.loading').show();
    var suppid = $(this).data('supplier');
    var req_no = $(this).data('rfq');
    $.ajax({
        url: base_url + 'client/send-rfq-to-supplier',
        data: {
            suppid:suppid,
            req_no:req_no
        },
        type: 'post',
        success: function(response){
            if(response.status == 200){
                alert(response.msg);
                toastr.success('Success', response.msg);
            }else{
                alert(response.msg);
                toastr.error('Error', response.msg);
            }
            $('.loading').hide();
        },
        error: function(error){
            $('.loading').hide();
            throw error;
        }
    })
})

$('.removeProd').on('click', function(){
    var id = $(this).data('id');
    if (confirm("Are you sure ?")) {
        $(this).parents('.srfq-box-main').find('input').removeAttr('required');
        $(this).parents('.srfq-box-main').hide();
        $('input[name=disabled'+id+']').val(1);
        $('input[name=quantity'+id+']').val(0);
        $('input[name=unit_price'+id+']').val(0);
    }
});

