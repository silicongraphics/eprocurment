$(document).on('change', '#search', function(){
    var value = $(this).val();
    $.ajax({
        url: base_url + '/products/searchByPart?part_no='+value,
        success: function(response){
            $('.result-content ul').html(`
                <li>${response.name}</li>
                <li>${response.part_no}</li>
                <li>${response.manufacture}</li>				
            `);
            $('.addProd').data('id', response.id);
        }
    })
});

$(document).on('click', '.addProd', function(){
    var value = $(this).data('id');
    if(value){
        $.ajax({
            url: base_url + '/products/link-to-supplier?id='+value,
            success: function(response){
                if(response.status == 200)
                    toastr.success('Product is linked to supplier','Success');
                else
                    toastr.error('Product is already linked to supplier','Error');
            }
        })
    }

});

$(document).on('click', '.addProdM', function(){
    var value = $('#manu_id').val();
    $.ajax({
        url: base_url + '/products/link-all-manuf-product-to-supplier?manu_id='+value,
        success: function(response){
            if(response.status == 200)
                toastr.success('All Product are linked to supplier','Success');
            else
                toastr.error('Some error occured','Error');
        }
    })
});