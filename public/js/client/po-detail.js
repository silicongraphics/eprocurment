$(document).on('click', '.sendPo' ,function(){
    var supp = $(this).data('supp');
    var poid = $(this).data('id');
    $.ajax({
        url: base_url + '/client/send-po-to-supplier',
        data: {
            supp: supp,
            poid: poid
        },
        type: 'post',
        success: function(response){
            if(response.status == 200)
                toastr.success(response.msg, 'Success');
            else
                toastr.error(response.msg, 'Error');
        },
        error: function(error){
            throw error;
        }
    })
})