var clicked = false;

$('.changeofrequest').on('submit', function(e){
    e.preventDefault(e);
    $('.loading').show();
    var data = $(this).serialize();
    var action = $(this).attr('action');
    $.ajax({
        url: action,
        type: 'post',
        data: data,
        success: function(response){
            if(response.status == 200)
                toastr.success(response.msg,'Success');
            else
                toastr.error(response.msg,'Error');
            
            $('.loading').hide();
        },
        error: function(error){
            $('.loading').hide();
            throw error;
        }
    }) 
});

$(document).on('click', '.createPo', function() {
    if(clicked == false){
        clicked = true;
        $('.loading').show();
        var type = $(this).data('type');
        var array = [];
        $('input[name^="selections"]').each(function() {
            if($(this).is(':checked')){            
                array.push({
                    supplier_id: $(this).data('supp'),
                    id: $(this).val(),
                });
            }        
        });
    
        if(!array.length){
            toastr.error('Please select some supplier to proceed','Error');
            $('.loading').hide();
            return false;
        }
        $.ajax({
            url: base_url + '/client/submit-purchase-order',
            data: {
                array: array,
                type: type,
                id: $('#rfq_id').val(),
            },
            type: 'post',
            success: function(response){
                if(response.status == 200)
                    window.location.href = base_url + '/client/my-purchase-orders'; 
                $('.loading').hide();
    
            },
            error: function(error){
                $('.loading').hide();
                throw error;
            }
        })
    
        console.log(array);
    }

    
});
