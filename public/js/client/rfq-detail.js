$(function(){
    $('.navfirst').trigger('click');
})
function showSupp(code, supp){
    console.log(supp);
    $('.sup_code').text(code);
    $('.sup_name').text(supp.company_name);
    $('.sup_add').text(supp.billing_address);
    $('.sup_contper').text(supp.approval_name);
    $('.sup_fax').text(supp.fax);
    $('.sup_phone').text(supp.phone);
    $('.supp_email').text(supp.email);
}

$(document).on('click','.sendToIndivSupp, .sendToAllSupp', function(){
    $('.loading').show();
    var suppid = '';
    if($(this).data('supplier'))
        suppid = $(this).data('supplier');
    var req_no = $(this).data('rfq');
    $.ajax({
        url: base_url + 'client/send-rfq-to-supplier',
        data: {
            suppid:suppid,
            req_no:req_no
        },
        type: 'post',
        success: function(response){
            if(response.status == 200){
                toastr.success('Success', response.msg);
                $('.rms'+suppid).remove();
                setTimeout(function(){
                    location.reload();
                },2000);

            }else{
                toastr.error('Error', response.msg);
            }
            $('.loading').hide();

        },
        error: function(error){
            $('.loading').hide();
            throw error;
        }
    })
})
