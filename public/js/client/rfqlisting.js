$(function() {
    $(".filter").on("change", function() {
        var value = $(this).data('sel').toLowerCase();
        $("table > tbody > tr").filter(function() {     
             $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});