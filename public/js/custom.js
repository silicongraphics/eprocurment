
$(document).ready(function(){
	$('.content-box-1.rfq-add.f-wi.has-rfq .zone-scroll').css('width',$('.supp-rfq-det-main .srfq-box-main').length*320 + 15);
    $('.left-panel ul.main-menu li.parent a').click(function() {
        $(this).parent().find('ul.dropdown.tog').slideToggle();
    });
    // $('.popup_open').click(function() {
    //     $('#ad-pop').show(300);
    // });
    // $('a.close').click(function() {
    //     $('#ad-pop').hide(300);
    // });
    $('.menu-ico').click(function () {
        $('.menu-main-has').addClass("sh-has");
    });
    $('img.menu-close-has').click(function () {
        $('.menu-main-has').removeClass("sh-has");
    });

    $(document).on('click', '.btn-h-main .popup_open', function () {
        $(this).next('.pop-over-main').show(300).addClass('visibleit');
        //$('.overlay-form').fadeIn(300);
    });
    $(document).on('click', '.btn-search.popup_open', function () {
        $('.search-popover-has').show(300).addClass('visibleit');
        //$('.overlay-form').fadeIn(300);
    });
    $(document).on('click', '.rfq-d-img .popup_open, .image-details .popup_open, .feild-supp .fa-search.popup_open, .feild-supp .fa-comments.popup_open, .feild-supp .fa-edit.popup_open', function () {
        $(this).next('.pop-over-main').show(300).addClass('visibleit');
        //$('.overlay-form').fadeIn(300);
    });
    $(document).on('click', '.popover-inner i.fa.fa-close, .overlay-form, h2.title.gradient img', function () {
        $('.pop-over-main').hide(300).removeClass('visibleit');
        $('.search-popover-has').hide(300).removeClass('visibleit');
        $('.overlay-form').fadeOut(300);
    });

    $(document).on('click', '.closepopup', function () {
        $('.pop-over-main').hide(300).removeClass('visibleit');
        $('.search-popover-has').hide(300).removeClass('visibleit');
        $('.overlay-form').fadeOut(300);
    });

    $(document).on('click', '.closeD', function () {
        $('.pop-over-main').hide(300).removeClass('visibleit');
        $('.search-popover-has').hide(300).removeClass('visibleit');
        $('.overlay-form').fadeOut(300);
    });    

    $(document).on('click', '.rmt', function () {
		$(this).next('.pop-over-main').show(300).addClass('visibleit');
        $('.overlay-form').fadeIn(300);
    });


    $(function(){
            // $('.order-panel').slimScroll({
            //     wheelStep: 20,
            //     color: '#dedc03'
            // });
    });
    $(document).on('click', '.menu-ico', function () {
        $('.dashboard-main').toggleClass('hide-menu');
    });
    $(function () {
        var url = window.location.pathname; //sets the variable "url" to the pathname of the current window
        var activePage = url.substring(url.lastIndexOf('/') + 1); //sets the variable "activePage" as the substring after the last "/" in the "url" variable
        $('.main-menu li a').each(function () { //looks in each link item within the primary-nav list
            var linkPage = this.href.substring(this.href.lastIndexOf('/') + 1); //sets the variable "linkPage" as the substring of the url path in each &lt;a&gt;

            if (activePage == linkPage) { //compares the path of the current window to the path of the linked page in the nav item
                $(this).parent().addClass('active'); //if the above is true, add the "active" class to the parent of the &lt;a&gt; which is the &lt;li&gt; in the nav list
            }
        });
    })


});
