<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
	protected $fillable = ['id', 'name', 'parent_id', 'description', 'status', 'custom_id', 'slug', 'label_name', 'level'];
     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function parentcategory() {
    	 return $this->belongsTo(Category::class, 'parent_id');
    }

    public function getparents() {
    	 $data = self::whereIn('id', function($query){
                $query->select('parent_id')->distinct()
                      ->from('categories');
        })->get();
       return $data;
    }

    // recursive, loads all descendants
    public function subs()
    {
       return $this->selectAttributeChildren()->with('subs');
    }

    public function selectAttributeChildren()
    {
       return $this->hasMany('App\Models\Category', 'parent_id')->select(['id', 'name as title', 'parent_id', 'label_name']);
    }    
    
    // parent
    public function parent()
    {
       return $this->belongsTo('App\Models\Category','parent_id');
    }
    
    // all ascendants
    public function parentRecursive()
    {
       return $this->parent()->with('parentRecursive');
    }    
    
	public static function increment_string($str, $separator = '-', $first = 1)
	{
	    preg_match('/(.+)'.$separator.'([0-9]+)$/', $str, $match);
	    return isset($match[2]) ? $match[1].$separator.($match[2] + 1) : $str.$separator.$first;
	}

	public static function slugRecurrsion($index,$slug){
	    $chk = self::where('slug', $slug)->first();
		if($chk){
			$index = $index++;
			return self::slugRecurrsion($index, self::increment_string($slug, '-', $index));
		}else{
			return $slug;
		}
    }
    
    public static function findBySlug($slug){
        return self::where('slug',$slug)->first();
    }


	public static function slug($input){
		$slug = str_slug($input);
		return self::slugRecurrsion(1,$slug);
	}


}
