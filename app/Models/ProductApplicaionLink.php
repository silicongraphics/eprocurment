<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductApplicaionLink extends Model
{
    protected $table = "product_application_links";
    protected $fillable = ['application_id'];
}
