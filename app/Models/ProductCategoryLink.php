<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryLink extends Model
{
    protected $table = 'product_category_links';
}
