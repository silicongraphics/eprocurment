<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientBrandLink extends Model
{
    protected $table = 'client_brand_links';
}
