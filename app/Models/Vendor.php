<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Vendor extends Model
{
    protected $table = 'vendors';
    protected $fillable = ['custom_id', 'name', 'description', 'status', 'company_id'];
 
    public function company()
    {
        return $this->belongsTo('App\Models\Company','company_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'vendor_products', 'vendor_id', 'product_id');
    }

     public function user() {
    	return $this->hasMany('App\Models\VendorUser', 'vendor_id', 'id');
    }

    public function documents() {
        return $this->belongsToMany('App\Models\DocType', 'vendor_documents', 'vendor_id', 'doc_type_id');
    }

}
