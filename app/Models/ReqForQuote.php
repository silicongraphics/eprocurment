<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ReqForQuote extends Model
{
    
    // use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    protected $table = 'req_for_quotes';
 
    protected $fillable = [
        'client_id', 'client_user_id', 'name', 'req_number', 'req_code', 'site_area', 'description'
    ];

    protected $orderDirection = 'DESC';
    

    public function selections(){
        return $this->belongsTo('App\Models\RfqSelection','id', 'req_for_quote_id');
    }

    public function terms(){
        return $this->hasMany('App\Models\RfqCartProductTerm','reqForQuote_id');
    }

    public function vendorRfqResponse(){
        return $this->hasMany('App\Models\VendorReceivedRfq','reqForQuote_id');        
    }

    public function rfqSelectionTab(){
        return $this->hasMany('App\Models\RfqSelectionTab','rfq_id');
    }

    // public function terms()
    // {
    //     return $this->hasManyDeep(
    //         '\App\Models\RfqCartProductTerm',
    //         ['\App\Models\RfqSelection', '\App\Models\RfqCartProduct'], // Intermediate models, beginning at the far parent
    //         [
    //            'req_for_quote_id', // Foreign key on the "RfqSelection" table.
    //            'rfq_selection_id',    // Foreign key on the "RfqCartProduct" table.
    //            'rfq_cart_product_id'     // Foreign key on the "RfqCartProductTerm" table.
    //         ],
    //         [
    //           'id', // Local key on the "req_for_quotes" table.
    //           'id', // Local key on the "RfqSelection" table.
    //           'id'  // Local key on the "RfqCartProduct" table.
    //         ]
    //     );
    // }

}
