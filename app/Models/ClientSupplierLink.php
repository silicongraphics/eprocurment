<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientSupplierLink extends Model
{
    protected $table = 'client_supplier_links';
}
