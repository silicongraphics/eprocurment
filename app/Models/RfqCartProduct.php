<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RfqCartProduct extends Model
{
    protected $table = 'rfq_cart_products';
 
    protected $fillable = [
        'rfq_selection_id', 'product_id', 'quantity_requested','reqForQuote_id', 'rfq_sel_tab_id'
    ];

    public function terms(){
        return $this->hasMany('App\Models\RfqCartProductTerm','rfq_cart_product_id');
    }

    public function termsViaSupp($vendor_id){
        return $this->hasMany('App\Models\RfqCartProductTerm','rfq_cart_product_id')->where('vendor_id', $vendor_id);
    }    

}
