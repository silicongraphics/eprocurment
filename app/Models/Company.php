<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Company extends Model
{
    protected $table = 'companies';
	const UPDATED_AT = null;
    protected $fillable = ['company_name', 'website', 'status', 'delivery_method_id',
	'payment_method_id', 'credit_term_id', 'billing_address', 'billing_street', 'billing_pobox',
	'billing_country_id', 'billing_city_id', 'shipping_address', 'shipping_street', 'shipping_pobox',
	'shipping_country_id', 'shipping_city_id', 'trade_license', 'tax_registration_no', 'vat_certificate',
	'approval_name', 'emirates_id', 'type', 'fax', 'phone', 'email'];

	public function vendor() {
		return $this->hasOne('App\Models\Vendor');
	}

	public function buyer() {
		return $this->hasOne('App\Models\Client', 'company_id', 'id');
	}
 
}
