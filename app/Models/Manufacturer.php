<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manufacturer extends Model
{
	use SoftDeletes;
	const UPDATED_AT = null;
	protected $fillable = ['id', 'name', 'description', 'url', 'status', 'custom_id', 'slug'];
    protected $table = 'manufacturers';
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

   /* protected $dispatchesEvents = [
    	'saved' => \App\Events\ManufacturerCreated::class
    ]; */

    public function clientbrands() {
    	return $this->hasMany(ClientBrandLink::class);
    }
 
}
