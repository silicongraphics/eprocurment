<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

// title: Estimate change of Request 

class EstimateCor extends Model
{
    protected $table = 'estimates_cor';
    
    protected $fillable = ['vrfq_id','quantity','reason', 'closed', 'updated_qty', 'updated_price'];

}
