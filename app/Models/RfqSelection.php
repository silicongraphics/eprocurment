<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RfqSelection extends Model
{
    protected $table = 'rfq_selections';
 
    protected $fillable = [
        'req_for_quote_id', 'name', 'description', 'status',
    ];

    public function products(){
        return $this->hasMany('App\Models\RfqCartProduct','rfq_selection_id');
    }

    public function productIds(){
        return $this->hasMany('App\Models\RfqCartProduct','rfq_selection_id')->select('product_id');
    }

}
