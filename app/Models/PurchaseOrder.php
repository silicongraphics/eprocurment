<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PurchaseOrder extends Model
{
    protected $table = 'purchase_orders';

    protected $orderDirection = 'desc';

    protected $orderBy = 'id';

    protected $filleable = ['rfq_id','po_number','status','term_ids','client_id','client_user_id', 'savedPo'];
 
    public function findByRef($id){
        return self::where('po_number',$id)->first();
    }

    public function detail(){
        return $this->hasMany('App\Models\PurchaseOrderDetail','po_id');
    }

}
