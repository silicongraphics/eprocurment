<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RfqCartProductTerm extends Model
{
    protected $table = 'rfq_cart_product_terms';
 
    protected $fillable = [
        'rfq_cart_product_id', 'vendor_id', 'warranty_terms', 'service_terms', 'credit_term_id', 'delivery_terms', 'reqForQuote_id'
    ];

}
