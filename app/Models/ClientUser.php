<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ClientResetPasswordNotification;

class ClientUser extends Authenticatable
{
    use Notifiable;

    protected $table = 'client_users';

    protected $guard = 'client';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'client_id', 'email', 'fax', 
        'mobile_no', 'phone', 'title', 'job_title', 'emirates_id', 'profile_picture', 
        'active', 'verified', 'notes', 'website', 'service_term_id', 
        'warranty_term_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rfq(){
        return $this->hasMany('App\Models\ReqForQuote','client_user_id');
    }    
    
    public function company()
    {
        return $this->belongsTo('App\Models\Company','company_id');
    } 

    public function vendors() {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }
    
    public function documents() {
       return $this->belongsToMany(DocType::class, 'client_documents', 'client_user_id', 'doc_type_id');    
    } 

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ClientResetPasswordNotification($token));
    }


}
