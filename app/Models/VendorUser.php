<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class VendorUser extends Authenticatable
{
    protected $table = 'vendor_users';

    protected $guard = 'vendor';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'client_id', 'email', 'fax', 'mobile_no', 'phone', 'title', 'job_title', 'emirates_id', 'profile_picture', 'active', 'verified', 'notes', 'website'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function vendors() {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company','company_id');
    } 

     public function documents() {
       return $this->belongsToMany(DocType::class, 'vendor_documents', 'vendor_user_id', 'doc_type_id');    
    } 
    
}
