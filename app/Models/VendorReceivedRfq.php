<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VendorReceivedRfq extends Model
{
    protected $table = 'vendor_received_rfqs';

    protected $orderBy = 'id';

    protected $orderDirection = 'DESC';

    protected $fillable = [
        'reqForQuote_id', 'vendor_id', 'warranty_terms', 'service_terms', 'credit_term_id', 'delivery_terms', 'general_remarks',
         'product_id', 'unit_price', 'quantity', 'disabled'
    ];    

    public function changeOfRequest(){
        return $this->hasMany('App\Models\EstimateCor','vrfq_id')->orderBy('id','desc');
    }

}
