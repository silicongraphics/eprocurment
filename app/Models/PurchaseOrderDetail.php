<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PurchaseOrderDetail extends Model
{
    protected $table = 'purchase_order_detail';

    protected $filleable = ['po_id','supplier','product_id','created_at','updated_at','quantity','price','warranty_term',
        'service_term','payment_term','delievery_term','status'];
 
}
