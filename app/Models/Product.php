<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    
    protected $fillable = ['custom_id', 'name', 'image', 'part_number', 'manufacturer_id', 'category_ids', 'attributes', 'description', 'status', 'priority'];

    protected $casts = [
      // 'attributes' => 'json'
    ];

    static public function searchByPart($part_no){
        return self::where('part_number',$part_no)->first();
    }

    public function manufacturer(){
        return $this->belongsTo('App\Models\Manufacturer');
    }

    static public function searchByAttribute($array){        
        return self::where($array)->get();
    }

    public function suppliers()
    {
        return $this->belongsToMany('App\Models\Vendor', 'vendor_products', 'product_id', 'vendor_id');
    }

    public function applications()
    {
        return $this->belongsToMany(Application::class, 'product_application_links')->withTrashed();
    }

    public function brands()
    {
        return $this->hasOne(Manufacturer::class, 'id', 'manufacturer_id')->withTrashed();
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category_links')->withTrashed();
    }

    public function attributes()
    {
       return $this->belongsToMany(Attribute::class, 'product_attribute_links')->withTrashed();
    }
    
}

