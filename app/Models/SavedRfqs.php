<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class SavedRfqs extends Model
{
    protected $table = 'saved_rfqs';
    
    protected $fillable = ['tempProdSelection', 'tempRfqSelection', 'rfq_name', 'client_id', 'client_user_id', 'deleted'];

}
