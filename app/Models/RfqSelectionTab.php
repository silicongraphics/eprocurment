<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RfqSelectionTab extends Model
{
    protected $table = 'rfq_selection_tabs';
 
    protected $fillable = [
        'rfq_id', 'name'
    ];

    public function products(){
        return $this->hasMany('App\Models\RfqCartProduct','rfq_sel_tab_id');
    }
}

