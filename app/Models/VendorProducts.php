<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VendorProducts extends Model
{
    protected $table = 'vendor_products';
    
    protected $fillable = ['product_id', 'vendor_id'];

    // public function supplier(){
    //     return $this->belongsTo('App/Models/Vendor','vendor_id');
    // }

}
