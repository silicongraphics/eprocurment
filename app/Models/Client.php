<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Model
{
    protected $table = 'clients';
    protected $fillable = ['custom_id', 'name', 'description', 'status', 'company_id'];
    
    public function company()
    {
        return $this->belongsTo('App\Models\Company','company_id');
    }

    public function user() {
    	return $this->hasMany('App\Models\ClientUser', 'client_id', 'id');
    }
    public function documentypes() {
		return $this->belongsToMany(DocType::class, 'client_documents', 'client_id', 'doc_type_id');	
	}
	
	public function manufactures() {
		return $this->belongsToMany('App\Models\Manufacturer', 'client_brand_links', 'client_id', 'manufacturer_id')->withTrashed();
	}
	
	public function suppliers() {
		return $this->belongsToMany('App\Models\Vendor', 'client_supplier_links');
	}
}
