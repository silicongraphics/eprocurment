<?php

namespace App\Observers;

use App\Models\Manufacturer;

class ManufacturerObserver
{
    /**
     * Handle the manufacturer "created" event.
     *
     * @param  \App\Models\Manufacturer  $manufacturer
     * @return void
     */
    public function created(Manufacturer $manufacturer)
    {
        $manufacturer->custom_id = "B".$manufacturer->id;
        $manufacturer->save();
    }

    /**
     * Handle the manufacturer "updated" event.
     *
     * @param  \App\Models\Manufacturer  $manufacturer
     * @return void
     */
    public function updated(Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Handle the manufacturer "deleted" event.
     *
     * @param  \App\Models\Manufacturer  $manufacturer
     * @return void
     */
    public function deleted(Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Handle the manufacturer "restored" event.
     *
     * @param  \App\Models\Manufacturer  $manufacturer
     * @return void
     */
    public function restored(Manufacturer $manufacturer)
    {
        //
    }

    /**
     * Handle the manufacturer "force deleted" event.
     *
     * @param  \App\Models\Manufacturer  $manufacturer
     * @return void
     */
    public function forceDeleted(Manufacturer $manufacturer)
    {
        //
    }

}
