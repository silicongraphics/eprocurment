<?php

namespace App\Observers;

use App\Models\VendorUser;

class SupplierUserObserver
{
    /**
     * Handle the vendor user "created" event.
     *
     * @param  \App\Models\VendorUser  $vendorUser
     * @return void
     */
    public function created(VendorUser $vendorUser)
    {
        //
    }

    /**
     * Handle the vendor user "updated" event.
     *
     * @param  \App\Models\VendorUser  $vendorUser
     * @return void
     */
    public function updated(VendorUser $vendorUser)
    {
        //
    }

    /**
     * Handle the vendor user "deleted" event.
     *
     * @param  \App\Models\VendorUser  $vendorUser
     * @return void
     */
    public function deleted(VendorUser $vendorUser)
    {
        //
    }

    /**
     * Handle the vendor user "restored" event.
     *
     * @param  \App\Models\VendorUser  $vendorUser
     * @return void
     */
    public function restored(VendorUser $vendorUser)
    {
        //
    }

    /**
     * Handle the vendor user "force deleted" event.
     *
     * @param  \App\Models\VendorUser  $vendorUser
     * @return void
     */
    public function forceDeleted(VendorUser $vendorUser)
    {
        //
    }

    public function deleting(VendorUser $vendorUser) {
       $vendorUser->documents()->delete();
    }
}
