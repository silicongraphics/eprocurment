<?php

namespace App\Observers;

use App\Models\ClientUser;

class ClientUserObserver
{
    /**
     * Handle the client user "created" event.
     *
     * @param  \App\Models\ClientUser  $clientUser
     * @return void
     */
    public function created(ClientUser $clientUser)
    {
        //
    }

    /**
     * Handle the client user "updated" event.
     *
     * @param  \App\Models\ClientUser  $clientUser
     * @return void
     */
    public function updated(ClientUser $clientUser)
    {
        //
    }

    /**
     * Handle the client user "deleted" event.
     *
     * @param  \App\Models\ClientUser  $clientUser
     * @return void
     */
    public function deleted(ClientUser $clientUser)
    {
        //
    }

    /**
     * Handle the client user "restored" event.
     *
     * @param  \App\Models\ClientUser  $clientUser
     * @return void
     */
    public function restored(ClientUser $clientUser)
    {
        //
    }

    /**
     * Handle the client user "force deleted" event.
     *
     * @param  \App\Models\ClientUser  $clientUser
     * @return void
     */
    public function forceDeleted(ClientUser $clientUser)
    {
        //
    }

    public function deleting(ClientUser $clientUser) {
       $clientUser->documents()->delete();
    }
}
