<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\Helper;
use App\Models\Vendor;
use App\Models\Product;
use App\Models\EstimateCor;
use App\Models\ReqForQuote;
use App\Models\RfqSelection;
use Illuminate\Http\Request;
use App\Models\PurchaseOrder;
use App\Models\RfqCartProduct;
use App\Models\VendorReceivedRfq;
use App\Models\RfqCartProductTerm;
use App\Models\PurchaseOrderDetail;
use App\Models\ClientUser;
use App\User;
use App\Mail\GenericEmail;
use Mail;

class VendorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:vendor');
    }

    // Vendor Home
    public function index()
    {
        return view('seller.home');
    }

    //  My RFQ received page
    public function myRfq(Request $request){
        $rfqs = DB::table('rfq_cart_product_terms as terms')
                    ->join('req_for_quotes as r','r.id','=','terms.reqForQuote_id')
                    ->where('terms.vendor_id',Auth::guard()->user()->id)
                    ->where('terms.sent_status',1)
                    ->groupBy('terms.reqForQuote_id')
                    ->paginate(10);
        return view('seller.rfqs.listing',['rfqs' => $rfqs]);
    }

    // RFQ detail page
    public function rfqDetails(Request $request, $id){
        $rfq = ReqForQuote::with(['selections.products.terms' => function($sql){
            $sql->where('vendor_id', Auth::guard()->user()->id);
        }])->where('req_code',$id)->first();
        return view('seller.rfqs.rfq-detail',['rfq' => $rfq]);
    }

    //  Product linked to vendor
    public function vendorProducts(){
        $data = Vendor::where('id',Auth::guard()->user()->vendor_id)->first();
        $data->setRelation('products', $data->products()->paginate(10));
        return view('seller.products.listing',['data' => $data]);
    }

    // Send RFQ response to Client
    public function submitRfq(Request $request){
        foreach($request->product_ids as $prod){
            if($request->{'quantity'.$prod}){
                VendorReceivedRfq::create([
                    'reqForQuote_id' => $request->reqForQuote_id,
                    'vendor_id' => Auth::guard()->user()->id,
                    'warranty_terms' => $request->{'war_terms'.$prod},
                    'service_terms' => $request->{'serv_terms'.$prod},
                    'credit_term_id' => $request->cred_terms,
                    'delivery_terms' => $request->del_terms,
                    'general_remarks' => $request->remarks,
                    'product_id' => $prod,
                    'quantity' => $request->{'quantity'.$prod},
                    'unit_price' => $request->{'price'.$prod},                
                    'disabled' => $request->{'disabled'.$prod},                
                ]);
            }
        }
        $rfq = ReqForQuote::find($request->reqForQuote_id);
        $client = ClientUser::find($rfq->client_user_id);
        $mailData = array(
            'rfqno' => $rfq->name,
            'c_name' => $client->first_name.' '.$client->last_name,
            's_name' => Auth::guard()->user()->first_name,
            
        );
        // Mail::to($client->email)->send(new GenericEmail('generateQuote', $mailData, 0));
        // foreach(User::where('active',1)->get() as $admin)
        //     Mail::to($admin->email)->send(new GenericEmail('generateQuote', $mailData, 1));

        $request->session()->flash('success', 'Rfq has been processed');
        return redirect()->route('vendor-my-rfq');
    }

    // My Estimates (Client change of Request)
    public function myEstimates(Request $request){
        $vendor_id = Auth::guard()->user()->vendor_id;
        $data = DB::table('vendor_received_rfqs as vr')
                    ->join('estimates_cor as e','e.vrfq_id','=','vr.id')
                    ->join('req_for_quotes as r','r.id','=','vr.reqForQuote_id')
                    ->where('vendor_id',$vendor_id)
                    ->groupBy('vr.reqForQuote_id')
                    ->select('vr.*', 'r.name', 'r.req_number', 'r.req_code')
                    ->paginate(10);
        return view('seller.estimates.listing',['data' => $data]);
    }

    // Estimation detail
    public function estimateDetail(Request $request, $id){
        $rfq = ReqForQuote::where('req_code',$id)->first();

        $chk = $this->redirectIfPoCreated($rfq->id);
        if($chk)
            return redirect('vendor/purchase-order-detail/'.$chk->po_number);


        $vendor_id = Auth::guard()->user()->vendor_id;
        $changes = VendorReceivedRfq::with('changeOfRequest')
                ->where('vendor_id', $vendor_id)
                ->get();
        $terms = Helper::getGeneralTerms();
        
        return view('seller.estimates.estimate-detail',['terms'=>$terms, 'changes' => $changes, 'rfq'=>$rfq]);
    }

    // Submit Vendor Response to Client
    public function submitEstimate(Request $request){
        EstimateCor::where('id', $request->change_id)->update([
            'closed' => 1,
            'updated_qty' => $request->quantity,
            'updated_price' => $request->price,
        ]);

        $EstimateCor = EstimateCor::find($request->change_id);
        $VendorReceivedRfq = VendorReceivedRfq::find($EstimateCor->vrfq_id);
        $rfq = ReqForQuote::find($VendorReceivedRfq->reqForQuote_id);
        $client = ClientUser::find($rfq->client_user_id);
        $mailData = array(
            'rfqno' => $rfq->name,
            'c_name' => $client->first_name.' '.$client->last_name,
            's_name' => Auth::guard()->user()->first_name,
        );
        // Mail::to($client->email)->send(new GenericEmail('supplierAnswerForCOR', $mailData, 0));
        // foreach(User::where('active',1)->get() as $admin)
        //     Mail::to($admin->email)->send(new GenericEmail('supplierAnswerForCOR', $mailData, 1));

        return response()->json(['status'=>200, 'msg'=>'Updation is performed']);
    }

    // My Purchase Order sent by client
    public function myPos(Request $request){
        $vendor_id          = Auth::guard()->user()->vendor_id;

        $pos                = PurchaseOrder::with('detail')
                                ->whereHas('detail', function($q) use ($vendor_id){
                                    return $q->where('supplier', $vendor_id)->orderBy('supplier');
                                })->paginate(10);

        return view('seller.po.all', compact('pos'));
    }

    // Purchase Order Detail
    public function poDetail(Request $request, $id){
        $vendor_id = Auth::guard()->user()->vendor_id;
        $po                 =   PurchaseOrder::with(['detail' => function($q) use ($vendor_id){
                                        $q->where('supplier', $vendor_id)->orderBy('supplier');
                                }])->where('po_number',$id)->first();
        $podetails          =   DB::table('purchase_order_detail as pod')
                                ->join('products as p','p.id','=','pod.product_id')
                                ->select('pod.*',  'p.name', 'p.part_number', 'p.description')
                                ->where('pod.po_id', $po->id)
                                ->where('supplier', $vendor_id)
                                ->orderBy('supplier')
                                ->get();
        $credit_terms       =   DB::table('credit_terms')->get();
        $delievery_terms    =   DB::table('delivery_terms')->get();
        $warranty_terms     =   DB::table('warranty_terms')->get();
        $service_terms      =   DB::table('service_terms')->get();
    

        return view('buyer.po.detail', compact('po','podetails','credit_terms', 'delievery_terms', 'warranty_terms', 'service_terms'));
    }

    //  Approve Purchase Order of Client
    public function approvePurchaseOrder(Request $request){   
            PurchaseOrderDetail::where('po_id', $request->poid)
                               ->where('supplier',$request->supp)
                               ->update(['s_status' => 1]);
                               
            return response()->json([
                'status' => 200,
                'msg' => 'Po has been sent to supplier'
            ]);        
    }

    //  Use for Redirect if Po Exist
    public static function redirectIfPoCreated($rfq_id){
        $chk = PurchaseOrder::where('rfq_id',$rfq_id)->first();
        return $chk;
    }



}
