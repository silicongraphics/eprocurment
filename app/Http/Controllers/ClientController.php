<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use App\Models\Product;
use App\Models\SavedRfqs;
use App\Models\EstimateCor;
use App\Models\ReqForQuote;
use App\Models\RfqSelection;
use Illuminate\Http\Request;
use App\Models\PurchaseOrder;
use App\Models\RfqCartProduct;
use App\Models\RfqSelectionTab;
use App\Models\RfqCartProductTerm;
use App\Models\PurchaseOrderDetail;
use App\Models\City;
use App\Mail\GenericEmail;
use App\Models\VendorUser;
use App\Models\VendorReceivedRfq;
use App\User;
use Mail;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:client', ['except'=>['getcities']]);
    }

    //get my general terms
    function getMyGeneralTerms(Request $request){

        return [    
            'warranty_terms' => (Auth::guard()->user()->warranty_term_id) ? (Auth::guard()->user()->warranty_term_id) : (DB::table('warranty_terms')->pluck('id')->first()),
            'service_terms' => (Auth::guard()->user()->service_term_id) ? (Auth::guard()->user()->service_term_id) : (DB::table('service_terms')->pluck('id')->first()),
            'credit_terms' => (Auth::guard()->user()->credit_term_id) ? (Auth::guard()->user()->credit_term_id) : (DB::table('credit_terms')->pluck('id')->first()),
            'delievery_terms' => (Auth::guard()->user()->delivery_method_id) ? (Auth::guard()->user()->delivery_method_id) : (DB::table('delivery_terms')->pluck('id')->first()),
        ];
    }

    // generate RFQ creation view 
    public function createRfq(Request $request){
        if($request->q){
            
            $r = SavedRfqs::find($request->q);

            if($r && $r->client_user_id == Auth::guard()->user()->id){
                Session::forget('tempProdSelection');
                Session::forget('tempRfqSelection');

                $temp1 = json_decode($r->tempProdSelection);
                $temp2 = json_decode($r->tempRfqSelection);

                $request->session()->put('tempProdSelection',$temp1);       
                $request->session()->put('tempRfqSelection', $temp2); 
                // return $temp1;
                // die;
                // Session::put('tempProdSelection',$temp1);
                // Session::put('tempRfqSelection',$temp2);


                return view('buyer.rfqs.create',['name' => $r->rfq_name, 'saved_rfq_id'=>$r->id]);
            }
            
        }
        //die;
        return view('buyer.rfqs.create',['name' => '', 'saved_rfq_id'=>0]);
    }

    // Client home 
    public function index()
    {
        return view('buyer.home');
    }

    // Estimates View after supplier has confirm client rfq 
    public function myEstimates(Request $request){
        $estimates =  ReqForQuote::with(['selections.products.terms','vendorRfqResponse'])
                                ->whereHas('vendorRfqResponse')
                                ->where('client_user_id',Auth::guard()->user()->id)
                                ->paginate(10);
         
        return view('buyer.rfqs.estimates',['estimates' => $estimates]);
    }

    // Estimate generated detail view
    public function estimateDetail(Request $request, $id){

        $rec = ReqForQuote::where('req_code',$id)->first();
        $chk = $this->redirectIfPoCreated($rec->id);
        if($chk)
            return redirect('client/purchase-order-detail/'.$chk->po_number);

        $estimate =  ReqForQuote::with(['selections.products.terms','vendorRfqResponse'])
                                ->whereHas('vendorRfqResponse')
                                ->where('req_code',$id)
                                ->where('client_user_id',Auth::guard()->user()->id)
                                ->first();
        $tabs = ReqForQuote::with('rfqSelectionTab.products.terms')
                            ->where('req_code',$id)
                            ->where('client_user_id',Auth::guard()->user()->id)
                            ->first();
        $terms = \App\Helper::getGeneralTerms();
        return view('buyer.rfqs.estimateDetails',['tabs'=>$tabs, 'gterms'=>$terms, 'terms'=>$terms, 'estimate' => $estimate]);
    }

    // all my rfq listings 
    public function myRfq(Request $request){
        $rfqs =  ReqForQuote::with('selections.products.terms')
                ->where('client_user_id',Auth::guard()->user()->id)
                ->orderBy('id','desc')
                ->paginate(10);
        return view('buyer.rfqs.listing',['rfqs' => $rfqs]);
    }


    //  My Saved Rfq
    public function mysavedRfq(Request $request){
        $savedrfq = SavedRfqs::where('client_user_id',Auth::guard()->user()->id)->where('deleted',0)->paginate(10);
        return view('buyer.rfqs.save-listing',['savedrfq' => $savedrfq]);
    }

    // my rfq detail page 
    public function rfqDetail(Request $request, $id){
        $rfq =  ReqForQuote::with('selections.products.terms')
                            ->where('client_user_id',Auth::guard()->user()->id)
                            ->where('req_code',$id)
                            ->first();
        $po = PurchaseOrder::where('rfq_id',$rfq->id)->first();
                                                
        return view('buyer.rfqs.rfq-detail',compact('rfq','po'));
    }


    //  Save RFQ for later use
    public function saveRFQ(Request $request){
        $temp = $request->session()->get('tempProdSelection');       
        $temp1 = $request->session()->get('tempRfqSelection');          
        
        SavedRfqs::create([
            'client_id' => Auth::guard()->user()->id,
            'client_user_id' => Auth::guard()->user()->client_id,
            'rfq_name' => $request->rfqname,
            'tempProdSelection' => json_encode($temp),
            'tempRfqSelection' => json_encode($temp1),
        ]);

        // Flusing the session created while creating RFQ
        $request->session()->forget('tempProdSelection');       
        $request->session()->forget('tempRfqSelection');       

        return response()->json([
            'status' => 200,
            'msg' => 'Rfq is saved'
        ],200);
    }

    // submit rfq 
    public function submitRFQ(Request $request){
        $reqForQuote = ReqForQuote::create([
            'client_id' => Auth::guard()->user()->id,
            'client_user_id' => Auth::guard()->user()->client_id,
            'name' => $request->rfqname,
            'req_code' => (int)microtime(),
            'req_number' => $request->requisition,
            'site_area' => $request->site_area,
            'description' => '',
        ]);

        $rfq = RfqSelection::create([
            'req_for_quote_id' => $reqForQuote['id'],
            'name' => $request->rfqname,
            'description' => '',
        ]);
        foreach($request->obj as $index => $obj){
            $tab = RfqSelectionTab::firstOrCreate(['rfq_id' => $reqForQuote['id'], 'name' => $obj['tabname']]);
            $rfqCart = RfqCartProduct::create([
                'rfq_selection_id' => $rfq['id'],
                'product_id' => $obj['product_id'],
                'quantity_requested' => $obj['quantity'],
                'rfq_sel_tab_id' => $tab->id,
                'reqForQuote_id' => $reqForQuote['id'],
            ]);            
            foreach($obj['supplier'] as $supp){
                $rfqCartTerms = RfqCartProductTerm::create([
                    'rfq_cart_product_id' => $rfqCart['id'],
                    'reqForQuote_id' => $reqForQuote['id'],
                    'vendor_id' => $supp['suppid'],
                    'warranty_terms' => $supp['terms']['warranty_terms'],
                    'service_terms' => $supp['terms']['service_terms'],
                    'credit_term_id' => $supp['terms']['credit_terms'],
                    'delivery_terms' => $supp['terms']['delievery_terms'],
                ]);            
            }    
        }

        SavedRfqs::where('id',$request->issavedrfq)->update(['deleted' => 1]);
                
        // Flusing the session created while creating RFQ
        $request->session()->forget('tempProdSelection');       
        $request->session()->forget('tempRfqSelection');       

        return response()->json([
            'status' => 200,
            'msg' => 'Rfq is created',
            'rfqcode' => ReqForQuote::find($reqForQuote['id'])->req_code
        ],200);

    }

    //delete product added while creating rfq for specific supplier 
    public function deleteProductSupp(Request $request, $id, $supp){
        DB::table('rfq_cart_product_terms')->where('id',$id)->delete();
        return redirect()->back();
    }

    // Send Supplier created RFQ both bulk and singular too
    public function submitRFQtoSuppInd(Request $request){
        $count = 0;
        $rfq = ReqForQuote::with('selections.products.terms')->where('req_code',$request->req_no)->first();
        foreach($rfq->selections->products as $pro){
            foreach($pro->terms as $term){
                if($request->suppid){
                    if($term->vendor_id == $request->suppid && $term->sent_status == 0){
                        DB::table('rfq_cart_product_terms')->where('id',$term->id)->update([
                            'sent_status' => 1
                        ]);
                        $count++;
                    }    
                }else{
                    DB::table('rfq_cart_product_terms')->where('id',$term->id)->update([
                        'sent_status' => 1
                    ]);
                    $count++;                    
                }
                //$supp = VendorUser::find($term->vendor_id);
                // $mailData = array(
                //     'rfqno' => $rfq->name,
                //     's_name' => $supp->first_name.' '.$supp->last_name,
                //     'c_name' => Auth::guard()->user()->first_name,
                    
                // );
                // Mail::to($supp->email)->send(new GenericEmail('createRfq', $mailData, 0));
                // foreach(User::where('active',1)->get() as $admin)
                //     Mail::to($admin->email)->send(new GenericEmail('createRfq', $mailData, 1));
            }
        }
        if($count){
            return response()->json([
                'status' => 200,
                'msg' => 'Rfq is sent to supplier'
            ],200);            
        }
        return response()->json([
            'status' => 500,
            'msg' => 'Rfq is already sent to supplier'
        ],200);

    }


    // Submit client response Change of request that he need to do in estimate
    // for terms given by supplier  
    public function changeOfRequest(Request $request){
        // /return $request->all();
        $validator = Validator::make($request->all(),[
            'term' => ['required'],
            'quantity' => ['required','numeric'],
            'reason' => ['required']
        ],[
            'term.required' => 'Request can not be performed at this moment',
        ]);
        if($validator->fails())
            return response()->json([ 'status' => 200, 'msg' => $validator->errors()->first()]);            
        
        EstimateCor::create(['vrfq_id' => $request->term, 'quantity' => $request->quantity,'reason' => $request->reason]);
        
        $VendorReceivedRfq = VendorReceivedRfq::find($request->term);
        $rfq = ReqForQuote::find($VendorReceivedRfq->reqForQuote_id);
        $supp = VendorUser::find($VendorReceivedRfq->vendor_id);
        $mailData = array(
            'rfqno' => $rfq->name,
            's_name' => $supp->first_name.' '.$supp->last_name,
            'c_name' => Auth::guard()->user()->first_name,
        );
        // Mail::to($supp->email)->send(new GenericEmail('changeOfRequest', $mailData, 0));
        // foreach(User::where('active',1)->get() as $admin)
        //     Mail::to($admin->email)->send(new GenericEmail('changeOfRequest', $mailData, 1));

        return response()->json(['status' => 200,'msg' => 'Your change of request is submitted']);
    }   

    // Search in array [HELPER FOR submitPurchaseOrder() ]
    public function searchInArr($array, $id){
        foreach($array as $arr){
            if($arr['id'] == $id){
                return $arr;
            }
        }
        return false;
    }


    // Create Purchase Order
    public function submitPurchaseOrder(Request $request){
        $savedPo=0;
        if($request->type)
            $savedPo=1;

        $po = new PurchaseOrder();
        $po->rfq_id = $request->id;
        $po->client_id = Auth::guard()->user()->id;
        $po->client_user_id = Auth::guard()->user()->client_id;
        $po->savedPo = $savedPo;
        $po->save();

        $PurchaseOrder = PurchaseOrder::find($po->id);

        $tabs = ReqForQuote::with('rfqSelectionTab.products.terms')
        ->where('id',$request->id)
        ->where('client_user_id',Auth::guard()->user()->id)
        ->first();
        foreach($tabs->RfqSelectionTab as $index => $tab){
            foreach($tab->products as $pro){
                foreach($pro->terms as $supp){
                    $return = $this->searchInArr($request->array, $supp->id);
                    if($return){
                        
                       $pod = new PurchaseOrderDetail();
                       //$chk = DB::table('estimates_cor')->where('')
                       $terms_history = \App\Helper::getSupplierRespForRFQ($tabs->id, $supp->vendor_id, $pro->product_id);
                       $didclientCOR = \App\Helper::checkIfChangeOfRequest($terms_history->first()->id);
                      
                       $pod->po_id = $po->id;
                       $pod->supplier = $return['supplier_id'];
                       $pod->product_id = $terms_history->first()->product_id;
                       $pod->warranty_term	 = $terms_history->first()->warranty_terms;
                       $pod->service_term = $terms_history->first()->service_terms;
                       $pod->payment_term = $terms_history->first()->credit_term_id;
                       $pod->delievery_term = $terms_history->first()->delivery_terms;
                    
                       if(count($didclientCOR)){
                            $pod->quantity = $didclientCOR->first()->updated_qty;
                            $pod->price = $didclientCOR->first()->updated_price;
                       }else{
                            $pod->quantity = $terms_history->first()->quantity;
                            $pod->price = $terms_history->first()->unit_price;
                       }

                       $pod->save();

                       
                       $rfq = ReqForQuote::find($request->id);
                       $supp = VendorUser::find($return['supplier_id']);
                       $mailData = array(
                           'rfqno' => $rfq->name,
                           'ponumber' => $PurchaseOrder->po_number,
                           's_name' => $supp->first_name.' '.$supp->last_name,
                           'c_name' => Auth::guard()->user()->first_name,
                       );
                       //Mail::to($supp->email)->send(new GenericEmail('purchaseOrder', $mailData, 0));
                    //    foreach(User::where('active',1)->get() as $admin)
                    //        Mail::to($admin->email)->send(new GenericEmail('changeOfRequest', $mailData, 1));
   

                    }

                }
            }
        }	
        $url = url('client/purchase-order-detail/'.$PurchaseOrder->po_number);
        return response()->json(['status'=>200, 'url'=> $url, 'msg'=>'Your purchase order is added']);
    }

    public function myPos(Request $request){
        $pos = PurchaseOrder::with('detail')->paginate(10);
        return view('buyer.po.all', compact('pos'));
    }

    public function poDetail(Request $request, $id){
        $po                 =   PurchaseOrder::with(['detail' => function($q){
                                    $q->orderBy('supplier');
                                }])->where('po_number',$id)->where('client_user_id',Auth::guard()->user()->id)->first();
        
        $podetails          =   DB::table('purchase_order_detail as pod')
                                ->join('products as p','p.id','=','pod.product_id')
                                ->select('pod.*',  'p.name', 'p.part_number', 'p.description')
                                ->where('pod.po_id', $po->id)
                                ->orderBy('supplier')
                                ->get();
        $credit_terms       =   DB::table('credit_terms')->get();
        $delievery_terms    =   DB::table('delivery_terms')->get();
        $warranty_terms     =   DB::table('warranty_terms')->get();
        $service_terms      =   DB::table('service_terms')->get();
    

        return view('buyer.po.detail', compact('po','podetails','credit_terms', 'delievery_terms', 'warranty_terms', 'service_terms'));
    }

    //  Send Purchase Order to supplier

    public function sendPotoSupplier(Request $request){
        $chk = PurchaseOrderDetail::where('po_id', $request->poid)
                                    ->where('supplier',$request->supp)
                                    ->where('status',1)
                                    ->get();
        if(count($chk)){
            return response()->json([
                'status' => 500,
                'msg' => 'Po is already been sent to supplier'
            ]);
        }

        PurchaseOrderDetail::where('po_id', $request->poid)
                           ->where('supplier',$request->supp)
                           ->update(['status' => 1]);
                           
        return response()->json([
            'status' => 200,
            'msg' => 'Po has been sent to supplier'
        ]);        
    }


    //  Use for Redirect if Po Exist
    public static function redirectIfPoCreated($rfq_id){
        $chk = PurchaseOrder::where('rfq_id',$rfq_id)->first();
        return $chk;
    }

    public function getcities(Request $request) {
       $cities = City::where('countries_id', $request->country_id)->get();
       $data['success'] = 1;
       $data['cities']  =  $cities;
       // return response()->json($data);
        return response()->json([
                'status' => 200,
                'cities' => $cities
            ]);    
    }


}
