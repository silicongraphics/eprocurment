<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Manufacturer;
use App\Models\Application;
use App\Models\Category;
use App\Models\Attribute;
use App\Models\AttributeOption;
use App\Models\Product;
use File;
use Intervention\Image\Facades\Image as Image;
use PHPUnit\Framework\Exception;

class ImportController extends Controller
{
  
  public function index(){
    return view('admin.importer.index');
  }

  public function sanatizeProducts(){
      $products = Product::all();
      foreach($products as $prod){
        echo '<br>';
        $attr = $prod->attributes;
        echo $prod->id.'  '.$attr;
        dump($prod->id, json_decode($attr));
      }
  }

  public function uploadFile(Request $request){

    // $file = $request->file('file');
    // $destinationPath = public_path('uploads');
    // $file->move($destinationPath,$file->getClientOriginalName());    

    ini_set('max_execution_time', 0); // for infinite time of execution 


    $uploaded_file = public_path('uploads/final.csv');

    $csvArr = $this->csvToArray($uploaded_file);
    foreach($csvArr as $index => $record){
      $pcheck = Product::where('part_number' , $record['Part_number'])->first();
      if(!$pcheck):
      //if($index < 50):

      $keys = array_keys($record);
      // Saving Applications
      
      $application_ids = array(); 
      $app_names = explode('|', $record['Applications']);
        foreach ($app_names as $app_name) {
          if($app_name == '#N/A' || $app_name == '' || $app_name == 'N/A') { } else { 
            $app_name = preg_replace("/[^A-Za-z0-9\. -]/", "", $app_name);
            $Application = Application::firstOrCreate(['slug' => str_slug($app_name)], ['name' => $app_name, 'slug'=>str_slug($app_name)]);
            $application_ids[] = $Application->id;
          }
        }
      //Saving Manufacturers
      $Manufacturer = preg_replace("/[^A-Za-z0-9\. -]/", "", $record['Manufacturer']);
      $manufacturers = Manufacturer::firstOrCreate(['slug' => str_slug($Manufacturer) ], 
      ['name' => $Manufacturer, 'slug' => str_slug($Manufacturer) ]);                  

      // Saving Categories Parent        

      $Category = preg_replace("/[^A-Za-z0-9\. -]/", "", $record['Category']);
      $Category = $record['Category'];
      $parent_id = Category::firstOrCreate(['slug' => str_slug($Category) ], 
          ['name' => $Category, 
          'slug' => str_slug($Category), 
          'parent_id' => NULL ]);                  

      //saving first level category
      $firstleveln = preg_replace("/[^A-Za-z0-9\. -]/", "", $record[$keys[5]]);
      $firstleveln = $record[$keys[5]];
      $firstlevel = Category::firstOrCreate(['slug' => str_slug($firstleveln) , 'parent_id' => $parent_id->id ], 
        ['name' => $firstleveln, 
         'slug' => str_slug($firstleveln), 
         'parent_id' => $parent_id->id,                  
         'label_name' => $keys[5],
         'level' => 1 ]);                  

      //saving second level category
      $secondLeveln = preg_replace("/[^A-Za-z0-9\. -]/", "", $record[$keys[6]]);
      $secondLeveln = $record[$keys[6]];
      $secondLevel = Category::firstOrCreate(['slug' => str_slug($secondLeveln) , 'parent_id' => $firstlevel->id ], 
        ['name' => $secondLeveln, 
        'slug' => str_slug($secondLeveln), 
        'parent_id' => $firstlevel->id,                  
        'label_name' => $keys[6] ,
        'level' => 2]);   

      //saving second level category
      $thirdLeveln = preg_replace("/[^A-Za-z0-9\. -]/", "", $record[$keys[7]]);
      $thirdLeveln = $record[$keys[7]];
      $thirdLevel = Category::firstOrCreate(['slug' => str_slug($thirdLeveln) , 'parent_id' => $parent_id->id ], 
        ['name' => $thirdLeveln, 
        'slug' => str_slug($thirdLeveln), 
        'parent_id' => $parent_id->id,                  
        'label_name' => $keys[7] ,
        'level' => 3]);  

      //saving attributes
      $attributes_array = array();
      $attrs = explode('|', $record['Attributes']);
      $attrs2 = array_filter($attrs, function ($a)
      {
        $x = explode('=', $a);
        return count($x) == 2 && !empty($x[1]);
      });
      $attributes_array[] = $attrs2;
      $product_attributes =  $attrs2;

      // for formatting product attribute option values
      $str = '{';
      $len = count($attrs2);
      foreach($attrs2 as $k => $v){
        $data = explode('=', $v);
        $val = '"'.$data[0].'":"'.$data[1].'"';
          $str .= $val;
          if($k == $len-1)
            $str .= ',';
      }
      $str .= '}';
      //sanatizing
      $str = str_replace('""','","', $str);
      $str = str_replace(',}','}', $str);

      $product_attr = $str;

      // For saving attribute option
      $stri_vals = array();
      foreach ($attributes_array as $key => $value) {
        if(count($value)){
            foreach($value as $k => $v){
            $data = explode('=', $v);
            $attrname = trim($data[0]);
            //$attrname = preg_replace('/[[:^print:]]/', '', $str);
            $attrvalue = preg_replace('/[[:^print:]]/', '', trim($data[1]));
            $attr = Attribute::firstOrCreate(['slug' => str_slug($attrname)], ['name' => $attrname, 'slug'=> str_slug($attrname) ]);
            $option = AttributeOption::where(['value' => $attrvalue, 'attribute_id' => $attr->id])->first();
                if($option) {
                    $AttributeOption = AttributeOption::where(['value' => $attrvalue, 'attribute_id' => $attr->id])
                                                      ->update(['value' => $attrvalue]);
                    $attri_vals[$attr->id] = ['attribute_option_value' => $AttributeOption['id']];
                } else {
                    $AttributeOption = AttributeOption::create(['value' => $attrvalue, 'attribute_id' => $attr->id]);
                    $attri_vals[$attr->id] = ['attribute_option_value' => $AttributeOption['id']];
                }

          }
        }
      }
      
      //saving priority attributes
      
      $priority_id = array(); 
      $prior_names = explode('|', $record['priority']);
        foreach ($prior_names as $prior_name) {
          if($prior_name == '#N/A' || $prior_name == '' || $prior_name == 'N/A') { } else { 
            //$prior_name = preg_replace("/[^A-Za-z0-9\. -]/", "", $prior_name);
            $prior_name = preg_replace('/[[:^print:]]/', '', trim($prior_name));
            $Attribute = Attribute::where('slug',str_slug($prior_name))->first();
            if($Attribute)
              $priority_id[] = $Attribute->id;
          }
      }

      $categoryids = array();
      array_push($categoryids, $parent_id->id);
      array_push($categoryids, $firstlevel->id);
      array_push($categoryids, $secondLevel->id);
      array_push($categoryids, $thirdLevel->id);

      // adding products
      $prod = Product::firstOrCreate(['Part_number' => $record['Part_number']], 
        ['name' => $record['Name'],
        'part_number' => $record['Part_number'],
        'image' => '' ,
        'manufacturer_id' => $manufacturers->id,
        'attributes' => $product_attr,
        'category_ids' => implode(',',$categoryids),
        'priority' => implode(',',$priority_id),
        'status' => 1]);      

      $pro_obj = Product::find($prod->id);
      $pro_obj->applications()->sync($application_ids);
      $pro_obj->categories()->sync([$parent_id->id, $firstlevel->id, $secondLevel->id, $thirdLevel->id]); 

      $attri_vals = array();
      foreach ($product_attributes as $proattrval) {
          $attroption = explode('=', $proattrval);
          $attrname = trim($attroption[0]);
          //$attrname = preg_replace('/[[:^print:]]/', '', $attrname);
          $attrvalue = preg_replace('/[[:^print:]]/', '', trim($attroption[1]));
          $attr_id = Attribute::where('slug', str_slug($attrname))->first()->id;
          $optionvalueid = AttributeOption::where(['value' => $attrvalue, 'attribute_id' => $attr->id])->first();
          if($optionvalueid){
            $attri_vals[$attr_id] = ['attribute_option_value' => $optionvalueid->id];
          }

          // $attr_id = Attribute::where('name', $attroption[0])->first()->id;
          // if($optionvalueid = AttributeOption::where(['value' => $attroption[1], 'attribute_id' => $attr_id])->first()){
          //     $opt_value_id = $optionvalueid->id;
          // }
          // $attri_vals[$attr_id] = ['attribute_option_value' => $opt_value_id];
      }
      $pro_obj->attributes()->sync($attri_vals);
      $pro_obj->attributes()->sync($attri_vals);

      echo $prod->id.' - '.$prod->name.' is imported';
      echo '<br>';
      endif;
    }
  }

  public function importImages(Request $request){

    // $file = $request->file('file');
    // $destinationPath = public_path('uploads');
    // $file->move($destinationPath,$file->getClientOriginalName());    

    $uploaded_file = public_path('uploads/final.csv');

    $csvArr = $this->csvToArray($uploaded_file);
    foreach($csvArr as $index => $record){
      //if($index < 50):

      $prod = Product::where('part_number', $record['Part_number'])->first();
      if($prod){
        if($prod->image == ''){
            $url = $record['Image'];
            if($url){
              $info = pathinfo($url);
              try{
                $contents = @file_get_contents($url);
                $directory = '/uploads/products/' . $prod->id . "/" . $info['basename'];
                //file_put_contents($file, $contents);
                if($contents){
                  \Storage::disk('public')->put($directory, $contents);
                  Product::where('id', $prod->id)->update([
                    'image' => '/uploads/products/' . $prod->id.'/'.$info['basename']
                  ]);  
      
                }
              }
              catch(Exception $e){
    
              }
      
            }  
          }
        }
     // endif;
    }
  }

  public function csvToArray($filename = '', $delimiter = ',')
  {
      if (!file_exists($filename) || !is_readable($filename))
          return false;
  
      $header = null;
      $data = array();
      if (($handle = fopen($filename, 'r')) !== false)
      {
          while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
          {
              if (!$header)
                  $header = $row;
              else
                  $data[] = array_combine($header, $row);
          }
          fclose($handle);
      }
  
      return $data;
  }

  
}
