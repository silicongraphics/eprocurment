<?php

namespace App\Http\Controllers;

use DB;
use Cart;
use Session;
use Redirect;
use App\Models\Vendor;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\VendorProducts;
use App\Models\City;
use App\Models\Category;
use Auth;
use App\Models\Attribute;
use function GuzzleHttp\json_decode;

class ProductController extends Controller
{

    public function array_flatten($array) { 
        if (!is_array($array)) { 
          return false; 
        } 
        $result = array(); 
        foreach ($array as $key => $value) { 
          if (is_array($value)) { 
            $result = array_merge($result, array_flatten($value)); 
          } else { 
            $result[$key] = $value; 
          } 
        } 
        return $result; 
    }

    public function getCatLevelList(Request $request){
        if($request->type == 'parent'){
            $level1 = Category::where('parent_id',$request->id)->where('level',1)->select('id','name as title','label_name')->get();
            $level3 = Category::where('parent_id',$request->id)->where('level',3)->select('id','name as title','label_name')->get();
            return [
                'level1' => $level1,
                'level3' => $level3,
            ];
        }else{
            $level2 = Category::where('parent_id',$request->id)->where('level',2)->select('id','name as title','label_name')->get();
            return [
                'level2' => $level2,
            ];
        }
    }

    public function getCategoryDpList(Request $request){
        $cats = Category::with('subs')->whereNull('parent_id')->where('id',$request->id)->select('id','name as title','label_name')->get();
        if($request->all){
            return $cats;
        }
        $dropdowns_array = array();
      
        foreach($cats as $cat){
            foreach($cat->subs as $child){
                if(!array_key_exists($child->label_name, $dropdowns_array)){
                    $dropdowns_array[$child->label_name] = array();
                }
                array_push($dropdowns_array[$child->label_name], $child);                
                if(count($child->subs)){
                    foreach($child->subs as $innerchild){
                        if(!array_key_exists($innerchild->label_name, $dropdowns_array)){
                            $dropdowns_array[$innerchild->label_name] = array();
                        }
                        array_push($dropdowns_array[$innerchild->label_name], $innerchild);    
                    }
                }
            }
        }
        return $dropdowns_array;
    }

    public function getCategoryList(Request $request){

        return Category::whereNull('parent_id')->select('id','name as title','label_name')->get();
    }
    
    public function searchByCats(Request $request){
        $products = DB::table('products as p');
        $c  = $request->c;
        $c1 = $request->c1;
        $c2 = $request->c2;
        $c3 = $request->c3;

        if($c && $c != 'undefined'){
            $products->whereExists(function ($query) use ($c) {
                $query->from('product_category_links')
                      ->whereRaw('product_id = p.id AND category_id = '.$c);
            });
        }
        if($c1 && $c1 != 'undefined'){
            $products->whereExists(function ($query1) use ($c1) {
                $query1->from('product_category_links')
                      ->whereRaw('product_id = p.id AND category_id = '.$c1);
            })->get();
        }
        if($c2 && $c2 != 'undefined'){
            $products->whereExists(function ($query2) use ($c2) {
                $query2->from('product_category_links')
                      ->whereRaw('product_id = p.id AND category_id = '.$c2);
            })->get();
        }
        if($c3 && $c3 != 'undefined'){
            $products->whereExists(function ($query3) use ($c3) {
                $query3->from('product_category_links')
                      ->whereRaw('product_id = p.id AND category_id = '.$c3);
            })->get();
        }
        $products = $products->paginate(6);

        return response()->json([
            'status' => 'success',
            'record' => $products
        ],200);  
    }

    public function search(Request $request){      


        $request->part_no = trim($request->part_no);
        $chk =  $this->checkIfPartnoExistInSearch($request, $request->part_no);
        if($chk){
            return response()->json([
                'status' => 'Error',
                'msg' => 'Part No is already added in selections'    
            ],500);
        }
        if($request->session()->has('tempProdSelection')){
            if(count($request->session()->get('tempProdSelection')) == 25){
                return response()->json([
                    'status' => 'Error',
                    'msg' => 'You can not add more than 25 selection tabs'    
                ],500);                
            }
        }        

        // \DB::listen(function($sql){
        //     dump($sql->sql);
        //     print_r($sql->bindings);
        // });

        $product = Product::searchByPart($request->part_no);
        if($product){

            if($product->attributes == '{}'){
                return response()->json([
                    'status' => 'Error',
                    'msg' => 'Product has no attributes'    
                ],500);
            }
            $Attribute = '';
            if($product->priority != '{}' || $product->priority != ''){
                //$priority = json_decode($product->priority);
                $priority = explode(',', $product->priority);
                $Attribute = Attribute::whereIn('id',$priority)->get();
            }

            $product_attributes = json_decode($product->attributes);
            
            //$catids = explode(',',$product->category_ids);
            $catids = explode(',',$product->category_ids);

            //$product_attributes = $product->attributes;
            $related = Product::query();
            foreach ($catids as $key => $cids) {
                $related = $related->whereRaw("FIND_IN_SET('$cids', category_ids)");
            }            
            if($Attribute){
                foreach ($Attribute as $key => $value) {
                    $related = $related->where('attributes->'.$value->name, '!=', '');
                }
                // foreach ($product_attributes as $key => $value) {
                //     $related = $related->where('attributes->'.$key, '!=', '');
                // }                
            }else{
                foreach ($product_attributes as $key => $value) {
                    $related = $related->where('attributes->'.$key, '!=', '');
                }
            }
            //$related = $related->whereJsonContains('category_ids', $catids);
            $results = $related->where('part_number','!=',$request->part_no)->take(20)->get();
             
            $sa = $sp = [];
 
            array_push($sa, [
                "id" =>  md5(microtime()),
                "product_id" => $product->id, 
                "custom_id" => $product->custom_id,
                "name" => $product->name,
                "image" => $product->image,
                "part_number" => $product->part_number,
                "manufacturer_id" =>$product->manufacturer_id,
                "category_ids" =>$product->category_ids,
                "attributes" =>$product->attributes,
                "description" =>$product->description,
                "price" => 0,
                "qty" => 1,
            ]);
            foreach($results as $r){
                array_push($sa, [
                    "id" =>  md5(microtime()),
                    "product_id" => $r->id, 
                    "custom_id" => $r->custom_id,
                    "name" => $r->name,
                    "image" => $r->image,
                    "part_number" => $r->part_number,
                    "manufacturer_id" =>$r->manufacturer_id,
                    "category_ids" =>$r->category_ids,
                    "attributes" =>$r->attributes,
                    "description" =>$r->description,
                    "price" => 0,
                    "qty" => 1,
    
                    ]);
            }
            array_push($sp, [
                'name' => $request->part_no,
                "id" =>  md5(microtime()),
                "attributes" => $product->attributes,
                "data" => $sa
            ]);

           $request->session()->push('tempProdSelection', $sp);
           return $request->session()->get('tempProdSelection');
        }
        return [];
    }

    public function searchByPart(Request $request){
        if(!$request->part_no){
            return response()->json([
                'status' => 'Error',
                'msg' => 'Please enter part number'    
            ],500);
        }
        $data =  Product::searchByPart($request->part_no);
        return ['id'=>$data->id, 'name'=> $data->name, 'part_no'=>$request->part_no, 'manufacture' => $data->manufacturer->name];
    }

    public function linkProductsToSupplier(Request $request){
        return $request->all();
        if(!$request->id){
            return response()->json([
                'status' => 500,
                'msg' => 'The product id is required'
            ]);
        }
        $vendor_id = Auth::guard('vendor')->user()->id;
        $chk = DB::table('vendor_products')->where('product_id',$request->id)->where('vendor_id',$vendor_id)->first();
        $linkp = VendorProducts::firstOrCreate(['product_id' => $request->id, 'vendor_id' => $vendor_id]);
        if(!$chk){
            return response()->json([
                'status' => 500,
                'msg' => 'The supplier is already linked'
            ]);
        }else{
            return $linkp;
        }    
    }
    
    public function linkProductsByManufactureToSupplier(Request $request){
        $vendor_id = Auth::guard('vendor')->user()->id;
        $manu_id = $request->manu_id;
        $data = DB::select("SELECT p.id as product_id, '$vendor_id' as vendor_id FROM `products` as p 
                    WHERE p.manufacturer_id = '$manu_id' AND p.id NOT IN (SELECT product_id FROM vendor_products WHERE vendor_id = $vendor_id)");
        $array = json_decode(json_encode($data),true);
        VendorProducts::insert($array);
        return response()->json([
            'status' => 200,
            'msg' => 'Products are linked'
        ]);
    }

    public function checkIfPartnoExistInSearch(Request $request, $partNo){
        $count = 0;
        if($request->session()->has('tempProdSelection')){
            foreach($request->session()->get('tempProdSelection') as $index => $data){
                if($data[0]['name'] == $partNo)
                    $count++;
            }    
        }
        return $count;
    }

    public function checkIfPartNoExistinRFQSelections(Request $request){
        if($request->session()->has('tempRfqSelection')){
            $count =  0;
            $temp = $request->session()->get('tempRfqSelection');
            foreach($request->session()->get('tempRfqSelection') as $index => $selections){
                if($request->partNo == $selections['partNo']){
                    $array = [];
                    $array['name'] = $request->name;
                    //$array['index'] = $request->index;
                    $array['partNo'] = $request->partNo;
                    $tpids = [];
                    foreach($request->selections as $sel){
                        if($sel['partNo'] == $request->partNo){
                            array_push($tpids, $sel['product_id']);   
                        }
                    }
                    
                    $final = Product::with('suppliers')->whereIn('id', $tpids)->get();
                    $array['products'] = $final;
                    $temp[$index] = $array;   
                    $count++;
                }
            }
            if($count){
                $request->session()->put('tempRfqSelection', $temp);
                return $request->session()->get('tempRfqSelection');
            }
        }
        return [];
    }

    public function saveRfqSelections(Request $request){ 
        $state = $this->checkIfPartNoExistinRFQSelections($request);
        if(count($state)){
            return $request->session()->get('tempRfqSelection');
        }
        else{
            $array = [];
            $array['name'] = $request->name;
            //$array['index'] = $request->index;
            $array['partNo'] = $request->partNo;
            $tpids = [];
            foreach($request->selections as $sel){
                if($sel['partNo'] == $request->partNo){
                    array_push($tpids, $sel['product_id']);   
                }
            }
            $final = Product::with('suppliers')->whereIn('id', $tpids)->get();
            $array['products'] = $final;
            $request->session()->push('tempRfqSelection', $array);
            return $request->session()->get('tempRfqSelection');
        }

    }

    public function getGeneralTerms(Request $request){
        $credit_terms = DB::table('credit_terms')->get();
        $delievery_terms = DB::table('delivery_terms')->get();
        $warranty_terms = DB::table('warranty_terms')->get();
        $service_terms = DB::table('service_terms')->get();

        return response()->json([
            'credit_terms' =>$credit_terms,  
            'delievery_terms' =>$delievery_terms, 
            'warranty_terms' =>$warranty_terms, 
            'service_terms' =>$service_terms  
        ]);
    }

    public function removeSelection(Request $request, $id){
        $partNo = '';
        $array = $request->session()->get('tempProdSelection');
        foreach($request->session()->get('tempProdSelection') as $key => $temp){
            if($temp[0]['id'] == $id){
                $partNo = $temp[0]['name'];
                unset($array[$key]);
            }
        }

        $array = array_values($array);
        $request->session()->put('tempProdSelection', $array);
        $this->removeRfqSelection($request, $partNo);
        return $request->session()->get('tempProdSelection');
    }
    

    public function removeRfqSelection(Request $request, $partno, $name=null){
        if($request->session()->has('tempRfqSelection')){
            $array = $request->session()->get('tempRfqSelection');
            foreach($array as $key => $temp){
                if(str_replace(' ','',trim($temp['partNo'])) == str_replace(' ','',trim($partno))){
                    unset($array[$key]);
                }
            }
            $array = array_values($array);
            $request->session()->put('tempRfqSelection', $array);
            return $request->session()->get('tempRfqSelection', $array);    
        }
   }

    public function removeRfqSelection_v1(Request $request){

        if($request->session()->has('tempRfqSelection')){
            $array = $request->session()->get('tempRfqSelection');
            foreach($array as $key => $temp){
                $loop_part_no =  $temp['partNo'];
                $loop_part_no = str_replace(' ','',$loop_part_no);

                $part_no = str_replace(' ','',$request->partNo);
                if($part_no == $loop_part_no){
                    unset($array[$key]);
                }
            }
            $array = array_values($array);
            $request->session()->put('tempRfqSelection', $array);
            return $request->session()->get('tempRfqSelection', $array);    
        }
   }

    public function getStoredSearch(Request $request){
        return $request->session()->get('tempProdSelection');           
    }
    public function getRFQselections(Request $request){

        $a = json_encode($request->session()->get('tempRfqSelection'));
        $b = json_decode($a);
        return $b;
        //return $request->session()->get('tempRfqSelection');           
    }

    public function removeStoredSearch(Request $request){
        $request->session()->forget('tempProdSelection');       
        $request->session()->forget('tempRfqSelection');       
    }

    public function getPartNoList(){
        return Product::select('part_number','name')->get();
    }

}




