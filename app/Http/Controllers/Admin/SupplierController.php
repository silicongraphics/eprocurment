<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Vendor;
use App\Models\PaymentMethod;
use App\Models\DeliveryMethod;
use App\Models\DeliveryTerm;
use App\Models\Country;
use App\Models\City;
use App\Models\CreditTerm;
use App\Models\VendorUser;
use App\Models\DocType;
use App\Models\Product;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use Session;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.supplier.suppliercompanies');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getsuppliers() {
          $componies = Company::with('vendor')->where('type', '0');
          return DataTables::eloquent($componies)
                 ->filter(function ($query) {
                            if (request()->has('name')) {
                                $query->where('company_name', 'like', "%" . request('name') . "%");
                            }
                            if (request()->has('id')) {
                                $query->where('id', 'like', "%" . request('id') . "%");
                            }
                            if (request()->has('status')) {
                                $query->where('status', 'like', "%" . request('status') . "%");
                            }
                        }, true)
                ->addColumn('status', function (Company $company) {
                    return $company->status ? 'Active': 'In-Active';
                })
                ->addColumn('actions', function (Company $company) {
                   return '<div class="btn-group mr-1 mb-1">
                              <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                              </button>
                          <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> 
                          <a class="dropdown-item" href="' . route('supplier.show', $company->vendor->id) .'"><i class="fa fa-eye"></i> View Supplier</a> 
                          <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit Supplier</a>
                             <form action="' . route('supplier.delete', $company->vendor->id) . '" method="POST">'
                                . csrf_field() . method_field("DELETE") .
                            '<button class="dropdown-item" type="submit" onClick="return confirm("Are you sure do you want to delete user?")"><i class="fa fa-trash"></i> Delete Supplier</button> 
                            </form> 
                          </div>
                        </div>';
                })
                ->escapeColumns([])
                ->toJson();
    }

     public function getusers(Request $request) {
        $supplierid = $request->supplier_id;
        $supplierS = Vendor::findOrFail($supplierid);
        $supplierusers = $supplierS->user();
          return DataTables::eloquent($supplierusers)
                ->addColumn('active', function ($supplierusers) {
                    return $supplierusers->active ? 'Active': 'In-Active';
                })
                ->addColumn('vendors', function($supplierusers) {
                     return $supplierusers->vendors->name;
                 })
                ->addColumn('actions', function($supplierusers) {
                    return '<div class="btn-group mr-1 mb-1">
                              <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                              </button>
                          <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="'. route('supplieruser.show', $supplierusers->id) . '"><i class="fa fa-eye"></i> View User</a> 
                          <a class="dropdown-item" href="' . route('supplieruser.edit', $supplierusers->id)  . '"><i class="fa fa-pencil"></i> Edit User</a> 
                          <form action="' . route('deletesupplieruser', $supplierusers->id) . '" method="POST">'
                                . csrf_field() . method_field("DELETE") .
                          '<button class="dropdown-item" type="submit" onClick="return confirm("Are you sure do you want to delete user?")"><i class="fa fa-trash"></i> Delete User</button> 
                           </div>
                        </div>';
                })
                ->addColumn('verified', function ($supplierusers) {
                   return $supplierusers->verified ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-close" aria-hidden="true"></i>';
                })
                ->escapeColumns([])
                ->toJson();
    }


    public function create()
    {
        $delivery_methods = DeliveryMethod::all();
        $payment_methods = PaymentMethod::all();
        $delivery_terms = DeliveryTerm::all();
        $countries = Country::all();
        $cities = City::all();
        $payment_terms = CreditTerm::all();
        $doc_type = DocType::all();
       // $products = Product::all();
        return view('admin.supplier.addsupplier', compact('delivery_methods', 'payment_methods', 'delivery_terms', 'countries', 'cities', 'payment_terms', 'doc_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'profile_picture' => 'mimes:jpeg,png,bmp,tiff |max:4096',
            'trade_liscense' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'vat_certificate' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'emirates_id_user' => 'mimes:jpeg,png,bmp,tiff, doc,pdf,docx,zip |max:2048',
            'emirates_id' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'password' => 'required|min:6',
            'email' => 'required|unique:vendor_users'
             ]
        );
        
        $trade_liscense = $request->file('trade_liscense');
        $vat_certificate = $request->file('vat_certificate');
        $emirates_id = $request->file('emirates_id');
        $profile_picture = $request->file('profile_picture');
        $emirates_id_user = $request->file('emirates_id_user');
        //creating comapny
        $company = new Company($request->all());
        $company->company_name = $request->name;
        $company->type = 0;
        $company->trade_license = ($trade_liscense) ? $trade_license_filename = $trade_liscense->getClientOriginalName() : $trade_license_filename = '';
        $company->vat_certificate = ($vat_certificate) ? $vat_certificate_filename = $vat_certificate->getClientOriginalName() : $vat_certificate_filename = '';
        $company->emirates_id = ($emirates_id) ? $emirates_id_filename = $emirates_id->getClientOriginalName() : $emirates_id_filename = '';
        $company->save();
        // creating supplier here
        $vendor = new Vendor($request->all());
        $vendor->company_id = $company->id;
        $vendor->save();
        $vendor->custom_id = "S" . $vendor->id;
        $vendor->save();
        // vendor created
        
        // Vendor Products
        $vendor->products()->sync($request->products_supplied);
        
        /* Document upload into a directory */
         $location = 'document/vendor/' . $vendor->id;
        // Upload file
        $trade_liscense->move($location,$trade_license_filename);
        $vat_certificate->move($location,$vat_certificate_filename);
        $emirates_id->move($location,$emirates_id_filename);
        /* Document into directory end */
        
        /* Vendor user */ 
        $vendoruser = new VendorUser($request->all());
        $pw = Hash::make($request->password);
        $vendoruser->vendor_id = $vendor->id;
        $vendoruser->profile_picture = ($profile_picture) ? $profile_picture_name = $profile_picture->getClientOriginalName() : $profile_picture = '';
        $vendoruser->emirates_id = ($emirates_id_user) ? $emirates_id_user_filename = $emirates_id_user->getClientOriginalName() : $emirates_id_user_filename = '';
        $vendoruser->user_group_id = 4;
        $vendoruser->password = $pw;
        $vendoruser->verified = $request->verified_user; 
        $vendoruser->active = $request->status_user;
        $vendoruser->company_id = $vendor->company->id;
        $vendoruser->save();
           /* Vendor user */ 

          /* Document upload of user */
        $location = 'document/vendor/users/' . $vendoruser->id;
        if($profile_picture):
        $profile_picture->move($location,$profile_picture_name);
        endif;
        if($emirates_id_user):
        $emirates_id_user->move($location,$emirates_id_user_filename);
        endif;
        /* Document upload of user end */

        Session::flash('success', 'Supplier has been updated successfully');
        return redirect()->route('suppliercompanies');
        // vendor user is created
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $vendor_inv = Vendor::findOrFail($id);
        $vendor = Company::findOrFail($vendor_inv->company_id);
        $custom_id = $vendor_inv->custom_id;
        $shipping_city = ($vendor->shipping_city_id) ? City::where('id', $vendor->shipping_city_id)->first()->name : '';
        $billing_city = ($vendor->billing_city_id) ? City::where('id', $vendor->billing_city_id)->first()->name : '';
        $shipping_country = ($vendor->shipping_country_id) ? Country::where('id', $vendor->shipping_country_id)->first()->name : '';
        $billing_country = ($vendor->billing_country_id) ? Country::where('id', $vendor->billing_country_id)->first()->name : '';
        $delivery_method = ($vendor->delivery_method_id) ? DeliveryMethod::where('id', $vendor->delivery_method_id)->first()->name : '';
        $payment_method = ($vendor->payment_method_id) ? PaymentMethod::where('id', $vendor->payment_method_id)->first()->name : '';
        $credit_term =    ($vendor->credit_term_id) ? CreditTerm::where('id', $vendor->credit_term_id)->first()->name : '';
        return view('admin.supplier.viewsupplier', compact('vendor', 'custom_id', 'id', 'billing_city', 'shipping_city', 'billing_country', 'shipping_country', 'delivery_method', 'payment_method', 'credit_term'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $delivery_methods = DeliveryMethod::all();
        $payment_methods = PaymentMethod::all();
        $delivery_terms = DeliveryTerm::all();
        $countries = Country::all();
        $cities = City::all();
        $payment_terms = CreditTerm::all();
        $doc_type = DocType::all();

        $supplier_one = Vendor::findOrFail($id);
        $supplier = $supplier_one->company()->first();
        $supplieruser  = $supplier_one->user()->first();
        $supplieruser = (!is_null($supplieruser)) ? $supplieruser : ''; 
        return view('admin.supplier.editsupplier', compact('id', 'supplier', 'supplieruser', 'delivery_methods', 'payment_methods', 'delivery_terms', 'countries', 'cities', 'payment_terms', 'doc_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $vendor = Vendor::findOrFail($id);
        $company_id = $vendor->company()->first()->id;
        $vendoruser_id = ($vendor->user()->first()) ? $vendor->user()->first()->id : '';
        $input = $request->all();

         $this->validate($request, [
            'profile_picture' => 'mimes:jpeg,png,bmp,tiff |max:4096',
            'trade_liscense' => 'mimes:doc,pdf,docx,zip |max:2048',
            'vat_certificate' => 'mimes:doc,pdf,docx,zip |max:2048',
            'emirates_id_user' => 'mimes:doc,pdf,docx,zip |max:2048',
            'password' => (!$vendoruser_id) ? 'required|min:6' :  ($request->password != '') ? 'min:6' : '',
            'email' => 'required|unique:vendor_users,email,'.$vendoruser_id
             ]
        );
        
        $trade_liscense = $request->file('trade_liscense');
        $vat_certificate = $request->file('vat_certificate');
        $emirates_id = $request->file('emirates_id');
        $profile_picture = $request->file('profile_picture');
        $emirates_id_user = $request->file('emirates_id_user');
        //creating comapny
        $company = Company::findOrFail($company_id);
        $company->fill($input)->save();  
        $company->company_name = $request->name;
        $company->type = 0;
        $company->trade_license = ($trade_liscense) ? $trade_license_filename = $trade_liscense->getClientOriginalName() : $trade_license_filename = $company->trade_license;
        $company->vat_certificate = ($vat_certificate) ? $vat_certificate_filename = $vat_certificate->getClientOriginalName() : $vat_certificate_filename = $company->vat_certificate;
        $company->emirates_id = ($emirates_id) ? $emirates_id_filename = $emirates_id->getClientOriginalName() : $emirates_id_filename = 
                                 $company->emirates_id;
        $company->save();
        
        // creating supplier here
        $vendor = Vendor::findOrFail($id);
        $vendor->fill($input)->save();
        // vendor created
        
        // Vendor Products
        $vendor->products()->sync($request->products_supplied);
        
        /* Document upload into a directory */
         $location = 'document/vendor/' . $vendor->id;
        // Upload file
        ($request->file('trade_liscense')) ? $trade_liscense->move($location,$trade_license_filename) : '';
        ($request->file('vat_certificate')) ? $vat_certificate->move($location,$vat_certificate_filename) : '';
        ($request->file('emirates_id')) ?  $emirates_id->move($location,$emirates_id_filename) : '';
        /* Document into directory end */
        
        /* Vendor user */ 
        $vendoruser =($vendoruser_id) ? VendorUser::findOrFail($vendoruser_id) : new VendorUser();
        $pw = Hash::make($request->password);
        $pw = ($pw != $vendoruser->password && $pw != '') ? $pw : $vendoruser->password;
        $vendoruser->vendor_id = $vendor->id;
        $vendoruser->user_group_id = 4;
        $vendoruser->company_id = $vendor->company->id;
        $vendoruser->fill($input)->save();
      
        $vendoruser->profile_picture = ($profile_picture) ? $profile_picture_name = $profile_picture->getClientOriginalName() : $profile_picture = '';
        $vendoruser->emirates_id = ($emirates_id_user) ? $emirates_id_user_filename = $emirates_id_user->getClientOriginalName() : $emirates_id_user_filename = '';
        $vendoruser->password = $pw;
        $vendoruser->save();
           /* Vendor user */ 

        /* Document upload of user */
        $location = 'document/vendor/users/' . $vendoruser->id;
        if($profile_picture):
        $profile_picture->move($location,$profile_picture_name);
        endif;
        ($request->file('emirates_id_user')) ? $emirates_id_user->move($location,$emirates_id_user_filename) : '';
        /* Document upload of user end */

        Session::flash('success', 'Supplier has been updated successfully');
        return redirect()->route('suppliercompanies');
        // vendor user is created
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = Vendor::findOrFail($id);
        $vendor->delete();
        Session::flash('success', 'Supplier has been deleted successfully');
        return redirect()->route('suppliercompanies');
    }
}
