<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Manufacturer;
use Yajra\DataTables\Facades\DataTables;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.product.brands');
    }

    public function getBrands()
    {
        $model = Manufacturer::query();
        return DataTables::eloquent($model)
                ->filter(function ($query) {
                            if (request()->has('name')) {
                                $query->where('name', 'like', "%" . request('name') . "%");
                            }
                            if (request()->has('id')) {
                                $query->where('id', 'like', "%" . request('id') . "%");
                            }
                            if (request()->has('url')) {
                                $query->where('url', 'like', "%" . request('url') . "%");
                            }
                            if (request()->has('status')) {
                                $query->where('status', 'like', "%" . request('status') . "%");
                            }
                        }, true)
                  ->addColumn('status', function(Manufacturer $brand) {
                        return $brand->status ? 'Active': 'In-Active';
                   })
                  ->addColumn('actions', function(Manufacturer $brand) {
                    return '<div class="btn-group mr-1 mb-1">
                              <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                              </button>
                          <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="'. route('brand.show', $brand->id) .'"><i class="fa fa-eye"></i> View Brand</a> <a class="dropdown-item" href="' . route('editbrand', $brand->id) . '">
                              <i class="fa fa-pencil"></i> Edit Brand</a> <a class="dropdown-item" href="#">
                              <form action="' . route('brand.delete', $brand->id) . '" method="POST">'
                                . csrf_field() . method_field("DELETE") . 
                                '<button class="dropdown-item" type="submit"><i class="fa fa-trash"></i> Delete Brand </button> </div>
                        </div>';
                })
                ->escapeColumns([])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.addbrand');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand = new Manufacturer($request->all());
        $brand->save();
        $brand->custom_id = "B" . $brand->id;
        $brand->save();
        return redirect()->route('brands')->with('success', 'New Brand has been added succcessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Manufacturer::findOrFail($id);
        return view('admin.product.showbrand', compact('brand', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $brand = Manufacturer::find($id);
        return view('admin.product.editbrand',compact('brand','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $brand = Manufacturer::findOrFail($id);
         $input = $request->all();
         $brand->fill($input)->save();
        return redirect()->route('brands')->with('success', 'Brand has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Manufacturer::find($id);
        $brand->delete();
        return redirect()->route('brands')->with('success', 'Brand has been deleted Successfully');
    }
}
