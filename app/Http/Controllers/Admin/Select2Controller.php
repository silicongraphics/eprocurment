<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;


class Select2Controller extends Controller
{
    public function getroducts(Request $request) {
    	$data = [];
       if($request->has('q')){
            $search = $request->q;
            $data = Product::where('name', 'like', "%" . $search . "%")->get(['id', 'name']);
        }
        else {
        	$data = Product::limit(5)->offset(5)->get();
        }
        return response()->json($data);
    }
}
