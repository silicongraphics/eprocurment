<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Manufacturer;
use App\Models\Application;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $products = Product::with(['applications', 'brands', 'categories'])->get();
        // return $products;
        // $products = Product::with(['attributes'])->where('id', '26')->get();
        return view('admin.product.products');
    }

    public function getproducts()
    {
        $products = Product::with(['applications', 'brands', 'categories']);
        // return Datatables::of($products)->make(true);
        return DataTables::eloquent($products)
                ->filter(function ($query) {
                            if (request()->has('name')) {
                                $query->where('name', 'like', "%" . request('name') . "%");
                            }
                            if (request()->has('id')) {
                                $query->where('id', 'like', "%" . request('id') . "%");
                            }
                            if (request()->has('part_number')) {
                                $query->where('part_number', 'like', "%" . request('part_number') . "%");
                            }
                            if (request()->has('status')) {
                                $query->where('status', 'like', "%" . request('status') . "%");
                            }
                        }, true)
                ->addColumn('applications', function (Product $product) {
                    return $product->applications->map(function($app) {
                        return $app->name;
                    })->implode('<br>');
                })
                ->addColumn('brand', function (Product $product) {
                    return $product->brands->name;
                })
                ->addColumn('categories', function (Product $product) {
                    return $product->categories->map(function($category) {
                        return $category->name;
                    })->implode('<br>');
                })
                ->addColumn('status', function (Product $product) {
                    return $product->status ? 'Active': 'In-Active';
                })
                ->addColumn('actions', function (Product $product) {
                    return '<div class="btn-group mr-1 mb-1">
                              <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                              </button>
                          <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="'.route('product.show', $product->id) . '"><i class="fa fa-eye"></i> View Product</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit Product</a> 
                             <form action="' . route('product.delete', $product->id) . '" method="POST">'
                                . csrf_field() . method_field("DELETE") .  
                             '<button class="dropdown-item" type="submit"><i class="fa fa-trash"></i> Delete Product</button> </div>
                        </div>';
                })
                ->escapeColumns([])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $attributes = Attribute::with('options')->get();
        $category = new Category();
        // $parentcategories = $category->getparents();
        $parentcategories = Category::all();
        $manufacturer = Manufacturer::all();
        $applications = Application::all();
        return view('admin.product.addproduct', compact('attributes', 'parentcategories', 'manufacturer', 'applications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $this->validate($request, [
            'part_number' => 'required|unique:products'
             ]
        );

        $pro = new Product($request->all());
        $file = $request->file('image');
        $pro->category_ids = implode(',', $request->parent_category_id);
        $pro->attributes = $this->setAttributes($request->all());
        // $pro->attributes = array_combine($request->key, $request->value);
        // $pro->attributes = '{"Power":"112"}';
        $pro->image = ($file) ? $filename = base64_encode(@file_get_contents($file)) : $filename = '';
        $pro->save();
        $pro->custom_id = "P" . $pro->id;
        $pro->save();
        $pro->applications()->attach($request->applications); 
        $pro->categories()->sync($request->parent_category_id); 
        $pro->attributes()->sync($request->attvals);
        $directory = 'product/' . $pro->id . "/";
        if($file) {
            if(!file_exists($directory . $pro->image)) {
                $directory = 'product/' . $pro->id . "/";
                Storage::putFile($directory, $request->file('image'));
            }
            else {
                 Session::flash('error', 'Product Image Already exists');
                 return redirect()->route('products');
            }
        }
        Session::flash('success', 'Product has been created successfully');
        return redirect()->route('products');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product->load(['applications', 'categories', 'manufacturer', 'attributes']);
        return view('admin.product.showproduct', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $attributes_all = Attribute::with('options')->get();
        $parentcategories = Category::all();
        $brands = Manufacturer::all();
        $applications = Application::all();
         $product->load(['applications', 'categories', 'manufacturer', 'attributes']);
         return view('admin.product.editproduct', compact('product', 'applications', 'parentcategories', 'brands', 'attributes_all'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        Session::flash('success', 'Product has been deleted Successfully');
        return redirect()->route('products');
    }

    public function setAttributes($data)
    {
        if(!empty($data['key']) && !empty($data['value']))
        {
            $attributes = array_combine($data['key'], $data['value']);
            $attributes = json_encode($attributes);
        }
        else
        {
            $attributes = NULL;
        }
        return $attributes;
    }

}
