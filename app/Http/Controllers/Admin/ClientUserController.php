<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ClientUser;
use App\Models\Client;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use Session;

class ClientUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.client.clientusersindex');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getclientusers() {
       $clientusers = ClientUser::with('vendors');
           return DataTables::eloquent($clientusers)
                ->filter(function ($query) {
                    if (request()->has('name')) {
                        $query->where('first_name', 'like', "%" . request('name') . "%");
                    }
                }, true)
                ->addColumn('active', function (ClientUser $user) {
                    return $user->active ? 'Active': 'In-Active';
                })
                ->addColumn('vendors', function(ClientUser $user) {
                     return $user->vendors->name;
                 })
                ->addColumn('actions', function (ClientUser $user) {
                    return '<div class="btn-group mr-1 mb-1">
                              <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                              </button>
                          <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="' . route('clientuser.show', $user->id) . '"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="' . route('editclientuser', $user->id) . '"><i class="fa fa-pencil"></i> Edit User</a> 
                              <form action="' . route('deleteclientuser', $user->id) . '" method="POST">'
                                . csrf_field() . method_field("DELETE") . 
                              '<button class="dropdown-item" type="submit" onClick="return confirm("Are you sure do you want to delete user?")"><i class="fa fa-trash"></i> Delete User</button> 
                           </div>
                        </div>';
                })
                ->addColumn('verified', function (ClientUser $user) {
                   return $user->verified ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-close" aria-hidden="true"></i>';
                })
                ->escapeColumns([])
                ->toJson();
    }


    public function create()
    {
         $clients = Client::all();
         return view('admin.client.clientuser', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, [
            'profile_picture' => 'mimes:jpeg,png,bmp,tiff |max:4096',
            'password' => 'required|min:6',
            'emirates_id' => 'mimes:doc,pdf,docx,zip |max:2048',
            'email' => 'required|unique:client_users'
             ]
        );

        $file = $request->file('profile_picture');
        ($file) ? $filename = $file->getClientOriginalName() : $filename = '';
        $emirates_id_file = $request->file('emirates_id');
        ($emirates_id_file) ? $filename_emirate = $emirates_id_file->getClientOriginalName() : $filename_emirate = '';
         
        // insert data here
        $client = Client::find($request->client_id);
        $clientuser = new ClientUser($request->all());
        $pw = Hash::make($request->password);
        $clientuser->profile_picture = $filename;
        $clientuser->emirates_id = $filename_emirate;
        $clientuser->user_group_id = 4;
        $clientuser->password = $pw;
        $clientuser->company_id = $client->company->id;
        $client->user()->save($clientuser);
        
         /* Document upload of user */
        $location = 'document/client/users/' . $clientuser->id;
        if($file):
        $file->move($location,$filename);
        endif;
        $emirates_id_file->move($location,$filename_emirate);
        /* Document upload of user end */
        
         /* Client user document */
        $user_documemnt = array(2 => ['name' => $filename_emirate, 'client_id' => $client->id],
                                0 => ['name' => $filename, 'client_id' => $client->id]);
        
        $clientuser->documents()->sync($user_documemnt);
        /* Client user document */

       // $clientuser->save();
        Session::flash('success', 'Client User has been created successfully');
        return redirect()->route('clientusers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clientuser = ClientUser::findOrFail($id);
        $company_name =  $clientuser->company->company_name;
        return view('admin.client.viewclientuser', compact('clientuser', 'id', 'company_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clientuser = ClientUser::find($id);
        $clients = Client::all();
        return view('admin.client.editclientuser',compact('clientuser','id', 'clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::find($request->client_id);
        $clientuser = ClientUser::findOrFail($id);

       // print_r($request->all()); die();
        $this->validate($request, [
            'profile_picture' => 'mimes:jpeg,png,bmp,tiff |max:4096',
            'password' => ($request->password != '') ? 'min:6' : '',
            'email' => 'required|unique:client_users,email,'.$id
             ]
        );

        $file = $request->file('profile_picture');
        ($file) ? $filename = $file->getClientOriginalName() : $filename = $clientuser->profile_picture;
        $emirates_id_file = $request->file('emirates_id');
        ($emirates_id_file) ? $filename_emirate = $emirates_id_file->getClientOriginalName() : $filename_emirate = $clientuser->emirates_id;

        // update data here
        $input = $request->all();
        $clientuser->fill($input)->save();
        $pw = Hash::make($request->password);
        $clientuser->profile_picture = $filename;
        $clientuser->emirates_id = $filename_emirate;
        $clientuser->user_group_id = 4;
        $clientuser->password = ($pw != $clientuser->password && $pw != '') ? $pw : $vendoruser->password;
        $clientuser->company_id = $client->company->id;
        $clientuser->save();

         /* Document upload of user */
        $location = 'document/client/users/' . $clientuser->id;
        if($file):
        ($file != $clientuser->profile_picture) ? $file->move($location,$filename) : '';
        endif;
        ($filename_emirate != $clientuser->emirates_id) ? $emirates_id_file->move($location,$filename_emirate) : '';
        /* Document upload of user end */
        
        /* Client user document */
        $user_documemnt = array(2 => ['name' => $filename_emirate, 'client_id' => $client->id],
                                0 => ['name' => $filename, 'client_id' => $client->id]);
        
        $clientuser->documents()->sync($user_documemnt);
        /* Client user document */

       // $clientuser->save();
        Session::flash('success', 'Client User has been updated successfully');
        return redirect()->route('clientusers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clientuser = ClientUser::find($id);
        $clientuser->delete();
        Session::flash('success', 'Client User has been deleted successfully');
        return redirect()->route('clientusers');
    }
}
