<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Client;
use App\Models\Manufacturer;
use App\Models\Vendor;
use App\Models\PaymentMethod;
use App\Models\DeliveryMethod;
use App\Models\DeliveryTerm;
use App\Models\Country;
use App\Models\City;
use App\Models\CreditTerm;
use App\Models\ClientUser;
use App\Models\DocType;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use Session;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.client.companies');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getclients() {
          $clients = Company::with('buyer')->where('type', '1');
          return DataTables::eloquent($clients)
                ->filter(function ($query) {
                            if (request()->has('name')) {
                                $query->where('company_name', 'like', "%" . request('name') . "%");
                            }
                            if (request()->has('id')) {
                                $query->where('id', 'like', "%" . request('id') . "%");
                            }
                            if (request()->has('status')) {
                                $query->where('status', 'like', "%" . request('status') . "%");
                            }
                        }, true)
                ->addColumn('status', function (Company $company) {
                    return $company->status ? 'Active': 'In-Active';
                })
                ->addColumn('actions', function (Company $company) {
                    ($company->buyer->id) ? $company->buyer->id : $company->id;
                    return '<div class="btn-group mr-1 mb-1">
                              <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                              </button>
                          <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> 
                             <a class="dropdown-item" href="' . route('client.show', $company->buyer->id) .'"><i class="fa fa-eye"></i> View Client</a> 
                             <a class="dropdown-item" href="' . route('client.edit', $company->buyer->id) . '"><i class="fa fa-pencil"></i> Edit Client</a> 
                             <form action="' . route('client.delete', $company->buyer->id) . '" method="POST">'
                                . csrf_field() . method_field("DELETE") .
                            '<button class="dropdown-item" type="submit" onClick="return confirm("Are you sure do you want to delete Buyer?")"><i class="fa fa-trash"></i> DELETE CLIENT</button> 
                            </form> 
                          </div>
                        </div>';
                })
                ->escapeColumns([])
                ->toJson();
    }

      public function getusers(Request $request) {
        $clientid = $request->client_id;
        $clientS = Client::findOrFail($clientid);
        $clientusers = $clientS->user();
          return DataTables::eloquent($clientusers)
                ->addColumn('active', function ($clientusers) {
                    return $clientusers->active ? 'Active': 'In-Active';
                })
                ->addColumn('vendors', function($clientusers) {
                     return $clientusers->vendors->name;
                 })
                ->addColumn('actions', function ($clientusers) {
                    return '<div class="btn-group mr-1 mb-1">
                              <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                              </button>
                          <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="' . route('clientuser.show', $clientusers->id) . '"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="' . route('editclientuser', $clientusers->id) . '"><i class="fa fa-pencil"></i> Edit User</a> 
                              <form action="' . route('deleteclientuser', $clientusers->id) . '" method="POST">'
                                . csrf_field() . method_field("DELETE") . 
                              '<button class="dropdown-item" type="submit" onClick="return confirm("Are you sure do you want to delete user?")"><i class="fa fa-trash"></i> Delete User</button> 
                           </div>
                        </div>';
                })
                ->addColumn('verified', function ($clientusers) {
                   return $clientusers->verified ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-close" aria-hidden="true"></i>';
                })
                ->escapeColumns([])
                ->toJson();
    }


    public function create()
    {
        $brands = Manufacturer::all();
        $delivery_methods = DeliveryMethod::all();
        $payment_methods = PaymentMethod::all();
        $delivery_terms = DeliveryTerm::all();
        $suppliers = Vendor::all();
        $countries = Country::all();
        $cities = City::all();
        $payment_terms = CreditTerm::all();
        $doc_type = DocType::all();
        return view('admin.client.addclient', compact('brands', 'delivery_methods', 'payment_methods', 'delivery_terms', 'suppliers', 'countries', 'cities', 'payment_terms', 'doc_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'profile_picture' => 'mimes:jpeg,png,bmp,tiff |max:4096',
            'trade_liscense' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'vat_certificate' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'emirates_id' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'password' => 'required|min:6',
            'email' => 'required|unique:client_users'
             ]
        );

        $trade_liscense = $request->file('trade_liscense');
        $vat_certificate = $request->file('vat_certificate');
        $emirates_id = $request->file('emirates_id');
        $profile_picture = $request->file('profile_picture');
        $emirates_id_user = $request->file('emirates_id_user');

        $company = new Company($request->all());
        $company->company_name = $request->name;
        $company->type = 1;
        $company->trade_license = ($trade_liscense) ? $trade_license_filename = $trade_liscense->getClientOriginalName() : $trade_license_filename = '';
        $company->vat_certificate = ($vat_certificate) ? $vat_certificate_filename = $vat_certificate->getClientOriginalName() : $vat_certificate_filename = '';
        $company->emirates_id = ($emirates_id) ? $emirates_id_filename = $emirates_id->getClientOriginalName() : $emirates_id_filename = '';
        $company->save();

        $client = new Client($request->all());
        $client->company_id = $company->id;
        $client->save();
        $client->custom_id = "C" . $client->id;
        $client->save();

        /* Document upload into a directory */
        $location = 'document/client/' . $client->id;
        // Upload file
        $trade_liscense->move($location,$trade_license_filename);
        $vat_certificate->move($location,$vat_certificate_filename);
        $emirates_id->move($location,$emirates_id_filename);
        /* Document into directory end */

        $client->manufactures()->sync($request->manufacturers);
        $client->suppliers()->sync($request->vendors);

        // client user is created
        $clientuser = new ClientUser($request->all());
        $pw = Hash::make($request->password);
        $clientuser->client_id = $client->id;
        $clientuser->profile_picture = ($profile_picture) ? $profile_picture_name = $profile_picture->getClientOriginalName()
            : $profile_picture = '';
        $clientuser->emirates_id = ($emirates_id_user) ? $emirates_id_user_filename = $emirates_id_user->getClientOriginalName() : $emirates_id_user_filename = '';
        $clientuser->user_group_id = 4;
        $clientuser->password = $pw;
        $clientuser->company_id = $client->company->id;
        $clientuser->active = $request->status_user;
        $clientuser->verified = $request->verified_user; 
        $clientuser->company_id = $client->company->id;
        $clientuser->save();
        // client user is created
        /* Client Document Relationship */
        $documents = array(2 => ['name' => $trade_license_filename],
            4 => ['name' => $vat_certificate_filename],
            3 => ['name' => $emirates_id_filename]);
        
        $client->documentypes()->sync($documents);
        /* Client Document Relationship */

        /* Document upload of user */
        $location = 'document/client/users/' . $clientuser->id;
        if($profile_picture):
        $profile_picture->move($location,$profile_picture_name);
        endif;
        if($emirates_id_user):
        $emirates_id_user->move($location,$emirates_id_user_filename);
        endif;
        /* Document upload of user end */

        /* Client user document */
        $user_documemnt = array(2 => ['name' => $emirates_id_user_filename, 'client_id' => $client->id],
                                0 => ['name' => $profile_picture_name, 'client_id' => $client->id]);
        
        $clientuser->documents()->sync($user_documemnt);

        // return $documents;
        // return $request->all();
        Session::flash('success', 'Client has been added successfully');
        return redirect()->route('companies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client_inv = Client::findOrFail($id);
        $client = Company::findOrFail($client_inv->company_id);
        $custom_id = $client_inv->custom_id;
        $shipping_city = ($client->shipping_city_id) ? City::where('id', $client->shipping_city_id)->first()->name : '';
        $billing_city = ($client->billing_city_id) ? City::where('id', $client->billing_city_id)->first()->name : '';
        $shipping_country = ($client->shipping_country_id) ? Country::where('id', $client->shipping_country_id)->first()->name : '';
        $billing_country = ($client->billing_country_id) ? Country::where('id', $client->billing_country_id)->first()->name : '';
        return view('admin.client.viewclient', compact('client', 'custom_id', 'id', 'billing_country', 'shipping_country', 'shipping_city', 'billing_city'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brands = Manufacturer::all();
        $delivery_methods = DeliveryMethod::all();
        $payment_methods = PaymentMethod::all();
        $delivery_terms = DeliveryTerm::all();
        $suppliers = Vendor::all();
        $countries = Country::all();
        $cities = City::all();
        $payment_terms = CreditTerm::all();
        $doc_type = DocType::all();
        $client_one = Client::findOrFail($id);
        $client = $client_one->company()->first();
        $clientuser  = $client_one->user()->first();
        $clientuser = (!is_null($clientuser)) ? $clientuser : ''; 
        return view('admin.client.editclient', compact('id', 'client', 'clientuser', 'brands', 'delivery_methods', 'payment_methods', 'delivery_terms', 'suppliers', 'countries', 'cities', 'payment_terms', 'doc_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $client = Client::findOrFail($id);
        $company_id = $client->company()->first()->id;
        $clientuser_id = ($client->user()->first()) ? $client->user()->first()->id : '';
        $input = $request->all();
        
        $this->validate($request, [
            'profile_picture' => 'mimes:jpeg,png,bmp,tiff |max:4096',
            'trade_liscense' => 'mimes:doc,pdf,docx,zip | max:2048',
            'vat_certificate' => 'mimes:doc,pdf,docx,zip |max:2048',
            'emirates_id' => 'mimes:doc,pdf,docx,zip |max:2048',
            'password' => (!$clientuser_id) ? 'required|min:6' :  ($request->password != '') ? 'min:6' : '',
            'email' => 'required|unique:client_users,email,'.$clientuser_id
             ]
        );

        $trade_liscense = $request->file('trade_liscense');
        $vat_certificate = $request->file('vat_certificate');
        $emirates_id = $request->file('emirates_id');
        $profile_picture = $request->file('profile_picture');
        $emirates_id_user = $request->file('emirates_id_user');

        $company = Company::findOrFail($company_id);

        $trd = $company->trade_license;
        $vt  = $company->vat_certificate;
        $emt = $company->emirates_id;

        $company->fill($input)->save();     
        $company->company_name = $request->name;
        $company->type = 1;
        $company->trade_license = ($trade_liscense) ? $trade_license_filename = $trade_liscense->getClientOriginalName() : $trade_license_filename = $company->trade_license;
        $company->vat_certificate = ($vat_certificate) ? $vat_certificate_filename = $vat_certificate->getClientOriginalName() : $vat_certificate_filename = $company->vat_certificate;
        $company->emirates_id = ($emirates_id) ? $emirates_id_filename = $emirates_id->getClientOriginalName() : $emirates_id_filename = $company->emirates_id;
        $company->save();
        
        $client = Client::findOrFail($id);
        $client->fill($input)->save();
        
          /* Document upload into a directory */
        $location = 'document/client/' . $client->id;
         ($trade_license_filename != $trd) ? $trade_liscense->move($location,$trade_license_filename) : '';
        ($vat_certificate_filename != $vt) ? $vat_certificate->move($location,$vat_certificate_filename) : '';
        ($emirates_id_filename != $emt) ? $emirates_id->move($location,$emirates_id_filename) : '';
        /* Document into directory end */
        
        $client->manufactures()->sync($request->manufacturers);
        $client->suppliers()->sync($request->vendors);
        
        /* Client Document Relationship */
        $documents = array(2 => ['name' => $trade_license_filename],
                        4 => ['name' => $vat_certificate_filename],
                        3 => ['name' => $emirates_id_filename]);
        
       // $client->documentypes()->sync($documents);
        /* Client Document Relationship */
        
         // client user
        $clientuser = ($clientuser_id) ? ClientUser::findOrFail($clientuser_id) : new ClientUser();
        $pw = Hash::make($request->password);
        $pw = ($pw != $clientuser->password && $pw != '') ? $pw : $clientuser->password;
        $clientuser->client_id = $client->id;
        $clientuser->user_group_id = 4;
        $clientuser->company_id = $client->company->id;

        $clientuser->fill($input)->save();
        
        $clientuser->password = $pw;
        $clientuser->profile_picture = ($profile_picture) ? $profile_picture_name = $profile_picture->getClientOriginalName()
            : $profile_picture_name = $clientuser->profile_picture;
        $clientuser->emirates_id = ($emirates_id_user) ? $emirates_id_user_filename = $emirates_id_user->getClientOriginalName() : $emirates_id_user_filename = $clientuser->emirates_id;
        $clientuser->save();
        // client user
        
         /* Document upload of user */
        $location = 'document/client/users/' . $clientuser->id;
        if($profile_picture):
        $profile_picture->move($location,$profile_picture_name);
        endif;
        ($request->file('emirates_id_user')) ? $emirates_id_user->move($location,$emirates_id_user_filename) : '';
        /* Document upload of user end */
        
        /* Client user document */
        $user_documemnt = array(3 => ['name' => $emirates_id_user_filename, 'client_id' => $client->id],
                                0 => ['name' => $profile_picture_name, 'client_id' => $client->id]);
        
        $clientuser->documents()->sync($user_documemnt);
        
        Session::flash('success', 'Buyer has been updated successfully');
        return redirect()->route('companies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::findOrFail($id);
        $client->delete();
        Session::flash('success', 'Buyer has been deleted successfully');
        return redirect()->route('companies');
    }

    public function pendingclients() {
        return view('admin.client.pendingclients');
    }

}
