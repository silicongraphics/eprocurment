<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\AttributeOption;
use Yajra\DataTables\Facades\DataTables;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.product.productattributes');
    }

    public function getattributes()
    {

        $attributes = Attribute::with('options');
        return DataTables::eloquent($attributes)
                      ->filter(function ($query) {
                            if (request()->has('name')) {
                                $query->where('name', 'like', "%" . request('name') . "%");
                            }
                            if (request()->has('id')) {
                                $query->where('id', 'like', "%" . request('id') . "%");
                            }
                            if (request()->has('status')) {
                                $query->where('status', 'like', "%" . request('status') . "%");
                            }
                            if (request()->has('attribute_option_value')) {
                               $query->whereHas('options', function($q) {
                                   $q->where('value', 'like', "%" . request('attribute_option_value') . "%");
                               });
                            }
                        }, true)
                  ->addColumn('status', function(Attribute $app) {
                        return $app->status ? 'Active': 'In-Active';
                   })
                  ->addColumn('actions', function(Attribute $app) {
                    return '<div class="btn-group mr-1 mb-1">
                              <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                              </button>
                          <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="' . route('attribute.show', $app->id). '"><i class="fa fa-eye"></i> View Attribute</a> <a class="dropdown-item" href="' . route('editattribute', $app->id) . '"><i class="fa fa-pencil"></i> Edit Attribute</a>
                               <form action="' . route('deleteattribute', $app->id) . '" method="POST">'
                                . csrf_field() . method_field("DELETE") .  
                              '<button class="dropdown-item" type="submit"><i class="fa fa-trash"></i> Delete Attribute</button> </div>
                        </div>';
                    })
                    ->addColumn('options', function (Attribute $app) {
                        return $app->options->map(function($option) {
                            return $option->value;
                        })->implode('<br>');
                    })
                ->escapeColumns([])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.addattribute');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:attributes'
             ]
        );

        $attribute = new Attribute($request->all());
        $attribute->save();
        foreach ($request->options as $option) {
           $option = new AttributeOption(['value' => $option]);
           $attribute->options()->save($option);   
        }
        return redirect()->route('productattributes')->with('success', 'Attribute has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attribute = Attribute::findOrFail($id);
        $options = $attribute->options()->get();
        return view('admin.product.showattribute', compact('attribute', 'options', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $attribute = Attribute::find($id);
        $options = $attribute->options()->get();
        return view('admin.product.editattribute', compact('options', 'attribute', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:attributes,name,'.$id
             ]
        );

        $attribute = Attribute::findOrFail($id);
        $input = $request->all();
        $attribute->fill($input)->save();
        $attribute->options()->delete();
        foreach ($request->options as $option) {
           $option = new AttributeOption(['value' => $option]);
           $attribute->options()->save($option);   
        }
        return redirect()->route('productattributes')->with('success', 'Attribute has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attribute = Attribute::find($id);
        $attribute->delete();
        return redirect()->route('productattributes')->with('success', 'Attribute has been deleted Successfully');
    }

    public function getattributeoptions(Request $request) {
        $attribute = Attribute::find($request->attr_id);
        $attribute_name = $request->attr_name;
        $attribute_id = $request->attr_id;
        $options = $attribute->options()->get();
        $view = view('admin.product.attributeopions', compact('options', 'attribute_name', 'attribute_id'))->render();
        return response()->json(['html'=>$view]);
    }
}
