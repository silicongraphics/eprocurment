<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\VendorUser;
use App\Models\Vendor;
use App\Models\DocType;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use Session;

class SupplierUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
         return view('admin.supplier.supplierusers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getsupplierusers(Request $request) {
          $supplierusers = VendorUser::with('vendors');
           return DataTables::eloquent($supplierusers)
                ->filter(function ($query) {
                    if (request()->has('name')) {
                        $query->where('first_name', 'like', "%" . request('name') . "%");
                    }
                }, true)
               ->addColumn('active', function (VendorUser $user) {
                    return $user->active ? 'Active': 'In-Active';
                })
                ->addColumn('vendors', function(VendorUser $user) {
                     return $user->vendors->name;
                 })
                ->addColumn('actions', function (VendorUser $user) {
                    return '<div class="btn-group mr-1 mb-1">
                              <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                              </button>
                          <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="'. route('supplieruser.show', $user->id) . '"><i class="fa fa-eye"></i> View User</a> 
                          <a class="dropdown-item" href="' . route('supplieruser.edit', $user->id)  . '"><i class="fa fa-pencil"></i> Edit User</a> 
                          <form action="' . route('deletesupplieruser', $user->id) . '" method="POST">'
                                . csrf_field() . method_field("DELETE") .
                          '<button class="dropdown-item" type="submit" onClick="return confirm("Are you sure do you want to delete user?")"><i class="fa fa-trash"></i> Delete User</button> 
                           </div>
                        </div>';
                })
                ->addColumn('verified', function (VendorUser $user) {
                   return $user->verified ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-close" aria-hidden="true"></i>';
                })
                ->escapeColumns([])
                ->toJson();
    }


    public function create()
    {
         $vendors = Vendor::all();
         return view('admin.supplier.addsupplieruser', compact('vendors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'profile_picture' => 'mimes:jpeg,png,bmp,tiff |max:4096',
            'password' => 'required|min:6',
            'email' => 'required|unique:vendor_users'
             ]
        );

         $file = $request->file('profile_picture');
        ($file) ? $filename = $file->getClientOriginalName() : $filename = '';
        $emirates_id_file = $request->file('emirates_id');
        ($emirates_id_file) ? $filename_emirate = $emirates_id_file->getClientOriginalName() : $filename_emirate = '';
        // insert data
        $vendor = Vendor::find($request->vendor_id);
        $vendor->id = $vendor->id;
        $vendoruser = new VendorUser($request->all());
        $pw = Hash::make($request->password);
        $vendoruser->profile_picture = $filename;
        $vendoruser->emirates_id = $filename_emirate;
        $vendoruser->user_group_id = 4;
        $vendoruser->password = $pw;
        $vendoruser->vendor_id = $request->vendor_id; 
        $vendoruser->company_id = $vendor->company->id;
        $vendoruser->save();

         /* Document upload of user */
        $location = 'document/vendor/users/' . $clientuser->id;
        if($file):
        $file->move($location,$filename);
        endif;
        $emirates_id_file->move($location,$filename_emirate);
        /* Document upload of user end */
      
        Session::flash('success', 'Supplier User has been created successfully');
        return redirect()->route('supplierusers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vendoruser = VendorUser::findOrFail($id);
        $company_name =  $vendoruser->company->company_name;
        return view('admin.supplier.viewsupplieruser', compact('vendoruser', 'id', 'company_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplieruser = VendorUser::findOrFail($id);
        $vendors = Vendor::all();
        return view('admin.supplier.editsupplieruser',compact('supplieruser','id', 'vendors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $vendor = Vendor::find($request->vendor_id);
        $vendoruser = VendorUser::findOrFail($id);

         $this->validate($request, [
            'profile_picture' => 'mimes:jpeg,png,bmp,tiff |max:4096',
            'password' => ($request->password != '') ? 'min:6' : '',
            'email' => 'required|unique:vendor_users,email,' . $id
             ]
        );

        $file = $request->file('profile_picture');
        ($file) ? $filename = $file->getClientOriginalName() : $filename = $vendoruser->profile_picture;
        $emirates_id_file = $request->file('emirates_id');
        ($emirates_id_file) ? $filename_emirate = $emirates_id_file->getClientOriginalName() : $filename_emirate = $vendoruser->emirates_id;
        
        // update data here
        $input = $request->all();
        $vendoruser->fill($input)->save();
        $pw = Hash::make($request->password);
        $vendoruser->profile_picture = $filename;
        $vendoruser->emirates_id = $filename_emirate;
        $vendoruser->user_group_id = 4;
        $vendoruser->password = ($pw != $vendoruser->password && $pw != '') ? $pw : $vendoruser->password;
        $vendoruser->vendor_id = $request->vendor_id; 
        $vendoruser->company_id = $vendor->company->id;
        $vendoruser->save();

         /* Document upload of user */
        $location = 'document/vendor/users/' . $vendoruser->id;
        if($file):
        ($file != $vendoruser->profile_picture) ? $file->move($location,$filename) : '';
        endif;
        ($filename_emirate != $vendoruser->emirates_id) ? $emirates_id_file->move($location,$filename_emirate) : '';
        /* Document upload of user end */
      
        Session::flash('success', 'Supplier User has been updated successfully');
        return redirect()->route('supplierusers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendoruser = VendorUser::find($id);
        $vendoruser->delete();
        Session::flash('success', 'Supplier User has been deleted successfully');
        return redirect()->route('supplierusers');
    }
}
