<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Application;
use Yajra\DataTables\Facades\DataTables;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('admin.product.applications');
    }

    public function getapplications() {
        $model = Application::query();
        return DataTables::eloquent($model)
                    ->filter(function ($query) {
                            if (request()->has('name')) {
                                $query->where('name', 'like', "%" . request('name') . "%");
                            }
                            if (request()->has('id')) {
                                $query->where('id', 'like', "%" . request('id') . "%");
                            }
                            if (request()->has('status')) {
                                $query->where('status', 'like', "%" . request('status') . "%");
                            }
                        }, true)
                  ->addColumn('status', function(Application $app) {
                        return $app->status ? 'Active': 'In-Active';
                   })
                  ->addColumn('actions', function(Application $app) {
                    return '<div class="btn-group mr-1 mb-1">
                              <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                              </button>
                          <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="' . route('application.show', $app->id) . '"><i class="fa fa-eye"></i> View Application</a> <a class="dropdown-item" href="' . route('editapplication', $app->id) . '">
                              <i class="fa fa-pencil"></i> Edit Application</a> 
                              <form action="' . route('deleteapplication', $app->id) . '" method="POST">'
                                . csrf_field() . method_field("DELETE") . 
                              '<button class="dropdown-item" type="submit"><i class="fa fa-trash"></i> Delete Application</button> </div>
                        </div>';
                })
                ->escapeColumns([])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.addapplication');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $app = new Application($request->all());
        $app->save();
        return redirect()->route('applications')->with('success', 'Application has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $app = Application::findOrFail($id);
        return view('admin.product.showapplication', compact('app', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $app = Application::find($id);
        return view('admin.product.editapplication',compact('app','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $app = Application::findOrFail($id);
         $input = $request->all();
         $app->fill($input)->save();
        return redirect()->route('applications')->with('success', 'Application has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $app = Application::find($id);
        $app->delete();
        return redirect()->route('applications')->with('success', 'Application has been deleted Successfully');
    }
}
