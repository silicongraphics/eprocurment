<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.product.categories');
    }

    public function getcategories() {
        $categories = Category::with(['parentcategory']);
         return DataTables::eloquent($categories)
                ->filter(function ($query) {
                            if (request()->has('name')) {
                                $query->where('name', 'like', "%" . request('name') . "%");
                            }
                            if (request()->has('id')) {
                                $query->where('id', 'like', "%" . request('id') . "%");
                            }
                            if (request()->has('status')) {
                                $query->where('status', 'like', "%" . request('status') . "%");
                            }
                        }, true)
                ->addColumn('status', function (Category $category) {
                    return $category->status ? 'Active': 'In-Active';
                })
                ->addColumn('parentcategory', function (Category $category) {
                      return ($category->parent_id) ? $category->parentcategory->name : '';
                })
                ->addColumn('actions', function (Category $category) {
                    return '<div class="btn-group mr-1 mb-1">
                              <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                              </button>
                          <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="'. route('category.show', $category->id) .'"><i class="fa fa-eye"></i> View Category</a> <a class="dropdown-item" href="' . route('editcategory', $category->id) . '"><i class="fa fa-pencil"></i> Edit Category</a>
                              <form action="' . route('deletecategory', $category->id) . '" method="POST">'
                                . csrf_field() . method_field("DELETE") .  
                             '<button class="dropdown-item" type="submit"><i class="fa fa-trash"></i> Delete Category</button> </div>
                        </div>';
                })
                ->escapeColumns([])
                ->toJson();
    }

    public function getCategoryList(){
        return Category::with('subs')->whereNull('parent_id')->select('id','name as title')->get();
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $category = new Category();
        // $data = $category->getparents();
        $data = Category::all();
        return view('admin.product.addcategory', ['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category($request->all());
        $category->slug =  Category::slug($request->name);
        $category->save();
        $category->custom_id = "CA".$category->id;
        $category->save();
        return redirect()->route('categories')->with('success', 'Category has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cat = Category::findOrFail($id);
        return view('admin.product.showcategory', compact('cat', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = Category::find($id);
        $category = new Category();
        $data = Category::all();
       // $data = $category->getparents();
        return view('admin.product.editcategory',compact('cat','id', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $input = $request->all();
        $category->fill($input)->save();
        return redirect()->route('categories')->with('success', 'Category has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = Category::find($id);
        $cat->delete();
        return redirect()->route('categories')->with('success', 'Category has been deleted Successfully');
    }
}
