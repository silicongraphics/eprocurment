<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:client')->except('logout');
        $this->middleware('guest:vendor')->except('logout');
    }

    public function showClientLogin()
    {
        return view('auth.login', ['url' => 'client']);
    }
    
    public function showVendorLogin()
    {
        return view('auth.login', ['url' => 'vendor']);
    }

    public function clientLogin(Request $request){
        if(!\App\Models\ClientUser::where('email',$request->email)->first())
            return back()->withErrors(['email' => 'The email address provided is not registered'])->withInput($request->only('email', 'remember'));  

            if (Auth::guard('client')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            return redirect()->intended('client/create-rfq');
        }
        return back()->withErrors(['email' => 'Incorrect Email/Password Combination'])->withInput($request->only('email', 'remember'));  
    }

    public function vendorLogin(Request $request){
        if (Auth::guard('vendor')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            return redirect()->intended('vendor/my-rfqs');
        }
        return back()->withInput($request->only('email', 'remember'));  
    }    

}
