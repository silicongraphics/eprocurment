<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Company;
use App\Models\Client;
use App\Models\Manufacturer;
use App\Models\Vendor;
use App\Models\PaymentMethod;
use App\Models\DeliveryMethod;
use App\Models\DeliveryTerm;
use App\Models\Country;
use App\Models\City;
use App\Models\CreditTerm;
use App\Models\ClientUser;
use App\Models\VendorUser;
use App\Models\DocType;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $brands = Manufacturer::all();
        $delivery_methods = DeliveryMethod::all();
        $warranty_terms = DB::table('warranty_terms')->get();
        $service_terms = DB::table('service_terms')->get();
        $payment_methods = PaymentMethod::all();
        $delivery_terms = DeliveryTerm::all();
        $suppliers = Vendor::all();
        $countries = Country::all();
        $cities = City::all();
        $payment_terms = CreditTerm::all();
        $doc_type = DocType::all();
        return view('auth.register', ['url' => 'client'], compact('brands', 'warranty_terms',
        'service_terms', 'delivery_methods', 'payment_methods', 'delivery_terms', 'suppliers', 'countries', 'cities', 'payment_terms', 'doc_type'));
    }

      public function showRegistrationVendorForm()
    {
        $brands = Manufacturer::all();
        $delivery_methods = DeliveryMethod::all();
        $payment_methods = PaymentMethod::all();
        $delivery_terms = DeliveryTerm::all();
        $countries = Country::all();
        $cities = City::all();
        $payment_terms = CreditTerm::all();
        $doc_type = DocType::all();
        return view('auth.vendor_register', ['url' => 'vendor'], compact('brands', 'delivery_methods', 'payment_methods', 'delivery_terms', 'countries', 'cities', 'payment_terms', 'doc_type'));
    }

    public function vendorRegister(Request $request)
    {
         $this->validate($request, [
            'profile_picture' => 'mimes:jpeg,png,bmp,tiff |max:4096',
            'trade_liscense' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'vat_certificate' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'emirates_id_user' => 'mimes:jpeg,png,bmp,tiff, doc,pdf,docx,zip |max:2048',
            'emirates_id' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'password' => 'required|min:6',
            'email' => 'required|unique:vendor_users'
             ]
        );

        $trade_liscense = $request->file('trade_liscense');
        $vat_certificate = $request->file('vat_certificate');
        $emirates_id = $request->file('emirates_id');
        $profile_picture = $request->file('profile_picture');
        $emirates_id_user = $request->file('emirates_id_user');
        //creating comapny
        $company = new Company($request->all());
        $company->company_name = $request->name;
        $company->type = 0;
        $company->trade_license = ($trade_liscense) ? $trade_license_filename = $trade_liscense->getClientOriginalName() : $trade_license_filename = '';
        $company->vat_certificate = ($vat_certificate) ? $vat_certificate_filename = $vat_certificate->getClientOriginalName() : $vat_certificate_filename = '';
        $company->emirates_id = ($emirates_id) ? $emirates_id_filename = $emirates_id->getClientOriginalName() : $emirates_id_filename = '';
        $company->save();
        // creating supplier here
        $vendor = new Vendor($request->all());
        $vendor->company_id = $company->id;
        $vendor->save();
        $vendor->custom_id = "S" . $vendor->id;
        $vendor->save();
        // vendor created
        
        // Vendor Products
        $vendor->products()->sync($request->products_supplied);
        
        /* Document upload into a directory */
         $location = 'document/vendor/' . $vendor->id;
        // Upload file
        $trade_liscense->move($location,$trade_license_filename);
        $vat_certificate->move($location,$vat_certificate_filename);
        $emirates_id->move($location,$emirates_id_filename);
        /* Document into directory end */
        
        /* Vendor user */ 
        $vendoruser = new VendorUser($request->all());
        $pw = Hash::make($request->password);
        $vendoruser->vendor_id = $vendor->id;
        $vendoruser->profile_picture = ($profile_picture) ? $profile_picture_name = $profile_picture->getClientOriginalName() : $profile_picture = '';
        $vendoruser->emirates_id = ($emirates_id_user) ? $emirates_id_user_filename = $emirates_id_user->getClientOriginalName() : $emirates_id_user_filename = '';
        $vendoruser->user_group_id = 4;
        $vendoruser->password = $pw;
        $vendoruser->verified = $request->verified_user; 
        $vendoruser->active = $request->status_user;
        $vendoruser->company_id = $vendor->company->id;
        $vendoruser->save();
           /* Vendor user */ 

          /* Document upload of user */
        $location = 'document/vendor/users/' . $vendoruser->id;
        if($profile_picture):
        $profile_picture->move($location,$profile_picture_name);
        endif;
        if($emirates_id_user):
        $emirates_id_user->move($location,$emirates_id_user_filename);
        endif;
        /* Document upload of user end */
        return redirect()->route('logintype');
    }

    public function clientRegister(Request $request)
    {

         $this->validate($request, [
            'profile_picture' => 'mimes:jpeg,png,bmp,tiff |max:4096',
            'trade_liscense' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'vat_certificate' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'emirates_id' => 'mimes:jpeg,png,bmp,tiff,doc,pdf,docx,zip |max:2048',
            'password' => 'required|min:6',
            'email' => 'required|unique:client_users'
             ]
        );

        $trade_liscense = $request->file('trade_liscense');
        $vat_certificate = $request->file('vat_certificate');
        $emirates_id = $request->file('emirates_id');
        $profile_picture = $request->file('profile_picture');
        $emirates_id_user = $request->file('emirates_id_user');

        $company = new Company($request->all());
        $company->company_name = $request->name;
        $company->type = 1;
        $company->trade_license = ($trade_liscense) ? $trade_license_filename = $trade_liscense->getClientOriginalName() : $trade_license_filename = '';
        $company->vat_certificate = ($vat_certificate) ? $vat_certificate_filename = $vat_certificate->getClientOriginalName() : $vat_certificate_filename = '';
        $company->emirates_id = ($emirates_id) ? $emirates_id_filename = $emirates_id->getClientOriginalName() : $emirates_id_filename = '';
        $company->save();

        $client = new Client($request->all());
        $client->company_id = $company->id;
        $client->save();
        $client->custom_id = "C" . $client->id;
        $client->save();

        /* Document upload into a directory */
        $location = public_path('document/client/' . $client->id);
        // Upload file
        $trade_liscense->move($location,$trade_license_filename);
        $vat_certificate->move($location,$vat_certificate_filename);
        $emirates_id->move($location,$emirates_id_filename);
        /* Document into directory end */

        $client->manufactures()->sync($request->manufacturers);
        $client->suppliers()->sync($request->vendors);

        // client user is created
        $clientuser = new ClientUser($request->all());
        $pw = Hash::make($request->password);
        $clientuser->client_id = $client->id;
        $clientuser->profile_picture = ($profile_picture) ? $profile_picture_name = $profile_picture->getClientOriginalName()
            : $profile_picture = '';
        $clientuser->emirates_id = ($emirates_id_user) ? $emirates_id_user_filename = $emirates_id_user->getClientOriginalName() : $emirates_id_user_filename = '';
        $clientuser->user_group_id = 4;
        $clientuser->password = $pw;
        $clientuser->active = $request->status_user;
        $clientuser->verified = $request->verified_user; 
        $clientuser->company_id = $client->company->id;
        $clientuser->service_term_id = $request->service_term;
        $clientuser->warranty_term_id = $request->warranty_terms;
        $clientuser->save();
        
        // client user is created
        /* Client Document Relationship */
        $documents = array(2 => ['name' => $trade_license_filename],
            4 => ['name' => $vat_certificate_filename],
            3 => ['name' => $emirates_id_filename],
            2 => ['name' => $emirates_id_user, 'client_user_id' => $clientuser->id]);
        /* Client Document Relationship */

        /* Document upload of user */
        $location = public_path('document/client/users/' . $clientuser->id);
        if($profile_picture):
        $profile_picture->move($location,$profile_picture_name);
        endif;
        if($emirates_id_user):
        $emirates_id_user->move($location,$emirates_id_user_filename);
        endif;
        /* Document upload of user end */
        return redirect()->route('logintype');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
