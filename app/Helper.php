<?php

namespace App;

use DB;
use Auth;
use Session;
use App\Models\Product;
use App\Models\ReqForQuote;
use App\Models\Manufacturer;
use App\Models\RfqSelection;
use Illuminate\Http\Request;
use App\Models\PurchaseOrder;
use App\Models\RfqCartProduct;
use App\Models\RfqCartProductTerm;

class Helper
{

    public static function getGeneralTerms(){
        $credit_terms = DB::table('credit_terms')->get();
        $delievery_terms = DB::table('delivery_terms')->get();
        $warranty_terms = DB::table('warranty_terms')->get();
        $service_terms = DB::table('service_terms')->get();

        return [
            'credit_terms' =>$credit_terms,  
            'delievery_terms' =>$delievery_terms, 
            'warranty_terms' =>$warranty_terms, 
            'service_terms' =>$service_terms  
        ];
    }

    public static function getVal($array, $id){
        foreach($array as $arr){
            if($arr->id == $id){
                return $arr;
            }
                
        }        
    }

    public static function getCategoryCount($products){
        //is used in listing.php
        $array = [];
        foreach($products as $p)
            array_push($array, $p->product_id);
        
        // $count = Product::whereIn('id', $array)->select(DB::raw('SUM(JSON_LENGTH(`category_ids`)) as count'))->first();
        $count = Product::whereIn('id', $array)->select(DB::raw("(CHAR_LENGTH(category_ids) - CHAR_LENGTH(REPLACE(category_ids, ',', '')) + 1) as count"))->first();
        return $count->count;        
    }

    public static function countSupplier($array, $retarr=false){
        //is used in listing.php
        $supp = [];
        foreach($array as $arr){
            foreach($arr->terms as $t){
                if(!in_array($t->vendor_id, $supp))
                    array_push($supp, $t->vendor_id);
            }
        }
        if($retarr)
            return $supp;
        return count($supp);
    }

    public static function getProductCount($rfq_id){
        $r = DB::table('rfq_cart_products')->where('reqForQuote_id', $rfq_id)->groupBy('product_id')->get();
        return count($r);
    }

    public static function getVendorDetailsForRFQ($array,$product_id){
        foreach($array as $key => $value){
            if($value->product_id == $product_id)
                return $value;
        }
    }

    public static function ifSupplierInArray($array, $vendor_id){
        //is used in listing.php
        $count = 0;
        $data = '';
        foreach($array as $arr){
            if($arr->vendor_id == $vendor_id){
                $count++;
                $data = $arr;
            }
        }
        if($count)
            return $data;
        return '';
    }
    
    public static function getRFQclientStatus($products){
        $totalrows = 0;
        $sent = 0;
        foreach($products as $pro){
            foreach($pro->terms as $term){
                if($term->sent_status)
                    $sent++;
                $totalrows++;
            }
        }
        if($totalrows == $sent)
            $status =  'Sent';
        else if($sent != 0)
            $status =  'Partially Sent';
        else
            $status =  'Pending';
        $chk = PurchaseOrder::where('rfq_id',$pro->reqForQuote_id)->first();
        if($chk)
            $status =  'Closed';

        return $status;


    }

    public static function getRFQVendorStatus($id){
        $vendor_id = Auth::guard('vendor')->user()->id;
        $rfq = ReqForQuote::with(['vendorRfqResponse' => function($q) use($vendor_id) {
                    $q->where('vendor_id',$vendor_id);
               }])->where('id',$id)->first();
        if(count($rfq->vendorRfqResponse)){
            $status =  'Sent';
        }
        else
            $status =  'Pending';    

        $chk = PurchaseOrder::where('rfq_id',$id)->first();
        if($chk)
            $status =  'Closed';

        return $status;
    }

    public static function checkIfChangeOfRequest($id){
        $rec = DB::table('estimates_cor')->where('vrfq_id',$id)->orderBy('id','desc')->get();        
        return $rec;
    }

    public static function getSupplierRespForRFQ($rfq_id, $vendor_id, $product_id){
        $data = DB::table('vendor_received_rfqs as r')
                    ->join('warranty_terms as w','r.warranty_terms','=','w.id')
                    ->join('service_terms as s','r.service_terms','=','s.id')
                    ->join('credit_terms as c','r.credit_term_id','=','c.id')
                    ->join('delivery_terms as d','r.delivery_terms','=','d.id')
                    ->where('r.reqForQuote_id',$rfq_id)
                    ->where('r.vendor_id',$vendor_id)
                    ->where('r.product_id',$product_id)
                    ->orderBy('id','desc')
                    ->select('r.*','w.term as wterm','c.name as cterm','d.term as dterm','s.term as sterm')
                    ->get();
        return $data;
    }

    public static function manufacturerList($name, $selected_id=null){
       $manufacturers = Manufacturer::where('status',1)->get();
       echo '<select name="'.$name.'" id="'.$name.'"  class="form-control">';
           echo '<option>Select</option>';
           foreach($manufacturers as $d){
                $sel = ($d->id == $selected_id) ? "selected" : "";
                echo '<option '.$sel.'value="'.$d->id.'">'.$d->name.'</option>';
           }
       echo '</select>';
    }

    public static function lastOrder($pid){
        $sql = "SELECT comp.company_name, pod.created_at, pod.supplier, pod.price, pod.quantity  FROM `purchase_orders` as po 
                JOIN purchase_order_detail as pod ON po.id = pod.po_id AND pod.product_id = '$pid'
                JOIN vendor_users as vu ON pod.supplier = vu.id 
                JOIN vendors as v ON v.id = vu.vendor_id 
                JOIN companies as comp ON v.company_id = comp.id                 
                WHERE po.client_user_id = ".Auth::guard('client')->user()->id."
                ORDER BY po.id desc LIMIT 1 ";

        $chk = DB::select($sql);        
        if(count($chk))
            return $chk[0];
        else
            return '';
    }

}
