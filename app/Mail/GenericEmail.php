<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GenericEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;

    public function __construct($type, $data, $isAdmin)
    {
        $this->type = $type;
        $this->data = $data;
        $this->isAdmin = $isAdmin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->type == 'createRfq'){
            return $this
            ->subject('Request For Quotation')
            ->view('email.createRfq')
            ->with([
               'data' => $this->data,
               'isAdmin' => $this->isAdmin,
            ]);

        }
        else if($this->type == 'generateQuote'){
            return $this
            ->subject('Quote Generation For '.$this->data['rfqno'])
            ->view('email.generateQuote')
            ->with([
               'data' => $this->data,
               'isAdmin' => $this->isAdmin,
            ]);            
        }
        else if($this->type == 'changeOfRequest'){
            return $this
            ->subject('Change of Quote Request For '.$this->data['rfqno'])
            ->view('email.changeOfRequest')
            ->with([
               'data' => $this->data,
               'isAdmin' => $this->isAdmin,
            ]);            
        }
        else if($this->type == 'supplierAnswerForCOR'){
            return $this
            ->subject('Supplier Answer for Change of Quote Request of '.$this->data['rfqno'])
            ->view('email.supplierAnswerForCOR')
            ->with([
               'data' => $this->data,
               'isAdmin' => $this->isAdmin,
            ]);            
        }
        else if($this->type == 'purchaseOrder'){
            return $this
            ->subject('New Purchase Order for RFQ - '.$this->data['rfqno'])
            ->view('email.purchaseOrder')
            ->with([
               'data' => $this->data,
               'isAdmin' => $this->isAdmin,
            ]);            
        }


    }
}
