<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ManufacturerCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct($manufacturer)
    {
        // dd($manufacturer, 'listener');
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        dd($event);
    }
}
