<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoginTokensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('login_tokens', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->unsigned();
			$table->char('token', 32);
			$table->string('duration', 32);
			$table->boolean('used')->default(0);
			$table->dateTime('created');
			$table->dateTime('expires');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('login_tokens');
	}

}
