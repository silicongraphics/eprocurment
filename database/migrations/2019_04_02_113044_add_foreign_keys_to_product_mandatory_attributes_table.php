<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductMandatoryAttributesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_mandatory_attributes', function(Blueprint $table)
		{
			$table->foreign('attribute_id', 'attribute_fk')->references('id')->on('attributes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('category_id', 'category_fk')->references('id')->on('categories')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_mandatory_attributes', function(Blueprint $table)
		{
			$table->dropForeign('attribute_fk');
			$table->dropForeign('category_fk');
		});
	}

}
