<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries', function(Blueprint $table)
		{
			$table->smallInteger('id', true);
			$table->string('name', 80)->unique('countries_uq_name');
			$table->string('full_name', 80)->nullable();
			$table->char('iso2', 2)->nullable()->comment('ISO alpha-2 code');
			$table->char('iso3', 3)->nullable()->comment('ISO alpha-3 code');
			$table->smallInteger('continents_id')->nullable()->index('countries_continentsid_fk');
			$table->string('capital_city', 30)->nullable()->comment('Name of the Capital city of the country.');
			$table->smallInteger('currencies_id')->nullable()->index('countries_currenciesid_fk');
			$table->string('calling_code', 8)->nullable()->comment('International dialing code');
			$table->string('numeric_code', 6)->nullable()->comment('United Nations numerical code M49 for countries');
			$table->string('postal_code', 45)->nullable()->comment('Pattern for postal code of the country.');
			$table->string('un_member', 12)->nullable();
			$table->string('cctld', 5)->nullable()->comment('country code top-level domain');
			$table->string('flag', 6)->nullable()->comment('Flag image name with extension');
			$table->text('description', 65535)->nullable();
			$table->boolean('is_active')->default(1)->comment('Active = 1, Inactive = 0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('countries');
	}

}
