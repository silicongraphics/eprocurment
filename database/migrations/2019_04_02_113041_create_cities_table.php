<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities', function(Blueprint $table)
		{
			$table->smallInteger('id', true);
			$table->string('name', 80);
			$table->char('code', 3)->nullable()->comment('IATA 3-Letter Codes');
			$table->boolean('is_capital')->default(0);
			$table->smallInteger('countries_id')->index('cities_countriesid_fk');
			$table->text('description', 65535)->nullable();
			$table->boolean('is_active')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities');
	}

}
