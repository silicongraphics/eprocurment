<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_users', function(Blueprint $table)
		{
			$table->foreign('country_id', 'client_country_fk')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('user_group_id', 'client_ugid_fk')->references('id')->on('user_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('billing_city_id', 'cu_bill_city_fk')->references('id')->on('cities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('billing_country_id', 'cu_bill_country_fk')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('client_id', 'cu_cid_fk')->references('id')->on('clients')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('shipping_city_id', 'cu_ship_city_fk')->references('id')->on('cities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('shipping_country_id', 'cu_ship_country_fk')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('client_users', function(Blueprint $table)
		{
			$table->dropForeign('client_country_fk');
			$table->dropForeign('client_ugid_fk');
			$table->dropForeign('cu_bill_city_fk');
			$table->dropForeign('cu_bill_country_fk');
			$table->dropForeign('cu_cid_fk');
			$table->dropForeign('cu_ship_city_fk');
			$table->dropForeign('cu_ship_country_fk');
		});
	}

}
