<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSavedRfqsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('saved_rfqs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('tempProdSelection', 65535);
			$table->text('tempRfqSelection', 65535);
			$table->string('rfq_name', 100);
			$table->string('client_id', 20);
			$table->string('client_user_id', 20);
			$table->timestamps();
			$table->boolean('deleted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('saved_rfqs');
	}

}
