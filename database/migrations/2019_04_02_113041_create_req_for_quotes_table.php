<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReqForQuotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('req_for_quotes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_id')->unsigned()->index('rfq_cid_fk');
			$table->integer('client_user_id')->unsigned()->index('rfq_cuid_fk');
			$table->string('name', 64);
			$table->string('req_number', 24)->comment('Requisition Number');
			$table->string('req_code', 14)->unique('rfq_req_code')->comment('Unique code generated to identify RFQ.');
			$table->text('site_area', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->boolean('status')->nullable()->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('req_for_quotes');
	}

}
