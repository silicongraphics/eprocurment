<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client_documents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_id')->unsigned()->index('cdoc_cid_fk');
			$table->integer('client_user_id')->unsigned()->nullable()->index('cdoc_cuid_fk');
			$table->boolean('doc_type_id')->index('cdoc_dtid_fk');
			$table->smallInteger('country_id')->nullable()->index('cdoc_countryid_fk');
			$table->string('name', 64);
			$table->text('description', 65535)->nullable();
			$table->string('path', 64)->nullable();
			$table->dateTime('valid_from')->nullable();
			$table->dateTime('valid_till')->nullable();
			$table->text('contents', 65535)->nullable();
			$table->timestamps();
			$table->boolean('status')->nullable()->default(0)->comment('Pending = 0, Verified = 1, Rejected = 2, Archived = 3');
			$table->text('admin_comments', 65535)->nullable()->comment('Comments / Instructions by Admin.');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('client_documents');
	}

}
