<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('custom_id', 7)->nullable();
			$table->string('name', 128);
			$table->string('image', 100)->nullable();
			$table->string('part_number', 64)->unique('prod_uq_partnumber');
			$table->integer('manufacturer_id')->unsigned()->index('prod_mid_fk');
			$table->text('category_ids', 65535)->comment('JSON array of CATEGORY_ID linked to the Product.');
			$table->text('attributes', 65535)->nullable();
			$table->text('description', 65535)->nullable();
			$table->boolean('status')->nullable()->default(0)->comment('Active = 1, Inactive = 0');
			$table->timestamps();
			$table->index(['name','part_number','attributes'], 'prod_ft_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
