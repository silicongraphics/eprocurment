<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('languages', function(Blueprint $table)
		{
			$table->smallInteger('id', true);
			$table->string('name', 24)->unique('languages_uq_name');
			$table->char('iso2', 2)->nullable()->comment('ISO 639 alpha-2 code');
			$table->char('iso3', 3)->nullable()->comment('ISO 639 alpha-3 code');
			$table->string('native_name', 144)->nullable();
			$table->text('description', 65535)->nullable();
			$table->boolean('is_active')->default(0)->comment('Active = 1, Inactive = 0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('languages');
	}

}
