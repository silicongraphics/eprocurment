<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('countries', function(Blueprint $table)
		{
			$table->foreign('continents_id', 'countries_continentsid_fk')->references('id')->on('continents')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('currencies_id', 'countries_currenciesid_fk')->references('id')->on('currencies')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('countries', function(Blueprint $table)
		{
			$table->dropForeign('countries_continentsid_fk');
			$table->dropForeign('countries_currenciesid_fk');
		});
	}

}
