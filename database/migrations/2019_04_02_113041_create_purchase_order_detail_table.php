<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePurchaseOrderDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('purchase_order_detail', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('po_id', 191);
			$table->string('supplier', 191);
			$table->string('product_id', 191);
			$table->timestamps();
			$table->string('quantity', 191);
			$table->string('price', 191);
			$table->string('warranty_term', 191);
			$table->string('service_term', 191);
			$table->string('payment_term', 191);
			$table->string('delievery_term', 191);
			$table->boolean('status')->default(0);
			$table->boolean('s_status')->default(0)->comment('supplier approval');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('purchase_order_detail');
	}

}
