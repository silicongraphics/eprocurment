<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSortingPreferencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sorting_preferences', function(Blueprint $table)
		{
			$table->foreign('user_group_id', 'user_role')->references('id')->on('user_groups')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sorting_preferences', function(Blueprint $table)
		{
			$table->dropForeign('user_role');
		});
	}

}
