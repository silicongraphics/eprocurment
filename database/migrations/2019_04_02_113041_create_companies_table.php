<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('company_name', 191)->nullable();
			$table->string('website', 191)->nullable();
			$table->string('delivery_method_id', 191)->nullable();
			$table->string('payment_method_id', 191)->nullable();
			$table->string('credit_term_id', 191)->nullable();
			$table->text('billing_address', 65535)->nullable();
			$table->text('billing_street', 65535)->nullable();
			$table->string('billing_pobox', 191)->nullable();
			$table->string('billing_country_id', 191)->nullable();
			$table->string('billing_city_id', 191)->nullable();
			$table->text('shipping_address', 65535)->nullable();
			$table->text('shipping_street', 65535)->nullable();
			$table->string('shipping_pobox', 191)->nullable();
			$table->string('shipping_country_id', 191)->nullable();
			$table->string('shipping_city_id', 191)->nullable();
			$table->text('trade_license', 65535)->nullable();
			$table->text('vat_certificate', 65535)->nullable();
			$table->string('approval_name', 191)->nullable();
			$table->string('emirates_id', 191)->nullable();
			$table->boolean('type')->default(0);
			$table->string('fax', 191)->nullable();
			$table->string('phone', 191)->nullable();
			$table->string('email', 191)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
