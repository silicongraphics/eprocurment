<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('custom_id', 5)->nullable();
			$table->integer('parent_id')->unsigned()->index('cat_parent_fk');
			$table->string('name', 64);
			$table->text('description', 65535)->nullable();
			$table->boolean('status')->nullable()->default(0)->comment('Active = 1, Inactive = 0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
