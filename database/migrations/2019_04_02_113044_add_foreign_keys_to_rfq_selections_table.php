<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRfqSelectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rfq_selections', function(Blueprint $table)
		{
			$table->foreign('req_for_quote_id', 'rfq_selection_rfqid_fk')->references('id')->on('req_for_quotes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rfq_selections', function(Blueprint $table)
		{
			$table->dropForeign('rfq_selection_rfqid_fk');
		});
	}

}
