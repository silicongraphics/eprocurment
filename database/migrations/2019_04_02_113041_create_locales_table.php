<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locales', function(Blueprint $table)
		{
			$table->smallInteger('id', true);
			$table->string('name', 16)->unique('locales_uq_name');
			$table->string('full_locale', 64);
			$table->smallInteger('languages_id')->index('locales_languagesid_fk');
			$table->smallInteger('countries_id')->index('locales_countriesid_fk');
			$table->text('description', 65535)->nullable();
			$table->boolean('is_active')->default(0)->comment('Active = 1, Inactive = 0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locales');
	}

}
