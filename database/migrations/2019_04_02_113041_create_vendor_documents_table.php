<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVendorDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vendor_documents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('vendor_id')->unsigned()->index('vdoc_vid_fk');
			$table->integer('vendor_user_id')->unsigned()->nullable()->index('vdoc_vuid_fk');
			$table->boolean('doc_type_id')->index('vdoc_dtid_fk');
			$table->smallInteger('country_id')->nullable()->index('vdoc_countryid_fk');
			$table->string('name', 64);
			$table->text('description', 65535)->nullable();
			$table->string('path', 64)->nullable();
			$table->dateTime('valid_from')->nullable();
			$table->dateTime('valid_till')->nullable();
			$table->text('contents', 65535)->nullable();
			$table->timestamps();
			$table->boolean('status')->nullable()->default(0)->comment('Pending = 0, Verified = 1, Rejected = 2, Archived = 3');
			$table->text('admin_comments', 65535)->nullable()->comment('Comments / Instructions by Admin.');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vendor_documents');
	}

}
