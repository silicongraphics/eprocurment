<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVendorProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vendor_products', function(Blueprint $table)
		{
			$table->foreign('product_id', 'vp_pid_fk')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('vendor_id', 'vp_vid_fk')->references('id')->on('vendors')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vendor_products', function(Blueprint $table)
		{
			$table->dropForeign('vp_pid_fk');
			$table->dropForeign('vp_vid_fk');
		});
	}

}
