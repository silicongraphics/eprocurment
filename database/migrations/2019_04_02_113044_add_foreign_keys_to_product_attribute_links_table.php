<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductAttributeLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_attribute_links', function(Blueprint $table)
		{
			$table->foreign('attribute_option_value', 'pattl_ato_fk')->references('id')->on('attribute_options')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('attribute_id', 'pattl_attid_fk')->references('id')->on('attributes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('product_id', 'pattl_pid_fk')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_attribute_links', function(Blueprint $table)
		{
			$table->dropForeign('pattl_ato_fk');
			$table->dropForeign('pattl_attid_fk');
			$table->dropForeign('pattl_pid_fk');
		});
	}

}
