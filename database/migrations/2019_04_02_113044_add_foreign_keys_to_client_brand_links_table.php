<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientBrandLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_brand_links', function(Blueprint $table)
		{
			$table->foreign('client_id', 'cm_cid_fk')->references('id')->on('clients')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('manufacturer_id', 'cm_mid_fk')->references('id')->on('manufacturers')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('client_brand_links', function(Blueprint $table)
		{
			$table->dropForeign('cm_cid_fk');
			$table->dropForeign('cm_mid_fk');
		});
	}

}
