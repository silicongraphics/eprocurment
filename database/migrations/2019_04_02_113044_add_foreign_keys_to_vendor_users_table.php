<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVendorUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vendor_users', function(Blueprint $table)
		{
			$table->foreign('user_group_id', 'vendor_ugid_fk')->references('id')->on('user_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('billing_city_id', 'vu_bill_city_fk')->references('id')->on('cities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('billing_country_id', 'vu_bill_country_fk')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('shipping_city_id', 'vu_ship_city_fk')->references('id')->on('cities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('shipping_country_id', 'vu_ship_country_fk')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('vendor_id', 'vu_vid_fk')->references('id')->on('vendors')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vendor_users', function(Blueprint $table)
		{
			$table->dropForeign('vendor_ugid_fk');
			$table->dropForeign('vu_bill_city_fk');
			$table->dropForeign('vu_bill_country_fk');
			$table->dropForeign('vu_ship_city_fk');
			$table->dropForeign('vu_ship_country_fk');
			$table->dropForeign('vu_vid_fk');
		});
	}

}
