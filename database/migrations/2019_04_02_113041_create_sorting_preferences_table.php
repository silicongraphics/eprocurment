<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSortingPreferencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sorting_preferences', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('user_group_id')->unsigned()->index('user_role');
			$table->string('table_id', 100)->nullable()->comment('Table Reference');
			$table->string('column_name', 100);
			$table->string('display_name', 100);
			$table->integer('sort_id')->comment('Sort number of column');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sorting_preferences');
	}

}
