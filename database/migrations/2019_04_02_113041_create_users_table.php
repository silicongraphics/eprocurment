<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_group_id')->unsigned()->index('users_ugid_fk');
			$table->string('first_name', 100);
			$table->string('last_name', 100)->nullable();
			$table->string('username', 100)->unique();
			$table->string('email', 191)->unique();
			$table->string('password', 191);
			$table->string('remember_token', 100)->nullable();
			$table->string('description', 191)->nullable();
			$table->boolean('active')->default(0)->comment('Active = 1, Inactive = 0, Ban = -1');
			$table->dateTime('created')->nullable();
			$table->integer('created_by')->unsigned();
			$table->dateTime('modified')->nullable();
			$table->integer('modified_by')->unsigned();
			$table->text('admin_comments', 65535)->nullable()->comment('Comments / Instructions by Admin.');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
