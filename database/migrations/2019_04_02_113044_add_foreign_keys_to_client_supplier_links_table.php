<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientSupplierLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_supplier_links', function(Blueprint $table)
		{
			$table->foreign('client_id', 'cv_cid_fk')->references('id')->on('clients')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('vendor_id', 'cv_vid_fk')->references('id')->on('vendors')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('client_supplier_links', function(Blueprint $table)
		{
			$table->dropForeign('cv_cid_fk');
			$table->dropForeign('cv_vid_fk');
		});
	}

}
