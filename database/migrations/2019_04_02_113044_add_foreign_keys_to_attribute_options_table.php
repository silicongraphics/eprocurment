<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAttributeOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('attribute_options', function(Blueprint $table)
		{
			$table->foreign('attribute_id', 'ao_attr_fk')->references('id')->on('attributes')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('attribute_options', function(Blueprint $table)
		{
			$table->dropForeign('ao_attr_fk');
		});
	}

}
