<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductAttributeLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_attribute_links', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id')->unsigned()->index('pattl_pid_fk');
			$table->integer('attribute_id')->unsigned()->index('pattl_attid_fk');
			$table->integer('attribute_option_value')->unsigned()->index('pattl_aov_fk')->comment('Attribute Option Id / Value selected for a product from available values in attribute_options table.');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_attribute_links');
	}

}
