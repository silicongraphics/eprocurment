<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstimatesCorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('estimates_cor', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('vrfq_id', 121);
			$table->integer('quantity');
			$table->text('reason', 65535)->nullable();
			$table->timestamps();
			$table->boolean('closed')->default(0);
			$table->string('updated_qty', 191)->nullable();
			$table->string('updated_price', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estimates_cor');
	}

}
