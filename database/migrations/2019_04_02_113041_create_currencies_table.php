<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCurrenciesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('currencies', function(Blueprint $table)
		{
			$table->smallInteger('id', true);
			$table->string('name', 32);
			$table->string('code', 16)->comment('ISO 4217 currency code');
			$table->string('symbol', 16);
			$table->float('foreign_exchange_rate', 10, 0)->nullable();
			$table->smallInteger('countries_id')->unique('currencies_uq_countriesid');
			$table->text('description', 65535)->nullable();
			$table->boolean('is_active')->default(0)->comment('Active = 1, Inactive = 0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('currencies');
	}

}
