<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVendorReceivedRfqsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vendor_received_rfqs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('reqForQuote_id', 10)->index('reqForQuote_id');
			$table->integer('vendor_id')->unsigned()->nullable();
			$table->string('warranty_terms', 12);
			$table->string('service_terms', 12);
			$table->integer('credit_term_id')->unsigned();
			$table->string('delivery_terms', 12);
			$table->timestamps();
			$table->text('general_remarks', 65535)->nullable();
			$table->string('product_id', 11)->nullable();
			$table->string('unit_price', 11);
			$table->string('quantity', 11);
			$table->boolean('disabled')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vendor_received_rfqs');
	}

}
