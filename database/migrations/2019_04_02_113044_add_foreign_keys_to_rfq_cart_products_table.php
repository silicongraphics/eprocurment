<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRfqCartProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rfq_cart_products', function(Blueprint $table)
		{
			$table->foreign('product_id', 'rfqcp_pid_fk')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('rfq_selection_id', 'rfqcp_selectionid_fk')->references('id')->on('rfq_selections')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rfq_cart_products', function(Blueprint $table)
		{
			$table->dropForeign('rfqcp_pid_fk');
			$table->dropForeign('rfqcp_selectionid_fk');
		});
	}

}
