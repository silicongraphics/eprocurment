<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePurchaseOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('purchase_orders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('rfq_id', 191);
			$table->string('po_number', 191);
			$table->boolean('status')->default(0);
			$table->integer('client_id');
			$table->integer('client_user_id');
			$table->timestamps();
			$table->boolean('savedPo')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('purchase_orders');
	}

}
