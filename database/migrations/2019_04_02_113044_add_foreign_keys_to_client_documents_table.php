<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToClientDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('client_documents', function(Blueprint $table)
		{
			$table->foreign('client_id', 'cdoc_cid_fk')->references('id')->on('clients')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('country_id', 'cdoc_countryid_fk')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('client_user_id', 'cdoc_cuid_fk')->references('id')->on('client_users')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('doc_type_id', 'cdoc_dtid_fk')->references('id')->on('doc_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('client_documents', function(Blueprint $table)
		{
			$table->dropForeign('cdoc_cid_fk');
			$table->dropForeign('cdoc_countryid_fk');
			$table->dropForeign('cdoc_cuid_fk');
			$table->dropForeign('cdoc_dtid_fk');
		});
	}

}
