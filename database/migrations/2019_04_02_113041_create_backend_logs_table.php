<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBackendLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('backend_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('page_name');
			$table->string('url');
			$table->string('subject')->nullable();
			$table->text('before_changes', 65535)->nullable();
			$table->text('after_changes', 65535)->nullable();
			$table->string('method');
			$table->string('ip');
			$table->string('agent')->nullable();
			$table->integer('user_id')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('backend_logs');
	}

}
