<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserGroupPermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_group_permissions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_group_id')->unsigned();
			$table->string('controller', 50);
			$table->string('action', 100);
			$table->boolean('allowed');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_group_permissions');
	}

}
