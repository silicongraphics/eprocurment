<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRfqCartProductTermsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rfq_cart_product_terms', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('rfq_cart_product_id')->unsigned()->index('rfqcpt_rcptid_fk');
			$table->integer('vendor_id')->unsigned()->nullable()->index('rfqcpt_vid_fk');
			$table->string('warranty_terms', 12);
			$table->string('service_terms', 12);
			$table->integer('credit_term_id')->unsigned()->index('rfqcpt_ctid_fk');
			$table->string('delivery_terms', 12);
			$table->timestamps();
			$table->text('note', 65535)->nullable();
			$table->boolean('sent_status')->default(0);
			$table->integer('reqForQuote_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rfq_cart_product_terms');
	}

}
