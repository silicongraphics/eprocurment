<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductApplicationLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_application_links', function(Blueprint $table)
		{
			$table->foreign('application_id', 'pappl_appid_fk')->references('id')->on('applications')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('product_id', 'pappl_pid_fk')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_application_links', function(Blueprint $table)
		{
			$table->dropForeign('pappl_appid_fk');
			$table->dropForeign('pappl_pid_fk');
		});
	}

}
