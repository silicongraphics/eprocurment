<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVendorDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('vendor_documents', function(Blueprint $table)
		{
			$table->foreign('country_id', 'vdoc_countryid_fk')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('doc_type_id', 'vdoc_dtid_fk')->references('id')->on('doc_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('vendor_id', 'vdoc_vid_fk')->references('id')->on('vendors')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('vendor_user_id', 'vdoc_vuid_fk')->references('id')->on('vendor_users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('vendor_documents', function(Blueprint $table)
		{
			$table->dropForeign('vdoc_countryid_fk');
			$table->dropForeign('vdoc_dtid_fk');
			$table->dropForeign('vdoc_vid_fk');
			$table->dropForeign('vdoc_vuid_fk');
		});
	}

}
