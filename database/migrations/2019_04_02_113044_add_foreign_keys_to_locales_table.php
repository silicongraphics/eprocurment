<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLocalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('locales', function(Blueprint $table)
		{
			$table->foreign('countries_id', 'locales_countriesid_fk')->references('id')->on('countries')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('languages_id', 'locales_languagesid_fk')->references('id')->on('languages')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('locales', function(Blueprint $table)
		{
			$table->dropForeign('locales_countriesid_fk');
			$table->dropForeign('locales_languagesid_fk');
		});
	}

}
