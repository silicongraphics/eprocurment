<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_id')->unsigned()->nullable()->index('cu_cid_fk');
			$table->integer('user_group_id')->unsigned()->index('client_ugid_fk');
			$table->string('first_name', 100);
			$table->string('last_name', 100)->nullable();
			$table->string('email', 191)->unique('client_email_unique');
			$table->string('email2', 191)->nullable();
			$table->string('password', 191);
			$table->string('remember_token', 100)->nullable();
			$table->text('notes', 65535)->nullable();
			$table->enum('title', array('Mr.','Mrs.','Ms.','Miss'))->nullable();
			$table->string('company_name', 100)->nullable();
			$table->string('website')->nullable();
			$table->string('job_title', 100)->nullable();
			$table->string('phone', 100)->nullable();
			$table->string('fax', 100)->nullable();
			$table->string('mobile_no', 100)->nullable();
			$table->string('mobile_no2', 100)->nullable();
			$table->date('dob')->nullable();
			$table->string('emirates_id', 100)->nullable();
			$table->string('trade_license', 100)->nullable();
			$table->string('tax_registration_no', 100)->nullable();
			$table->string('vat_certificate', 100)->nullable();
			$table->string('name_2', 100)->nullable();
			$table->string('emirates_id2', 100)->nullable();
			$table->smallInteger('country_id')->nullable()->index('client_country_fk');
			$table->smallInteger('city_id')->nullable();
			$table->integer('pobox_number')->nullable();
			$table->string('shipping_address', 64)->nullable();
			$table->string('shipping_street', 64)->nullable();
			$table->integer('shipping_pobox')->nullable();
			$table->smallInteger('shipping_country_id')->nullable()->index('cu_ship_country_fk');
			$table->smallInteger('shipping_city_id')->nullable()->index('cu_ship_city_fk');
			$table->string('billing_address', 64)->nullable();
			$table->string('billing_street', 64)->nullable();
			$table->integer('billing_pobox')->nullable();
			$table->smallInteger('billing_country_id')->nullable()->index('cu_bill_country_fk');
			$table->smallInteger('billing_city_id')->nullable()->index('cu_bill_city_fk');
			$table->boolean('same_addresses')->nullable()->default(0);
			$table->integer('payment_method_id')->unsigned()->nullable();
			$table->integer('delivery_method_id')->unsigned()->nullable();
			$table->integer('credit_term_id')->unsigned()->nullable();
			$table->string('profile_picture', 100)->nullable();
			$table->boolean('verified')->nullable()->default(0);
			$table->boolean('active')->default(0)->comment('Active = 1, Inactive = 0, Rejected = 2, Ban = -1');
			$table->timestamps();
			$table->string('company_id', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('client_users');
	}

}
