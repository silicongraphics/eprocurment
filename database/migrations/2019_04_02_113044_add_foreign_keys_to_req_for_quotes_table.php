<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReqForQuotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('req_for_quotes', function(Blueprint $table)
		{
			$table->foreign('client_id', 'rfq_cid_fk')->references('id')->on('clients')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('client_user_id', 'rfq_cuid_fk')->references('id')->on('client_users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('req_for_quotes', function(Blueprint $table)
		{
			$table->dropForeign('rfq_cid_fk');
			$table->dropForeign('rfq_cuid_fk');
		});
	}

}
