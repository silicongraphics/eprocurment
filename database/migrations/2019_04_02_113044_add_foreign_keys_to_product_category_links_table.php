<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductCategoryLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_category_links', function(Blueprint $table)
		{
			$table->foreign('category_id', 'pcatl_catid_fk')->references('id')->on('categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('product_id', 'pcatl_pid_fk')->references('id')->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_category_links', function(Blueprint $table)
		{
			$table->dropForeign('pcatl_catid_fk');
			$table->dropForeign('pcatl_pid_fk');
		});
	}

}
