<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRfqCartProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rfq_cart_products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('rfq_selection_id')->unsigned()->nullable()->index('rfqcp_selectionid_fk');
			$table->integer('product_id')->unsigned()->nullable()->index('rfqcp_pid_fk');
			$table->smallInteger('quantity_requested')->default(0)->comment('Product Quantity Requested by Client.');
			$table->timestamps();
			$table->integer('reqForQuote_id');
			$table->integer('rfq_sel_tab_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rfq_cart_products');
	}

}
