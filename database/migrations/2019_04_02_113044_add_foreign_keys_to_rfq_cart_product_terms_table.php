<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRfqCartProductTermsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rfq_cart_product_terms', function(Blueprint $table)
		{
			$table->foreign('credit_term_id', 'rfqcpt_ctid_fk')->references('id')->on('credit_terms')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('rfq_cart_product_id', 'rfqcpt_rcptid_fk')->references('id')->on('rfq_cart_products')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('vendor_id', 'rfqcpt_vid_fk')->references('id')->on('vendors')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rfq_cart_product_terms', function(Blueprint $table)
		{
			$table->dropForeign('rfqcpt_ctid_fk');
			$table->dropForeign('rfqcpt_rcptid_fk');
			$table->dropForeign('rfqcpt_vid_fk');
		});
	}

}
