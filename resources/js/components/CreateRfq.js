import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import SearchHeader from './SearchHeader';
import ChooseProduct from './ChooseProduct';
import RFQSelections from './RFQSelections';
import axios from 'axios';
//axios.baseURL = 'http://localhost:8000/';
axios.defaults.baseURL = document.head.querySelector('meta[name="api-base-url"]').content;

export default class CreateRfq extends Component {

    constructor(){
        super();
        this.state= {
          chosedProducts : [],
          rfqSelections  : [],
          HSC: {},
          rfqadded: false,
          added: false,
        }
        this.getData = this.getData.bind(this);
    }

    childResponse(params){
        this.setState({
            chosedProducts: params,
            added: true,
            rfqadded: false,
        });
    }

    componentDidMount(){
        this.getData();
    }

    getData(added = false){
        axios.get('/products/get-stored-search')
        .then( (res) => {
            this.setState({
                chosedProducts: res.data,
                added: added,
                rfqadded: false,
            });
        })
        .catch(function(error){
            throw error;
        });

        
        axios.get('/products/get-rfq-selections')
        .then( (res) => {
            this.setState({
                rfqSelections: res.data,
                added: added,
                rfqadded: false,

            });
        })
        .catch(function(error){
            throw error;
        });
    }

    ChooseProductResponse(params){
        this.setState({
            rfqSelections: params,
            added: false,
            rfqadded: true,

        });        
    }

    handleCallback1(){
        this.getData(true);
    }

    handlechangeCallback(params){
        this.setState({
            HSC: params,
            added: false,
            rfqadded: false,

        })
    }

    render() {
        return (
                <div className="right-panel-content new-full-search">
                    <SearchHeader base_url={axios.defaults.baseURL} callback={ this.childResponse.bind(this) }/>
                    <div id="ChooseProduct" className="content-parent content-box-1 no-shadow">
                       <ChooseProduct   callback={ this.ChooseProductResponse.bind(this)  }
                                        callback1={ this.handleCallback1.bind(this)}
                                        HSC = {this.state.HSC}  
                                        added = {this.state.added}
                                        chosedProducts={this.state.chosedProducts} />
                    </div>
                    <div id="RFQSelections" className="content-parent content-box-1 no-shadow">
                       <RFQSelections   changeCallback={this.handlechangeCallback.bind(this)} 
                                        rfqadded = {this.state.rfqadded}
                                        rfqSelections={this.state.rfqSelections} />
                    </div>

                </div>
        );
    }
}


if (document.getElementById('CreateRfq')) {
    ReactDOM.render(<CreateRfq />, document.getElementById('CreateRfq'));
}
