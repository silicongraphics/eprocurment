import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {isEmpty, BASE_URL} from '../lib/utils';
import toastr from 'toastr';
import axios from 'axios';
//axios.baseURL = BASE_URL;
axios.defaults.baseURL = document.head.querySelector('meta[name="api-base-url"]').content;



export default class ChooseProduct extends Component {

    constructor(props) {
        super(props);
        this.state = {
            chosedProducts: [],
            activeIndex: 0,
            activePartNo: '',
            rfqSelection: '',
            selectProducts: [],
            HSC: {},
            activeClass:'',
            checkedItems: new Map(),
            activeTabs: new Map(),
            
        }
        this.handleRFQSelection = this.handleRFQSelection.bind(this);
        this.handleCallbackFromRFQ = this.handleCallbackFromRFQ.bind(this);
        this.tempselectProducts = [];
    }
    
        
    componentDidMount(){
    }   

    checkImage (src, good, bad) {
        var img = new Image();
        img.onload = good; 
        img.onerror = bad;
        img. src = src;
    }

    handleRFQSelection(){
        var count = 0;
        this.tempselectProducts.map((item, index) => {
            if(item.partNo == this.state.activePartNo)
                count++;
        });    
        
        if(!count)
            alert('Please select some products to proceed');
        else{
            axios.post('/product/save-rfq-selections', {
                selections: this.tempselectProducts,
                index: this.state.activeIndex,
                partNo: this.state.activePartNo,
                name: (this.state.rfqSelection) ? this.state.rfqSelection : this.state.activePartNo
            })
            .then((response) => {
                this.props.callback(response.data);
                this.tempselectProducts = [];
                this.setState({ rfqSelection: '' });
                this.uncheckCheckbox();

            })
            .catch(function(error){
                toastr.error('Error', error);
                throw error;
            })    
        }
    }

    insertElseRemoveFromList(obj, list) {
        var i, count = 0;
        var newList = list;
        if(newList.length == 0)
            newList.push(obj);
        else{
            list.map((item, index) => {
                if(Number(item.product_id) === Number(obj.product_id) && (item.partNo === obj.partNo) ){
                    count = count + 1;
                    newList.splice(i, 1);
                }
            });

            if(count == 0){
                newList.push(obj); 
            }
        }
        return newList;

    }

    handleInputChange(e, defchk){
        var value = e.target.value;
        const isChecked = e.target.checked;
        var list = [];
        list = this.tempselectProducts;
        var result = this.insertElseRemoveFromList({
                'product_id' : value, 'partNo':this.state.activePartNo
            }, list);
        this.tempselectProducts = result;

    }

    componentWillReceiveProps(nextProps){ 
        if(this.state.chosedProducts !== nextProps.chosedProducts){
            this.setState({chosedProducts: nextProps.chosedProducts});
        }
        if(nextProps.HSC){
            this.handleCallbackFromRFQ(nextProps.HSC); 
        }
        if(nextProps.added === true){
            if(nextProps.chosedProducts.length){
                var $pnumber = nextProps.chosedProducts[nextProps.chosedProducts.length - 1][0].name;
                this.setState({activeClass: $pnumber});
                this.setState({activePartNo: $pnumber});
                $(".cp .nav-item").removeClass("active");
                $(".cp .nav-link a").removeClass("active");
                $(".cp .nav-item-slctr"+$pnumber.replace(/\s+/g, '')).addClass("active");
                $(".cp .tab-pane").removeClass("active");
                $(".cp .tab-pane-slctr"+$pnumber.replace(/\s+/g, '')).addClass("active");           
            }
        }        

    }

    uncheckCheckbox(){
        $('input[type=checkbox').each(function(){
            this.checked = false;
        });        
    }

    handleCallbackFromRFQ(obj){
        if(!isEmpty(obj)){
            this.setState({activeClass: obj.partNo});
            this.setState({activePartNo: obj.partNo});
            this.setState({HSC: obj});
            this.setState({rfqSelection: obj.selection});
            $(".cp .nvitcp").removeClass("active");
            $(".cp .nav-item-slctr"+obj.partNo.replace(/\s+/g, '')).addClass("active");
            $(".cp .tbpncp").removeClass("active");
            $(".cp .tab-pane-slctr"+obj.partNo.replace(/\s+/g, '')).addClass("active");
            $('input[type=checkbox').each(function(){
                var partNo = this.getAttribute("data-partno");
                var value = this.value;
                if(partNo == obj.partNo && obj.pids.indexOf(Number(value)) > -1){
                    this.checked = true;
                }
            });
            this.tempselectProducts = [];
            var list = [];
            var result = [];
            obj.pids.map((prod) => {
                var p = prod;
                result = this.insertElseRemoveFromList({
                    'product_id' : parseInt(p,10), 'partNo':obj.partNo
                }, list);
            })
            this.tempselectProducts = result;        
        }
    }

    ImageExist(url) 
    {
       var img = new Image();
       try{
        img.src = url;
        return img.height != 0;
       }catch(e){}
       return 0;
    }    

    removeSelection(id){
        axios.get('/product/remove-selection/'+id)
        .then((res) => {
            if(!res.data.length)
                var newIndex = 0;
            else
                var newIndex = res.data.length-1;
            this.setState({chosedProducts:res.data});
            this.setState({activeIndex:newIndex});
            if(res.data.length){
                var $pnumber = res.data[0][0].name;
                setTimeout(function(){
                    this.setState({activeClass: $pnumber});
                    this.setState({activePartNo: $pnumber});
                    $(".cp .nvitcp").removeClass("active");
                    $(".cp .nvlkcp a").removeClass("active");
                    $(".cp .nav-item-slctr"+$pnumber.replace(/\s+/g, '')).addClass("active");
                    $(".cp .tbpncp").removeClass("active");
                    $(".cp .tab-pane-slctr"+$pnumber.replace(/\s+/g, '')).addClass("active");      
                },500);
            }

            this.props.callback1();
        })
        .catch(function(error){
            throw error;
        });
    }
    
    render() {
        const image_base_url = 'https://dev71.onlinetestingserver.com/eprocurment/';
        if(this.state.chosedProducts.length){
            if(this.state.activePartNo == '')
                this.setState({ activePartNo: this.state.chosedProducts[0][0].name });
            return (
                <div>
                    <h2 className="title gradient">Selections</h2>
                    <div className="vertical-tabs">
                        <ul className="nav nav-tabs cp" role="tablist">
                            {
                                    this.state.chosedProducts.map( (res, index) => {
                                        if(this.state.activeClass == res[0].name){
                                            var c = 'active';
                                        }
                                        else{
                                            if(index == this.state.activeIndex){
                                                var c = 'active';
                                                //this.setState({activePartNo:res[0].name});
                                            }
                                            else{
                                                var c = '';
                                            }
                                        }                  
                    
                                        
                                        return(<li key={index} className={"nav-item nvitcp " + c + " nav-item-slctr"+res[0].name.replace(/\s+/g, '')}>
                                            <a  className="nav-link nvlkcp" 
                                            onClick={() => this.setState({
                                                activeIndex:index, 
                                                activePartNo:res[0].name,
                                            }) } 
                                            data-toggle="tab" href={"#" + res[0].id} role="tab" aria-controls="home" 
                                            title={res[0].name} aria-expanded="false">
                                            {res[0].name}</a>
                                        </li>);                                
                                    })   
                            }
                        </ul>
                        <div className="tab-content cp">
                            {
                                this.state.chosedProducts.map( (res, index) => {
                                if(this.state.activeClass == res[0].name){
                                    var c = 'active';
                                }
                                else{
                                    if(index == this.state.activeIndex)
                                        var c = 'active';
                                    else
                                        var c = '';
                                }                            
                                var attributes = JSON.parse(res[0].attributes);
                                var attrs = Object.entries(attributes);
                                return ( 
                                <div key={index} className={"tab-pane tbpncp " + c + " tab-pane-slctr"+res[0].name.replace(/\s+/g, '')} id={res[0].id} role="tabpanel">
                                <div className="sv-tab-panel">
                                    <div className="main-panel">
                                        <div className="tab-left-details">
                                            <div className="pro-box-left">
                                                <ul>
                                                    <li>Attributes</li>
                                                    {
                                                        attrs.map(  (attropt, attrindex) => {
                                                            return (
                                                                <li key={attrindex}>{attropt[0]}</li>
                                                            )
                                                        })
                                                    }                                            
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="tab-right-details">
                                            <h2>{res[0].name}</h2>
                                            <a className="closex rmt"><i className="fa fa-close"></i> </a>
                                            <div className="rmtp pop-over-main">
                                                <div className="popover-inner gt">
                                                    <div className="search-popover no-border">
                                                        <h3 className="h31">Are you sure </h3>
                                                        <h3 className="h32">you want to remove this selection</h3>
                                                        <div className="result-search gt">
                                                            <div className="col-md-6 add-btn">
                                                                <a href="javascript:;" className="closeD">Cancel</a>
                                                            </div>
                                                            <div className="col-md-6 add-btn">
                                                                <a ref={res[0].id} onClick={ () => this.removeSelection(res[0].id) } href="javascript:;" 
                                                                className="closeD">Remove</a>
                                                            </div>    
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                                
                                            <div className="zone-wrapper">
                                                <div style={{ width: res[0].data.length*300 }} className="zone-scroll">
                                                    <div className="pro-box">
                                                        {
                                                            res[0].data.map( (product, pindex) => {
                                                                var defchk;
                                                                if(!isEmpty(this.state.HSC)){
                                                                    if(this.state.HSC.pids.indexOf(product.product_id) > -1)
                                                                        defchk = true;
                                                                    else
                                                                        defchk = false;                                                                                                                    
                                                                }else{
                                                                    defchk = false;                                                                    
                                                                }

                                                                if(attrs.length == Object.entries(JSON.parse(product.attributes)).length){
                                                                    // if(this.ImageExist(image_base_url+product.product_id+'/'+product.image))
                                                                    //     var img = image_base_url+product.product_id+'/'+product.image;
                                                                    // else
                                                                    // var img = 'https://via.placeholder.com/112x102.png/ccc';
                                                                    if(product.image)
                                                                        var img = image_base_url+product.image;
                                                                     else
                                                                        var img = 'https://via.placeholder.com/112x102.png/ccc';
                                                                    return(
                                                                        <div key={pindex} className="pro-box-main">
                                                                        <div className="pro-box-inner">
                                                                            <div className="img-main-new">
                                                                                <img 
                                                                                onError={(e)=>{e.target.onerror = null; e.target.src="https://via.placeholder.com/112x102.png/ccc"}}
                                                                                style={{width:50, height:100}} className="" src={img} 
                                                                                alt="Product image"/>
                                                                            </div>
                                                                            <ul>
                                                                                <li className="fc">{product.name}</li>
                                                                                {
                                                                                    Object.entries(JSON.parse(product.attributes)).map( (attropt, attrindex) => {
                                                                                        return (
                                                                                            <li key={attrindex}>{(attropt[1]) ? attropt[1] : 'NAN'}</li>
                                                                                        )
                                                                                    })                                                                    
                                                                                }
                                                                            </ul>
                                                                            <form>
                                                                                <div className="check-parent-new">
                                                                                    <label><input
                                                                                    type="checkbox" 
                                                                                    data-partno={res[0].name} 
                                                                                    checked={this.state.checkedItems.get(res[0].name+'_pro_'+product.product_id)}
                                                                                    onChange={ (e) => this.handleInputChange(e, res[0].name+'_pro_'+product.product_id )  }
                                                                                    value={product.product_id} 
                                                                                    className="checkbox-pro" name="Pro-check"/> 
                                                                                    Select Product</label>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>                                                                
                                                                    )                                                            
                                                                }
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="selection-panel">
                                               
                                                <form>
                                                    <input onChange={ (e) => this.setState({rfqSelection: e.target.value})  }
                                                    className="form-field" value={this.state.rfqSelection} type="text" 
                                                    placeholder="Type Comparison Name" required=""/>
                                                    <input onClick={this.handleRFQSelection} type="button" style={{float:'right'}} value="Add to Comparison Table"/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            );
                            })   

                            }                       
                        </div>
                    </div>
                </div>           
            )
        }
        else
            return ('')
    }
}


ChooseProduct.protoTypes = {
    callback : PropTypes.func,
    callback1 : PropTypes.func,
}

