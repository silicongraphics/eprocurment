import React, { Component } from 'react';
import GeneralTerms from './general/GeneralTerms'

import PropTypes from 'prop-types';

class RFQSelections extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rfqSelections: [],
            activeIndex: 0,
            rfqname:'',
            requisition:'',
            site_area:'',
            clicked: false,
        }
        this.handleEditRFQ = this.handleEditRFQ.bind(this);
        this.handleDeleteRFQ = this.handleDeleteRFQ.bind(this);
        this.numberValidate = this.numberValidate.bind(this);
        this.generalTerms = [];
        this.SuppGeneralTerms = [];
    }

    componentDidMount(){
        var issavedrfq = $('#issavedrfq').val();
        var savedrfqName = $('#savedrfqName').val();
        if(Number(issavedrfq)){
            this.setState({
                rfqname: savedrfqName
            });
        }

        axios.get('/client/get-my-general-terms')
        .then((res) => {
            var params = {
                partNo: '',
                supplier: '',
                product: '',
                warranty_terms:res.data.warranty_terms,
                service_terms:res.data.service_terms,
                credit_terms:res.data.credit_terms,
                delievery_terms:res.data.delievery_terms
           }
            this.generalTerms.push(params);
            console.log(this.generalTerms);
    
        })
        .catch(function(error){
            throw error;
        });
    }

    setGlobalGeneralTerms(){
        
    }

    componentWillReceiveProps(nextProps){
        if(this.state.rfqSelections !== nextProps.rfqSelections){
            this.setState({rfqSelections: nextProps.rfqSelections});
            $('html,body').animate({scrollTop: document.body.scrollHeight},"fast");
        }
        if(nextProps.rfqadded === true){
            if(nextProps.rfqSelections.length){
                var $pnumber = nextProps.rfqSelections[nextProps.rfqSelections.length - 1].name;
                this.setState({activeClass: $pnumber});
                this.setState({activePartNo: $pnumber});
                setTimeout(function(){
                    $(".nvitr").removeClass("active");
                    $(".nvlkr a").removeClass("active");
                    $(".nav-item-rslctr"+$pnumber.replace(/\s+/g, '')).addClass("active");
                    $(".tbpnr").removeClass("active");
                    $(".tab-pane-rslctr"+$pnumber.replace(/\s+/g, '')).addClass("active");           
                },100);
            }
        }           
    }

    handleEditRFQ(partNo, selection){
        var pids = [];
        this.state.rfqSelections.map((item) => {
            if(item.partNo == partNo){
                item.products.map((product) => {
                    pids.push(product.id);
                })
            }
        })    
        var obj = {
            partNo: partNo,
            selection: selection,
            type: 1,
            pids: pids
        }
        this.props.changeCallback(obj);
        $('html,body').animate({scrollTop: $('#ChooseProduct').offset().top},"fast");

    }

    handleDeleteRFQ(partNo, selection){
        axios.post('/product/remove-rfq-selection', {
            partNo: partNo,
            selection: selection,
        })
        //axios.post('/product/remove-rfq-selection/'+partNo+'/'+selection)
        .then((res) => {
            var newIndex = res.data.length-1;
            this.setState({rfqSelections:res.data});
            if(res.data.length)
                this.setState({activeIndex:newIndex});
        })
        .catch(function(error){
            throw error;
        });
        return;
    }

    handleCallback(params){
        this.generalTerms = [];
        this.generalTerms.push(params);
    }

    saveRFQTemp(){
        if(!this.state.saverfqname){
            alert('RfqName is required');
            return;            
        } 
        if(this.state.clicked === false){
            $('.loading').show();
            this.setState({clicked:true});
            axios.post('/client/submit/save-my-rfq', {
                rfqname:this.state.saverfqname,
            })
            .then( (response) => {
                $('.loading').hide();                
                if(response.data.status == 200)
                    window.location.href = document.head.querySelector('meta[name="api-base-url"]').content+'/client/my-saved-rfqs'; 
                this.setState({clicked:false});
            })
            .catch(function(error){
                $('.loading').hide();
                this.setState({clicked:false});
                throw error;
            })        
        }else{
            
        }
    }

    numberValidate(e) 
    {
        var value = e.target.value;
        var min = 1;
        if(parseInt(value) < 1 || isNaN(parseInt(value))) 
            e.target.value =  min; 
        else 
            e.target.value = value;
    }    

    saveRFQ(){
        var issavedrfq = $('#issavedrfq').val();
        var savedrfqName = $('#savedrfqName').val();
        var gterms = this.SuppGeneralTerms;
        var globalterms = this.generalTerms;
        var obj = [];
        $.each($(".quantity"), function(){            
            var suppobj = [];
            var value = $(this).val();
            var partno = $(this).data('partno');
            var tabname = $(this).data('tabname');
            var product = $(this).data('product');
            if(product && partno && value){
                $.each($("input[name='selected_suppliers']:checked"), function(){            
                    var count=0;
                    var suppid = $(this).val();
                    var pid = $(this).data('product_id');
                    var pn = $(this).data('partno');
                    if(pn == partno && Number(pid) == Number(product)){
                        gterms.map((terms) => {
                            if(terms.partNo == partno && Number(terms.product) == Number(pid) && terms.supplier == suppid){
                                suppobj.push({
                                    product_id:pid,
                                    suppid:suppid,
                                    terms: terms,
                                });                                        
                                count++;
                            }
                        });
                        if(count == 0){
                            suppobj.push({
                                product_id:pid,
                                suppid:suppid,
                                terms: globalterms[0],
                            });                             
                        }

                    }
                });		
                obj.push({
                    product_id: product,
                    quantity: value,
                    tabname: tabname,
                    partno: partno,
                    supplier: suppobj
                })
            }
        });

        if(!this.state.rfqname ||  !this.state.requisition){
            alert('RfqName , Requisition #  is required');
            return;            
        }
        if(gterms.length == 0 && globalterms.length == 0){
            alert('Please select some terms for supplier');
            return;
        }
        if(!obj.length){
            alert('Please select some supplier with quantity to proceed');
            return;            
        }
        if(this.state.clicked === false){
            $('.loading').show();
            this.setState({clicked:true});
            axios.post('/client/submit/create-my-rfq', {
                generalTerms:this.generalTerms,
                SuppGeneralTerms:this.SuppGeneralTerms,
                rfqname:this.state.rfqname,
                requisition:this.state.requisition,
                site_area:this.state.site_area,
                obj:obj,
                issavedrfq:issavedrfq,
            })
            .then( (response) => {
                $('.loading').hide();
                if(response.data.status == 200)
                    window.location.href = document.head.querySelector('meta[name="api-base-url"]').content+'/client/my-rfq/'+response.data.rfqcode; 
                this.setState({clicked:false});
                console.log(response.data);
            })
            .catch(function(error){
                $('.loading').hide();
                this.setState({clicked:false});
                throw error;
            })
        }else{
            
        }
    }

    handleIndCallback(params){
        this.SuppGeneralTerms.push(params);
    }

    render() {
        const image_base_url = 'https://dev71.onlinetestingserver.com/eprocurment/';
        if(this.state.rfqSelections.length)             
        return (
            <div>
            <div className="content-parent content-box-2 no-shadow">
            <h2 className="title gradient">Comparison Table
            <div className="btn-h-main"><a href="javascript:;" className="popup_open">General Terms</a>
                <div className="pop-over-main">
                    <div className="popover-inner gt">
                        <div className="title no-border">
                            <i className="fa fa-close"></i>
                        </div>
                        <div className="search-popover no-border">
                            <form>
                                <GeneralTerms partNo='' product='' supplier='' callback={this.handleCallback.bind(this)} name="Set General Terms"  />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </h2>
            <div className="vertical-tabs">
            
            <ul className="nav nav-tabs" role="tablist">
                {
                    this.state.rfqSelections ? (
                        this.state.rfqSelections.map( (res, index) => {
                            if(index == this.state.activeIndex)
                                var c = 'active';
                            else
                                var c = '';
                            return(<li key={index} className={"nav-item nvitr " + c + " nav-item-rslctr"+res.name.replace(/\s+/g, '')} >
                                <a  className="nav-link nvlkcr" onClick={() => this.setState({activeIndex:index}) }
                                 data-toggle="tab" href={"#sel_" + res.name.replace(/\s+/g, '')}
                                 role="tab" aria-controls="home" 
                                title={res.name} aria-expanded="false">
                                {res.name}</a>
                            </li>);                                
                        })   

                    ) : ""
                }
            </ul>
            <div className="tab-content">
            {
                this.state.rfqSelections ? (
                this.state.rfqSelections.map( (res, index) => {
                if(index == this.state.activeIndex)
                    var c = 'active';
                else
                    var c = '';
                
                var attributes = JSON.parse(res.products[0].attributes);
                var attrs = Object.entries(attributes);
                return (               
                    <div key={index}  
                    className={"tab-pane tbpnr " + c + " tab-pane-rslctr"+res.name.replace(/\s+/g, '')}
                    id={"sel_"+res.name.replace(/\s+/g, '')} role="tabpanel">
                    <div className="sv-tab-panel">
                    <div className="main-panel">
                       <div className="tab-left-details">
                           <div className="pro-box-left">
                               <ul>
                                    <li>Attributes</li>
                                    {
                                        attrs.map(  (attropt, attrindex) => {
                                            return (
                                                <li key={attrindex}>{attropt[0]}</li>
                                            )
                                        })
                                    }  
                               </ul>
                           </div>
                       </div>
                       <div className="tab-right-details">
                           <h2>{res.name}</h2>
                           <a onClick={ () => this.handleEditRFQ(res.partNo, res.name) } className="edit"><i className="fa fa-edit"></i></a>
                           <a className="closex rmt"><i className="fa fa-close"></i> </a>
                            <div className="rmtp pop-over-main">
                                <div className="popover-inner gt">
                                    <div className="search-popover no-border">
                                        <h3 className="h31">Are you sure </h3>
                                        <h3 className="h32">you want to remove this selection</h3>
                                        <div className="result-search gt">
                                            <div className="col-md-6 add-btn">
                                                <a href="javascript:;" className="closeD">Cancel</a>
                                            </div>
                                            <div className="col-md-6 add-btn">
                                                <a onClick={ () => this.handleDeleteRFQ(res.partNo, res.name) }  href="javascript:;" 
                                                className="closeD">Remove</a>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </div> 


                           <div className="zone-wrapper">
                               <div style={{ width: res.products.length*300 + 400 }} className="zone-scroll">
                                   <div className="pro-box">
                                            {
                                                res.products.map((product, pindex) => {
                                                    //var img = 'https://via.placeholder.com/112x102.png/ccc';
                                                    if(product.image)
                                                        var img = image_base_url+product.image;
                                                    else
                                                        var img = 'https://via.placeholder.com/112x102.png/ccc';                                                    
                                                    return(
                                                    <div key={pindex}  className="pro-box-main">
                                                    <div className="pro-box-inner">
                                                        <div className="img-main-new">
                                                            <img 
                                                            onError={(e)=>{e.target.onerror = null; e.target.src="https://via.placeholder.com/112x102.png/ccc"}}
                                                            style={{width:50, height:100}}  className="" src={img} 
                                                            alt="Product image"/>
                                                        </div>
                                                        <ul>
                                                            <li className="fc">{product.name}</li>
                                                    {
                                                        Object.entries(JSON.parse(product.attributes)).map( (attropt, attrindex) => {
                                                            return (
                                                                <li key={attrindex}>{(attropt[1]) ? attropt[1] : 'NAN'}</li>
                                                            )
                                                        })                                                                    
                                                    }
                                                    </ul>
                                                    <form>
                                                        <div className="check-parent-new">
                                                            <div className="quantity">
                                                                <label>Qty</label>
                                                                <input type="number" 
                                                                    data-product={product.id} 
                                                                    data-partno={res.partNo} 
                                                                    data-tabname={res.name}
                                                                    onChange={(e) => this.numberValidate(e) }
                                                                    className="pro quantity" name="P"/>
                                                            </div>
                                                        </div>
                                                        {
                                                            product.suppliers.map((supplier, sindex) => {
                                                                return(
                                                                <div key={sindex} className="feild-supp">
                                                                <label><input 
                                                                value={supplier.id} 
                                                                data-product_id={product.id} 
                                                                data-partno={res.partNo} 
                                                                type="checkbox" 
                                                                className="checkbox-pro" name="selected_suppliers"/>
                                                                {supplier.name}</label>
                                                                    <i className="fa fa-edit popup_open"></i>
                                                                    <div className="pop-over-main supp-rfq edit-pop indi">
                                                                        <div className="popover-inner">
                                                                            <div className="title no-border">
                                                                                <i className="fa fa-close"></i>
                                                                            </div>
                                                                            <div className="search-popover no-border">
                                                                                    {
                                                                                        <GeneralTerms 
                                                                                        supplier={supplier.id}
                                                                                        partNo={res.partNo} 
                                                                                        product={product.id}
                                                                                        callback={this.handleIndCallback.bind(this)} 
                                                                                        name="Set Individual Product
                                                                                        Terms"  />
                                                                                    }
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                )
                                                            })
                                                        }
                                                    </form>
                                                </div>
                                                </div>
                                                    )
                                                })
                                            }
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
                )
                }) 
                ) : ""
            } 

            <div className="crfq selection-panel-new">
                <div className="search-h">
                    <h3 className="title-box">Save Comparison Table</h3>
                    <form>
                        <div className="col-md-8 no-padding">
                            <input onChange={ (e) => this.setState({saverfqname : e.target.value}) } className="form-field" 
                            type="text" placeholder="Type Comparison Table Name"/>
                        </div>
                        <div className="col-md-4">
                            <input onClick={this.saveRFQTemp.bind(this)}  className="saverfqbtn" type="button" value="Save"/>
                        </div>
                    </form>
                </div>
                <div className="search-cat new-has-rfq">
                    <h3 className="title-box">Create RFQ</h3>
                    <form>
                        <input onChange={ (e) => this.setState({rfqname : e.target.value}) } value={this.state.rfqname}  className="form-field" type="text" placeholder="Type RFQ Name/Project Name"/>
                        <input onChange={ (e) => this.setState({requisition : e.target.value}) } className="form-field" type="text" placeholder="Type Requisition"/>
                        <input onClick={this.saveRFQ.bind(this)} type="button" className="" value="Submit"/>
                    </form>
                </div>
            </div>

            </div>
            </div>
            </div>                    
            </div>
        );
        else
            return ('')                
            
    }
}

RFQSelections.propTypes = {
    changeCallback: PropTypes.func,
}

export default RFQSelections;