import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Autocomplete from 'react-autocomplete';
import toastr from 'toastr'
axios.defaults.baseURL = document.head.querySelector('meta[name="api-base-url"]').content;
import ReactPaginate from 'react-paginate';


export default class SearchHeader extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            query:'',
            productsData:[],
            productPartno: [],
            catsData: [],
            catChild: [],
            dropdown1: [],
            dropdown2: [],
            dropdown3: [],
            searchData: [],
			completed: false,
			pageCount: 1,
			currentPage: 1            
        };
        this.searchProductsByKeywords = this.searchProductsByKeywords.bind(this);
        this.getCategories = this.getCategories.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeSub = this.handleChangeSub.bind(this);
        this.handleChangeSubLevel2 = this.handleChangeSubLevel2.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.matchStateToTerm = this.matchStateToTerm.bind(this);
		this.handlePageClick = this.handlePageClick.bind(this);
		this.performSearch = this.performSearch.bind(this);
    }

    async handlePageClick(data) {
		const page = data.selected >= 0 ? data.selected + 1 : 0;
		await Promise.resolve(this.setState(() => ({ currentPage: page })));

		this.handleSearch();
	}    

    async handleSearch(){
        if (history.pushState) {
            const newUrl = `/products/searchByCategories?c=${this.state.category_id}&c1=${this.state.category_sub1}&c2=${this.state.category_sub2}&c3=${this.state.category_sub3}&page=${this.state.currentPage}`;
            const response = await axios.get(newUrl);    
            try {
                if (response.data.status == 'success') {
                    this.setState(() => ({
                        searchData: response.data.record.data,
                        currentPage: response.data.record.current_page,
                        pageCount: response.data.record.last_page
                    }));
                } else {
                    console.log(error);
                }
            } catch (error) {
                console.log(error);
            }
        }
    }
    
    performSearch(part_number){
        $('.loading').show();
        axios.post('/products/search', {
            part_no:part_number,
        })
        //axios.get('/products/search?part_no='+part_number)
        .then((response) => {
            $('.loading').hide();
            this.setState({query: ''})
            this.setState({productsData: response.data})
            this.props.callback(response.data);
        })
        .catch(function(error){
            $('.loading').hide();
            alert(error.response.data.msg);
            //alert('The Part no donot exist or Its already been added into the selections');
            throw error; 
        })
    }

    searchProductsByKeywords(){
        $('.loading').show();
        var q = this.state.query;        
        axios.post('/products/search', {
            part_no:q,
        })
        .then((response) => {
            $('.loading').hide();
            this.setState({query: ''})
            this.setState({productsData: response.data})
            this.props.callback(response.data);
        })
        .catch(function(error){
            $('.loading').hide();
            alert(error.response.data.msg);
            throw error; 
        })
    }

    handleChangeSubLevel2(e){
        this.setState({[e.target.name] : e.target.value});
        axios.get('/product/get-categories-level-list?id='+e.target.value+'&type=')
        .then((response) => {
            this.setState({dropdown2:  response.data.level2 });
        })
        .catch(function(error){
            alert('Some error occured');
            throw error; 
        })            
      
    }

    handleChangeSub(e){
        this.setState({[e.target.name] : e.target.value});
    }

    getCategories(){
        axios.get('/product/get-categories')
        .then((response) => {
            this.setState({catsData: response.data})
        })
        .catch(function(error){
            alert('Some error occured');
            throw error; 
        })
    }

    matchStateToTerm(productPartno, value) {
        return (
            productPartno.name.toLowerCase().indexOf(value.toLowerCase()) !== -1 
            || productPartno.part_number.replace(/ /g,'').toLowerCase().indexOf(value.toLowerCase().replace(/ /g,'')) !== -1

        )
    }

    handleChange(e){
        this.setState({category_id:e.target.value});
        axios.get('/product/get-categories-level-list?id='+e.target.value+'&type=parent')
        .then((response) => {
            this.setState({dropdown1:  response.data.level1 });
            this.setState({dropdown3:  response.data.level3 });
        })
        .catch(function(error){
            alert('Some error occured');
            throw error; 
        })
    }

    getChilds(id){
        if(!id)
            return [];
        var count = 0;
        var arr1 = [];
        this.state.catsData.map((res, index) => {
            if(res.id == id){
                if(count < 3){
                    //for populate placeholders
                    res.subs.map((res1, index1) => {
                        var temp = {
                            "main":res1.title,
                            "child":res1.subs
                        }; 
                        arr1.push(temp);  
                    })            
                    count++;
                }
    
            }
        })  
        return arr1;
    }       

    componentDidMount(){
        this.getCategories();
        axios.get('/products/get-part-no-lists')
        .then( (res) => {
            this.setState({productPartno: res.data});        
        })
        .catch(function(error){
            throw error;
        });
    }

    render() {
        const image_base_url = 'https://dev71.onlinetestingserver.com/eprocurment/';
        return (
            <div>
            <div className="content-parent content-box-1">
            <h2 className="title gradient">Search Area</h2>
            <div className="box-content">
                <div className="search-h">
                    <h3 className="title-box">Search By Part Number/Name/Brand</h3>
                    <form>
                        <div className="col-md-7 no-padding">
                        <Autocomplete
                            inputProps={{ className: 'form-field', placeholder: 'Search By Part Number/Name/Brand'}}
                            wrapperStyle={{ position: 'relative', display: 'inline-block' }}
                            value={this.state.query}
                            items={this.state.productPartno}
                            getItemValue={(item) => item.part_number}
                            shouldItemRender={this.matchStateToTerm}
                            onChange={(event, value) => this.setState({ query:value })}
                            onSelect={value => this.setState({ query:value })}
                            renderMenu={(children) => (
                                <div className="part-menu">
                                  {
                                    children.length === 0 ? (
                                        <div className="item">This part no is not available</div>
                                  ) : children }
                                </div>
                            )}
                            wrapperProps={{className: 'autocompletediv'}}
                            renderItem={(item, isHighlighted) => (
                              <div
                                className={`item ${isHighlighted ? 'item-highlighted' : ''}`}
                                key={item.part_number}
                              >{item.name+' - '+item.part_number}
                              </div>
                            )}
                          />
                        </div>
                        <input onClick={this.searchProductsByKeywords} type="button" value="Add to Selection"/>
                    </form>
                </div>
                <div className="search-cat">
                    <h3 className="title-box">Search By Category</h3>
                    <form>
                        <div className="full-line">
                            <input className="form-field" type="text" placeholder="Enter Keyword or part number"/>
                        </div>
                        <div className="col-md-3 select-fields no-padding-left">
                            <select onChange={(e) => this.handleChange(e) } name="category-search" id="cat">
                                <option value="">Category</option>
                                {
                                    this.state.catsData.length ? (
                                        this.state.catsData.map( (res, index) => {
                                            return(
                                                <option key={index} value={res.id}>{res.title}</option>
                                            );                                
                                        })   

                                    ) : ""
                                }                              
                            </select>
                        </div>
                        <div className="col-md-3 select-fields no-padding-left">
                            <select onChange={(e) => this.handleChangeSubLevel2(e) } name="category_sub1" >
                            {
                                (this.state.dropdown1.length ) ? 
                                    <option value="">{this.state.dropdown1[0].label_name}</option>
                                : ""
                            }
                            {
                                (this.state.dropdown1.length ) ? ( 
                                    this.state.dropdown1.map((res, index) => {
                                        return (
                                            <option key={index} value={res.id}>{res.title}</option>
                                        )
                                    })
                                    ) : ""
                            }
                            </select>
                        </div>
                        <div className="col-md-3 select-fields no-padding-left">
                            <select onChange={(e) => this.handleChangeSub(e) } name="category_sub2">
                            {
                                (this.state.dropdown2.length) ? 
                                    <option value="">{this.state.dropdown2[0].label_name}</option>
                                : ""
                            }
                            {
                                (this.state.dropdown2.length) ? ( 
                                    this.state.dropdown2.map((res, index) => {
                                        return (
                                            <option key={index} value={res.id}>{res.title}</option>
                                        )
                                    })
                                    ) : ""
                            }
                            </select>
                        </div>
                        <div className="col-md-3 select-fields no-padding">
                            <select onChange={(e) => this.handleChangeSub(e) } name="category_sub3" >
                            {
                                (this.state.dropdown3.length) ? 
                                    <option value="">{this.state.dropdown3[0].label_name}</option>
                                : ""
                            }
                            {
                                (this.state.dropdown3.length) ? ( 
                                    this.state.dropdown3.map((res, index) => {
                                        return (
                                            <option key={index} value={res.id}>{res.title}</option>
                                        )
                                    })
                                    ) : ""
                            }
                            </select>
                        </div>
                        <div className="btn-search select-fields popup_open">
                            <input onClick={this.handleSearch} data-target="#productModel" data-toggle="modal" type="button" value="Search"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>      
        <div id="productModel" className="modal fade" role="dialog">
        <div className="modal-dialog">
            <div className="modal-content">
            <div className="">
                <h2 className="title gradient">Product Search Results <img onClick={() => this.setState({searchData:[]})} data-dismiss="modal" src={this.props.base_url + '/images/close.png'} alt="Close"/></h2>
            </div>
            <div className="modal-body">
                <div className="search-inner-content">
                   {
                       this.state.searchData.length ? (
                           this.state.searchData.map((res, index) => {
                            if(res.image)
                                var img = image_base_url+res.image;
                            else
                                var img = 'https://via.placeholder.com/112x102.png/ccc';
                            return (
                                <div className="search-box-main col-md-6">
                                    <div className="search-box">
                                        <div className="box-search-image">
                                            <img  
                                            onError={(e)=>{e.target.onerror = null; e.target.src="https://via.placeholder.com/112x102.png/ccc"}}
                                            style={{width:50, height:100}} className="" src={img} 
                                                                                alt="Product image"/>
                                        </div>
                                        <div className="box-search-content">
                                            <h3>Part # {res.part_number}<br/>
                                                {res.name}</h3>
                                            <a data-dismiss="modal"  onClick={() => this.performSearch(res.part_number)} href="javascript:;"><b>Select</b></a>
                                        </div>
                                    </div>
                                </div>

                               )
                           })
                       ) : ""
                   }

                </div>
            </div>
            <div className="text-center">
                    <ReactPaginate
                        pageCount={this.state.pageCount}
                        initialPage={this.state.currentPage - 1}
                        forcePage={this.state.currentPage - 1}
                        pageRangeDisplayed={4}
                        marginPagesDisplayed={2}
                        previousLabel="&#x276E;"
                        nextLabel="&#x276F;"
                        containerClassName="pagination h"
                        activeClassName="active"
                        disabledClassName="disabled"
                        onPageChange={this.handlePageClick}
                        disableInitialCallback={true}
                    />
            </div>
            </div>
        </div>
        </div>
        </div>  
        );
    }
}

SearchHeader.protoTypes = {
    callback : PropTypes.func,
}

