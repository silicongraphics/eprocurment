import React, { Component } from 'react';
import axios from 'axios';

class GeneralTerms extends Component {

    constructor(props) {
        super(props);
        this.state = {
           terms: [],
           supplier: props.supplier,
           partNo : props.partNo,
           product: props.product,
           warranty_terms: '',
           service_terms: '',
           credit_terms: '',
           delievery_terms: '',           
        }

        this.getGeneralTerms = this.getGeneralTerms.bind(this);
    }


    getGeneralTerms(){
        axios.get('/products/get-general-terms')
        .then( (res) => {
            this.setState({
                terms: res.data
            });
        })
        .catch(function(error){
            throw error;
        });
    }

    handleClick(){
       var warranty_terms =  (this.state.warranty_terms) ? (this.state.warranty_terms) : (this.state.terms.warranty_terms[0].id) ;
       var service_terms =  (this.state.service_terms) ? (this.state.service_terms) : (this.state.terms.service_terms[0].id) ;
       var credit_terms =  (this.state.credit_terms) ? (this.state.credit_terms) : (this.state.terms.credit_terms[0].id) ;
       var delievery_terms =  (this.state.delievery_terms) ? (this.state.delievery_terms) : (this.state.terms.delievery_terms[0].id) ;
       
       this.props.callback({
            partNo: this.state.partNo,
            supplier: this.state.supplier,
            product: this.state.product,
            warranty_terms,
            service_terms,
            credit_terms,
            delievery_terms
       });

    }

    componentDidMount(){
        this.getGeneralTerms();    
    }

    render() {        
        if(Object.keys(this.state.terms).length)
        return (
                <div>
                        <label>Warranty Terms</label>
                        <select onChange={ (e) => this.setState({warranty_terms:e.target.value}) } id="manu-pop">
                            {
                                this.state.terms.warranty_terms.map( (item, index) => {
                                   return(
                                       <option key={index} value={item.id}>{item.term}</option>
                                   )     
                                })
                            }
                        </select>
                        <label>Service Terms</label>
                        <select onChange={ (e) => this.setState({service_terms:e.target.value}) }  id="manu-pop">
                            {
                                    this.state.terms.service_terms.map( (item, index) => {
                                    return(
                                        <option key={index} value={item.id}>{item.term}</option>
                                    )     
                                    })
                            }
                        </select>
                        <label>Payment Terms</label>
                        <select onChange={ (e) => this.setState({credit_terms:e.target.value}) } id="manu-pop">
                            {
                                this.state.terms.credit_terms.map( (item, index) => {
                                return(
                                    <option key={index} value={item.id}>{item.name}</option>
                                )     
                                })
                            }
                        </select>
                        <label>Delivery Terms</label>
                        <select onChange={ (e) => this.setState({delievery_terms:e.target.value}) } id="manu-pop">
                            {
                                this.state.terms.delievery_terms.map( (item, index) => {
                                return(
                                    <option key={index} value={item.id}>{item.term}</option>
                                )     
                                })
                            }
                        </select>   
                        <div className="result-search gt">
                                <div className="add-btn">
                                    <a className="closepopup" onClick={this.handleClick.bind(this)} href="javascript:;">{this.props.name}</a>
                                </div>
                        </div>

                </div>
        )
        else
            return ('')
    }
}

export default GeneralTerms;