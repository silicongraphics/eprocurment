@extends('layout.app')

@section('content')

<div class="right-panel">
        <div class="right-panel-content">
			
			@if(Session::has('success'))
				<div class="alert alert-success">
					<p>{{Session::get('success')}}</p>
				</div>
			@endif
			@if(Session::has('error'))
				<div class="alert alert-danger">
					<p>{{Session::get('error')}}</p>
				</div>
			@endif

			<div class="shows-listing-main" style="display: none;">
				<div class="shows-list-inner">
					<form>
						<label><input type="radio" class="filter" data-sel="" name="filter_row">Show All</label>
						<label><input type="radio" class="filter" data-sel="sent" name="filter_row">Show Published RFQs</label>
						<!-- <label><input type="radio" class="filter" data-sel="partially sent" name="filter_row">Show Semi-Published RFQs</label> -->
						<label><input type="radio" class="filter" data-sel="pending" name="filter_row">Show Pending RFQs</label>
						<label><input type="radio" class="filter" data-sel="closed" name="filter_row">Show Closed RFQs</label>
					</form>
				</div>
			</div>

            <div class="content-parent content-box-1">
                <h2 class="title gradient text-center">Received RFQs</h2>
                <div class="box-content">
                    <div class="search-h no-padding-full">
						<div class="table-responsive rfq-listing-detils">
							<table class="table table-h">
								<thead>
								<tr>
									<th>RFQ Name</th>
									<th>Requsition #</th>
									<th>RFQ Code</th>
									<th>Product Count</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								@foreach($rfqs as $rfq)
                                <tr>
									<td>
                                         <a href="{{ url('vendor/rfq/'.$rfq->req_code) }}">
										 <i class="fa fa-plus-square"></i></a>
                                         {{ $rfq-> name}}
                                    </td>
									<td>{{$rfq->req_number}}</td>
									<td>{{$rfq->req_code}}</td>
									<td>{{\App\Helper::getProductCount($rfq->id)}}</td>
									<td>{{\App\Helper::getRFQVendorStatus($rfq->id)}}</td>
                                                              									<td>
										<div class="par-sm-pop"><i class="fa fa-ellipsis-v" aria-hidden="true"></i>
											<div class="pop-over-main supp-rfq sm-pop-hov">
												<div class="popover-inner">
													<div class="result-search supp-rfq">
														<div class="result-content">
															<ul>
																<li>
																	<a href="{{ url('vendor/rfq/'.$rfq->req_code) }}"><i class="fa fa-eye" aria-hidden="true"></i>
																		View</a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</td>
                                </tr>
                                @endforeach							
                            </tbody>
                        </table>
						</div>
						<div class="pagination-h">
                            {{ $rfqs->links() }}

						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
$(function() {
    $(".filter").on("change", function() {
        var value = $(this).data('sel').toLowerCase();
        $("table > tbody > tr").filter(function() {     
             $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
</script>
<script>
	$(document).ready(function() {
		$('table tr').click(function() {
			var href = $(this).find("a").attr("href");

			if(href) {
				window.location = href;
			}
		});
	});
</script>	
@endsection