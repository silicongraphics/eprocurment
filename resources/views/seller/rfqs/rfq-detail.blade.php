@extends('layout.app')

@section('content')
<div class="right-panel">
   <div class="right-panel-content">
      <form action="{{url('vendor/submit-rfq')}}" method="post">
      @csrf
      <input type="hidden" name="reqForQuote_id" value="{{$rfq->id}}">
      <div class="company-details">
         <div class="co-pro col-md-5">
            <h3>{{\App\Models\Client::find($rfq->client_id)->name}}
               <span>Name: {{$rfq->name}}</span>
               <span>Code: {{$rfq->req_code}}</span>
               @php
                    $terms = \App\Helper::getGeneralTerms();
               @endphp
            </h3>
         </div>

            @php
                $status = \App\Models\ReqForQuote::with(['vendorRfqResponse' => function($query){
                $query->where('vendor_id',Auth::guard()->user()->id);
                }])->where('id',$rfq->id)->first();
                $chk = \App\Models\PurchaseOrder::where('rfq_id',$rfq->id)->first();
                $sel_pt=$sel_dt=$remarks='';
                if(count($status->vendorRfqResponse)){
                    $sel_pt = $status->vendorRfqResponse->first()->credit_term_id;
                    $sel_dt = $status->vendorRfqResponse->first()->delivery_terms;
                    $remarks = $status->vendorRfqResponse->first()->general_remarks;
                }

            @endphp

         <div class="co-pro-form col-md-7">
               <div class="col-md-6">
                  <label>Payment Terms</label>
                  <select name="cred_terms"  id="">
                    @foreach($terms['credit_terms'] as $pay)
                        <option {{ ($sel_pt == $pay->id) ? 'selected' : '' }}  value="{{$pay->id}}">{{$pay->name}}</option>
                    @endforeach
                  </select>
                  <label class="t-m">Delivery Terms</label>
                  <select name="del_terms" id="">
                     @foreach($terms['delievery_terms'] as $del)
                         <option {{ ($sel_dt == $del->id) ? 'selected' : '' }}  value="{{$del->id}}">{{$del->term}}</option>
                     @endforeach
                  </select>
               </div>
               <div class="col-md-6 gen-msg">
                  <label>General Remarks</label>
                  <textarea required name="remarks">{{$remarks}}</textarea>

                  @if($chk)
                      <div class="send-to-supp"><a href="javascript:;">Closed</a></div>
                  @else 
                     @if(!count($status->vendorRfqResponse))                  
                     <input type="submit" value="Generate Quote">
                     @endif
                     <div class="send-to-supp"><a href="javascript:;">Cancel</a></div>
                  @endif                 
               </div>
         </div>
      </div>
      <div class="add-details">
         <div class="content-box-1 rfq-add f-wi has-rfq">
            <h2 class="title gradient">Received RFQs Details</h2>
            <div class="zone-wrapper">
    <div class="zone-scroll">
        <div class="pro-box">
            <div class="box-content supp-rfq-det-main">

                @foreach($rfq->selections->products as $index => $prod)
                @php 
                $dtchk=true;
                if(count($status->vendorRfqResponse))
                    $dtchk = \App\Helper::getVendorDetailsForRFQ($status->vendorRfqResponse, $prod->product_id);
                @endphp
                @if($prod->terms->first() && $dtchk)                
                @php 
                     $product = \App\Models\Product::find($prod->product_id);
 
                     $warranty_terms =  ($prod->terms->first()->warranty_terms);
                     $service_terms  =  ($prod->terms->first()->service_terms);
                     $credit_term_id =  ($prod->terms->first()->credit_term_id);
                     $delivery_terms =  ($prod->terms->first()->delivery_terms);

                     $warranty_terms =  \App\Helper::getVal($terms['warranty_terms'],$warranty_terms)->term;
                     $service_terms =  \App\Helper::getVal($terms['service_terms'],$service_terms)->term;
                     $credit_term_id =  \App\Helper::getVal($terms['credit_terms'],$credit_term_id)->name;
                     $delivery_terms =  \App\Helper::getVal($terms['delievery_terms'],$delivery_terms)->term;
                     $war_pt=$srv_pt=$dt='';    
                     if(count($status->vendorRfqResponse)){
                        $dt = \App\Helper::getVendorDetailsForRFQ($status->vendorRfqResponse, $prod->product_id);
                        $war_pt = $dt->warranty_terms;
                        $srv_pt = $dt->service_terms;
                     }
                
                @endphp
                <div class="srfq-box-main n-d">
                    <div class="srfq-box-inner">
                        <div class="rfq-d-img">
                            <input type="hidden" name="product_ids[]" value="{{$prod->product_id}}">
                            @if($product->image)
                            <img class="imgsize" src="{{asset($product->image)}}" alt="Image">
                            @else
                            <img src="https://via.placeholder.com/101x91.png/ccc" alt="Image">
                            @endif
                            <i class="fa fa-info-circle popup_open" aria-hidden="true"></i>
                            <div class="pop-over-main supp-rfq">
                                <div class="popover-inner">
                                    <div class="title">
                                        <h3>Client Requested Terms</h3>
                                        <i class="fa fa-close"></i>
                                    </div>
                                    <div class="result-search supp-rfq">
                                        <div class="result-content">
                                            <ul>
                                                <li>Warranty Terms <span>{{ $warranty_terms }}</span></li>
                                                <li>Service Terms <span>{{ $service_terms }}</span></li>
                                                <li>Payment Terms <span>{{ $credit_term_id }}</span></li>
                                                <li>Delivery Terms <span>{{ $delivery_terms }}</span></li>
                                            </ul>
                                        </div>
                                    </div>
                            </div>
                            </div>
                        </div>
                        <div class="rfq-d-content">
                            @if(!count($status->vendorRfqResponse))
                            <i data-id="{{$product->id}}" class="removeProd fa fa-close" aria-hidden="true"></i>
                            @endif
                            <h4>Article # {{$product->part_number}}
                                <span>Qty Requested: <num class="req_qty{{$product->id}}">{{$prod->quantity_requested}}</num></span>
                                <input type="hidden" value="0" name="disabled{{$product->id}}">
                            </h4>
                                <div class="f-field">
                                    <label>Unit Price</label>
                                    <input type="number" 
                                    value="{{  ( $dt ) ? $dt->unit_price : 1 }}" 
                                    class="price"  min=1 name="price{{$product->id}}" required>
                                </div>
                                <div class="f-field">
                                    <label>Qty Available</label>
                                    <input type="number"
                                    value="{{  ( $dt ) ? $dt->quantity : 1 }}" 
                                    onchange="this.value = minmax(this.value, 1, {{$prod->quantity_requested}})" 
                                    onkeyup="this.value = minmax(this.value, 1, {{$prod->quantity_requested}})" class="qty"  name="quantity{{$product->id}}" required>                                </div>
                                <div class="f-field">
                                    <label class="f-m">Warranty Terms</label>
                                    <select name="war_terms{{$product->id}}">
                                        @foreach($terms['warranty_terms'] as $war)
                                        <option {{ ($war_pt == $war->id) ? 'selected' : '' }}  value="{{$war->id}}">{{$war->term}}</option>
                                        @endforeach
                                     </select>
                                </div>
                                <div class="f-field">
                                    <label class="f-m">Service Terms</label>
                                    <select name="serv_terms{{$product->id}}">
                                        @foreach($terms['service_terms'] as $sevt)
                                        <option {{ ($srv_pt == $sevt->id) ? 'selected' : '' }}  value="{{$sevt->id}}">{{$sevt->term}}</option>
                                        @endforeach
                                     </select>
                                </div>
                        </div>
                        <div class="pro-det">
                            <p>{{$product->name}}</p>
                            <p>Description: <small>{{$product->description}}</small></p>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach                
            </div>
        </div>
    </div>
</div>

         </div>
      </div>
      </form>
   </div>
</div>


@endsection

@section('js')
<script src="{{asset('js/vendor/rfq-detail.js')}}"></script>
@endsection
