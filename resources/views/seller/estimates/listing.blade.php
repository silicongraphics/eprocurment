@extends('layout.app')
@section('css')
<style>
table .collapse.in {
	display:table-row !important;
}
</style>
@endsection
@section('content')

<div class="right-panel">
        <div class="right-panel-content">
			
			@if(Session::has('success'))
				<div class="alert alert-success">
					<p>{{Session::get('success')}}</p>
				</div>
			@endif
			@if(Session::has('error'))
				<div class="alert alert-danger">
					<p>{{Session::get('error')}}</p>
				</div>
			@endif

			<div class="shows-listing-main" style="display: none;">
				<div class="shows-list-inner">
					<form>
						<label><input type="radio" name="Show Publish RFQ">Show Publish RFQ</label>
						<label><label><input type="radio" name="Show Publish RFQ">Show Semi-Publish RFQ</label>
						<input type="radio" name="Show Publish RFQ">Show Pending RFQ</label>
					</form>
				</div>
			</div>

            <div class="content-parent content-box-1">
                <h2 class="title gradient text-center">Sent Estimates</h2>
                <div class="box-content">
                    <div class="search-h no-padding-full">
						<div class="table-responsive rfq-listing-detils">
							<table class="table table-h">
								<thead>
								<tr>
									<th>RFQ Name</th>
									<th>Requsition #</th>
									<th>RFQ Code</th>
								</tr>
								</thead>
								<tbody>
                                    @foreach($data as $rfq)
                                    <tr>
                                        <td>
                                            <a href="{{ url('vendor/estimate/'.$rfq->req_code) }}">
                                            <i class="fa fa-plus-square"></i></a>
                                            {{ $rfq-> name}}
                                        </td>
                                        <td>{{$rfq->req_number}}</td>
                                        <td>{{$rfq->req_code}}</td>
                                    </tr>
                                    @endforeach	                                  
                                </tbody>
                        </table>
						</div>
						<div class="pagination-h">

						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')

<script>
	$(document).ready(function() {
		$('table tr').click(function() {
			var href = $(this).find("a").attr("href");

			if(href) {
				window.location = href;
			}
		});
	});
</script>	
@endsection