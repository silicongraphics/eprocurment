@extends('layout.app')

@section('content')
<div class="right-panel">
   <div class="right-panel-content">
      <input type="hidden" name="reqForQuote_id" value="{{$rfq->id}}">
      <div class="company-details">
         <div class="co-pro col-md-5">
            <h3>{{\App\Models\Client::find($rfq->client_id)->name}}
               <span>Name: {{$rfq->name}}</span>
               <span>Code: {{$rfq->req_code}}</span>
            </h3>
         </div>
         <div class="co-pro-form col-md-7">
               <div class="col-md-6">
                  <label>Payment Terms</label>
                  <select disabled name="cred_terms"  id="">
                    @foreach($terms['credit_terms'] as $pay)
                        <option {{ ($pay->id == $changes[0]->credit_term_id) ? 'selected' : '' }} value="{{$pay->id}}">{{$pay->name}}</option>
                    @endforeach
                  </select>
                  <label class="t-m">Delivery Terms</label>
                  <select disabled name="del_terms" id="">
                     @foreach($terms['delievery_terms'] as $del)
                         <option {{ ($pay->id == $changes[0]->delivery_terms) ? 'selected' : '' }} value="{{$del->id}}">{{$del->term}}</option>
                     @endforeach
                  </select>
               </div>
               <div class="col-md-6 gen-msg">
                  <label>General Remarks</label>
                  <textarea disabled required name="remarks">{{$changes[0]->general_remarks}}</textarea>
               </div>
         </div>
      </div>
      <div class="add-details">
         <div class="content-box-1 rfq-add">
            <h2 class="title gradient text-center">Client Change of Request on Estimates Sent to Client</h2>
            <div class="box-content supp-rfq-det-main">

               @foreach($changes as $index => $change)
               @php
                    $newprice = $change->unit_price;
                    $newquantity = $change->quantity;
                    $count=0;
                    $product = \App\Models\Product::find($change->product_id);
               @endphp
               <div class="srfq-box-main n-d">
							<div class="srfq-box-inner">
								<div class="rfq-d-img">
                        <input type="hidden" name="product_ids[]" value="{{$change->product_id}}">
                        @if($product->image)
                        <img class="imgsize" src="{{asset($product->image)}}" alt="Image">
                        @else
                        <img src="https://via.placeholder.com/101x91.png/ccc" alt="Image">
                        @endif                        
									<i class="fa fa-info-circle popup_open" aria-hidden="true"></i>
									<div class="pop-over-main supp-rfq">
                           <div class="popover-inner">
                              <div class="title">
                                 <h3>Client amendments</h3>
                                 <i class="fa fa-close"></i>
                              </div>
                              <div class="result-search supp-rfq">
                                 <div class="result-content">
                                    <ul>
                                        @foreach($change->changeOfRequest as $cor)
                                            <p>Edit: {{$cor->created_at}}</p>
                                            <div>
                                                <small><b>Quantity</b>: {{$cor->quantity}}</small>
                                                <br>
                                                <small><b>Reason</b>: {{$cor->reason}}</small>
                                                @if($cor->closed)
                                                   @php
                                                      if($count == 0){
                                                         $newprice = $cor->updated_price;
                                                         $newquantity = $cor->updated_qty;                                                      
                                                         $count++;
                                                      }
                                                   @endphp
                                                    <p>Supplier Response</p>
                                                    <small><b>Upd Quantity</b>: {{$cor->updated_qty}}</small>
                                                    <br>
                                                    <small><b>Upd Price</b>: {{$cor->updated_price}}</small>
                                                @endif
                                            </div>
                                        @endforeach
                                     </ul>
                                 </div>
                              </div>
                           </div>
									</div>
								</div>
								<div class="rfq-d-content">
									<h4>Part # {{$product->part_number}}
                              <span>Offered Quantity: <num class="offered_qty{{$change->id}}">{{$newquantity}}</num></span>
                              <span>Offered Price: <num class="offered_price{{$change->id}}">{{$newprice}}</num></span>
                              <span>{{$product->name}}</span>
                           </h4>
                              <form class="cor" action="{{url('vendor/estimate/update-rfq-terms')}}" method="post">
                              <div class="f-field">
                                 <label>Update Qty</label>
                                 <input type="number" value="{{$newquantity}}" class="qty"  name="quantity" required>
                              </div>
                              <div class="f-field">
                                 <label>Update Price</label>
                                 <input type="number" value="{{$newprice}}" class="price"  min="1" name="price" required>
                              </div>

                              @if(count($change->changeOfRequest))
                              <input type="hidden" name="change_id" value="{{$change->changeOfRequest[0]->id}}">
                              <div class="f-field">
                              <button type="submit" class="btnn">Update</button>
                              </div>    
                              @endif    
                           </form> 									
								</div>
								<div class="pro-det">
                           @php
                              $warranty_terms =  \App\Helper::getVal($terms['warranty_terms'],$change->warranty_terms)->term;
                              $service_terms =  \App\Helper::getVal($terms['service_terms'],$change->service_terms)->term;
                           @endphp
									<p>Warrenty Terms: {{$warranty_terms}}</p>
									<p>Service Terms: {{$service_terms}}</p>
								</div>
							</div>
						</div>
               @endforeach
            </div>
         </div>
      </div>
   </div>
</div>


@endsection

@section('js')
<script src="{{asset('js/vendor/estimate-detail.js')}}"></script>
@endsection