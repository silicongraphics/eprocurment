@extends('layout.app')

@section('content')

<div class="right-panel">
        <div class="right-panel-content">
			<div class="shows-listing-main"  style="display: none;">
				<div class="shows-list-inner">
					<form>
						<label><input type="radio" name="Show Publish RFQ">Show Published RFQs</label>
						<label><input type="radio" name="Show Publish RFQ">Show Semi-Published RFQs</label>
						<label><input type="radio" name="Show Publish RFQ">Show Pending RFQs</label>
					</form>
				</div>
			</div>
            <div class="content-parent content-box-1">
                <h2 class="title gradient text-center">Received Purchase Orders</h2>
                <div class="box-content">
                    <div class="search-h no-padding-full">
						<div class="table-responsive rfq-listing-detils">
							<table class="table table-h">
								<thead>
								<tr>
									<th>PO Number</th>
									<th>Reference</th>
                                    <th>Created at</th>
                                    <th>Client</th>
									<th>Action</th>
									<!-- <th>Status</th> -->
								</tr>
								</thead>
								<tbody>
                                @foreach($pos as $key => $po)
                                @php 
                                    $user = \App\Models\ClientUser::find($po->client_user_id);
                                    $company = $user->company;
                                    /*$status = 'Pending';
									if($po->status == 1)
										$status = 'Approved';*/
								@endphp
                                <tr>
									<td>
                                         <a href="{{ url('vendor/purchase-order-detail/'.$po->po_number) }}">
										 <i class="fa fa-plus-square"></i></a>
                                         PO-{{ $po->id}}
                                    </td>                                    
									<td>{{$po->po_number}}</td>
									<td>{{$po->created_at}}</td>
									<td>{{$company->company_name }}</td>
									<td><div class="par-sm-pop"><i class="fa fa-ellipsis-v" aria-hidden="true"></i>
											<div class="pop-over-main supp-rfq sm-pop-hov">
												<div class="popover-inner">
													<div class="result-search supp-rfq">
														<div class="result-content">
															<ul>
																<li>
																	<a href="{{ url('vendor/purchase-order-detail/'.$po->po_number) }}"><i class="fa fa-eye" aria-hidden="true"></i>
																		View</a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</td>								
                                </tr>
                                @endforeach							
                            </tbody>
                        </table>
						</div>
						<div class="pagination-h">
                            {{ $pos->links() }}

						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
	$(document).ready(function() {
		$('table tr').click(function() {
			var href = $(this).find("a").attr("href");

			if(href) {
				window.location = href;
			}
		});
	});
</script>	
@endsection