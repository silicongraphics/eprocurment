@extends('layout.app')

@section('content')
<div class="right-panel">
        <div class="right-panel-content">
            <div class="content-parent content-box-1 supp-parent">
				<h2 class="title gradient text-center"><span>Supplier Dealing Products</span>
					<div class="btn-h-main"><a href="#" class="popup_open"><i class="fa fa-plus-square"></i> Add New Product to Supplier</a>
						<div class="pop-over-main">
							<div class="popover-inner">
								<div class="title">
									<h3>Add Product to Supplier</h3>
									<i class="fa fa-close"></i>
								</div>
								<div class="search-popover">
									<label>Search Article Number</label>
										<input type="text" id="search" placeholder="Enter Keyword">
									<div class="result-search">
										<label>Results</label>
										<div class="result-heading">
											<ul>
												<li>Name</li>
												<li>Article Number</li>
												<li>Brand</li>
											</ul>
										</div>
										<div class="result-content">
											<ul></ul>
										</div>
										<div class="add-btn">
											<a class="addProd" href="javascript:;"><i class="fa fa-plus-square "></i> Add</a>
										</div>
									</div>
								</div>
								<div class="search-popover no-border">
									<label>Add Product By Manufacture:</label>
									{{\App\Helper::manufacturerList('manu_id')}}
									<div class="result-search">
										<div class="add-btn">
											<a class="addProdM" href="javascript:;"><i class="fa fa-plus-square "></i> Add</a>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</h2>
                <div class="box-content">
                    <div class="search-h no-padding-full">
						<div class="table-responsive rfq-listing-detils">
							<table class="table table-h">
								<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Article Number</th>
									<th>Manufactuter</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
                                @foreach($data->products as $prod)
                                <tr>
									<td>{{$prod->id}}</td>
									<td>{{$prod->name}}</td>
									<td>{{$prod->part_number}}</td>
									<td>{{$prod->manufacturer->name}}</td>
									<td>{{  ($prod->status) ? "Active" : "Inactive" }}</td>
									<td><i class="fa fa-search"></i> <i class="fa fa-trash"></i></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
							<div class="text-center">
								{{$data->products->links()}}
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
@endsection

@section('js')
<script src="{{ asset('js/vendor/listing.js') }}"></script>
@endsection