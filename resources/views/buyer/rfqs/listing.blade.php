@extends('layout.app')

@section('content')

<div class="right-panel">
        <div class="right-panel-content">
			<div class="shows-listing-main">
				<div class="shows-list-inner">
					<form>
						<label><input type="radio" class="filter" data-sel="" name="filter_row">Show All</label>
						<label><input type="radio" class="filter" data-sel="sent" name="filter_row">Show Sent RFQs</label>
						<label><input type="radio" class="filter" data-sel="partially sent" name="filter_row">Show Partially Sent RFQs</label>
						<label><input type="radio" class="filter" data-sel="pending" name="filter_row">Show Pending RFQs</label>
						<label><input type="radio" class="filter" data-sel="closed" name="filter_row">Show Closed RFQs</label>
					</form>
				</div>
			</div>
            <div class="content-parent content-box-1">
                <h2 class="title gradient text-center">My RFQs</h2>
                <div class="box-content">
                    <div class="search-h no-padding-full">
						<div class="table-responsive rfq-listing-detils table-responsive">
							<table class="table table-h">
								<thead>
								<tr>
									<th>RFQ Name</th>
									<th>Requsition #</th>
									<th>RFQ Code</th>
									<th>Product Categories</th>
									<th>No of Suppliers</th>
									<th>No of Items</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								@foreach($rfqs as $rfq)
								@php
									$status = \App\Helper::getRFQclientStatus($rfq->selections->products);                
								@endphp					
                                <tr>
									<td>
                                         <a href="{{ url('client/my-rfq/'.$rfq->req_code) }}">
										 <i class="fa fa-plus-square"></i></a>
                                         {{ $rfq-> name}}
                                    </td>
									<td>{{$rfq->req_number}}</td>
									<td>{{$rfq->req_code}}</td>
									<td>{{	\App\Helper::getCategoryCount($rfq->selections->products) }}</td>
									<td>{{	\App\Helper::countSupplier($rfq->selections->products) }}</td>
									<td>{{ count($rfq->selections->products) }}</td>
									<td>{{$status}}</td>
                               									<td>
										<div class="par-sm-pop"><i class="fa fa-ellipsis-v" aria-hidden="true"></i>
											<div class="pop-over-main supp-rfq sm-pop-hov">
												<div class="popover-inner">
													<div class="result-search supp-rfq">
														<div class="result-content">
															<ul>
																<li>
																	<a href="{{ url('client/my-rfq/'.$rfq->req_code) }}"><i class="fa fa-eye" aria-hidden="true"></i>
																		View</a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
                                @endforeach							
                            </tbody>
                        </table>
						</div>
						<div class="pagination-h">
                            {{ $rfqs->links() }}

						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script src="{{asset('js/client/rfqlisting.js')}}"></script>
<script>
	$(document).ready(function() {
		$('table tr').click(function() {
			var href = $(this).find("a").attr("href");

			if(href) {
				window.location = href;
			}
		});
	});

</script>
@endsection