@extends('layout.app')

@section('content')

<div class="right-panel">
        <div class="right-panel-content">
            <div class="content-parent content-box-1">
                <h2 class="title gradient text-center">My Saved Comparison Tables</h2>
                <div class="box-content">
                    <div class="search-h no-padding-full">
						<div class="table-responsive rfq-listing-detils">
							<table class="table table-h">
								<thead>
								<tr>
									<th>Sno</th>
									<th>Comparison Name</th>
									<th>Created At</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								@foreach($savedrfq as $key => $rfq)					
                                <tr>
									<td>{{ ++$key}}</td>
									<td>{{$rfq->rfq_name}}</td>
									<td>{{$rfq->created_at}}</td>
								<td>
										<div class="par-sm-pop"><i class="fa fa-ellipsis-v" aria-hidden="true"></i>
											<div class="pop-over-main supp-rfq sm-pop-hov">
												<div class="popover-inner">
													<div class="result-search supp-rfq">
														<div class="result-content">
															<ul>
																<li>
																	<a href="{{url('client/create-rfq?q='.$rfq->id)}}"><i class="fa fa-plus" aria-hidden="true"></i>
																		Create RFQ</a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</td>
                                </tr>
                                @endforeach							
                            </tbody>
                        </table>
						</div>
						<div class="pagination-h">
                            {{ $savedrfq->links() }}

						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
$(document).ready(function() {
    $('table tr').click(function() {
        var href = $(this).find("a").attr("href");

        if(href) {
            window.location = href;
        }
    });
});
</script>
@endsection