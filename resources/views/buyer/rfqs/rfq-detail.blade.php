@extends('layout.app')

@section('content')
<div class="right-panel">
        <div class="right-panel-content">
            <div class="rfq-no">{{$rfq->name}}
            </div>
            @php
                $status = \App\Helper::getRFQclientStatus($rfq->selections->products);                
            @endphp

            <label class="rfq-no">Status: {{$status}}</label>

			<div class="company-details">

                  <div class="co-logo">
					<img src="{{ asset('images/co-logo.png') }}" alt="Logo Company">
                </div>
                @php
                    $user = Auth::guard()->user();
                    $company = $user->company;
                @endphp
				<div class="co-details">
					<h3>{{ $user->company_name }}</h3>
                    <ul>
                        <li>P.O.Box: {{$company->billing_pobox}}, {{$company->billing_address}} {{ $company->billing_street}}</li>
						<li>Tel: {{$company->phone}}</li>
						<li>Fax: {{$company->fax}}</li>
						<li>{{$company->website}}</li>
					</ul>
				</div>
			</div>
			<div class="add-details">
				<div class="content-box-1 rfq-add col-md-4 no-padding-left">
					<h2 class="title gradient text-center">Supplier Address</h2>
					<div class="box-content">
						<ul>
							<li><span>Code</span> <supp class="sup_code">   </supp> </li>
							<li><span>Name</span> <supp class="sup_name"> </supp> </li>
							<li><span>Address</span> <supp class="sup_add">   </supp> </li>
							<li><span>Contact Person</span> <supp class="sup_contper">  </supp> </li>
							<li><span>FAX</span> <supp class="sup_fax">  </supp> </li>
							<li><span>Phone</span> <supp class="sup_phone">   </supp> </li>
							<li><span>Email</span> <supp class="supp_email">  </supp> </li>
						</ul>
					</div>
				</div>
				<div class="content-box-1 rfq-add col-md-4 no-padding">
					<h2 class="title gradient text-center">Purchase Query</h2>
					<div class="box-content">
						<ul>
							<li><span>Code</span> {{$rfq->req_code}}</li>
							<li><span>Name</span> {{$company->company_name}}</li>
							<li><span>Address</span> P.O.Box: {{$company->billing_pobox}}, {{$company->billing_address}} {{ $company->billing_street}}</li>
							<li><span>Contact Person</span> {{$company->approval_name}}</li>
							<li><span>FAX</span> {{$company->phone}}</li>
							<li><span>Phone</span> {{$company->fax}}</li>
							<li><span>Email</span> {{$company->email}}</li>
						</ul>
					</div>
				</div>
				<div class="content-box-1 rfq-add col-md-4 no-padding-right">
					<h2 class="title gradient text-center">Shipping Address</h2>
					<div class="box-content">
						<ul>
                        <li><span>Code</span> {{$rfq->req_code}}</li>
							<li><span>Name</span> {{$company->company_name}}</li>
							<li><span>Address</span> P.O.Box: {{$company->shipping_pobox}}, {{$company->shipping_address}} {{ $company->shipping_street}}</li>
							<li><span>Contact Person</span> {{$company->approval_name}}</li>
							<li><span>FAX</span> {{$company->phone}}</li>
							<li><span>Phone</span> {{$company->fax}}</li>
							<li><span>Email</span> {{$company->email}}</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="content-parent content-box-2 ">
				<h2 class="title gradient">Chosen Products</h2>
				<div class="vertical-tabs">
					<ul class="nav nav-tabs" role="tablist">
                        @php
                            $supp_ids = \App\Helper::countSupplier($rfq->selections->products, true);
                            $suppliers = \App\Models\Vendor::with('company')->whereIn('id',$supp_ids)->get();
                        @endphp

                        @foreach($suppliers as $index => $supp)

                        <li class="nav-item  {{  ($index == 0) ? 'active' : '' }} " >
                            @if($index == 0)
							<a onclick="showSupp('{{$supp->custom_id}}', {{json_encode($supp->company)}})" class="nav-link navfirst" data-toggle="tab" href="#tab{{$supp->id}}" role="tab" aria-controls="home" 
                                title="{{$supp->name}}">{{$supp->name}}</a>
                            @else
							<a onclick="showSupp('{{$supp->custom_id}}', {{json_encode($supp->company)}})" class="nav-link" data-toggle="tab" href="#tab{{$supp->id}}" role="tab" aria-controls="home" 
                                title="{{$supp->name}}">{{$supp->name}}</a>
                            @endif
                        </li>
                        @endforeach
                    </ul>

					<div class="tab-content rfq-details-tabs">
                        @foreach($suppliers as $index => $supplier)
                            <div class="tab-pane {{  ($index == 0) ? 'active' : '' }} " id="tab{{$supplier->id}}" role="tabpanel">
							<div class="sv-tab-panel">
								<div class="main-panel">
									<div class="rfq-listing-detils">
										<table class="table table-h">
											<thead>
											<tr>
												<th>Brand</th>
												<th>Name</th>
												<th>Article Number</th>
												<th>Quantity</th>
												<th>Remove</th>
											</tr>
											</thead>
											<tbody>
											@foreach($rfq->selections->products as $prod)
											@php
												$chk = \App\Helper::ifSupplierInArray($prod->terms, $supplier->id);
											@endphp
                                            @if($chk)
                                            @php
                                                $product = \App\Models\Product::find($prod->product_id);
                                                $manuf = $product->manufacturer;
                                            @endphp
   											<tr>
												<td>{{$manuf->name}}</td>
												<td>{{$product->name}}</td>
												<td>{{$product->part_number}}</td>
                                                <td>{{ $prod->quantity_requested }}</td>
                                                @if($status == 'Sent' || $chk->sent_status)
													<td></td>
                                                @else
													<td>
														<a 
														class="rms{{$supplier->id}}"
														onclick="return confirm('Are you sure?')"
														href="{{url('client/rfq/delete-product/'.$chk->id.'/'.$supplier->id )}}">
														<i class="fa fa-close"></i></a>
													</td>

                                                @endif
                                            </tr>
                                            @endif
                                            @endforeach
                                            </tbody>
                                            </table>
                                            @if($status != 'Sent')
											<div class="send-to-supp">
                                                <a data-rfq="{{$rfq->req_code}}" data-supplier="{{$supplier->id}}" 
												class="sendToIndivSupp gry" href="javascript:;">Send to {{$supplier->name}}</a>
                                            </div>
                                            @endif
									</div>
								</div>
							</div>
						</div>
						 @endforeach       
						 @if(!$po)     
                         @if($status != 'Sent')
                           <div class="send-to-supp has-a"><a data-rfq="{{$rfq->req_code}}" class="sendToAllSupp" href="javascript:;">Send to All Suppliers</a></div>
						 @endif
						 @endif
				</div>
			</div>
        </div>
    </div>
</div>


@endsection

@section('js')
<script src="{{asset('js/client/rfq-detail.js')}}"></script>

@endsection
