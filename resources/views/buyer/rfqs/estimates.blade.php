@extends('layout.app')

@section('content')

<div class="right-panel">
        <div class="right-panel-content">
			<div class="shows-listing-main">
				<div class="shows-list-inner">
					<form>
						<input type="radio" class="filter" data-sel="" name="filter_row">Show All
						<input type="radio" class="filter" data-sel="sent" name="filter_row">Show Published RFQs
						<input type="radio" class="filter" data-sel="pending" name="filter_row">Show Pending RFQs
					</form>
				</div>
			</div>
            <div class="content-parent content-box-1">
                <h2 class="title gradient text-center">My Estimates</h2>
                <div class="box-content">
                    <div class="search-h no-padding-full">
						<div class="rfq-listing-detils">
							<table class="table table-h">
								<thead>
								<tr>
									<th>RFQ Name</th>
									<th>Requsition #</th>
									<th>RFQ Code</th>
									<th>Product Categories</th>
									<th>No of Suppliers</th>
									<th>No of Items</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
								</thead>
								<tbody>
							
								@foreach($estimates as $est)
								@php
									$chk = \App\Models\PurchaseOrder::where('rfq_id',$est->id)->first();
								@endphp						
                                <tr>
									<td>
                                        <a href="{{ url('client/my-estimate/'.$est->req_code) }}">
										<i class="fa fa-plus-square"></i></a>
                                        {{ $est-> name}}
                                    </td>
									<td>{{$est->req_number}}</td>
									<td>{{$est->req_code}}</td>
									<td>{{\App\Helper::getCategoryCount($est->selections->products) }}</td>
									<td>{{\App\Helper::countSupplier($est->selections->products) }}</td>
									<td>{{count($est->selections->products) }}</td>
									@if($chk)
										<td>Sent</td>
									@else
										<td>Pending</td>
									@endif
                                                              									<td>
										<div class="par-sm-pop"><i class="fa fa-ellipsis-v" aria-hidden="true"></i>
											<div class="pop-over-main supp-rfq sm-pop-hov">
												<div class="popover-inner">
													<div class="result-search supp-rfq">
														<div class="result-content">
															<ul>
																<li>
																	<a href="{{ url('client/my-estimate/'.$est->req_code) }}"><i class="fa fa-eye" aria-hidden="true"></i>
																		View</a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</td>
                                </tr>
                                @endforeach							
                            </tbody>
                        </table>
						</div>
						<div class="pagination-h">
                            {{ $estimates->links() }}

						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
	<script>	
		$(function() {
			$(".filter").on("change", function() {
				var value = $(this).data('sel').toLowerCase();
				$("table > tbody > tr").filter(function() {     
					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
				});
			});
		});	
	</script>

	<script>
	$(document).ready(function() {
		$('table tr').click(function() {
			var href = $(this).find("a").attr("href");

			if(href) {
				window.location = href;
			}
		});
	});
	</script>	
@endsection