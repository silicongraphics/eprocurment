@extends('layout.app')

@section('content')

<div class="right-panel">
        <div class="right-panel-content estimate-details">
			<div class="send-to-supp">
				<a data-type="1" class="createPo" href="javascript:;">Create PO</a>
				<a data-type="0" class="createPo" href="javascript:;">Save</a></div>
            <div class="content-parent content-box-2 ">
                <h2 class="title gradient">Estimation Table</h2>
                <div class="vertical-tabs">
                    <ul class="nav nav-tabs" role="tablist">
						<input type="hidden" id="rfq_id" value="{{$estimate->id}}">
						@php
							$product_arr = [];
						@endphp
						@foreach($tabs->rfqSelectionTab as $index => $tab)
	                        <li class="nav-item {{ 	(!$index) ? 'active' : '' }}">
								<a class="nav-link" data-toggle="tab" href="#tab{{$index}}" role="tab" aria-controls="home" 
								title="{{$tab->name}}">{{$tab->name}}</a>
                        	</li>
						@endforeach
					
                    </ul>
                    <div class="tab-content">
						@foreach($tabs->RfqSelectionTab as $index => $tab)	
						@php
							$attributes = \App\Models\Product::find($tab->products[0]->product_id)->attributes;
							$attributes = json_decode($attributes);
						@endphp
                        <div class="tab-pane {{ (!$index) ? 'active' : '' }}" id="tab{{$index}}" role="tabpanel">
                            <div class="sv-tab-panel">
                                <div class="main-panel">
                                    <div class="tab-left-details">
                                        <div class="pro-box-left">
                                            <ul>	
												<li>Applications & Attributes</li>
												@foreach($attributes as $attr => $value)			
												<li>{{$attr}}</li>
												@endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-right-details">
                                        <h2>{{$tab->name}}</h2>
                                        <div class="zone-wrapper">
                                            <div class="zone-scroll">
                                                <div class="pro-box">
													@foreach($tab->products as $pro)
													@php
														$pr = \App\Models\Product::find($pro->product_id);
													@endphp
													<div class="pro-box-main">
														<div class="pro-box-inner">
															<div class="image-details-main">
																<div class="image-details">
																	@if($pr->image)
																	<img class="imgsize" src="{{asset($pr->image)}}" alt="Image">
																	@else
																	<img src="https://via.placeholder.com/101x91.png/ccc" alt="Image">
																	@endif																	
																	<i class="fa fa-history popup_open"></i>
																	<div class="pop-over-main supp-rfq">
																		<div class="popover-inner">
																			<div class="title">
																				<h3>Last Order</h3>
																				<i class="fa fa-close"></i>
																			</div>
																			<div class="result-search supp-rfq">
																				<div class="result-content">
																					@php
																						$lastorder = \App\Helper::lastOrder($pr->id);
																					@endphp
																					<ul>
																						@if($lastorder)
																						<li>Name:
																							<span>{{$lastorder->company_name}}</span></li>
																						<li>
																							Quantity:
																							<span>{{$lastorder->quantity}}</span></li>
																						<li>
																							Price:
																							<span>{{number_format($lastorder->price,2)}}</span></li>
																						<li>
																							Date:
																							<span>{{$lastorder->created_at}}</span></li>
																					@endif
																					</ul>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="image-details-content">
																	<h3><span>Part # {{$pr->part_number}}</span>
																		Quantity: {{$pro->quantity_requested}}</h3>
																	<p>{{$pr->name}}</p>
																	<br>
																</div>
															</div>
															<ul>
																	@foreach(json_decode($pr->attributes) as $attr => $value)			
																	<li>{{$value}}</li>
																	@endforeach
															</ul>
																@foreach($pro->terms as $supp)
																@php
																	$supplier = \App\Models\Vendor::find($supp->vendor_id);
																	$terms_history = \App\Helper::getSupplierRespForRFQ($tabs->id, $supp->vendor_id, $pr->id);
																	$count = count($terms_history);
																	$warranty_term =  $terms['warranty_terms'][0]->term;
																	$service_term  =  $terms['service_terms'][0]->term;
																	$credit_term =  $terms['credit_terms'][0]->name;
																	$delivery_term =  $terms['delievery_terms'][0]->term;
																@endphp																
																<div class="feild-supp">
																	<i class="fa fa-comments popup_open"></i>
																	<div class="pop-over-main supp-rfq">
																		<div class="popover-inner">
																			<div class="title">
																				<h3>Terms Offered to Supplier</h3>
																				<i class="fa fa-close"></i>
																			</div>
																			<div class="result-search supp-rfq">
																				<div class="result-content">
																					<ul>
																						<li>Warranty Terms
																							<span>{{$warranty_term}}</span>
																						</li>
																						<li>
																							Service Terms
																							<span>{{$service_term}}</span>
																						</li>
																						<li>
																							Payment Terms
																							<span>{{$credit_term}}</span>
																						</li>																						
																						<li>
																							Delivery Terms
																							<span>{{$delivery_term}}</span>
																						</li>
																					</ul>
																				</div>
																			</div>
																			@if(count($terms_history))
																				@if(!$terms_history->first()->disabled)
																				<div class="title">
																					<h3>Terms Offered By Supplier</h3>
																				</div>
																				<div class="result-search supp-rfq">
																					<div class="result-content">
																						<ul>
																							<li>Warranty Terms:
																							<small>{{ getVal($terms['warranty_terms'], $terms_history->first()->warranty_terms)->term }}</small>
																							</li>
																							<li>
																							Service Terms:
																							<small>{{ getVal($terms['service_terms'], $terms_history->first()->service_terms)->term }}</small>
																							</li>
																							<li>
																							Payment Terms:
																							<small>{{ getVal($terms['credit_terms'], $terms_history->first()->credit_term_id)->name }}</small>
																							</li>																						<li>
																							Delivery Terms:
																							<small>{{ getVal($terms['delievery_terms'], $terms_history->first()->delivery_terms)->term }}</small>
																							</li>
																							</li>																						<li>
																							General Remarks:
																							<small>{{ $terms_history->first()->general_remarks }}</small>
																							</li>	
																								
																																											
																						</ul>
																					</div>
																				</div>
																				@endif
																			@endif
																			<div class="title">
																				<h3>History</h3>
																			</div>
																			<div class="result-search supp-rfq">
																				<div class="result-content">
																					<ul>
																						@foreach($terms_history as $hist)
																						@if(!$hist->disabled)

																						@php
																							$didclientCOR = \App\Helper::checkIfChangeOfRequest($hist->id);
																						@endphp
																						@if(count($didclientCOR))
																							@foreach($didclientCOR as $dc)
																								<li class="darkli">Date:
																									<span>{{$dc->created_at}}</span>
																								</li>
																								<li>Client QTY:
																									<span>{{$dc->quantity}}</span>
																								</li>
																								<li>
																									Reason:
																									<span>{{$dc->reason}}</span>
																								</li>
																								@if($dc->updated_qty)
																								<li>Supplier Response</li>
																								<li>
																									Quantity:
																									<span>{{$dc->updated_qty}}</span>
																								</li>
																								<li>
																									Price:
																									<span>AED {{number_format($dc->updated_price, 2)}}</span>
																								</li>
																								@endif
																							@endforeach
																						@endif
																						@endif
																						@endforeach																						
																					</ul>
																				</div>
																			</div>
																		</div>
																	</div>
																	<label class="est-suppname">{{$supplier->name}}</label>
																	@if($count)
																		@if(!$terms_history->first()->disabled)
																			<i class="fa fa-edit popup_open "></i>
																		@else
																			<i class="fa fa-edit "></i>
																		@endif																
																	@else
																		<i class="fa fa-edit "></i>
																	@endif
																	<div class="pop-over-main supp-rfq edit-pop">
																		<div class="popover-inner">
																			<div class="title no-border">
																				<i class="fa fa-close"></i>
																			</div>
																			<div class="edit-p col-md-6">
																				@if($count)
																				
																				<form action="{{url('client/estimate/change-of-request')}}" class="changeofrequest" method="post">
																				<label>Amend quantity</label>
																				<input type="hidden" name="term" value="{{$terms_history->first()->id}}">
																				<input type="number" name="quantity" value="1">
																				<label class="reason">Reason</label>
																				<input type="text" name="reason" class="reason">
																				<button type="submit" class="btnn">Confirm</button>
																				</form>
																				@endif
																			</div>
																			<div class="edit-p col-md-6">
																				<div class="sup-es">
																					<h3>Supplier Estimates</h3>
																					<a class="downl-se" href="#">Download</a>
																				</div>
																			</div>
																		</div>
																	</div>
																	<input {{  (!$count || $terms_history->first()->disabled ) ? 'disabled' : '' }}
																	type="checkbox" data-supp="{{$supplier->id}}"  class="checkbox-pro" value="{{$supp->id}}" name="selections[]">
																	@if($count)
																		@if(!$terms_history->first()->disabled)
																			@php
																				$didclientCOR = \App\Helper::checkIfChangeOfRequest($terms_history->first()->id);
																			@endphp
																				@if(count($didclientCOR))
																					<label>AED{{number_format($didclientCOR->first()->updated_price,2)}} QTY {{$didclientCOR->first()->updated_qty}}</label>
																				@else
																					<label>AED{{number_format($terms_history->first()->unit_price,2)}} QTY {{$terms_history->first()->quantity}}</label>
																				@endif
																		@endif
																	@endif

																</div>
																@endforeach
														</div>
													</div>
													@endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						@endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('js')
<script src="{{asset('js/client/estimate-detail.js')}}"></script>
@endsection
