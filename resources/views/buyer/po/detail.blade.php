@extends('layout.app')

@section('content')

<div class="right-panel">
        <div class="right-panel-content po-details h-det">
            <div class="content-parent content-box-2 ">

               	@if(Auth::guard('vendor')->check())
               		<h2 class="title gradient">Received Purchase Order</h2>
               	@else
               		<h2 class="title gradient">Purchase Order</h2>
				@endif
                                             
                <div class="vertical-tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        @php
                            $s_id = '';
                            $supp_arr = [];
                            $user = \App\Models\ClientUser::find($po->client_user_id);
                            $company = $user->company;
                            $rfq = \App\Models\ReqForQuote::find($po->rfq_id); 
                        @endphp
                        @foreach($po->detail  as $index => $p)
                        @php
							$supplier = \App\Models\Vendor::with('company')->where('id',$p->supplier)->first();
							$supplier->posendStatus = $p->status;
							$supplier->isSupplierApproved = $p->s_status;
                            $supp_arr[$supplier->id] = $supplier;
                        @endphp
                        @if($s_id != $p->supplier)
                        <li class="nav-item {{$index==0 ? 'active' : ''}}">
                            <a class="nav-link" data-toggle="tab" href="#tab{{$p->supplier}}" role="tab" aria-controls="home" 
                            title="{{$supplier->name}}" aria-expanded="false">{{$supplier->name}}</a>
                        </li>
                        @endif
                        @php
                            $s_id = $p->supplier;
                        @endphp
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @php $i=0; @endphp
                        @foreach($supp_arr as $index => $supp)
                        @php
                            $sc = $supp->company;
                            $sum=0;
							$suppinfo = \App\Models\VendorUser::find($index);
                        @endphp
                        <div class="tab-pane {{$i==0 ? 'active' : ''}}" id="tab{{$index}}" role="tabpanel">
                            <div class="sv-tab-panel">
                                <div class="main-panel">
									<div class="send-to-supp send-po">
										<ul>
											<li><span>PO#{{$po->id}}</span></li>
											<li><span>Reference:</span>{{$po->po_number}}</li>
											<li><span>Project:</span> {{$rfq->name}}</li>
											<li><span>Supplier:</span> {{ $supp->name }}</li>
										</ul>
									
										@if(Auth::guard('vendor')->check())
											@if($supp->isSupplierApproved)
												<a href="javascript:;">Approved</a>
											@else
												<a class="approvePo" data-id="{{$po->id}}" data-supp="{{$index}}" href="javascript:;">Approve PO</a>
											@endif
										@endif
									</div>
									<div class="company-details">
										<div class="co-title">
											<h2>{{ $company->company_name }}</h2>
										</div>
										<div class="co-logo">
											<img src="{{ asset('images/co-logo.png') }}" alt="Logo Company">
										</div>
										<div class="co-details">
											<h3>{{ $company->company_name }}</h3>
											<ul>
                                            <li>P.O.Box: {{$company->billing_pobox}}, {{$company->billing_address}} {{ $company->billing_street}}</li>
                                            <li>Tel: {{$company->phone}}</li>
                                            <li>Fax: {{$company->fax}}</li>
                                            <li>{{$company->website}}</li>
											</ul>
										</div>
									</div>
									<div class="company-details main-parnt no-shadow">
										<div class="content-main-po">
											<div class="con-left">
												<h3>PO No: {{$po->id}}</h3>
												<div class="con-inner-content">
													<ul class="to-po-details">
														<li><span>To:</span> {{ $supp->name }}</li>
														<li><span>Address:</span> {{$sc->billing_pobox}},{{$sc->billing_address}}</li>
														<li><span>Contact Person:</span>{{ $suppinfo->first_name.' '.$suppinfo->last_name }}</li>
													</ul>
													<ul class="left-second-ul">
														<li><span>Tel:</span> {{$sc->phone}}</li>
														<li><span>Fax:</span> {{$sc->fax}}</li>
														<li><span>Email:</span> {{$sc->email}}</li>
													</ul>
												</div>
											</div>
											<div class="con-right">
												<h3></h3>
												<div class="con-parnt-content">
													<div class="con-inner-content">
														<ul class="left-second-ul">
															<li><span>Po Reference #</span> {{$po->po_number}}</li>
															<li><span>RFQ No.</span> {{$rfq->req_code}}</li>
															<li><span>Project</span> {{$rfq->name}}</li>
															<li><span>Requisition No.</span> {{$rfq->req_number}}</li>
															<li><span>Site Area</span> {{$rfq->site_area}}</li>
															<li><span>Date</span> {{$po->created_at}}</li>
															<li><span>Account</span> NAN</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
										<p>Kindly quote your best Price for the Supply of the followings</p>
										<div class="content-table-pro">
											<table class="table table-h">
												<thead>
												<tr>
													<th width="65%">Product Description</th>
													<th>QTY</th>
													<th>Price</th>
													<th>Amount</th>
												</tr>
												</thead>
												<tbody>
                                                @foreach($podetails as $pd)
                                                @if($pd->supplier == $supp->id)
                                                @php
                                                    $res = $pd->quantity*$pd->price;
                                                    $sum += $res;
                                                @endphp
                                                <tr>
													<td>{{$pd->name}}
														<span>Warranty Term: {{ \App\Helper::getVal($warranty_terms, $pd->warranty_term)->term  }} </span>
														<span>Service Term: {{ \App\Helper::getVal($service_terms, $pd->service_term)->term  }}</span>
													</td>
													<td>{{$pd->quantity}}</td>
													<td>{{$pd->price}}</td>
													<td>{{ number_format($res, 2) }}</td>
                                                </tr>
                                                @endif
                                                @endforeach
                                                


											</tbody></table>
										</div>
										<div class="content-main-total">
											<div class="totalcon-left">
												<ul>
													<li><span>Prepared:</span> {{$user->first_name}}</li>
													<li><span>Contact Person:</span> {{$suppinfo->first_name.' '.$suppinfo->last_name}}</li>
													<li><span>Project/Location:</span> {{$rfq->name}}</li>
													<li><span>Delivery:</span> By Supplier</li>
													<!-- <li><span>Delivery Date:</span> 29-November-2018</li> -->
													<li><span>Payment Term:</span> {{ \App\Helper::getVal($credit_terms, $podetails->first()->payment_term)->name  }}</li>
													<li><span>Delivery Term:</span> {{ \App\Helper::getVal($delievery_terms, $podetails->first()->delievery_term)->term  }}</li>
													<li>Invoice for the delivered materials must be presented along with
														delivery note Original LPO should be collected from head office</li>
												</ul>
											</div>
											<div class="totalcon-right">
												<ul>
                                                    <li>Total <span>{{number_format($sum,2)}} AED</span></li>
                                                    @php
                                                        $tax = $sum*5/100;
                                                        $grand = $sum+$tax;
                                                    @endphp
													<li>VAT (5%) <span>{{number_format($tax, 2)}} AED</span></li>
													<li>Gross Total <span>{{number_format($grand, 2)}} AED</span></li>
												</ul>
											</div>
										</div>
										<div class=" content-main-sign">
											<div class="col-md-4 no-padding-left">
												<h4>Approved By (Signature)</h4>
												<div class="signature-box">

												</div>
											</div>
											<div class="col-md-4">
												<h4>Approved By (Signature)</h4>
												<div class="signature-box">
												</div>
											</div>
											<div class="col-md-4 no-padding-right">
												<h4>Approved By (Signature)</h4>
												<div class="signature-box">
												</div>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                        @php $i++; @endphp
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('js')
<script src="{{asset('js/vendor/po-detail.js')}}"></script>
@endsection