@extends('layout.app')

@section('content')

<div class="right-panel">
        <div class="right-panel-content">
			<div class="shows-listing-main" style="display: none;">
				<div class="shows-list-inner">
					<form>
						<label><input type="radio" name="Show Publish RFQ">Show Publish RFQ</label>
						<label><input type="radio" name="Show Publish RFQ">Show Semi-Publish RFQ</label>
						<label><input type="radio" name="Show Publish RFQ">Show Pending RFQ</label>
					</form>
				</div>
			</div>
            <div class="content-parent content-box-1">
                <h2 class="title gradient text-center">My Purchase Orders</h2>
                <div class="box-content">
                    <div class="search-h no-padding-full">
						<div class="table-responsive rfq-listing-detils">
							<table class="table table-h">
								<thead>
								<tr>
									<th>PO Number</th>
									<th>Reference</th>
									<th>Created at</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								@foreach($pos as $key => $po)
								@php 
									$tc = count($po->detail);
									$count = 0;
									foreach($po->detail as $pd):
										if($pd->status == 1)
											$count++;
									endforeach;
									if($count == $tc)
										$status = 'Sent';
									else if($count == 0)
										$status = 'Pending';
									else
										$status = 'Partially Sent';
									
									/*$s_status = 'Pending';
									if($po->status == 1)
										$s_status = 'Approved';*/


									@endphp
                                <tr>
									<td>
                                         <a href="{{ url('client/purchase-order-detail/'.$po->po_number) }}">
										 <i class="fa fa-plus-square"></i></a>
                                         PO-{{ $po->id}}
                                    </td>
									<td>{{$po->po_number}}</td>
									<td>{{$po->created_at}}</td>
									<td>{{$status}}</td>
									<td><div class="par-sm-pop"><i class="fa fa-ellipsis-v" aria-hidden="true"></i>
											<div class="pop-over-main supp-rfq sm-pop-hov">
												<div class="popover-inner">
													<div class="result-search supp-rfq">
														<div class="result-content">
															<ul>
																<li>
																	<a href="{{ url('client/purchase-order-detail/'.$po->po_number) }}"><i class="fa fa-eye" aria-hidden="true"></i>
																		View</a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</td>	
								</tr>
                                @endforeach							
                            </tbody>
                        </table>
						</div>
						<div class="pagination-h">
                            {{ $pos->links() }}

						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
	$(document).ready(function() {
		$('table tr').click(function() {
			var href = $(this).find("a").attr("href");

			if(href) {
				window.location = href;
			}
		});
	});
	</script>	
@endsection