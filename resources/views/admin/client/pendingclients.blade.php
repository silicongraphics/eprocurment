@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body app-page app-page-buyer">
                <!-- Basic form layout section start -->
                <section id="configuration" class="user">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pro-main">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="app-page-header row">
                                            <div class="col-sm-10">
                                                <h1 class="pull-left">Pending Client Companies</h1>
                                            </div>
                                            <div class="col-sm-2">
                                                  <div class="form-actions right">
                                                      <button onclick="window.location.href='eproc-add-application.html'" type="submit" class="btn btn-primary">
                                                        <i class="fa fa-pencil"></i>Edit Columns
                                                      </button>
                                                  </div>
                                            </div>   
                                        </div>
                                    
                                        <div class="clearfix"></div>

                                        <form class="form form-horizontal striped-rows form-bordered">
                                            <div class="form-body">
                                                <div class="row buyer_row">
                                                      <div class="col-sm-8">
                                                        <div class="form-group">
                                                              <label for="issueinput6">Search by Name</label>
                                                              <select id="issueinput6" name="status" class="form-control" data-original-title="" title="">
                                                                <option value="not started">Not Started</option>
                                                                <option value="started">Started</option>
                                                                <option value="fixed">Fixed</option>
                                                              </select>
                                                        </div>
                                                      </div>
                                                      <div class="col-sm-4">
                                                          <div class="form-actions right">
                                                              <button type="submit" class="btn btn-primary">
                                                                <i class=""></i> Search
                                                              </button>
                                                          </div>
                                                      </div>
                                                </div>

                                            </div>
                                        </form>
                                        <div class="clearfix"></div>

                                        <div class="maain-tabble">
                                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="DataTables_Table_0"></label></div></div></div><div class="row"><div class="col-sm-12"><table class="table table-striped table-bordered zero-configuration dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                              <thead>
                                                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Job ID: activate to sort column descending" style="width: 94px;">ID</th><th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Jobs Title: activate to sort column ascending" style="width: 212px;">Title</th><th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="State: activate to sort column ascending" style="width: 105px;">Display Name</th><th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Category: activate to sort column ascending" style="width: 136px;">Company Name</th><th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Creation Date: activate to sort column ascending" style="width: 186px;">User Group</th><th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Creation Date: activate to sort column ascending" style="width: 186px;">Email</th><th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Creation Date: activate to sort column ascending" style="width: 186px;">Mobile No</th><th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Creation Date: activate to sort column ascending" style="width: 186px;">Registered on</th><th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Creation Date: activate to sort column ascending" style="width: 186px;">Status</th><th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 120px;">Actions</th></tr>
                                              </thead>
                                              <tbody>
                                                
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                              <tr role="row" class="odd">
                                                  <td class="sorting_1">5</td>
                                                  <td>Mr.</td>
                                                  <td>Test Test Vendor</td>
                                                  <td>ABC Company</td>
                                                  <td>Company Admin</td>
                                                  <td>testvendor@test.com</td>
                                                  <td>+971 (56) 333333</td>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                  <td><i class="fa fa-exclamation" aria-hidden="true"></i></td>
                                                  <td>
                                                     <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn  btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-ellipsis-v"></i>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="top-start" style="position: absolute;    transform: translate3d(0px, -99px, 0px);top: 0px;left: 0px;will-change: transform;"> <a class="dropdown-item" href="#"><i class="fa fa-eye"></i> View User</a> <a class="dropdown-item" href="#"><i class="fa fa-pencil"></i> Edit User</a> <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Delete User</a> </div>
                                                    </div>
                                                  </td>
                                              </tr>
                                            </tbody>
                                            </table></div></div></div>
                                          </div>
                                    </div>
                                </div>
                                <!--main card end-->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection