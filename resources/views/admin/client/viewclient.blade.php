@extends('admin.layout.master')
@section('title')
  Client - View
@endsection
@section('content')
	<div class="app-content content view-only product-page edit_client view_buyer app-page">
	<div class="content-wrapper">
	<div class="content-body ">
	<!-- Basic form layout section start -->
	<section id="configuration" class="user">
	<div class="row">
	<div class="col-12">
	<div class="card rounded pro-main">
	<div class="card-content collapse show">
	<div class="card-body table-responsive card-dashboard">
	<div class="row">
	<div class="col-sm-6">
	<h1 class="pull-left">View Buyer Company</h1>
	</div>
	<div class="col-sm-2">
	<div class="form-actions right">
	<button onclick="window.location.href='eproc-edit-product.html'" type="submit" class="btn btn-primary">
	<i class="fa fa-eye"></i>View Client RFQS
	</button>
	</div>
	</div>
	<div class="col-sm-2">
	<div class="form-actions right">
	<button onclick="window.location.href='{{ route('addclient') }}'" type="submit" class="btn btn-primary">
	<i class="fa fa-plus"></i>New Company User
	</button>
	</div>
	</div>
	<div class="col-sm-2">
	<div class="form-actions right">
	<button onclick="window.location.href='{{ route('client.edit', $id) }}'" type="submit" class="btn btn-primary">
	<i class="fa fa-pencil"></i>Edit Client
	</button>
	</div>
	</div>
	</div>
	<form class="form">
	<div class="row">
	<div class="col-sm-12 card-body2">
	<div class="form-group">
	<label for="userinput5">Company Name</label>
	<div class="profile_image">

	</div>
	</div>
	<div class="profile_img">

	</div>
	</div>
	<div class="col-sm-12 card-body2">
	<div class="form-group">
	<label for="issueinput6">Documents</label>
	<div class="custom-file">
	<input type="file" class="custom-file-input" id="inputGroupFile01">

	<label class="custom-file-label-doc" for="inputGroupFile01">Documents</label>
	</div>
	</div>
	</div>
	</div>
	</form>
	<div class="clearfix"></div>
	<form class="form billing_form">
	<div class="form-body">
	<div class="row">
	<div class="col-sm-12">
	<div class="col-sm-2 edit_client_btn">
	<div class="form-actions right">
	<button onclick="window.location.href='{{ route('client.edit', $id) }}'" type="submit" class="btn btn-primary">
	<i class="fa fa-pencil"></i>Edit Client
	</button>
	</div>
	</div>
	<h4 class="form-section">Company Admin Details</h4>

	<section class="grid-with-inline-row-label " id="grid-with-inline-row-label">
	<div class="row">
	<div class="col-6">
	<div class="card card2">
	<div class="card-content collapse show">
	<div class="card-body card-body2">
	<form action="#">
	<div class="form-body">
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Id</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $custom_id }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Name</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $client->company_name }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>

	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Work Number</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $client->phone }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">BILLING ADDRESS</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<div class="form-control pro_ul">
	<ul>
	<li>Address: {{ $client->billing_address }}</li>
	<li>Street: {{ $client->billing_street }}</li>
	<li>PoBox Number:  {{ $client->billing_pobox }}</li>
	<li>Country: {{ $billing_country }}</li>
	<li>City: {{ $billing_city }}</li>
	</ul>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	
	</div>
	</form>
	</div>
	</div>
	</div>
	</div>

	<div class="col-lg-6">

	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Primary Email</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $client->email }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Mobile No</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $client->mobile_no }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Website</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $client->website }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">SHIPPING ADDRESS</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<div class="form-control pro_ul">
	<ul>
	<li>Address: {{ $client->shipping_address }}</li>
	<li>Street: {{ $client->shipping_street }}</li>
	<li>PoBox Number:  {{ $client->shipping_pobox }}</li>
	<li>Country: {{ $shipping_country }}</li>
	<li>City: {{ $shipping_city }}</li>
	</ul>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	
	</div>
	</form>
	</div>
	</div>
	</div>
	</div>
	</div>

	</section>

	<h4 class="form-section">Client Company Details</h4>

	<section class="grid-with-inline-row-label " id="grid-with-inline-row-label">
	<div class="row">
	<div class="col-6">
	<div class="card card2">
	<div class="card-content collapse show">
	<div class="card-body card-body2">
	<form action="#">
	<div class="form-body">
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Id</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">First Name</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Job Title</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Primary Email</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</form>
	</div>
	</div>
	</div>
	</div>

	<div class="col-6">
	<div class="card card2">
	<div class="card-content collapse show">
	<div class="card-body card-body2">
	<form action="#">
	<div class="form-body">
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Id</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">First Name</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Job Title</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Primary Email</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>

	</div>
	</form>
	</div>
	</div>
	</div>
	</div>
	</div>
	</section>
	<h4 class="form-section">Related Users</h4>
    <div class="maain-tabble">
	<div class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="DataTables_Table_0"></label></div></div></div><div class="row"><div class="col-sm-12">
	<table id="client_users" class="table table-striped table-bordered zero-configuration dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
	<thead>
	<tr role="row">
	<th style="width: 94px;">ID</th>
	<th style="width: 212px;">First Name</th>
	<th style="width: 105px;">Last Name</th>
	<th style="width: 136px;">Primary Email</th>
	<th style="width: 186px;">Status</th>
	<th style="width: 120px;">Actions</th></tr>
	</thead>
	<tbody>
	</tbody>
	</table></div></div></div>
	</div>
	</div>
	</div>

	</div>

	</form>

	</div>
	</div>
	<!--main card end-->

	</div>
	</div>
	</div>
	</section>
	<!-- // Basic form layout section end -->
	</div>
	</div>
	</div>
@endsection
@section('js')
<script>
   var oTable = '';
$(function() {
     oTable = $('#client_users').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
           url: '{{ route("users.get") }}',
           data: {
               "client_id": <?php echo $id ?>
           },
           type: 'post',
          },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'first_name', name: 'first_name' },
            { data: 'last_name', name: 'last_name' },
            { data: 'email', name: 'email' },
            { data: 'active', name: 'active' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ]
    });
});

</script>
@endsection