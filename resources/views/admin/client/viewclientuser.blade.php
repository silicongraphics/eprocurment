  @extends('admin.layout.master')
  @section('title')
    Buyer User View 
  @endsection
  @section('content')
	<div class="app-content content view-only product-page edit_client view_buyer app-page">
	<div class="content-wrapper">
	<div class="content-body ">
	<!-- Basic form layout section start -->
	<section id="configuration" class="user">
	<div class="row">
	<div class="col-12">
	<div class="card rounded pro-main">
	<div class="card-content collapse show">
	<div class="card-body table-responsive card-dashboard">
	<div class="row">
	<div class="col-sm-10">
	<h1 class="pull-left">View Buyer Company User</h1>
	</div>
	<div class="col-sm-2">
	<div class="form-actions right">
	<button onclick="window.location.href='{{ route('editclientuser', $id) }}'" type="submit" class="btn btn-primary">
	<i class="fa fa-pencil"></i>Edit User
	</button>
	</div>
	</div>
	</div>
	<form class="form">
	<div class="row">
	<div class="col-sm-12 card-body2">
	<div class="form-group">
	<label for="userinput5">Company Name: {{ $company_name }}</label>
	<div class="profile_image">

	</div>
	</div>
	<div class="profile_img">
	@if(file_exists(public_path() . '/document/client/users/' . $id . '/' . $clientuser->profile_picture))
       <img src="{{ asset('/document/client/users/' . $id . '/' . $clientuser->profile_picture) }}" style="width: 100%;
    height: 100%;">
    @else
        <img src="{{ asset('admin-assets/images/placeholder.png') }}" style="width: 100%;
    height: 100%;">
    @endif
	</div>
	</div>
	<div class="col-sm-12 card-body2">
	<div class="form-group">
	<label for="issueinput6">Documents</label>
	<div class="custom-file">
	<input type="file" class="custom-file-input" id="inputGroupFile01">

	<label class="custom-file-label-doc" for="inputGroupFile01">Documents</label>
	</div>
	</div>
	</div>
	</div>
	</form>
	<div class="clearfix"></div>
	<form class="form billing_form">
	<div class="form-body">
	<div class="row">
	<div class="col-sm-12">
	<h4 class="form-section">Client User Details</h4>

	<section class="grid-with-inline-row-label " id="grid-with-inline-row-label">
	<div class="row">
	<div class="col-6">
	<div class="card card2">
	<div class="card-content collapse show">
	<div class="card-body card-body2">
	<form action="#">
	<div class="form-body">
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Id</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->id }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Title</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->title }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Last Name</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->last_name }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>

	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Primary Email</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->email }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>

	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Fax Number</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->fax }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Emirates Id</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<a href="{{ asset('/document/client/users/' . $id . '/' . $clientuser->emirates_id) }}" download style="width:100%">
	<input type="text" class="form-control" placeholder="Download: {{ $clientuser->emirates_id }}" readonly="" style="cursor: pointer">
    </a>
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Verified</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ ($clientuser->verified) ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-cross" aria-hidden="true"></i>' }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Created At</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->created_at }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
    
    <div class="form-group">
	<div class="row">
	<label class="col-lg-4">Notes</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->notes }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>

	</div>
	</form>
	</div>
	</div>
	</div>
	</div>

	<div class="col-6">
	<div class="card card2">
	<div class="card-content collapse show">
	<div class="card-body card-body2">
	<form action="#">
	<div class="form-body">
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Comapany Name</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $company_name }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">First Name</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->first_name }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Job Title</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->job_title }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Mobile No</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->mobile_no }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Work Number</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->phone }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Website</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->website }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	

	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Status</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ ($clientuser->active) ? 'Active' : 'In-Active' }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Updated At</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $clientuser->updated_at }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</form>
	</div>
	</div>
	</div>
	</div>
	</div>

	</section>

	</div>
	</div>

	</div>

	</form>

	</div>
	</div>
	<!--main card end-->

	</div>
	</div>
	</div>
	</section>
	<!-- // Basic form layout section end -->
	</div>
	</div>
	</div>
  @endsection