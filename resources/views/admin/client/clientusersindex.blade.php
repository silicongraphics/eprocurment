@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
           @if(Session::has('success'))
          <div class="alert alert-success">{{ Session::get('success') }}</div>
         @endif
            <div class="content-body app-page app-page-buyer">
                <!-- Basic form layout section start -->
                <section id="configuration" class="user">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pro-main">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="app-page-header row" style="margin-bottom: 0px;">
                                            <div class="col-sm-10">
                                                <h1 class="pull-left">Client Users</h1>
                                            </div>
                                            <div class="col-sm-2">
                                                  <div class="form-actions right">
                                                      <button onclick="window.location.href='#'" type="submit" class="btn btn-primary">
                                                        <i class="fa fa-pencil"></i>Edit Columns
                                                      </button>
                                                  </div>
                                            </div>   
                                        </div>

                                        <div class="app-page-header row">
                                            <div class="col-sm-10">
                                            </div>
                                            <div class="col-sm-2">
                                                  <div class="form-actions right">
                                                      <button onclick="window.location.href='{{ route('clientuser') }}'" type="submit" class="btn btn-primary">
                                                        <i class="fa fa-plus-circle"></i>Add New User
                                                      </button>
                                                  </div>
                                            </div>   
                                        </div>
                                    
                                        <div class="clearfix"></div>

                                        <form method="post" id="search-form" class="form form-horizontal striped-rows form-bordered">
                                            <div class="form-body">
                                                <div class="row buyer_row">
                                                      <div class="col-sm-8">
                                                        <div class="form-group">
                                                              <label for="issueinput6">Search by Name</label>
                                                              <input type="text" id="issueinput6" name="name" class="form-control" data-original-title="" title="" />
                                                        </div>
                                                      </div>
                                                      <div class="col-sm-4">
                                                          <div class="form-actions right">
                                                              <button type="submit" class="btn btn-primary">
                                                                <i class=""></i> Search
                                                              </button>
                                                          </div>
                                                      </div>
                                                </div>

                                            </div>
                                        </form>
                                        <div class="clearfix"></div>
                                       <table class="table table-striped table-bordered zero-configuration dataTable no-footer" id="client_users" role="grid" aria-describedby="DataTables_Table_0_info">
                                              <thead>
                                                <tr role="row">
                                                  <th>ID</th>
                                                  <th>Title</th>
                                                  <th>Display Name</th>
                                                  <th>Company Name</th>
                                                  <th>Client Name</th>
                                                  <th>Email</th>
                                                  <th>Mobile No</th>
                                                  <th>Verified</th>
                                                  <th>Status</th>
                                                  <th>Actions</th></tr>
                                              </thead>
                                            </table></div></div></div>
                                          </div>
                                    </div>
                                </div>
                                <!--main card end-->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
   var oTable = '';
$(function() {
     oTable = $('#client_users').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
           url: '{{ route("getclientusers") }}',
           type: 'post',
           data: function (d) {
                d.name = $('input[name=name]').val();
            }
          },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'title', name: 'title' },
            { data: 'first_name', name: 'first_name' },
            { data: 'vendors', name: 'vendors' },
            { data: 'vendors', name: 'vendors' },
            { data: 'email', name: 'email' },
            { data: 'mobile_no', name: 'mobile_no' },
            { data: 'verified', name: 'verified' },
            { data: 'active', name: 'active' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ]
    });
});
$('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
});
</script>
@endsection