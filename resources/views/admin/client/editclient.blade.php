  @extends('admin.layout.master')
  @section('title')
     Administration - Edit Client
  @endsection
  @section('content')
  <!-- Edit Client -->
  <div class="app-content content">
  <div class="content-wrapper">
  <div class="content-body product-page edit_client">
  <!-- Basic form layout section start -->
  <section id="configuration" class="user">
  <div class="row">
  <div class="col-12">
  <div class="card rounded pro-main">
  <div class="card-content collapse show">
  <div class="card-body table-responsive card-dashboard">
  <div class="row">
  <div class="col-12">
  <h1 class="pull-left">Edit Client</h1>
   @if ($errors->any())
  <div class="alert alert-danger">
  <ul>
   @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
   @endforeach
  </ul>
  </div>
  @endif
  </div>

  </div>
  <div class="clearfix"></div>
  <form id="ClientEditForm" class="form" method="post" action="{{ route('client.update', $id) }}" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="form-body">
  <div class="row">
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Company Name</label>
  <input class="form-control" type="text" name="name" value="{{ $client->company_name }}" placeholder="" id="company_name" required>
  </div>
  </div> 
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Website</label>
  <input class="form-control" type="text" name="website" value="{{ $client->website }}" placeholder="" id="website" required>
  </div>
  </div>
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Tax Registration No</label>
  <input class="form-control" type="number" name="tax_registration_no" value="{{ $client->tax_registration_no }}" placeholder="Tax Registration No" id="tax_registration" required>
  </div>
  </div>
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Delivery Method</label>
  <select id="delivery_method" name="delivery_method_id" class="form-control" data-original-title="" title="">
  <option value="">Select Delivery Method</option>
  @foreach($delivery_methods as $delivery_method)
  <option value="{{ $delivery_method->id }}"> {{ $delivery_method->name }} </option>
  @endforeach               
  </select>
  </div>
  </div>
  </div>

  <div class="row">

  <div class="col-sm-3">
  <div class="form-group">
  <label for="issueinput6">Preffered Brands</label>
  <select id="prefered_brand" name="manufacturers[]" multiple="multiple" class="form-control" data-original-title="" title="">
  <option value="">Select Prefered Brand</option>
  @foreach($brands as $brand)
  <option value="{{ $brand->id }}"> {{ $brand->name }} </option>
  @endforeach               
  </select>
  </div>
  </div>
  <div class="col-sm-3">
  <div class="form-group">
  <label for="issueinput6">Preffered Suppliers</label>
  <select id="prefered_supplier" name="vendors[]" multiple="multiple" class="form-control" data-original-title="" title="">
  <option value=""></option>
  @foreach($suppliers as $supplier)
  <option value="{{ $supplier->id }}"> {{ $supplier->name }} </option>
  @endforeach 
  </select>
  </div>
  </div>
  <div class="col-sm-3">
  <div class="form-group">
  <label for="issueinput6">Payment Terms</label>
  <select id="payment_term" name="credit_term_id" class="form-control" data-original-title="" title="">
  <option value=""></option>
  @foreach($payment_terms as $payment_term)
  <option value="{{ $payment_term->id }}"> {{ $payment_term->name }} </option>
  @endforeach 
  </select>
  </div>
  </div>
  <div class="col-sm-3">
  <div class="form-group">
  <label for="issueinput6">Payment Method</label>
  <select id="payment_method" name="payment_method_id" class="form-control" data-original-title="" title="">
  <option value=""></option>
  @foreach($payment_methods as $payment_method)
  <option value="{{ $payment_method->id }}"> {{ $payment_method->name }} </option>
  @endforeach 
  </select>
  </div>
  </div>
  </div>

  <div class="row">

  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Title</label>
  <select id="title" name="title" class="form-control" data-original-title="" title="" required>
  <option value="">Select title</option>
  <option {{ ($clientuser) ? ($clientuser->title == 'Mr.') ? 'selected=selected' : '' : '' }} value="Mr.">Mr</option>
  <option {{ ($clientuser) ? ($clientuser->title == 'Ms.') ? 'selected=selected' : '' : '' }} value="Ms.">Ms</option>
  <option {{ ($clientuser) ? ($clientuser->title == 'Mrs.') ? 'selected=selected' : '' : '' }} value="Mrs.">Mrs</option>
  <option {{ ($clientuser) ? ($clientuser->title == 'Miss') ? 'selected=selected' : '' : '' }} value="Miss">Miss</option>
  </select>
  </div>
  </div> 

  <div class="col-sm-3">
  <div class="form-group">
  <label for="first name">First Name</label>
  <input class="form-control" type="text" value="{{ ($clientuser) ? $clientuser->first_name : '' }}" name="first_name" placeholder="" id="first_name">
  </div>
  </div>

  <div class="col-sm-3">
  <div class="form-group">
  <label for="Last name">Last Name</label>
  <input class="form-control" type="text" value="{{ ($clientuser) ? $clientuser->last_name : '' }}" name="last_name" placeholder="" id="last_name">
  </div>
  </div>

  <div class="col-sm-3">
  <div class="form-group">
  <label for="job title">Job Title</label>
  <input class="form-control" type="text" placeholder="" name="job_title" value="{{ ($clientuser) ? $clientuser->job_title : '' }}" id="job_title">
  </div>
  </div> 

  </div>

  <div class="row">
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Primary Email</label>
  <input class="form-control" type="email" placeholder="" name="email" value="{{ ($clientuser) ? $clientuser->email : '' }}" id="email" required>
  </div>
  </div> 

  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Mobile Number</label>
  <input class="form-control" type="number" value="{{ ($clientuser) ? $clientuser->mobile_no : '' }}" name="mobile_no" placeholder="" id="mobile_no" required="required">
  </div>
  </div>

  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Work Number</label>
  <input class="form-control" type="number" value="{{ ($clientuser) ? $clientuser->phone : '' }}" name="phone" placeholder="" id="phone">
  </div>
  </div> 

  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Fax Number</label>
  <input class="form-control" type="number" name="fax" value="{{ ($clientuser) ? $clientuser->fax : '' }}" placeholder="" id="fax">
  </div>
  </div> 

  </div>

  <div class="row">

  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">New Password</label>
  <input class="form-control" type="password" placeholder="" name="password" value="" id="password" @if(!$clientuser) required @endif>
  <label class="label-note" for="userinput6">Minimum Password length must be 6 characters.</label>
  </div>
  </div> 

  </div>

  <div class="form billing_form">
  <div class="form-body">
  <div class="row">
  <div class="col-sm-6">
  <h4 class="form-section">Billing Address</h4>
  <div class="row">
  <div class="col-md-12">
  <div class="form-group">
  <label for="projectinput1">Address</label>
  <input type="text" id="projectinput1" class="form-control billing_address" value="{{ $client->billing_address }}" placeholder="Billing Address" name="billing_address">
  </div>
  <div class="form-group">
  <label for="projectinput3">Street</label>
  <input type="text" id="projectinput3" class="form-control billing_street" value="{{ $client->billing_street }}" placeholder="Billing Street" name="billing_street">
  </div>
  <div class="form-group">
  <label for="projectinput3">PoBox</label>
  <input type="text" id="projectinput3" class="form-control billing_pobox" value="{{ $client->billing_pobox }}" placeholder="Billing PO Box" name="billing_pobox">
  </div>
  <div class="form-group">
  <label for="issueinput6">Country</label>
  <select id="billing_country" name="billing_country_id" class="form-control billing_country_id" data-original-title="" title="">
  <option value="">Country</option>
  @foreach($countries as $country)
  <option {{ ($country->id == $client->billing_country_id) ? 'selected=selected' : '' }} value="{{ $country->id }}"> {{ $country->name }}</option>
  @endforeach
  </select>
  </div>
  <div class="form-group">
  <label for="issueinput6">City</label>
  <select id="billing_city" name="billing_city_id" class="form-control billing_city_id" data-original-title="" title="">
  <option value="">City</option>
  @foreach($cities as $city)
  <option {{ ($city->id == $client->billing_city_id) ? 'selected=selected' : '' }} value="{{ $city->id }}"> {{ $city->name }}</option>
  @endforeach
  </select>
  </div>
  </div>
  </div>
  </div>
  <div class="col-sm-6">
  <h4 class="form-section">Shipping Address</h4>
  <div class="row">
  <div class="col-md-12">
  <div class="form-group">
  <fieldset class="checkboxsas">
  <label class="label-note">
  <input type="checkbox" id="" class="same_addresses" value="">
  Same as Billing Address </label>
  </fieldset>
  <label for="projectinput1">Address</label>
  <input type="text" id="projectinput1" class="form-control shipping_address" value="{{ $client->shipping_address }}" placeholder="First Name" name="shipping_address">
  </div>
  <div class="form-group">
  <label for="projectinput3">Street</label>
  <input type="text" id="projectinput3" class="form-control shipping_street" value="{{ $client->shipping_street }}" placeholder="Shipping Street" name="shipping_street">
  </div>
  <div class="form-group">
  <label for="projectinput3">PoBox</label>
  <input type="text" id="projectinput3" class="form-control shipping_pobox" value="{{ $client->shipping_pobox }}" placeholder="Shipping PoBox" name="shipping_pobox">
  </div>
  <div class="form-group">
  <label for="issueinput6">Country</label>
  <select id="shipping_country" name="shipping_country_id" class="form-control shipping_country_id" data-original-title="" title="">
  <option value="">Country</option>
  @foreach($countries as $country)
  <option {{ ($country->id == $client->shipping_country_id) ? 'selected=selected' : '' }} value="{{ $country->id }}"> {{ $country->name }}</option>
  @endforeach
  </select>
  </div>
  <div class="form-group">
  <label for="issueinput6">City</label>
  <select id="shipping_city" name="shipping_city_id" class="form-control shipping_city_id" data-original-title="" title="">
  <option value="">City</option>
  @foreach($cities as $city)
  <option {{ ($city->id == $client->shipping_city_id) ? 'selected=selected' : '' }} value="{{ $city->id }}"> {{ $city->name }}</option>
  @endforeach
  </select>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>

  <div class="form">
  <div class="form-body">
  <div class="row">
  <div class="col-sm-4">
  <div class="form-group">
  <label for="userinput5">Notes</label>
  <input class="form-control" type="text" name="notes" value="{{ $client->notes }}" placeholder="" id="notes">
  </div>
  </div>
  <div class="col-sm-4"> 
  <div class="form-group">
  <label for="issueinput6">Trade Liscense</label>
  <div class="custom-file">
  <input type="file" class="custom-file-input" name="trade_liscense" id="inputGroupFile01" placeholder="No File Chosen" @if(!$client->trade_license) required @endif>
  <input type="hidden" name="doctype[]" value="2" />
  <div class="input-group-prepend">
  <span class="input-group-text">Choose File</span>
  </div> 
  </div>
  @if($client->trade_license)
  <a href="{{ asset('/document/client/' . $id . '/' . $client->trade_license) }}" download> Download File: {{ $client->trade_license }}</a>
  @endif
  </div>
  </div>
  <div class="col-sm-4"> 
  <div class="form-group">
  <label for="issueinput6">VAT Certificate</label>
  <div class="custom-file">
  <input type="file" class="custom-file-input" name="vat_certificate" placeholder="No File Chosen" id="inputGroupFile03" @if(!$client->vat_certificate) required @endif>
  <input type="hidden" name="doctype[]" value="4" />
  <div class="input-group-prepend">
  <span class="input-group-text">Choose File</span>
  </div> 
  @if($client->vat_certificate)
  <a href="{{ asset('/document/client/' . $id . '/' . $client->vat_certificate) }}" download> Download File: {{ $client->vat_certificate }}</a>
  @endif
  </div>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="col-sm-6"> 
  <div class="form-group">
  <label for="issueinput6">Profile Picture</label>
  <div class="custom-file">
  <input type="file" name="profile_picture" class="custom-file-input" placeholder="No File Chosen" id="profile_picture" @if($clientuser) @if(!$clientuser->profile_picture) required @endif @endif> 
  <div class="input-group-prepend">
  <span class="input-group-text">Choose File</span>
  </div> 
  @if(($clientuser) && $clientuser->profile_picture)
  <a href="{{ asset('/document/client/users/' . $id . '/' . $clientuser->profile_picture) }}" download> Download File: {{ $clientuser->profile_picture }}</a>
  @endif
  </div>
  </div>
  </div>
  <div class="col-sm-6"> 
  <div class="form-group">
  <label for="issueinput6">Emirates Id</label>
  <div class="custom-file">
  <input type="file" name="emirates_id_user" placeholder="No File Chosen" class="custom-file-input" id="emirates_id_user" @if($clientuser) @if(!$clientuser->emirates_id) required @endif @endif>
  <div class="input-group-prepend">
  <span class="input-group-text">Choose File</span>
  </div> 
  @if(($clientuser) && $clientuser->emirates_id)
  <a href="{{ asset('/document/client/users/' . $id . '/' . $clientuser->emirates_id) }}" download> Download File: {{ 
  $clientuser->emirates_id }}</a>
  @endif
  </div>
  </div>
  </div>
  </div>

  </div>
  </div>

  <div class="form billing_form">
  <div class="form-body">
  <div class="row">
  <div class="col-sm-12">
  <h4 class="form-section">Additional Approver For Purchase Order</h4>
  <div class="row">
  <div class="col-md-6">
  <div class="form-group">
  <label for="projectinput1">Approver Name</label>
  <input type="text" id="projectinput1" name="approval_name" value="{{ $client->approval_name }}" class="form-control" placeholder="First Name" name="fname">
  </div>
  </div>
  <div class="col-sm-6">
  <div class="form-group">
  <label for="issueinput6">Approver Emirates Id</label>
  <div class="custom-file">
  <input type="file" class="custom-file-input" name="emirates_id" placeholder="No File Chosen" id="inputGroupFile03"  @if(!$client->emirate_id) required @endif>
  <input type="hidden" name="doctype[]" value="3" />
  <div class="input-group-prepend"> <span class="input-group-text">Choose File</span> </div>
  @if($client->emirates_id)
  <a href="{{ asset('/document/client/' . $id . '/' . $client->emirates_id) }}" download> Download File: {{ 
  $client->emirates_id }}</a>
  @endif
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>

  <div class="row">
  <div class="col-sm-6">
  <div class="form-group">
  <label class="status_label" for="issueinput6">Status</label>
  <fieldset class="checkboxsas">
  <label class="label-note">
  <input type="checkbox" value="1" name="verified" {{ ($client->verified == 1) ? 'checked=checked' : '' }}>
  Verified </label>
  </fieldset>
  <select id="status" name="status" class="form-control" data-original-title="" title="" required>
  <option value="">Select Status</option>
  <option {{ ($client->status == 1) ? 'selected=selected' : '' }}  value="1">Active</option>
  <option {{ ($client->status == 0) ? 'selected=selected' : '' }} value="0">In-Active</option>
  </select>
  </div>
  </div>
  <div class="col-sm-6">
  <div class="form-actions right">
  <button id="save" type="submit" class="btn btn-primary"> <i class=""></i> Save </button>
  </div>
  </div>
  </div>

  </div>
  </div>
   </form>
  <!--main card end-->

  </div>
  </div>
  </div>
  </section>
  <!-- // Basic form layout section end -->
  </div>
  </div>
  </div>
  <!-- End -->
  @endsection
  @section('js')
  <script type="text/javascript">
  $("#delivery_method").select2({
  placeholder: 'Select Delivery Method',
  allowClear: true,
  });
  $("#prefered_brand").select2({
  placeholder: 'Select Prefered Brand',
  multiple: true,
  });
  $("#prefered_supplier").select2({
  placeholder: 'Select Prefered Supplier',
  multiple: true,
  });
  $("#payment_term").select2({
  placeholder: 'Select Payment Term',
  allowClear: true,
  });
  $("#payment_method").select2({
  placeholder: 'Select Payment Method',
  });
  </script>
  @endsection