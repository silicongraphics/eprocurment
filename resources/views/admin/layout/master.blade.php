<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{ asset('admin-assets/images/fav.png') }}" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/app-assets/css/vendors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/app-assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/app-assets/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/app-assets/css/plugins/calendars/fullcalendar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/app-assets/vendors/css/calendars/fullcalendar.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css') }}">
    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/assets/css/responsive.css') }}">
    @yield('css')
    <!-- END Custom CSS-->
</head>
<body>
    @include('admin.includes.header')
    @include('admin.includes.nav')     
    @yield('content')

     <!-- BEGIN VENDOR JS-->
     <!-- jQuery -->
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="{{ asset('admin-assets/app-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin-assets/app-assets/vendors/js/tables/datatable/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin-assets/app-assets/vendors/js/tables/datatable/dataTables.colReorder.min.js') }}" type="text/javascript"></script>
     <script type="text/javascript">
        var base_url = "{{url('/')}}/";
     </script>
    <!-- <script src="{{ asset('admin-assets/app-assets/js/scripts/tables/datatables/datatable-basic.js') }}" type="text/javascript"></script> -->

    <!-- DataTables -->
    <!-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
    <script src="{{ asset('admin-assets/app-assets/js/scripts/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin-assets/app-assets/vendors/js/extensions/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin-assets/app-assets/vendors/js/extensions/fullcalendar.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin-assets/app-assets/js/scripts/extensions/fullcalendar.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin-assets/app-assets/js/core/app-menu.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin-assets/app-assets/js/core/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin-assets/app-assets/js/scripts/customizer.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin-assets/app-assets/js/scripts/modal/components-modal.js') }}" type="text/javascript"> </script>
     <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script> 
    <script src="{{ asset('admin-assets/app-assets/js/scripts/custom.js') }}" type="text/javascript"></script> 
    <script src="{{ asset('admin-assets/app-assets/js/scripts/jquery.validate.js') }}" type="text/javascript"></script>   
    <!-- for Datatable Code here -->
     @yield('js')
    <!-- End Datatable Code here -->
    <!-- END STACK JS-->
  </body>
</html>

