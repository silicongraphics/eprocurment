@extends('admin.layout.master')
@section('title')
   Suppliers
@endsection
@section('content')
     <div class="app-content content">
        <div class="content-wrapper">
          @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
           @endif
            <div class="content-body app-page app-page-buyer">
                <!-- Basic form layout section start -->
                <section id="configuration" class="user">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pro-main">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="app-page-header row">
                                            <div class="col-sm-10">
                                                <h1 class="pull-left">Suppliers</h1>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="col-sm-12">
                                                  <div class="form-actions right">
                                                      <button onclick="window.location.href='{{ route('addsupplier') }}'" type="submit" class="btn btn-primary">
                                                        <i class="fa fa-plus-circle"></i>New Suppliers
                                                      </button>
                                                  </div>
                                              </div>
                                              <div class="col-sm-12">
                                                  <div class="form-actions right">
                                                      <button onclick="window.location.href='eproc-add-application.html'" type="submit" class="btn btn-primary">
                                                        <i class="fa fa-pencil"></i>Edit Columns
                                                      </button>
                                                  </div>
                                              </div>
                                            </div>
                                            
                                            
                                        </div>
                                    
                                        <div class="clearfix"></div>

                                        <form id="search-form" method="post" class="form form-horizontal striped-rows form-bordered">
                                            <div class="form-body">
                                                <div class="row">
                                                   <div class="col-sm-2">
                                                        <div class="form-group">
                                                          <label class="" for="">Search by Id</label>
                                                          <input type="text" id="" class="form-control" placeholder="Search By Id" name="supplier_id">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                          <label class="" for="">Search by Name</label>
                                                          <input type="text" id="" class="form-control" placeholder="Search By Name" name="supplier_name">
                                                        </div>
                                                    </div>
                                                      <div class="col-sm-2">
                                                        <div class="form-group">
                                                              <label for="issueinput6">Search by Status</label>
                                                              <select id="selected-option" name="status" class="form-control" data-original-title="" title="">
                                                                <option value="">Search By Status</option>
                                                                <option value="1">Active</option>
                                                                <option value="0">In-Active</option>
                                                              </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2"></div>
                                                    <div class="col-sm-2"></div>
                                                    <div class="col-sm-2">
                                                        <div class="form-actions right">
                                                            <button type="submit" class="btn btn-primary">
                                                              <i class=""></i> Search
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                   <table class="table table-striped table-bordered zero-configuration dataTable no-footer" id="suppliers" role="grid" aria-describedby="DataTables_Table_0_info">
                                              <thead>
                                                <tr role="row">
                                                <th>Created At</th>
                                                <th>Modified At</th>
                                                <th>Status</th>
                                                <th>Supplier Name</th>
                                                <th>Supplier Id</th>
                                                <th>Actions</th></tr>
                                              </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <!--main card end-->
                               </div>
                            </div>
                         </div>
                       </section>
                        <!-- // Basic form layout section end -->
                    </div>
                </div>
            </div>
@endsection
@section('js')
<script>
   var oTable = '';
$(function() {
    oTable = $('#suppliers').DataTable({
        processing: true,
        serverSide: true,
         rowReorder: {
          "update": false,
        },
        colReorder: true,
        ajax: {
          url: '{{ route("getsuppliers") }}',
          type: 'post',
            data: function (d) {
                d.name = $('input[name=supplier_name]').val();
                d.id = $('input[name=supplier_id]').val();
                d.status  = $('#selected-option option:selected').val();
            }
        },
        columns: [
            { data: 'created_at', name: 'created_at' },
            { data: 'modified_at', name: 'modified_at' },
            { data: 'status', name: 'status' },
            { data: 'company_name', name: 'company_name' },
            { data: 'id', name: 'id' },
            { data: 'actions', name: 'actions' }
        ]
    });
});

$('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
});

</script>
@endsection