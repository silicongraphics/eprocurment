@extends('admin.layout.master')
@section('content')
	<div class="app-content content">
	<div class="content-wrapper">
    @if ($errors->any())
	<div class="alert alert-danger">
	<ul>
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	</ul>
	</div>
	@endif
	<div class="content-body product-page edit_client"> 
	<!-- Basic form layout section start -->
	<section id="configuration" class="user">
	<div class="row">
	<div class="col-12">
	<div class="card rounded pro-main">
	<div class="card-content collapse show">
	<div class="card-body table-responsive card-dashboard">
	<div class="row">
	<div class="col-12">
	<h1 class="pull-left">Add New Supplier</h1>
	</div>
	</div>
	<form id="SupplierAddForm" class="form" method="post" action="{{ route('supplier.save') }}" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="clearfix"></div>
	<ul class="nav nav-tabs">
	<li class="nav-item"> <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">Supplier Company Info</a> </li>
	<li class="nav-item"> <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false">Supplier Admin Info</a> </li>
	</ul>
	<div class="tab-content px-1 pt-1">
	<div role="tabpanel" class="tab-pane fade show active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
	<div class="form-body">
	<div class="row">
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Name</label>
	<input class="form-control" type="text" name="name" placeholder="Name" id="company_name" required>
	</div>
	</div>
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Website</label>
	<input class="form-control" type="text" name="website" placeholder="Webiste" id="website" required>
	</div>
	</div>
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Tax Registration No</label>
	<input class="form-control" type="number" name="tax_registration_no" placeholder="Tax Registration No" id="tax_registration" required>
	</div>
	</div>
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Products Supplied</label>
	<select id="products_supplied" name="products_supplied[]" multiple="multiple" class="form-control" data-original-title="" title="">
	<option value="">Products Supplied</option>							
	</select>
	</div>
	</div>
	</div>
	<div class="row">
	<div class="col-sm-3">
	<div class="form-group">
	<label for="issueinput6">Delivery Method</label>
	<select id="delivery_method" name="delivery_method_id" class="form-control" data-original-title="" title="">
	<option value="">Select Delivery Method</option>
	@foreach($delivery_methods as $delivery_method)
	<option value="{{ $delivery_method->id }}"> {{ $delivery_method->name }} </option>
	@endforeach								
	</select>
	</div>
	</div>
	<div class="col-sm-3">
	<div class="form-group">
	<label for="issueinput6">Payment Method</label>
	<select id="payment_method" name="payment_method_id" class="form-control" data-original-title="" title="">
	<option value=""></option>
	@foreach($payment_methods as $payment_method)
	<option value="{{ $payment_method->id }}"> {{ $payment_method->name }} </option>
	@endforeach	
	</select>
	</div>
	</div>
	<div class="col-sm-6">
	<div class="form-group">
	<label for="issueinput6">Payment Terms</label>
	<select id="payment_term" name="credit_term_id" class="form-control" data-original-title="" title="">
	<option value=""></option>
	@foreach($payment_terms as $payment_term)
	<option value="{{ $payment_term->id }}"> {{ $payment_term->name }} </option>
	@endforeach	
	</select>
	</div>
	</div>
	</div>
	</div>
	<div class="form billing_form">
	<div class="form-body">
	<div class="row">
	<div class="col-sm-6">
	<h4 class="form-section">Billing Address</h4>
	<div class="row">
	<div class="col-md-12">
	<div class="form-group">
	<label for="projectinput1">Address</label>
	<input type="text" id="billing_address" class="form-control billing_address" placeholder="Billing Address" name="billing_address">
	</div>
	<div class="form-group">
	<label for="projectinput3">Street</label>
	<input type="text" id="billing_street" class="form-control billing_street" placeholder="Billing Street" name="billing_street">
	</div>
	<div class="form-group">
	<label for="projectinput3">PoBox</label>
	<input type="text" id="billing_pobox" class="form-control billing_pobox" placeholder="Billing PO Box" name="billing_pobox">
	</div>
	<div class="form-group">
	<label for="issueinput6">Country</label>
	<select id="billing_country" name="billing_country_id" class="form-control billing_country_id" data-original-title="" title="">
	<option value="">Country</option>
	@foreach($countries as $country)
	<option value="{{ $country->id }}"> {{ $country->name }}</option>
	@endforeach
	</select>
	</div>
	<div class="form-group">
	<label for="issueinput6">City</label>
	<select id="billing_city" name="billing_city_id" class="form-control billing_city_id" data-original-title="" title="">
	<option value="">City</option>
	@foreach($cities as $city)
	<option value="{{ $city->id }}"> {{ $city->name }}</option>
	@endforeach
	</select>
	</div>
	</div>
	</div>
	</div>
	<div class="col-sm-6">
	<h4 class="form-section">Shipping Address</h4>
	<div class="row">
	<div class="col-md-12">
	<div class="form-group">
	<fieldset class="checkboxsas">
	<label class="label-note">
	<input type="checkbox" id="" class="same_addresses" value="">
	Same as Billing Address </label>
	</fieldset>
	<label for="projectinput1">Address</label>
	<input type="text" id="projectinput1" class="form-control shipping_address" placeholder="Shipping Address" name="shipping_address">
	</div>
	<div class="form-group">
	<label for="projectinput3">Street</label>
	<input type="text" id="projectinput3" class="form-control shipping_street" placeholder="Shipping Street" name="shipping_street">
	</div>
	<div class="form-group">
	<label for="projectinput3">PoBox</label>
	<input type="text" id="projectinput4" class="form-control shipping_pobox" placeholder="Shipping PoBox" name="shipping_pobox">
	</div>
	<div class="form-group">
	<label for="issueinput6">Country</label>
	<select id="shipping_country" name="shipping_country_id" class="form-control shipping_country_id" data-original-title="" title="">
	<option value="">Country</option>
	@foreach($countries as $country)
	<option value="{{ $country->id }}"> {{ $country->name }}</option>
	@endforeach
	</select>
	</div>
	<div class="form-group">
	<label for="issueinput6">City</label>
	<select id="shipping_city" name="shipping_city_id" class="form-control shipping_city_id" data-original-title="" title="">
	<option value="">City</option>
	@foreach($cities as $city)
	<option value="{{ $city->id }}"> {{ $city->name }}</option>
	@endforeach
	</select>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form">
	<div class="form-body">
	<div class="row">
	<div class="col-sm-4">
	<div class="form-group">
	<label for="userinput5">Notes</label>
	<input class="form-control" type="text" name="notes" placeholder="" id="notes">
	</div>
	</div>
	<div class="col-sm-4">
	<div class="form-group">
	<label for="issueinput6">Trade Liscense</label>
	<div class="custom-file">
	<input type="file" class="custom-file-input" name="trade_liscense" id="inputGroupFile01" required placeholder="No File Chosen">
	<input type="hidden" name="doctype[]" value="3" />
	<div class="input-group-prepend"> <span class="input-group-text">Choose File</span> </div>
	</div>
	</div>
	</div>
	<div class="col-sm-4">
	<div class="form-group">
	<label for="issueinput6">VAT Certificate</label>
	<div class="custom-file">
	<input type="file" class="custom-file-input" name="vat_certificate" id="inputGroupFile02" required placeholder="No File Chosen">
	<input type="hidden" name="doctype[]" value="4" />
	<div class="input-group-prepend"> <span class="input-group-text">Choose File</span> </div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form billing_form">
	<div class="form-body">
	<div class="row">
	<div class="col-sm-12">
	<h4 class="form-section">Additional Approver For Purchase Order</h4>
	<div class="row">
	<div class="col-md-6">
	<div class="form-group">
	<label for="projectinput1">Approver Name</label>
	<input type="text" id="approval_name" name="approval_name" class="form-control" placeholder="Approval Name" name="fname">
	</div>
	</div>
	<div class="col-sm-6">
	<div class="form-group">
	<label for="issueinput6">Approver Emirates Id</label>
	<div class="custom-file">
	<input type="file" class="custom-file-input" name="emirates_id" id="inputGroupFile03" required placeholder="No File Chosen">
	<input type="hidden" name="doctype[]" value="2" />
	<div class="input-group-prepend"> <span class="input-group-text">Choose File</span> </div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<div>
	<div class="row">
	<div class="col-sm-6">
	<div class="form-group">
	<label class="status_label" for="issueinput6">Status</label>
	<fieldset class="checkboxsas">
	<label class="label-note">
	<input type="checkbox" value="1" name="verified">
	Verified </label>
	</fieldset>
	<select id="status" name="status" class="form-control" data-original-title="" title="" required>
	<option value="">Select Status</option>
	<option value="1">Active</option>
	<option value="0">In-Active</option>
	</select>
	</div>
	</div>
	<div class="col-sm-6">
	<div class="form-actions right">
	<button id="next-tab-supplier" class="btn btn-primary"> <i class=""></i> Next </button>
	</div>
	</div>
	</div>
	</div>
	</div>
	<div role="tabpanel" class="tab-pane fade" id="tab2" aria-expanded="true" aria-labelledby="base-tab2">
	<div class="form">
	<div class="form-body">
	<div class="row">
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Title</label>
	<select id="title" name="title" class="form-control" data-original-title="" title="" required>
	<option value="">Select title</option>
	<option value="Mr.">Mr</option>
	<option value="Ms.">Ms</option>
	<option value="Mrs.">Mrs</option>
	<option value="Miss">Miss</option>
	</select>
	</div>
	</div>
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">First Name</label>
	<input class="form-control" type="text" name="first_name" placeholder="" id="first_name" required>
	</div>
	</div>
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Last Name</label>
	<input class="form-control" type="text" name="last_name" placeholder="" id="last_name">
	</div>
	</div>
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Job Title</label>
	<input class="form-control" type="text" name="job_title" placeholder="" id="job_title" required>
	</div>
	</div>
	</div>
	<div class="row">
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Primary Email</label>
	<input class="form-control" type="email" name="email" placeholder="" id="email_user" required>
	</div>
	</div>
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Mobile Number</label>
	<input class="form-control" name="mobile_no" type="number" placeholder="" id="mobile_no" required>
	</div>
	</div>
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Work Number</label>
	<input class="form-control" type="number" name="phone" placeholder="" id="phone">
	</div>
	</div>
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Fax Number</label>
	<input class="form-control" type="text" name="fax" placeholder="" id="fax">
	</div>
	</div>
	</div>
	<div class="row">
	<div class="col-sm-3">
	<div class="form-group">
	<label for="userinput5">Password</label>
	<input class="form-control" type="password" placeholder="" id="password" required="required" name="password">
	<label class="label-note" for="password">Minimum Password length must be 6 characters.</label>
	</div>
	</div>
	<div class="col-sm-4">
	<div class="form-group">
	<label for="issueinput6">Emirates Id</label>
	<div class="custom-file">
	<input type="file" name="emirates_id_user" class="custom-file-input" id="inputGroup01" required placeholder="No File Chosen">
	<div class="input-group-prepend"> <span class="input-group-text">Choose File</span> </div>
	</div>
	</div>
	</div>
	<div class="col-sm-4">
	<div class="form-group">
	<label for="issueinput6">Profile Picture</label>
	<div class="custom-file">
		<input type="file" name="profile_picture" class="custom-file-input" id="inputGroup02" placeholder="No File Chosen">
	<div class="input-group-prepend"> <span class="input-group-text">Choose File</span> </div>
	</div>
	</div>
	</div>
	</div>
	<div class="row">
	<div class="col-sm-3">
	<div class="form-group">
	<label class="status_label" for="issueinput6">Status</label>
	<fieldset class="checkboxsas">
	<label class="label-note">
	<input type="checkbox" value="1" name="verified_user">
	Verified </label>
	</fieldset>
	<select id="status_user" name="status_user" class="form-control" data-original-title="" title="" required>
	<option value="">Select Status</option>
	<option value="1">Active</option>
	<option value="0">In-Active</option>
	</select>
	</div>
	</div>
	<div class="col-sm-3">
	<div class="form-actions right">
	<button type="submit" class="btn btn-primary"> <i class=""></i> Add Supplier </button>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</form>
	</div>
	</div>
	<!--main card end--> 

	</div>
	</div>
	</div>
	</section>
	<!-- // Basic form layout section end --> 
	</div>
	</div>
	</div>
@endsection
@section('js')
	<script type="text/javascript">
	$("#delivery_method").select2({
	placeholder: 'Select Delivery Method',
	allowClear: true,
	});
	$("#payment_term").select2({
	placeholder: 'Select Payment Term',
	allowClear: true,
	});
	$("#payment_method").select2({
	placeholder: 'Select Payment Method',
	});
	/* $("#products_supplied").select2({
	multiple: true,
	placeholder: 'Select Products Supplied',
	}); */

    $('#products_supplied').select2({
        placeholder: 'Select Products Supplied',
        multiple: true,
        ajax: {
          url: base_url + 'admin/getroducts',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
      });

	</script>
	@endsection