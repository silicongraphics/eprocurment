  @extends('admin.layout.master')
   @section('title')
     Administration - Edit Supplier
  @endsection
  @section('content')
  <!-- End Edit Supplier here -->
  <div class="app-content content">
  <div class="content-wrapper">
  <div class="content-body product-page edit_client">
  <!-- Basic form layout section start -->
  <section id="configuration" class="user">
  <div class="row">
  <div class="col-12">
  <div class="card rounded pro-main">
  <div class="card-content collapse show">
  <div class="card-body table-responsive card-dashboard">
  <div class="row">
  <div class="col-12">
  <h1 class="pull-left">Edit Supplier</h1>
    @if ($errors->any())
  <div class="alert alert-danger">
  <ul>
   @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
   @endforeach
  </ul>
  </div>
  @endif
  </div>

  </div>
  <div class="clearfix"></div>

  <form method="post" class="form" id="supplierEditForm" action="{{ route('supplier.update', $id) }}" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="form-body">
  <div class="row">
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Supplier Company Name</label>
  <input class="form-control" name="name" type="text" value="{{ $supplier->company_name }}" placeholder="" id="company_name">
  </div>
  </div> 
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Website</label>
  <input class="form-control" type="text" name="website" placeholder="" value="{{ $supplier->website }}" id="website">
  </div>
  </div> 
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Tax Registration No</label>
  <input class="form-control" name="tax_registration_no" type="" placeholder="" value="{{ $supplier->tax_registration_no }}" id="tax_registration_no">
  </div>
  </div>
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Products Supplied</label>
  <select id="products_supplied" name="products_supplied[]" multiple="multiple" class="form-control" data-original-title="" title="">
  <option value="">Products Supplied</option>             
  </select>
  </div>
  </div> 
  </div>

  <div class="row">

  <div class="col-sm-3"> 
  <div class="form-group">
  <label for="issueinput6">Delivery Method</label>
  <select id="delivery_method" name="delivery_method_id" class="form-control" data-original-title="" title="">
  <option value="">Select Delivery Method</option>
  @foreach($delivery_methods as $delivery_method)
  <option value="{{ $delivery_method->id }}"> {{ $delivery_method->name }} </option>
  @endforeach               
  </select>
  </div>
  </div>

  <div class="col-sm-3"> 
  <div class="form-group">
  <label for="issueinput6">Payment Terms</label>
  <select id="payment_term" name="credit_term_id" class="form-control" data-original-title="" title="">
  <option value=""></option>
  @foreach($payment_terms as $payment_term)
  <option value="{{ $payment_term->id }}"> {{ $payment_term->name }} </option>
  @endforeach 
  </select>
  </div>
  </div> 
  <div class="col-sm-3"> 
  <div class="form-group">
  <label for="issueinput6">Payment Method</label>
  <select id="payment_method" name="payment_method_id" class="form-control" data-original-title="" title="">
  <option value=""></option>
  @foreach($payment_methods as $payment_method)
  <option value="{{ $payment_method->id }}"> {{ $payment_method->name }} </option>
  @endforeach 
  </select>
  </div>
  </div>
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Title</label>
  <select id="title" name="title" class="form-control" data-original-title="" title="" required>
  <option value="">Select title</option>
  <option {{ ($supplieruser) ? ($supplieruser->title == 'Mr.') ? 'selected=selected' : '' : '' }} value="Mr.">Mr</option>
  <option {{ ($supplieruser) ? ($supplieruser->title == 'Ms.') ? 'selected=selected' : '' : '' }} value="Ms.">Ms</option>
  <option {{ ($supplieruser) ? ($supplieruser->title == 'Mrs.') ? 'selected=selected' : '' : '' }} value="Mrs.">Mrs</option>
  <option {{ ($supplieruser) ? ($supplieruser->title == 'Miss') ? 'selected=selected' : '' : '' }} value="Miss">Miss</option>
  </select>
  </div>
  </div> 
  </div>

  <div class="row">

  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">First Name</label>
  <input class="form-control" name="first_name" type="text" value="{{ ($supplieruser) ? $supplieruser->first_name : '' }}" placeholder="" id="first_name">
  </div>
  </div> 
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Last Name</label>
  <input class="form-control" name="last_name" type="text" value="{{ ($supplieruser) ? $supplieruser->last_name : '' }}" placeholder="" id="last_name">
  </div>
  </div>
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Job Title</label>
  <input class="form-control" name="job_title" type="text" value="{{ ($supplieruser) ? $supplieruser->job_title : '' }}" placeholder="" id="job_title">
  </div>
  </div> 
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Primary Email</label>
  <input class="form-control" name="email" type="email" value="{{ ($supplieruser) ? $supplieruser->email : '' }}" placeholder="" id="email">
  </div>
  </div> 

  </div>

  <div class="row">
  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Mobile Number</label>
  <input class="form-control" name="mobile_no" type="number" value="{{ ($supplieruser) ? $supplieruser->mobile_no : '' }}" placeholder="" id="mobile_no">
  </div>
  </div>

  <div class="col-sm-3">
  <div class="form-group">
  <label for="userinput5">Work Number</label>
  <input class="form-control" type="number" name="phone" value="{{ ($supplieruser) ? $supplieruser->phone : '' }}" placeholder="" id="phone">
  </div>
  </div> 

  <div class="col-sm-6">
  <div class="form-group">
  <label for="userinput5">Fax Number</label>
  <input class="form-control" type="text" name="fax" value="{{ ($supplieruser) ? $supplieruser->fax : '' }}" placeholder="" id="userinput5">
  </div>
  </div> 

  </div>

  <div class="row">

  <div class="col-sm-6">
  <div class="form-group">
  <label for="userinput5">New Password</label>
  <input class="form-control" type="password" name="password" placeholder="" id="userinput5">
  <label class="label-note" for="userinput6">Minimum Password length must be 6 characters.</label>
  </div>
  </div> 
  </div>

  <div class="form billing_form">
  <div class="form-body">
  <div class="row">
  <div class="col-sm-6">
  <h4 class="form-section">Billing Address</h4>
  <div class="row">
  <div class="col-md-12">
  <div class="form-group">
  <label for="projectinput1">Address</label>
  <input type="text" id="billing_address" class="form-control billing_address" value="{{ $supplier->billing_address }}" placeholder="Billing Address" name="billing_address">
  </div>
  <div class="form-group">
  <label for="projectinput3">Street</label>
  <input type="text" id="billing_street" class="form-control billing_street" value="{{ $supplier->billing_street }}" placeholder="Billing Street" name="billing_street">
  </div>
  <div class="form-group">
  <label for="projectinput3">PoBox</label>
  <input type="text" id="billing_pobox" class="form-control billing_pobox" value="{{ $supplier->billing_pobox }}" placeholder="Billing PO Box" name="billing_pobox">
  </div>
  <div class="form-group">
  <label for="issueinput6">Country</label>
  <select id="billing_country" name="billing_country_id" class="form-control billing_country_id" data-original-title="" title="">
  <option value="">Country</option>
  @foreach($countries as $country)
  <option {{ ($supplier->billing_country_id == $country->id) ? 'selected=selected' : '' }} value="{{ $country->id }}"> {{ $country->name }}</option>
  @endforeach
  </select>
  </div>
  <div class="form-group">
  <label for="issueinput6">City</label>
  <select id="billing_city" name="billing_city_id" class="form-control billing_city_id" data-original-title="" title="">
  <option value="">City</option>
  @foreach($cities as $city)
  <option {{ ($supplier->billing_city_id == $city->id) ? 'selected=selected' : '' }} value="{{ $city->id }}"> {{ $city->name }}</option>
  @endforeach
  </select>
  </div>
  </div>
  </div>
  </div>
  <div class="col-sm-6">
  <h4 class="form-section">Shipping Address</h4>
  <div class="row">
  <div class="col-md-12">
  <div class="form-group">
  <fieldset class="checkboxsas">
  <label class="label-note">
  <input type="checkbox" id="" class="same_addresses" value="">
  Same as Billing Address </label>
  </fieldset>
  <label for="projectinput1">Address</label>
  <input type="text" id="projectinput1" class="form-control shipping_address" value="{{ $supplier->shipping_address }}" placeholder="Shipping Address" name="shipping_address">
  </div>
  <div class="form-group">
  <label for="projectinput3">Street</label>
  <input type="text" id="projectinput3" class="form-control shipping_street" value="{{ $supplier->shipping_street }}" placeholder="Shipping Street" name="shipping_street">
  </div>
  <div class="form-group">
  <label for="projectinput3">PoBox</label>
  <input type="text" id="projectinput4" class="form-control shipping_pobox" value="{{ $supplier->shipping_pobox }}" placeholder="Shipping PoBox" name="shipping_pobox">
  </div>
  <div class="form-group">
  <label for="issueinput6">Country</label>
  <select id="shipping_country" name="shipping_country_id" class="form-control shipping_country_id" data-original-title="" title="">
  <option value="">Country</option>
  @foreach($countries as $country)
  <option {{ ($supplier->shipping_country_id == $country->id) ? 'selected=selected' : '' }} value="{{ $country->id }}"> {{ $country->name }}</option>
  @endforeach
  </select>
  </div>
  <div class="form-group">
  <label for="issueinput6">City</label>
  <select id="shipping_city" name="shipping_city_id" class="form-control shipping_city_id" data-original-title="" title="">
  <option value="">City</option>
  @foreach($cities as $city)
  <option {{ ($supplier->shipping_city_id == $city->id) ? 'selected=selected' : '' }}  value="{{ $city->id }}"> {{ $city->name }}</option>
  @endforeach
  </select>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>

  <div class="form">
  <div class="form-body">
  <div class="row">
  <div class="col-sm-4">
  <div class="form-group">
  <label for="userinput5">Notes</label>
  <input class="form-control" type="text" value="{{ $supplier->notes }}" name="notes" placeholder="" id="userinput5">
  </div>
  </div>
  <div class="col-sm-4"> 
  <div class="form-group">
  <label for="issueinput6">Trade Liscense</label>
  <div class="custom-file">
  <input type="file" class="custom-file-input" name="trade_liscense" id="trade_liscense" placeholder="No File Chosen" @if(!$supplier->trade_license) required @endif>
  <input type="hidden" name="doctype[]" value="2" />
  <div class="input-group-prepend">
  <span class="input-group-text">Choose File</span>
  </div> 
  </div>
   @if($supplier->trade_license)
  <a href="{{ asset('/document/vendor/' . $id . '/' . $supplier->trade_license) }}" download> Download File: {{ $supplier->trade_license }}</a>
  @endif
  </div>
  </div>
  <div class="col-sm-4"> 
  <div class="form-group">
  <label for="issueinput6">VAT Certificate</label>
  <div class="custom-file">
  <input type="file" class="custom-file-input" name="vat_certificate" placeholder="No File Chosen" id="inputGroupFile02" @if(!$supplier->vat_certificate) required @endif />
  <input type="hidden" name="doctype[]" value="4" />
  <div class="input-group-prepend">
  <span class="input-group-text">Choose File</span>
  </div> 
   @if($supplier->vat_certificate)
  <a href="{{ asset('/document/vendor/' . $id . '/' . $supplier->vat_certificate) }}" download> Download File: {{ $supplier->vat_certificate }}</a>
  @endif
  </div>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="col-sm-6"> 
  <div class="form-group">
  <label for="issueinput6">Profile Picture</label>
  <div class="custom-file">
  <input type="file" name="profile_picture" placeholder="No File Chosen" class="custom-file-input" id="input01" @if($supplieruser) @if(!$supplieruser->profile_picture) required @endif @endif />
  <div class="input-group-prepend">
  <span class="input-group-text">Choose File</span>
  </div> 
   @if(($supplieruser) && $supplieruser->profile_picture)
  <a href="{{ asset('/document/vendor/users/' . $id . '/' . $supplieruser->profile_picture) }}" download> Download File: {{ $supplieruser->profile_picture }}</a>
  @endif
  </div>
  </div>
  </div>
  <div class="col-sm-6"> 
  <div class="form-group">
  <label for="issueinput6">Emirates Id</label>
  <div class="custom-file">
  <input type="file" placeholder="No File Chosen" class="custom-file-input" name="emirates_id_user" id="emirates_id_user" @if($supplieruser) @if(!$supplieruser->emirates_id) required @endif @endif />
  <div class="input-group-prepend">
  <span class="input-group-text">Choose File</span>
  </div> 
   @if(($supplieruser) && $supplieruser->emirates_id)
  <a href="{{ asset('/document/vendor/users/' . $id . '/' . $supplieruser->emirates_id) }}" download> Download File: {{ 
  $supplieruser->emirates_id }}</a>
  @endif
  </div>
  </div>
  </div>
  </div>

  </div>
  </div>

  <div class="form billing_form">
  <div class="form-body">
  <div class="row">
  <div class="col-sm-12">
  <h4 class="form-section">Additional Approver For Purchase Order</h4>
  <div class="row">
  <div class="col-md-6">
  <div class="form-group">
  <label for="projectinput1">Approver Name</label>
  <input type="text" id="approval_name" name="approval_name" class="form-control" placeholder="approval name" value="{{ $supplier->approval_name }}">
  </div>
  </div>
  <div class="col-sm-6"> 
  <div class="form-group">
  <label for="issueinput6">Approver Emirates Id</label>
  <div class="custom-file">
  <input type="file" class="custom-file-input" name="emirates_id"  placeholder="No File Chosen" id="approval_emirate_id" @if(!$supplier->emirate_id) required @endif>
  <input type="hidden" name="doctype[]" value="2" />
  <div class="input-group-prepend">
  <span class="input-group-text">Choose File</span>
  </div> 
  @if($supplier->emirates_id)
  <a href="{{ asset('/document/vendor/' . $id . '/' . $supplier->emirates_id) }}" download> Download File: {{ 
  $supplier->emirates_id }}</a>
  @endif
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>

  <div class="row">
  <div class="col-sm-6">
  <div class="form-group">
  <label class="status_label" for="issueinput6">Status</label>
  <fieldset class="checkboxsas">
  <label class="label-note">
  <input type="checkbox" value="1" name="verified" {{ ($supplier->verified == 1) ? 'checked=checked' : '' }}> Verified
  </label>
  </fieldset>
  <select id="status" name="status" class="form-control" data-original-title="" title="" required>
  <option value="">Select Status</option>
  <option {{ ($supplier->status == 1) ? 'selected=selected' : '' }} value="1">Active</option>
  <option {{ ($supplier->status == 0) ? 'selected=selected' : '' }} value="0">In-Active</option>
  </select>
  </div>
  </div>
  <div class="col-sm-6">
  <div class="form-actions right">
  <button id="save" type="submit" class="btn btn-primary"> <i class=""></i> Save </button>
  </div>
  </div>
  </div> 
  </div>
  </div>
    </form>
  <!--main card end-->

  </div>
  </div>
  </div>
  </section>
  <!-- // Basic form layout section end -->
  </div>
  </div>
  </div>
  <!-- End Edit Supplier here -->
  @endsection
  @section('js')
  <script type="text/javascript">
  $("#delivery_method").select2({
  placeholder: 'Select Delivery Method',
  allowClear: true,
  });
  $("#payment_term").select2({
  placeholder: 'Select Payment Term',
  allowClear: true,
  });
  $("#payment_method").select2({
  placeholder: 'Select Payment Method',
  });
  /* $("#products_supplied").select2({
  multiple: true,
  placeholder: 'Select Products Supplied',
  }); */

    $('#products_supplied').select2({
        placeholder: 'Select Products Supplied',
        multiple: true,
        ajax: {
          url: base_url + 'admin/getroducts',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
      });

  </script>
  @endsection