	@extends('admin.layout.master')
	@section('title')
	Supplier - View
	@endsection
	@section('content')
	<div class="app-content content view-only product-page edit_client view_buyer app-page">
	<div class="content-wrapper">
	<div class="content-body ">
	<!-- Basic form layout section start -->
	<section id="configuration" class="user">
	<div class="row">
	<div class="col-12">
	<div class="card rounded pro-main">
	<div class="card-content collapse show">
	<div class="card-body table-responsive card-dashboard">
	<div class="row">
	<div class="col-sm-8">
	<h1 class="pull-left">View Supplier</h1>
	</div>
	<div class="col-sm-2">
	<div class="form-actions right">
	<button onclick="window.location.href='{{ route('supplieruser') }}'" type="submit" class="btn btn-primary">
	<i class="fa fa-plus"></i>New Supplier User
	</button>
	</div>
	</div>
	<div class="col-sm-2">
	<div class="form-actions right">
	<button onclick="window.location.href='{{ route('supplier.edit', $id) }}'" type="submit" class="btn btn-primary">
	<i class="fa fa-pencil"></i>Edit Supplier
	</button>
	</div>
	</div>
	</div>
	<form class="form">
	<div class="row">
	<div class="col-sm-2">
	<div class="form-actions right">
	<button onclick="window.location.href='eproc-edit-product.html'" type="submit" class="btn btn-primary">
	<i class="fa fa-eye"></i>View Supplier RFQS
	</button>
	</div>
	</div>
	<div class="col-sm-2">
	<div class="form-actions right">
	<button onclick="window.location.href='eproc-edit-product.html'" type="submit" class="btn btn-primary">
	<i class="fa fa-eye"></i>View Supplier Products
	</button>
	</div>
	</div>
	<div class="col-sm-2">
	<div class="form-actions right">
	<button onclick="window.location.href='eproc-edit-product.html'" type="submit" class="btn btn-primary">
	Assign Product
	</button>
	</div>
	</div>
	<div class="col-sm-12 card-body2">
	<div class="form-group">
	<label for="issueinput6">Documents</label>
	<div class="custom-file">
	<input type="file" class="custom-file-input" id="inputGroupFile01">

	<label class="custom-file-label-doc" for="inputGroupFile01">Documents</label>
	</div>
	</div>
	</div>
	</div>
	</form>
	<div class="clearfix"></div>
	<form class="form billing_form">
	<div class="form-body">
	<div class="row">
	<div class="col-sm-12">
	<div class="col-sm-2 edit_client_btn">
	<div class="form-actions right">
	<button onclick="window.location.href='{{ route('supplier.edit', $id) }}'" type="submit" class="btn btn-primary">
	<i class="fa fa-pencil"></i>Edit User Details
	</button>
	</div>
	</div>
	<h4 class="form-section">Supplier Admin Details</h4>

	<section class="grid-with-inline-row-label " id="grid-with-inline-row-label">
	<div class="row">
	<div class="col-6">
	<div class="card card2">
	<div class="card-content collapse show">
	<div class="card-body card-body2">
	<form action="#">
	<div class="form-body">
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Id</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $custom_id }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">First Name</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->company_name }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>

	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Mobile No</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->mobile_no }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">BILLING ADDRESS</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<div class="form-control pro_ul">
	<ul>
	<li>Address: {{ $vendor->billing_address }}</li>
	<li>Street:  {{ $vendor->billing_street }}</li>
	<li>PoBox Number: {{ $vendor->billing_pobox }}</li>
	<li>Country: {{ $billing_country }}</li>
	<li>City: {{ $billing_city }}</li>
	</ul>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Payment Terms</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $credit_term }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Notes</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->notes }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Tax Registration Number</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->tax_registration_no }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Vat Certificate</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Additional Signatory Emirates Id</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Status</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ ($vendor->status) ? 'Active' : 'In-Active' }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Updated At</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->modified_at }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</form>
	</div>
	</div>
	</div>
	</div>

	<div class="col-6">
	<div class="card card2">
	<div class="card-content collapse show">
	<div class="card-body card-body2">
	<form action="#">
	<div class="form-body">
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Primary Email</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->email }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Work Number</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->phone }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Website</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->website }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">SHIPPING ADDRESS</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<div class="form-control pro_ul">
	<ul>
	<li>Address: {{ $vendor->shipping_address }}</li>
	<li>Street: {{ $vendor->shipping_street }}</li>
	<li>PoBox Number:  {{ $vendor->shipping_pobox }}</li>
	<li>Country: {{ $shipping_country }}</li>
	<li>City: {{ $shipping_city }}</li>
	</ul>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Payment Method</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $payment_method }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Delivery Method</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $delivery_method }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Emirates Id</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Trade Liscense</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Additional Signatory Personal Name</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="P00026" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Verified</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->verified }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Created At</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->created_at }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</form>
	</div>
	</div>
	</div>
	</div>
	</div>

	</section>

	<h4 class="form-section">Supplier Details</h4>

	<section class="grid-with-inline-row-label " id="grid-with-inline-row-label">
	<div class="row">
	<div class="col-6">
	<div class="card card2">
	<div class="card-content collapse show">
	<div class="card-body card-body2">
	<form action="#">
	<div class="form-body">
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Id</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $custom_id }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Name</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->company_name }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Description</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->description }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>

	</div>
	</form>
	</div>
	</div>
	</div>
	</div>

	<div class="col-6">
	<div class="card card2">
	<div class="card-content collapse show">
	<div class="card-body card-body2">
	<form action="#">
	<div class="form-body">
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Status</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ ($vendor->status) ? 'Active' : 'In-Active' }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Created At</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->created_at }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="form-group">
	<div class="row">
	<label class="col-lg-4">Updated At</label>
	<div class="col-lg-8">
	<div class="row">
	<div class="col-md-12">
	<input type="text" class="form-control" placeholder="{{ $vendor->modified_at }}" readonly="">
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</form>
	</div>
	</div>
	</div>
	</div>
	</div>

	</section>

	<h4 class="form-section">Related Users</h4>
    <div class="maain-tabble">
	<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6">
	<div class="dataTables_length" id="DataTables_Table_0_length">
	<label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-control form-control-sm">
	<option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="DataTables_Table_0"></label></div></div></div><div class="row"><div class="col-sm-12">
	<table class="table table-striped table-bordered zero-configuration dataTable no-footer" id="supplier_users" role="grid" aria-describedby="DataTables_Table_0_info">
	<thead>
	<tr role="row">
	<th style="width: 94px;">ID</th>
	<th style="width: 212px;">First Name</th>
	<th style="width: 105px;">Last Name</th>
	<th style="width: 136px;">Primary Email</th>
	<th style="width: 186px;">Status</th>
	<th style="width: 120px;">Actions</th></tr>
	</tr>
	</thead>
	<tbody>
	</tbody>
	</table></div></div></div>
	</div>
	</div>
	</div>

	</div>

	</form>

	</div>
	</div>
	<!--main card end-->

	</div>
	</div>
	</div>
	</section>
	<!-- // Basic form layout section end -->
	</div>
	</div>
	</div>

	@endsection
	@section('js')
<script>
   var oTable = '';
$(function() {
     oTable = $('#supplier_users').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
           url: '{{ route("usersupplier.get") }}',
           type: 'post',
           data: {
               "supplier_id": <?php echo $id ?>
           },
          },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'first_name', name: 'first_name' },
            { data: 'last_name', name: 'last_name' },
            { data: 'email', name: 'email' },
            { data: 'active', name: 'active' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ]
    });
});

</script>
@endsection