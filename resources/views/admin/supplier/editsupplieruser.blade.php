@extends('admin.layout.master')
@section('content')
  <div class="app-content content">
        <div class="content-wrapper">
          @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
            <div class="content-body product-page">
                <!-- Basic form layout section start -->
                <section id="configuration" class="user">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pro-main">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12">
                                                <h1 class="pull-left"> Edit Supplier User </h1>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>

                                        <form class="form" method="post" enctype="multipart/form-data" action="{{ route('supplieruser.update', $id) }}">
                                             {{ csrf_field() }}
                                          <div class="form-body">
                                            <div class="row">
                                                <div class="col-sm-3"> 
                                                    <div class="form-group">
                                                      <label for="issueinput6">Company Name</label>
                                                      <select id="issueinput6" name="vendor_id" class="form-control" data-original-title="" title="" required="required">
                                                        <option value="">Select Company Name</option>
                                                        @foreach($vendors as $vendor)
                                                        <option value="{{ $vendor->id }}" {{ ($supplieruser->vendor_id == $vendor->id) ? 'selected=selected' : '' }} >{{ $vendor->name }}</option>
                                                        @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                      <label for="userinput5">Website</label>
                                                      <input class="form-control" name="website" type="text" placeholder="Enter Website" value="{{ $supplieruser->website }}" id="userinput5" required="required">
                                                    </div>
                                                </div> 
                                                <div class="col-sm-3"> 
                                                    <div class="form-group">
                                                      <label for="issueinput6">Title</label>
                                                      <select id="issueinput6" name="title" class="form-control" data-original-title="" title="">
                                                        <option value="">Select title</option>
                                                        <option value="Mr." {{ ($supplieruser->title == 'Mr.') ? 'selected=selected' : '' }}>Mr</option>
                                                        <option value="Ms." {{ ($supplieruser->title == 'Ms.') ? 'selected=selected' : '' }}>Ms</option>
                                                        <option value="Mrs." {{ ($supplieruser->title == 'Mrs.') ? 'selected=selected' : '' }}>Mrs</option>
                                                        <option value="Miss" {{ ($supplieruser->title == 'Miss') ? 'selected=selected' : '' }}>Miss</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                      <label for="userinput5">First Name</label>
                                                      <input class="form-control" name="first_name" type="text" placeholder="" id="userinput5" required="required" value="{{ $supplieruser->first_name }}" >
                                                    </div>
                                                </div> 
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                      <label for="userinput5">Last Name</label>
                                                      <input name="last_name" class="form-control" type="text" placeholder="" id="last_name" value="{{ $supplieruser->last_name }}">
                                                    </div>
                                                </div> 
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                      <label for="userinput5">Job Title</label>
                                                      <input class="form-control" type="text" name="job_title" placeholder="Enter Job Title" id="userinput5" value="{{ $supplieruser->job_title }}">
                                                    </div>
                                                </div> 
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                      <label for="userinput5">Mobile Number</label>
                                                      <input class="form-control" type="text" name="mobile_no" placeholder="" id="mobile_no" required="required" value="{{ $supplieruser->mobile_no }}">
                                                    </div>
                                                </div> 

                                                 <div class="col-sm-3">
                                                    <div class="form-group">
                                                      <label for="userinput5">Work Number</label>
                                                      <input class="form-control" type="text" name="phone" placeholder="" id="work_number" value="{{ $supplieruser->phone }}">
                                                    </div>
                                                </div> 

                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                      <label for="userinput5">Fax Number</label>
                                                      <input class="form-control" type="text" name="fax" placeholder="" id="fax_number" value="{{ $supplieruser->fax }}">
                                                    </div>
                                                </div> 
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                      <label for="userinput5">Primary Email</label>
                                                      <input class="form-control" type="email" name="email" placeholder="" id="email" required="required" value="{{ $supplieruser->email }}">
                                                    </div>
                                                </div> 
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                      <label for="userinput5">New Password</label>
                                                      <input class="form-control" type="password" placeholder="" id="password" name="password" value="">
                                                      <label class="label-note" for="userinput6">Minimum Password length must be 6 characters.</label>
                                                    </div>
                                                </div> 
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                      <label for="userinput5">Notes</label>
                                                      <input class="form-control" type="text" placeholder="" id="userinput5" name="notes" value="{{ $supplieruser->notes }}">
                                                    </div>
                                                </div> 
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-sm-3"> 
                                                    <div class="form-group">
                                                      <label for="issueinput6">Profile Picture</label>
                                                      <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="inputGroupFile01" name="profile_picture" placeholder="No File Chosen">
                                                        <div class="input-group-prepend">
                                                          <span class="input-group-text">Choose File</span>
                                                         </div> 
                                                    
                                                      </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3"> 
                                                    <div class="form-group">
                                                      <label for="issueinput6">Emirates Id</label>
                                                      <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="inputGroupFile01" name="emirates_id" placeholder="No File Chosen">
                                                        <div class="input-group-prepend">
                                                          <span class="input-group-text">Choose File</span>
                                                         </div> 
                                                      
                                                      </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                      <label class="status_label" for="issueinput6">Status</label>
                                                      <fieldset class="checkboxsas">
                                                          <label class="label-note">
                                                            <input type="checkbox" name="verified" value="1" {{ ($supplieruser->verified == 1) ? 'checked=checked' : '' }}"> Verified </label>
                                                        </fieldset>
                                                      <select id="issueinput6" name="active" class="form-control" data-original-title="" title="" required="required">
                                                        <option value="">Select Status</option>
                                                        <option value="1" {{ ($supplieruser->active == 1) ? 'selected=selected' : '' }}>Active</option>
                                                        <option value="0" {{ ($supplieruser->active == 0) ? 'selected=selected' : '' }}>In-Active</option>
                                                      </select>
                                                    </div>
                                                </div>

                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-sm-3"> </div>
                                                <div class="col-sm-3"> </div>
                                                <div class="col-sm-3"> </div>
                                                <div class="col-sm-3">
                                                    <div class="form-actions right">
                                                        <button type="submit" class="btn btn-primary">
                                                          <i class=""></i> Save
                                                        </button>
                                                    </div>
                                                </div>
                                          </div>
                                        </form>
                                    </div>
                                </div>
                                <!--main card end-->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection