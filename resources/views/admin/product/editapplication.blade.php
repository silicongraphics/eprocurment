@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="user">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pro-main">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12">
                                                <h1 class="pull-left">EDIT APPLICATION</h1>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>

                                        <form class="form form-half" method="post" action="{{ route('application.update', $id) }}">
                                           {{ csrf_field() }}
                                          <div class="form-body">
                                            
                                            <div class="form-group">
                                              <label for="userinput5">Name</label>
                                              <input class="form-control" type="text" name="name" placeholder="" id="userinput5" value="{{ $app->name }}" required>
                                            </div>
                                            <div class="form-group">
                                              <label for="userinput8">Description</label>
                                              <textarea id="userinput8" rows="5" class="form-control" name="description" placeholder="">{{ $app->description }}</textarea>
                                            </div>
                                            <div class="form-group">
                                              <label for="issueinput6">Status</label>
                                              <select id="issueinput6" name="status" class="form-control" data-original-title="" title="" required>
                                                <option value="">Select Status</option>
                                                <option value="1" 
                                                {{ $app->status == 1 ? 'selected=selected' : '' }}>Active</option>
                                                <option value="0" {{ $app->status == 0 ? 'selected=selected' : '' }}>In-active</option>
                                              </select>
                                            </div>
                                          </div>
                                          
                                          <div class="form-actions right">
                                            <button type="submit" class="btn btn-primary">
                                              <i class=""></i> Save
                                            </button>
                                          </div>
                                        </form>
                                    </div>
                                </div>
                                <!--main card end-->
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection