@extends('admin.layout.master')
@section('content')
  <div class="app-content content view-only">
  <div class="content-wrapper">
  <div class="content-body ">
  <!-- Basic form layout section start -->
  <section id="configuration" class="user">
  <div class="row">
  <div class="col-12">
  <div class="card rounded pro-main">
  <div class="card-content collapse show">
  <div class="card-body table-responsive card-dashboard">
  <div class="row">
  <div class="col-sm-10">
  <h1 class="pull-left">VIEW APPLICATION</h1>
  </div>
  <div class="col-sm-2">
  <div class="form-actions right">
  <button onclick="window.location.href='{{ route('editapplication', $id) }}'" type="submit" class="btn btn-primary">
  <i class="fa fa-pencil"></i>EDIT APPLICATION
  </button>
  </div>
  </div>
  </div>
  <div class="clearfix"></div>

  <section class="grid-with-inline-row-label " id="grid-with-inline-row-label">
  <div class="row">
  <div class="col-sm-6">
  <div class="card">
  <div class="card-content collapse show">
  <div class="card-body">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Id</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $app->id }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Description</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $app->description }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Created At</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $app->created_at }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>
  <div class="col-sm-6">
  <div class="card">
  <div class="card-content collapse show">
  <div class="card-body">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Name</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $app->name }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Status</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ ($app->Status) ? 'Active' : 'In-Active' }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Updated At</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $app->updated_at }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>
  </div>
  </section>
  </div>
  </div>
  <!--main card end-->

  </div>
  </div>
  </div>
  </section>
  <!-- // Basic form layout section end -->
  </div>
  </div>
  </div>
  @endsection