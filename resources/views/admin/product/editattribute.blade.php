@extends('admin.layout.master')
@section('content')
 <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="user">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pro-main">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12">
                                                <h1 class="pull-left">Edit Product Attribute</h1>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                      <form class="form form-half" method="post" action="{{ route('attribute.update', $id) }}">
                                           {{ csrf_field() }}
                                          <div class="form-body">
                                            
                                            <div class="form-group">
                                              <label for="userinput5">Name</label>
                                              <input class="form-control" type="text" name="name" placeholder="" id="userinput5" value="{{ $attribute->name }}" required="required">
                                            </div>
                                            <div class="form-group">
                                              <label for="userinput6">Option Value</label>
                                              <select class="form-control" multiple="multiple" name="options[]" id="userinput6" required="required">
                                                @foreach($options as $option)
                                                  <option selected="selected">{{ $option->value }}</option>
                                                @endforeach
                                              </select>
                                              <label class="label-note" for="userinput6">Note: To add option value, press enter after typing the value.</label>
                                            </div>
                                            <div class="form-group">
                                              <label for="userinput8">Description</label>
                                              <textarea id="userinput8" rows="5" class="form-control" name="description" placeholder="">{{ $attribute->description }}</textarea>
                                            </div>
                                            <div class="form-group">
                                              <label for="issueinput6">Status</label>
                                              <select id="issueinput6" name="status" class="form-control" data-original-title="" title="" required="required">
                                                <option value="">Select Option</option>
                                                <option value="1" {{ $attribute->status == 1 ? 'selected=selected' : '' }}>Active</option>
                                                <option value="0" {{ $attribute->status == 0 ? 'selected=selected' : '' }}>In-Active</option>
                                              </select>
                                            </div>
                                          </div>
                                          
                                          <div class="form-actions right">
                                            <button type="submit" class="btn btn-primary">
                                              <i class=""></i> Save
                                            </button>
                                          </div>
                                        </form>
                                    </div>
                                </div>
                                <!--main card end-->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection
@section('js')
<script type="text/javascript">
$("#userinput6").select2({
  tags: true
});
</script>
@endsection