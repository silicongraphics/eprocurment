@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="user">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pro-main">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12">
                                                <h1 class="pull-left">Edit Category</h1>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>

                                        <form class="form form-half" method="post" action="{{ route('category.update', $id) }}">
                                            {{ csrf_field() }}
                                          <div class="form-body">
                                            
                                            <div class="form-group">
                                              <label for="userinput5">Name</label>
                                              <input class="form-control" type="text" name="name" placeholder="" value="{{ $cat->name }}" id="userinput5">
                                            </div>
                                            <div class="form-group">
                                              <label for="issueinput6">Parent</label>
                                              <select id="issueinput6" name="parent_id" class="form-control" data-original-title="" title="">
                                              @foreach($data as $parent)
                                                  <option value="{{ $parent->id }}" @if($parent->id == $cat->parent_id) selected @endif> {{ $parent->name }}</option>
                                              @endforeach
                                              </select>
                                            </div>
                                            <div class="form-group">
                                              <label for="userinput8">Description</label>
                                              <textarea id="userinput8" rows="5" class="form-control" name="description" placeholder="">{{ $cat->description }}</textarea>
                                            </div>
                                            <div class="form-group">
                                              <label for="issueinput6">Status</label>
                                              <select id="issueinput6" name="status" class="form-control" data-original-title="" title="" name="status">
                                                <option value="">Select Status</option>
                                                <option value="1" {{ $cat->status == 1 ? 'selected=selected' : '' }}>Active</option>
                                                <option value="0" {{ $cat->status == 0 ? 'selected=selected' : '' }}>Inactive</option>
                                              </select>
                                            </div>
                                          </div>
                                          
                                          <div class="form-actions right">
                                            <button type="submit" class="btn btn-primary">
                                              <i class=""></i> Save
                                            </button>
                                          </div>
                                        </form>
                                    </div>
                                </div>
                                <!--main card end-->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection