 @extends('admin.layout.master')
    @section('content')
    <style>
    .pro-add-attrib .select2-container {
    width: 90% !important;
    float: left;
    }
    .pro-options .select2-container {
    width: 48% !important;
    float: left;
    }
   </style>
    <div class="app-content content">
    <div class="content-wrapper">
    <div class="content-body product-page">
    <!-- Basic form layout section start -->
    <section id="configuration" class="user">
    <div class="row">
    <div class="col-12">
    <div class="card rounded pro-main">
    <div class="card-content collapse show">
    <div class="card-body table-responsive card-dashboard">
    <div class="row">
    <div class="col-12">
    <h1 class="pull-left">Edit Product</h1>
    </div>

    </div>
    <div class="clearfix"></div>

    <form class="form" method="post" action="{{ route('product.save') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-body">
    <div class="row">
    <div class="col-sm-4">
    <div class="form-group">
    <label for="userinput5">Name</label>
    <input class="form-control" type="text" name="name" placeholder="" value="{{ $product->name }}" id="userinput5" required="required">
    </div>
    </div>
    <div class="col-sm-4">
    <div class="form-group">
    <label for="userinput5">Article Number</label>
    <input class="form-control" type="number" name="part_number" placeholder="" value="{{ $product->part_number }}" id="userinput5" required="required">
    </div>
    </div> 
    <div class="col-sm-4"> 
    <div class="form-group">
    <label for="issueinput6">Manufacturer</label>
    <select id="ProductManufacturerId" name="manufacturer_id" class="form-control" data-original-title="" title="" required="required">
    <option value=""></option>
    @foreach($brands as $brand)
    <option {{ ($brand->id == $product->manufacturer->id) ? 'selected=selected' : '' }} value="{{ $brand->id }}">{{ $brand->name }}</option>
    @endforeach
    </select>
    </div>
    </div> 
    </div>

    <div class="row">
    <div class="col-sm-4">
    <div class="form-group">
    <label for="issueinput6">Category</label>
    <select id="ProductCategories" name="parent_category_id[]" class="form-control" data-original-title="" title="">
    <option value="">Select Category</option>
    @foreach($parentcategories as $parent)
    <option value="{{ $parent->id }}">{{ $parent->name }}</option>
    @endforeach
    </select>
    </div>
    </div>
    
    <div class="col-sm-4">
    <div class="form-group">
    <label for="issueinput6">Applications</label>
    <select class="form-control" multiple="multiple" name="applications[]" id="ProductApplications">
    <option value=""></option>
    @foreach($applications as $app)
    <option value="{{ $app->id }}">{{ $app->name }}</option>
    @endforeach
    </select>
    </div>
    </div> 
    
    <div class="col-sm-4"> 
    <div class="form-group">
    <label for="issueinput6">Prodcuct Image</label>
    <div class="custom-file">
    <input type="file" name="image" class="custom-file-input" id="inputGroupFile01">
    <div class="input-group-prepend">
    <span class="input-group-text">Choose File</span>
    </div> 
    <label class="custom-file-label" for="inputGroupFile01">No file chosen</label>
    </div>
    </div>
    </div>
    </div>

    <div class="row">
    <div class="col-sm-8">
    <div class="form-group">
    <label for="userinput8">Description</label>
    <textarea id="userinput8" rows="5" class="form-control" name="description" placeholder="">{{ $product->description }}</textarea>
    </div>
    </div>
    <div class="col-sm-4"> 
    <div class="form-group pro-add-attrib">
    <label for="issueinput6">Attributes</label>
    <select id="ProductAttributesAll" name="attributes[]" class="form-control" data-original-title="" title="">
    <option value="">Select Attribute</option>
    @foreach($attributes_all as $attribute)
    <option value="{{ $attribute->id }}">{{ $attribute->name }}</option> 
    @endforeach
    </select>
    <div class="form-actions right">
    <span id="add_input" class="btn btn-primary">
    <i class="fa fa-plus" aria-hidden="true"></i>
    </span>
    </div>
    <!-------------- Show Exists Option ------------------->
    <div id="showattributes_exists">
    <?php 
    foreach($attributes_all as $attributeone):
        foreach ($product->attributes as $key => $proattribute):
           print_r($attributeone->name);
    ?>

        <div class="pro-del-attrib pro-options" id="optvalue-{{ $attributeone->id }}">
    <input class="form-control" data-type="key" name="key[]" type="text" placeholder="" id="userinput-{{ $attributeone->name }}" value="{{ $attributeone->name }}" readonly="readonly">

     <input type="hidden" name="attids[{{ $attributeone->id }}]" value="{{ $attributeone->id }}">
     <input type="hidden" name="att-namekeys[][{{ $attributeone->name }}]" value="{{ $attributeone->id }}">

    <select name="attvals[{{ $attributeone->id }}][attribute_option_value]" data-attrid="{{ $attributeone->id }}" class="attr-select form-control" data-original-title="" title="">
        <option value="">Select Attribute Value</option>
        @foreach($attributeone->options as $option)
        <option value="{{ $option->id }}">{{ $option->value }}</option>
        @endforeach
    </select>
    
    <input type="hidden" id="value-{{ $attributeone->id }}" data-type="value" name="value[]" readonly="readonly" class="value" value="">

     <div class="form-actions right">
        <span class="btn btn-primary btn_remove" id="{{ $attributeone->id }}">
          <i class="fa fa-trash" aria-hidden="true"></i>
        </span>
    </div>  
      </div>
    <?php  endforeach; endforeach; ?> 
    </div>
    <!-------------- Show Exists Option ------------------->
    <div id="showattributes"></div>
    </div>

    </div> 
    </div>

    <div class="row">
    <div class="col-sm-8">
    <div class="form-group">
    <label for="issueinput6">Status</label>
    <select id="ProductStatus" name="status" class="form-control" data-original-title="" title="" required="required">
    <option value="">Select Status</option>
    <option {{ ($product->status == 1) ? 'selected=selected' : '' }} value="1">Active</option>
    <option {{ ($product->status == 0) ? 'selected=selected' : '' }} value="0">In-Active</option>
    </select>
    </div>
    </div>
    <div class="col-sm-4">
    <div class="form-actions right">
    <button type="submit" class="btn btn-primary">
    <i class=""></i> Save
    </button>
    </div>
    </div>
    </div>
    </form>
    </div>
    </div>
    <!--main card end-->

    </div>
    </div>
    </div>
    </section>
    <!-- // Basic form layout section end -->
    </div>
    </div>
    </div>
    @endsection
    @section('js')
    <script type="text/javascript">
    $("#userinput6").select2({
    });
    $("#ProductManufacturerId").select2({
    placeholder: 'Select Manufacturer',
    });
    $("#ProductApplications").select2({
    placeholder: 'Select Applications',
    multiple: true,
    });
    $("#ProductStatus").select2({
    placeholder: 'Select Status',
    minimumResultsForSearch: -1
    });
    $("#ProductAttributesAll").select2({
    placeholder: 'Select Attribute',
    allowClear: true,
    });
    $("#ProductCategories").select2({
    placeholder: 'Select Category',
    });

    $('#add_input').click(function(){
    var cur_attr_key = $("#ProductAttributesAll").val();
    var cur_attr_text = $('#ProductAttributesAll :selected').text();
    if(cur_attr_key == "" || cur_attr_text == "")
    {
    alert("Select an attribute and it's value.");
    return false;
    }
    existingOptions = optionExists(cur_attr_key);
    if($.inArray(cur_attr_text, existingOptions) > -1)
    {
    alert("The option '" + cur_attr_text + "'' already exists. For updating attribute value, first remove the existing one please.");
    return false;
    }
    //initSelect2(cur_attr_text);
    clickAdd(cur_attr_text, cur_attr_key);
    $('#ProductAttributesAll').val('').trigger('change');
    });

    $(document).on('click', '.btn_remove', function(){
    var button_id = $(this).attr("id");
    $('#optvalue-'+button_id).remove();
    });

    $('body').on('change', '.attr-select',function(){
        var id  =   $(this).data('attrid');
        var value   =   $(this).val();
        $('body #value-'+id).val(value);
    });

    </script>
    @endsection