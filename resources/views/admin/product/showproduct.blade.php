@extends('admin.layout.master')
@section('content')
 <div class="app-content content view-only">
  <div class="content-wrapper">
  <div class="content-body ">
  <!-- Basic form layout section start -->
  <section id="configuration" class="user">
  <div class="row">
  <div class="col-12">
  <div class="card rounded pro-main">
  <div class="card-content collapse show">
  <div class="card-body table-responsive card-dashboard">
  <div class="row">
  <div class="col-sm-10">
  <h1 class="pull-left">View Product</h1>
  </div>
  <div class="col-sm-2">
  <div class="form-actions right">
  <button onclick="window.location.href='{{ route('editproduct', $product->id) }}'" type="submit" class="btn btn-primary">
  <i class="fa fa-pencil"></i>Edit Product
  </button>
  </div>
  </div>
  </div>
  <div class="clearfix"></div>

  <section class="grid-with-inline-row-label " id="grid-with-inline-row-label">
  <div class="row">
  <div class="col-6">
  <div class="card card2">
  <div class="card-content collapse show">
  <div class="card-body card-body2">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Id</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $product->custom_id }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Article Number</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $product->part_number }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>

  <div class="col-6">
  <div class="card card2">
  <div class="card-content collapse show">
  <div class="card-body card-body2">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Name</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $product->name }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Brand</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ (!empty($product_brands->name)) ? '$product_brands->name' : '' }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="col-12">
  <div class="card card2">
  <div class="card-content collapse show">
  <div class="card-body card-body2">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2 label_t">Categories</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control field_t" placeholder="{{ $product->categories->implode('name', ',') }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2 label_t"></label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <div class="form-control field_t pro_ul">
  <ul>
  <li>{!! $product->categories->implode('name', '</li><li>') !!}</li>
  </ul>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="col-6">
  <div class="card card2">
  <div class="card-content collapse show">
  <div class="card-body card-body2">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Applications</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $product->applications->implode('name', ',') }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Description</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $product->description }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Created At</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $product->created_at }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <!-- Image -->
   <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Image</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <img src="{{ asset('storage/product/'. $product->id . '/data:image/png;base64,' . $product->image) }}" alt="Product-image" />
  </div>
  </div>
  </div>
  </div>
  </div>
  <!-- Image -->
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>

  <div class="col-6">
  <div class="card card2">
  <div class="card-content collapse show">
  <div class="card-body card-body2">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Attributes</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">

  <input type="text" class="form-control" placeholder="{{ collect($product->attributes)->map(function($value, $key){ 
    return $key . ': ' . $value; 
  })->implode(',') }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Status</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ ($product->status) ? 'Active' : 'In-Active' }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Modified At</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $product->updated_at }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>
  </div>
  </section>
  </div>
  </div>
  <!--main card end-->

  </div>
  </div>
  </div>
  </section>
  <!-- // Basic form layout section end -->
  </div>
  </div>
  </div>
  @endsection
