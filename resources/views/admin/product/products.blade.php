@extends('admin.layout.master')
@section('content')
   <div class="app-content content">
        <div class="content-wrapper">
         @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
           @endif
            <div class="content-body app-page">
                <!-- Basic form layout section start -->
                <section id="configuration" class="user">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pro-main">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="app-page-header row">
                                            <div class="col-sm-10">
                                                <h1 class="pull-left">Products</h1>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-actions right">
                                                    <button onclick="window.location.href='{{ route('addproduct') }}'" type="submit" class="btn btn-primary">
                                                      <i class="fa fa-plus-circle"></i>New Products
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                        <form id="search-form" method="post" class="form form-horizontal striped-rows form-bordered">
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                          <label class="" for="">Search by Id</label>
                                                          <input type="text" id="" class="form-control" placeholder="Search by Id" name="id">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                         <div class="form-group">
                                                          <label class="" for="">Search by Name</label>
                                                          <input type="text" id="" class="form-control" placeholder="Search by Name" name="name">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                              <label for="issueinput6">Search by Article Number</label>
                                                              <input type="text" id="" class="form-control" placeholder="Search by Article Number" name="part_number">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                              <label for="issueinput6">Search by Status</label>
                                                              <select id="selected-option" name="status" class="form-control" data-original-title="" title="">
                                                                <option value="">Search By Status</option>
                                                                <option value="1">Active</option>
                                                                <option value="0">In-Active</option>
                                                              </select>
                                                        </div>
                                                    </div>
                                                      <div class="col-sm-2"> </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-actions right">
                                                            <button type="submit" class="btn btn-primary">
                                                              <i class=""></i> Search
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                               
                                  <table class="table table-striped table-bordered zero-configuration dataTable no-footer" id="products_table" role="grid" aria-describedby="DataTables_Table_0_info">
                                <thead>
                                    <tr role="row">
                                      <th>ID</th>
                                      <th>Product Name</th>
                                      <th>Article Number</th>
                                      <th>Application</th>
                                      <th>Brand</th>
                                      <th>Categories</th>
                                      <th>Status</th>
                                      <th>Actions</th>
                                      </tr>
                                </thead>
                                 </table>
                                    </div>
                                </div>
                                <!--main card end-->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
   var oTable = '';
$(function() {
    oTable = $('#products_table').DataTable({
        processing: true,
        serverSide: true,
        rowReorder: {
          "update": false,
        },
        colReorder: true,
        order:[0,'desc'],
        ajax: {
          url: '{{ route("getproducts") }}',
          type: 'post',
          data: function (d) {
                d.name = $('input[name=name]').val();
                d.id = $('input[name=id]').val();
                d.part_number = $('input[name=part_number]').val();
                d.status  = $('#selected-option option:selected').val();
            }
        },
        columns: [
            { data: 'custom_id', name: 'custom_id' },
            { data: 'name', name: 'name' },
            { data: 'part_number', name: 'part_number' },
            { data: 'applications', name: 'applications' },
            { data: 'brand', name: 'brand' },
            { data: 'categories', name: 'categories' },
            { data: 'status', name: 'status' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false },
        ]
    });
});

$('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
});

</script>
@endsection