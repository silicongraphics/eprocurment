<div class="pro-del-attrib pro-options" id="optvalue-{{ $attribute_id }}">
    <input class="form-control" data-type="key" name="key[]" type="text" placeholder="" id="userinput-{{ $attribute_name }}" value="{{ $attribute_name }}" readonly="readonly">

     <input type="hidden" name="attids[{{ $attribute_id }}]" value="{{ $attribute_id }}">
     <input type="hidden" name="att-namekeys[][{{ $attribute_name }}]" value="{{ $attribute_id }}">

    <select name="attvals[{{ $attribute_id }}][attribute_option_value]" data-attrid="{{ $attribute_id }}" class="attr-select form-control" data-original-title="" title="">
    	<option value="">Select Attribute Value</option>
    	@foreach($options as $option)
        <option value="{{ $option->id }}">{{ $option->value }}</option>
        @endforeach
    </select>
    
    <input type="hidden" id="value-{{ $attribute_id }}" data-type="value" name="value[]" readonly="readonly" class="value" value="">

     <div class="form-actions right">
        <span class="btn btn-primary btn_remove" id="{{ $attribute_id }}">
          <i class="fa fa-trash" aria-hidden="true"></i>
        </span>
    </div>  
</div>