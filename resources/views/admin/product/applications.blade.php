@extends('admin.layout.master')
@section('content')
   <div class="app-content content">
        <div class="content-wrapper">
           @if(session()->get('success'))
          <div class="alert alert-success">
            {{ session()->get('success') }}  
          </div>
           @endif
            <div class="content-body app-page">
                <!-- Basic form layout section start -->
                <section id="configuration" class="user">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pro-main">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="app-page-header row">
                                            <div class="col-sm-10">
                                                <h1 class="pull-left">Applications</h1>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-actions right">
                                                    <button onclick="window.location.href='{{ route('addapplication') }}'" type="submit" class="btn btn-primary">
                                                      <i class="fa fa-plus-circle"></i>New Applications
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                        <form method="post" class="form form-horizontal striped-rows form-bordered" id="search-form">
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                          <label class="" for="">Search by ID</label>
                                                          <input type="text" id="" class="form-control" placeholder="Search By Id" name="application_id">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                              <label for="issueinput6">Search by Name</label>
                                                              <input type="text" id="issueinput6" class="form-control" placeholder="Saerch By Name" name="application_name">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                              <label for="issueinput6">Search by Status</label>
                                                            <select id="selected-option" name="status" class="form-control" data-original-title="" title="">
                                                                <option value="">Search By Status</option>
                                                                <option value="1">Active</option>
                                                                <option value="0">In-Active</option>
                                                              </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2"> </div>
                                                    <div class="col-sm-2"> </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-actions right">
                                                            <button type="submit" class="btn btn-primary">
                                                              <i class=""></i> Search
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                       <table class="table table-striped table-bordered zero-configuration dataTable no-footer" id="applications" role="grid" aria-describedby="DataTables_Table_0_info">
                                              <thead>
                                                <tr role="row">
                                                  <th>ID</th>
                                                  <th>Name</th>
                                                  <th>Status</th>
                                                  <th>Created At</th>
                                                  <th>Modified At</th>
                                                  <th>Actions</th></tr>
                                              </thead>
                                              <tbody> 
                                    </div>
                                </div>
                                <!--main card end-->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
    <!-- applications --> 
@endsection
@section('js')
<script>
   var oTable = '';
$(function() {
     oTable = $('#applications').DataTable({
        processing: true,
        serverSide: true,
        rowReorder: {
          "update": false,
        },
        colReorder: true,
        ajax: {
            url: '{{ route("getapplications") }}',
            type: 'post',
            data: function (d) {
                d.name = $('input[name=application_name]').val();
                d.id = $('input[name=application_id]').val();
                d.status  = $('#selected-option option:selected').val();
            }
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'actions', name: 'actions', orderable: false, searchable: false}
        ]
    });
});

$('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
});

</script>
@endsection