@extends('admin.layout.master')
@section('content')
@section('css')
<link rel="stylesheet" href="{{asset('css/treeview.css')}}">
@endsection
   <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="configuration" class="user">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded pro-main">
                                <div class="card-content collapse show">
                                    <div class="card-body table-responsive card-dashboard">
                                        <div class="row">
                                            <div class="col-12">
                                                <h1 class="pull-left">ADD Category</h1>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>

                                        <form class="form form-half" method="post" action="{{ route('savecategory') }}">
                                            {{ csrf_field() }}
                                          <div class="form-body">
                                            
                                            <div class="form-group">
                                              <label for="userinput5">Name</label>
                                              <input class="form-control" type="text" name="name" placeholder="" id="userinput5" required>
                                            </div>
                                            <div class="form-group">
                                              <label for="issueinput6">Parent</label>
                                              <input type="text" autocomplete="off" name="parent_id_sel" id="parent_id_sel" placeholder="Type to filter"/>
                                              <input type="hidden" autocomplete="off" name="parent_id" id="parent_id"/>
                                            </div>
                                            <div class="form-group">
                                              <label for="userinput8">Description</label>
                                              <textarea id="userinput8" rows="5" name="description" class="form-control" placeholder=""></textarea>
                                            </div>
                                            <div class="form-group">
                                              <label for="issueinput6">Status</label>
                                              <select id="issueinput6" name="status" class="form-control" data-original-title="" title="" required>
                                                <option value="">Select Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                              </select>
                                            </div>
                                          </div>
                                          
                                          <div class="form-actions right">
                                            <button type="submit" class="btn btn-primary">
                                              <i class=""></i> Save
                                            </button>
                                          </div>
                                        </form>
                                    </div>
                                </div>
                                <!--main card end-->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection

@section('js')
<script src="{{ asset('js/comboTreePlugin.js') }}"></script>
<script>

  var comboTree2;
    $(function(){
        var $load = $.get(base_url + '/admin/category/get-list',function(){
            JSONdata = $load.responseJSON;
        comboTree2 = $('#parent_id_sel').comboTree({
          source : JSONdata,
          isMultiple: false
        });
        });
    })

</script>
@endsection