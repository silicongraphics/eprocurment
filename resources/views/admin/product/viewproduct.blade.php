  @extends('admin.layout.master')
@section('title')
  View - Product
@endsection
@section('content')
  <div class="app-content content view-only">
  <div class="content-wrapper">
  <div class="content-body ">
  <!-- Basic form layout section start -->
  <section id="configuration" class="user">
  <div class="row">
  <div class="col-12">
  <div class="card rounded pro-main">
  <div class="card-content collapse show">
  <div class="card-body table-responsive card-dashboard">
  <div class="row">
  <div class="col-sm-10">
  <h1 class="pull-left">View Products</h1>
  </div>
  <div class="col-sm-2">
  <div class="form-actions right">
  <button onclick="window.location.href='#'" type="submit" class="btn btn-primary">
  <i class="fa fa-pencil"></i>Edit Product
  </button>
  </div>
  </div>
  </div>
  <div class="clearfix"></div>

  <section class="grid-with-inline-row-label " id="grid-with-inline-row-label">
  <div class="row">
  <div class="col-6">
  <div class="card card2">
  <div class="card-content collapse show">
  <div class="card-body card-body2">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Id</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $product->id }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Article Number</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="{{ $product->part_number }}" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>

  <div class="col-6">
  <div class="card card2">
  <div class="card-content collapse show">
  <div class="card-body card-body2">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Name</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="Flex Angle Grinder" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Brand</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="Flex" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="col-12">
  <div class="card card2">
  <div class="card-content collapse show">
  <div class="card-body card-body2">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2 label_t">Categories</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control field_t" placeholder="Power Tools, Corded, Angle, Straight, Bench Grinder, Angle Grinder" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2 label_t"></label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <div class="form-control field_t pro_ul">
  <ul>
  <li>Power Tools</li>
  <li>Corded > Power Tools</li>
  <li>Angle > Corded > Power Tools</li>
  <li>Straight > Corded > Power Tools</li>
  <li>Bench Grinder > Corded > Power Tools</li>
  <li>Angle Grinder > Angle > Corded > Power Tools</li>
  </ul>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="col-6">
  <div class="card card2">
  <div class="card-content collapse show">
  <div class="card-body card-body2">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Applications</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="Metal Work and Fabrication, Concrete Work, Stone Working" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Description</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="P00026" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Created At</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="2018-10-15 07:58:50" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>

  <div class="col-6">
  <div class="card card2">
  <div class="card-content collapse show">
  <div class="card-body card-body2">
  <form action="#">
  <div class="form-body">
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Attributes</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="Power:18V Voltage:18V" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Status</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="Active" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div class="row">
  <label class="col-lg-2">Modified At</label>
  <div class="col-lg-10">
  <div class="row">
  <div class="col-md-12">
  <input type="text" class="form-control" placeholder="2018-10-15 07:58:50" readonly="">
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>
  </div>
  </section>
  </div>
  </div>
  <!--main card end-->

  </div>
  </div>
  </div>
  </section>
  <!-- // Basic form layout section end -->
  </div>
  </div>
  </div>
  @endsection