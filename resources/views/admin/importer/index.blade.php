@extends('admin.layout.master')
@section('title')
  Import CSV Data to MySQL database with Laravel
@endsection
@section('content')
   <div class="app-content content">
        <div class="content-wrapper">
     <!-- Message -->
     @if(Session::has('message'))
        <p >{{ Session::get('message') }}</p>
     @endif

     <!-- Form -->
     @if(@$_GET['onlyimage'])
     <form method='post' action="{{ route('uploadImgFile') }}" enctype='multipart/form-data' >

     @else
     <form method='post' action="{{ route('uploadFile') }}" enctype='multipart/form-data' >

     @endif
       {{ csrf_field() }}
       <input type='file' name='file' >
       <input type='submit' name='submit' value='Import'>
     </form>
  </div>
</div>
@endsection