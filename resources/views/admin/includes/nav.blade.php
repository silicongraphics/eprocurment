<div class="main-menu menu-fixed menu-light menu-accordion" data-scroll-to-active="true">
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class="nav-item"><a href=""><i class="fa fa-th-large"></i><span class="menu-title" data-i18n="">products</span></a>
                    <ul class="open_nav">
                        <li>
                            <a href="{{ route('addproduct')}}">Add new Product</a>
                            <a href="{{ route('products')}}">Product Listing</a>
                            <a href="{{ route('productattributes')}}">Product Attributes</a>
                            <a href="{{ route('applications') }}">Product Applications</a>
                            <a href="{{ route('categories') }}">Product Categories</a>
                            <a href="{{ route('brands') }}">Product Brands</a>
                            <!-- <a href="#">Product Mandatory Attributes</a> -->
                        </li>
                    </ul>
                </li>
                <li class="nav-item "><a href=""><i class="fa fa-exclamation-circle"></i><span class="menu-title" data-i18n="">suppliers</span></a>
                    <ul class="open_nav">
                        <li>
                            <a href="{{ route('addsupplier') }}">Add new Supplier</a>
                            <a href="{{ route('suppliercompanies') }}">Supplier Companies</a>
                            <a href="{{ route('supplierusers') }}">Supplier Users</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item "><a href=""><i class="fa fa-comments"></i><span class="menu-title" data-i18n="">Clients</span></a>
                    <ul class="open_nav">
                        <li>
                            <a href="{{ route('addclient')}}">Add new Client</a>
                            <a href="{{ route('pendingclients')}}">Pending Clients</a>
                            <a href="{{ route('companies')}}">Client Companies</a>
                            <a href="{{ route('clientusers')}}">Client User</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item "><a href=""><i class="fa fa-file-text"></i><span class="menu-title" data-i18n="">RFQS</span></a>
                    <ul class="open_nav">
                        <li>
                            <a href="eproc-client-related-rfqs.html">Add new Client</a>
                            <a href="eproc-vendor-related-rfqs.html">Pending Clients</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item "><a href=""><i class="fa fa-user-circle"></i><span class="menu-title" data-i18n="">user management</span></a></li>
            </ul>
        </div>
    </div>