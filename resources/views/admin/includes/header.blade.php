<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    <!-- fixed-top-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-light navbar-border">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item"> <a class="navbar-brand" href="{{ route('admin') }}"> <img class="brand-logo" alt="stack admin logo" src="{{ asset('admin-assets/images/logo.png') }}"> </a> </li>
                    <li class="nav-item d-md-none"> <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a> </li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                    </ul>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-user nav-item"> <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"> <span class="avatar avatar-online"> <img src="{{ asset('admin-assets/app-assets/images/portrait/small/avatar-s-1.png') }}" alt="avatar"><i></i></span> <span class="user-name">{{ Auth::guard()->user()->first_name }}</span> 
                        </a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- <a class="dropdown-item" href="a-profile.html"><i class="ft-user"></i> Edit Profile</a> <a class="dropdown-item" href="#"><i class="ft-mail"></i> My Inbox</a> <a class="dropdown-item" href="#"><i class="ft-check-square"></i> Task</a> <a class="dropdown-item" href="#"><i class="ft-message-square"></i> Chats</a>
                                -->                               
                                <div class="dropdown-divider"></div>
                                <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="javascript:;" class="dropdown-item"><i class="ft-power"></i> Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>