@extends('layout.app')

@section('content')

    <div class="login-form">
		<div class="container">
			<h1>Welcome to E-Procurement</h1>
			<span>Please Select login type for your account</span>
			<form class="form-login-panel">
				<p>Login as</p>
				<div class="form-group">
    				<a href="{{url('login/client')}}"  name="submit" class="btn-block btn-subs">Client</a>
				</div>
				<p>OR</p>
				<div class="form-group">
                    <a href="{{url('login/vendor')}}"  name="submit" class="btn-block btn-subs">Vendor</a>
				</div>
			</form>
		</div>
	</div>

@endsection