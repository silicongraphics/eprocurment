@extends('layout.app')
@section('title', 'Register - Vendor')
@section('content')
<style>
.tab-content {
    width: 100%;
    float: left;
    background: transparent;
    padding: 25px 0;
    /* box-shadow: 0 0 10px rgba(0, 0, 0, 0.12); */
    border: none;
}

.nav.nav-tabs {
    border-bottom: 2px solid #0a435c;
    border-radius: 0;
	    margin-bottom: 20px;
}

.nav-tabs .nav-item {
    margin-bottom: -2px;
}

.nav-tabs .nav-link.active,
.nav-tabs .nav-item.show .nav-link {
    color: #4E5154;
    background-color: #F5F7FA;
    border-color: #626E82 #626E82 #F5F7FA;
}

.nav.nav-tabs .nav-item .nav-link {
    padding: 0.5rem 0.7rem;
    display: inline-flex;
}

.nav.nav-tabs .nav-item .nav-link {
    padding: 12px 25px;
    color: #4f575d;
    text-decoration: none;
}

.nav.nav-tabs .nav-item.active .nav-link {
    background-color: #0a435c !important;
    border-radius: 0;
    color: #fff;
}

.nav>li>a:focus,
.nav>li>a:hover {
    text-decoration: none;
    background-color: transparent;
}
	.select2-container--default.select2-container--focus .select2-selection--multiple {
    border: none;
    outline: 0;
    border-bottom: 2px solid #00658a !important;
}
.select2-container--default .select2-selection--multiple {
    background-color: white;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: text;
    border: none;
    background: transparent;
    box-shadow: none;
    border-bottom: 2px solid #c6c6da;
    border-radius: 0;
    height: 66px;
    color: #2d2e4f;
    font-size: 13px;
    padding: 0 30px 0 4px;
    margin-bottom: 20px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 23px;
}
	.select2-dropdown {
    background-color: white;
    border: none;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
    border: none;
    background: transparent;
}
	.select2-search--dropdown {
    display: block;
    padding: 0;
    background: #edf0f5;
    border: none;
}
	.select2-container--default .select2-selection--single .select2-selection__placeholder {
    height: 44px;
    color: #66899a;
    font-size: 13px;
    margin-bottom: 0;
    font-weight: 400;
		    border-bottom: 2px solid #c6c6da;
}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 28px;
    height: 44px;
    color: #2d2e4f;
    font-size: 13px;
    float: left;
    padding: 0;
    width: 100%;
    padding: 0;
    text-align: left;
    border-bottom: 2px solid #c6c6da;
}
.select2-container--default .select2-selection--single {
    background-color: transparent;
    border: transparent;
    border-radius: 0;
}
.nav-tabs>li>a:hover {
    border-color: transparent;
}

form label {
    color: #0a435c;
    font-size: 12px;
    font-weight: 700;
    text-align: left;
    float: left;
}

form .form-control {
    border: none;
    background: transparent !important;
    box-shadow: none;
    border-bottom: 2px solid #c6c6da;
    border-radius: 0;
    height: 44px;
    color: #2d2e4f;
    font-size: 13px;
    padding: 0 30px 0 4px;
    margin-bottom: 20px;
}

form .form-section {
    color: #404E67;
    line-height: 3rem;
    margin-bottom: 20px;
    border-bottom: 1px solid #404E67;
}

.pro-main h4 {
    color: #202021;
    text-transform: uppercase;
    margin: 0 0 25px;
    font-size: 18px;
    text-align: left;
    color: #0e405b;
}

.edit_client .form-section {
    border-bottom: 3px solid #d8dee6;
}

.checkboxsas {
    float: right;
}

.label-note {
    color: #7a7a7a;
    margin-bottom: 0;
}

.checkboxsas input[type="checkbox"] {
    position: relative;
    top: 2px;
}

form.form.billing_form {
    margin-top: 30px;
}

.custom-file {
    position: relative;
    display: inline-block;
    width: 100%;
    height: calc(2.75rem + 2px);
    margin-bottom: 0;
}

.custom-file-input {
    position: relative;
    z-index: 2;
    width: 100%;
    height: calc(2.75rem + 2px);
    margin: 0;
    opacity: 0;
}

.input-group-prepend {
    position: absolute;
    top: 0;
}

.custom-file-label {
    position: absolute;
    top: 0;
    right: 0;
    left: 85px;
    z-index: 1;
    height: 45px;
    padding: 0.75rem 2rem;
    line-height: 28px;
    color: #4E5154;
    background-color: transparent;
    border: none;
    border-radius: 0;
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
    border-left: none;
    border-bottom: 2px solid #c6c6da;
}

.custom-file-label::after {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    z-index: 3;
    display: block;
    height: calc(calc(2.75rem + 2px) - 1px * 2);
    padding: 0.75rem 1rem;
    line-height: 1.25;
    color: #4E5154;
    content: "Browse";
    background-color: #F5F7FA;
    border-left: 1px solid #BABFC7;
    border-radius: 0 0.25rem 0.25rem 0;
    display: none;
}
.nav-item {
    width: auto;
}
	.rounded {
    border-radius: 0;
    display: block;
    background: transparent;
    border: none;
}
custom-file-label-doc::after {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0px;
    z-index: 3;
    display: block;
    height: calc(calc(2.75rem + 1px) - 1px * 2);
    padding: 0.75rem 1rem;
    line-height: 1.35;
    color: #4E5154;
    content: "\f067" !important;
    font-family: FontAwesome;
    background-color: #F5F7FA;
    border-left: 1px solid #ccd6e6;
    border-radius: 0 0.25rem 0.25rem 0;
    display: none;
}

.custom-file-input:lang(en)~.custom-file-label::after {
    content: "Browse";
}

.input-group-text {
    display: flex;
    align-items: center;
    padding: 0.75rem 1rem;
    margin-bottom: 0;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.25;
    color: #4E5154;
    font-size: 12px !important;
    text-align: center;
    white-space: nowrap;
    background-color: #F5F7FA;
    border-bottom: 2px solid #c6c6da;
    border-radius: 0;
}

.product-page .input-group-prepend .input-group-text {
    border: 1px solid #ccd6e6;
}

button.btn.btn-primary {
    border-color: #0a435c !important;
    background-color: #0a435c !important;
    color: #FFF !important;
    width: 100%;
    margin-top: 20px;
    height: 44px;
    border-radius: 0;
    }
    .select2-container *:focus {
        outline: none;
    }
    @media (min-width: 320px) and (max-width: 991px) {
}

@media (min-width: 320px) and (max-width: 991px) {
    .table-responsive {
        border: none;
    }

    .login-form span {
        font-size: 13px;
        line-height: 28px;
        margin: 0px 0 10px;
    }

    form.form.billing_form {
        margin-top: 0;
    }

    .custom-file {
        height: auto;
    }

    .custom-file-input {
        height: 44px;
    }

    .pro-main h4 {
    font-size: 14px;
    }
    .select2-container *:focus {
        outline: none;
    }
        font-size: 14px;
           .merge_file_a14280
    }
}

</style>
<div class="login-form">
<div class="container">
    <h1>Welcome to E-Procurement</h1>
    <span>Please create your account.</span>
    <div class="card rounded pro-main">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form id="SupplierAddForm" class="form" method="post" action="{{ url('register/vendor') }}"
            enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card-content collapse show">
                <div class="card-body table-responsive card-dashboard">
                    <div class="clearfix"></div>
                    <ul class="nav nav-tabs">
                        <li class="nav-item active">
                            <a class="nav-link" id="base-tab1" data-toggle="tab" aria-controls="tab1"
                               href="#tab1" aria-expanded="true">Vendor Company
                                Info</a></li>
                        <li class="nav-item">
                            <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2"
                                href="#tab2" aria-expanded="false">Vendor
                                Admin Info</a></li>
                    </ul>
                    <div class="tab-content px-1 pt-1">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab1"
                            aria-expanded="true" aria-labelledby="base-tab1">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="userinput5">Company Name</label>
                                            <input class="form-control" type="text" value="{{ old('name') }}" name="name" placeholder="Enter company name"
                                                id="company_name" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="userinput5">Website</label>
                                            <input class="form-control" type="text" value="{{ old('website') }}" name="website"
                                                placeholder="Enter Website" id="website" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="userinput5">Tax Registration No</label>
                                            <input class="form-control" type="number" name="tax_registration_no" placeholder="Tax Registration No" value="{{ old('tax_registration_no') }}" id="registration_no">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                         <label for="userinput5">Products Supplied</label>
                                            <select id="products_supplied" name="products_supplied[]" multiple="multiple" class="form-control" data-original-title="" title="">
                                            <option value="">Products Supplied</option>                         
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                     <div class="form-group">
                                            <label for="userinput5">Delievery Method</label>
                                            <select id="delivery_method" name="delivery_method_id"
                                                class="form-control" data-original-title="" title="">
                                                <option value="">Select Delivery Method</option>
                                                @foreach($delivery_methods as $delivery_method)
                                                    <option {{ (old('delivery_method_id') == $delivery_method->id) ? 'selected=selected' : '' }} value="{{ $delivery_method->id }}">
                                                        {{ ucfirst($delivery_method->name) }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                      </div>
                                   <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="issueinput6">Payment Method</label>
                                            <select id="payment_method" name="payment_method_id"
                                                class="form-control" data-original-title="" title="">
                                                <option value=""></option>
                                                @foreach($payment_methods as $payment_method)
                                                <option {{ (old('payment_method_id') == $payment_method->id) ? 'selected=selected' : '' }}  value="{{ $payment_method->id }}">
                                                    {{ ucfirst($payment_method->name) }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="issueinput6">Payment Terms</label>
                                            <select id="payment_term" name="credit_term_id" class="form-control"
                                                data-original-title="" title="">
                                                <option value=""></option>
                                                @foreach($payment_terms as $payment_term)
                                                <option {{ (old('credit_term_id') == $payment_term->id) ? 'selected=selected' : '' }} value="{{ $payment_term->id }}">
                                                    {{ $payment_term->name }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="form billing_form">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h4 class="form-section">Billing Address</h4>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Address</label>
                                                        <input type="text" id="projectinput1"
                                                            class="form-control billing_address"
                                                            placeholder="Billing Address"
                                                            name="billing_address" value="{{ old('billing_address') }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="projectinput3">Street</label>
                                                        <input type="text" id="projectinput3"
                                                            class="form-control billing_street"
                                                            placeholder="Billing Street" value="{{ old('billing_street') }}" name="billing_street">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="projectinput3">PoBox</label>
                                                        <input type="text" id="projectinput3"
                                                            class="form-control billing_pobox"
                                                            placeholder="Billing PO Box" value="{{ old('billing_pobox') }}" name="billing_pobox">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="issueinput6">Country</label>
                                                        <select id="billing_country" name="billing_country_id"
                                                            class="form-control billing_country_id"
                                                            data-original-title="" title="">
                                                            <option value="">Country</option>
                                                            @foreach($countries as $country)
                                                            <option {{ (old('billing_country_id') == $country->id) ? 'selected=selected' : '' }} value="{{ $country->id }}">
                                                                {{ $country->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="issueinput6">City</label>
                                                        <select id="billing_city" name="billing_city_id"
                                                            class="form-control billing_city_id"
                                                            data-original-title="" title="">
                                                            <option value="">City</option>
                                                            @foreach($cities as $city)
                                                            <option {{ (old('billing_city_id') == $city->id) ? 'selected=selected' : '' }} value="{{ $city->id }}"> {{ $city->name }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="form-section">Shipping Address</h4>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <fieldset class="checkboxsas">
                                                            <label class="label-note">
                                                                <input type="checkbox" id=""
                                                                    class="same_addresses" value="">
                                                                Same as Billing Address </label>
                                                        </fieldset>
                                                        <label for="projectinput1">Address</label>
                                                        <input type="text" id="projectinput1"
                                                            class="form-control shipping_address"
                                                            placeholder="Shipping Address" name="shipping_address" value="{{ old('shipping_address') }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="projectinput3">Street</label>
                                                        <input type="text" id="projectinput3"
                                                            class="form-control shipping_street"
                                                            placeholder="Shipping Street"
                                                            name="shipping_street" value="{{ old('shipping_street') }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="projectinput3">PoBox</label>
                                                        <input type="text" id="projectinput3"
                                                            class="form-control shipping_pobox"
                                                            placeholder="Shipping PoBox" name="shipping_pobox" value="{{ old('shipping_pobox') }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="issueinput6">Country</label>
                                                        <select id="shipping_country" name="shipping_country_id"
                                                            class="form-control shipping_country_id"
                                                            data-original-title="" title="">
                                                            <option value="">Country</option>
                                                            @foreach($countries as $country)
                                                            <option {{ (old('shipping_country_id') == $country->id) ? 'selected=selected' : '' }} value="{{ $country->id }}">
                                                                {{ $country->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="issueinput6">City</label>
                                                        <select id="shipping_city" name="shipping_city_id"
                                                            class="form-control shipping_city_id"
                                                            data-original-title="" title="">
                                                            <option value="">City</option>
                                                            @foreach($cities as $city)
                                                            <option {{ (old('shipping_city_id') == $city->id) ? 'selected=selected' : '' }} value="{{ $city->id }}"> {{ $city->name }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="userinput5">Notes</label>
                                                <input class="form-control" type="text" name="notes"
                                                    placeholder="Notes" id="notes" value="{{ old('notes') }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="issueinput6">Trade Liscense</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"
                                                        name="trade_liscense" id="inputGroupFile01" required>
                                                    <div class="input-group-prepend"><span
                                                            class="input-group-text">Choose File</span>
                                                    </div>
                                                    <label class="custom-file-label" for="inputGroupFile01">No
                                                        file
                                                        chosen</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="issueinput6">VAT Certificate</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"
                                                        name="vat_certificate" id="inputGroupFile02" required>
                                                    <div class="input-group-prepend"><span
                                                            class="input-group-text">Choose File</span>
                                                    </div>
                                                    <label class="custom-file-label" for="inputGroupFile01">No
                                                        file
                                                        chosen</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form billing_form">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="form-section">Additional Approver For Purchase Order</h4>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput1">Approver Name</label>
                                                        <input type="text" id="projectinput1"
                                                            name="approval_name" class="form-control"
                                                            placeholder="Approver Name" value="{{ old('approval_name') }}" >
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="issueinput6">Approver Emirates Id</label>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input"
                                                                name="emirates_id" id="inputGroupFile03"
                                                                required>
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Choose
                                                                    File</span>
                                                            </div>
                                                            <label class="custom-file-label"
                                                                for="inputGroupFile01">No
                                                                file chosen</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="row">
                                    <input type="hidden" name="verified" value="0" >
                                    <input type="hidden" name="status" value="0" >
                                   <!--   <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="status_label" for="issueinput6">Status</label>
                                            <fieldset class="checkboxsas">
                                                <label class="label-note">
                                                    <input type="checkbox" value="1" name="verified" {{ (old('verified') == 1) ? 'checked=checked' : '' }}>
                                                    Verified </label>
                                            </fieldset>
                                            <select id="status" name="status" class="form-control"
                                                data-original-title="" title="" required>
                                                <option value="">Select Status</option>
                                                <option {{ (old('status') == 1) ? 'selected=selected' : '' }} value="1">Active</option>
                                                <option {{ (old('status') == 0) ? 'selected=selected' : '' }} value="0">In-Active</option>
                                            </select>
                                        </div>
                                    </div> -->
                                    <div class="col-sm-6">
                                        <div class="form-actions right">
                                            <button id="next-tab" class="btn btn-primary"><i class=""></i>
                                                Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab2" aria-expanded="true"
                            aria-labelledby="base-tab2">
                            <div class="form">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="userinput5">Title</label>
                                                <select id="title" name="title" class="form-control"
                                                    data-original-title="" title="" required>
                                                    <option value="">Select title</option>
                                                    <option {{ (old('title') == 'Mr.') ? 'selected=selected' : '' }} value="Mr.">Mr</option>
                                                    <option {{ (old('title') == 'Ms.') ? 'selected=selected' : '' }} value="Ms.">Ms</option>
                                                    <option {{ (old('title') == 'Mrs.') ? 'selected=selected' : '' }} value="Mrs.">Mrs</option>
                                                    <option {{ (old('title') == 'Miss') ? 'selected=selected' : '' }} value="Miss">Miss</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="userinput5">First Name</label>
                                                <input class="form-control" type="text" name="first_name"
                                                    placeholder="" id="first_name" required value="{{ old('first_name') }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="userinput5">Last Name</label>
                                                <input class="form-control" type="text" name="last_name"
                                                    placeholder="" id="last_name" value="{{ old('last_name') }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="userinput5">Job Title</label>
                                                <input class="form-control" type="text" name="job_title"
                                                    placeholder="" id="job_title" required value="{{ old('job_title') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="userinput5">Primary Email</label>
                                                <input class="form-control" type="email" name="email"
                                                    placeholder="" id="email_user" required value="{{ old('email') }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="userinput5">Mobile Number</label>
                                                <input class="form-control" name="mobile_no" type="number"
                                                    placeholder="" id="mobile_no" required value="{{ old('mobile_no') }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="userinput5">Work Number</label>
                                                <input class="form-control" type="number" name="phone"
                                                    placeholder="" id="phone" value="{{ old('phone') }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="userinput5">Fax Number</label>
                                                <input class="form-control" type="text" name="fax"
                                                    placeholder="" id="fax" value="{{ old('fax') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="userinput5">Password</label>
                                                <input class="form-control" type="password" placeholder=""
                                                    id="password" required="required" name="password" value="{{ old('password') }}">
                                                <label class="label-note" for="password">Minimum Password length
                                                    must be 6 characters.</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="issueinput6">Emirates Id</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"
                                                        name="emirates_id_user" id="inputGroup01">
                                                    <div class="input-group-prepend"><span
                                                            class="input-group-text">Choose File</span>
                                                    </div>
                                                    <label class="custom-file-label" for="inputGroupFile01">No
                                                        file
                                                        chosen</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="issueinput6">Profile Picture</label>
                                                <div class="custom-file">
                                                    <input type="file" name="profile_picture"
                                                        class="custom-file-input" id="inputGroup02">
                                                    <div class="input-group-prepend"><span
                                                            class="input-group-text">Choose File</span>
                                                    </div>
                                                    <label class="custom-file-label" for="inputGroupFile01">No
                                                        file
                                                        chosen</label>
                                                </div>
                                            </div>
                                        </div>
                                         <input type="hidden" name="verified_user" value="0" >
                                         <input type="hidden" name="status_user" value="0" >

                                       <!--  <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="status_label" for="issueinput6">Status</label>
                                                <fieldset class="checkboxsas">
                                                    <label class="label-note">
                                                        <input type="checkbox" value="1" {{ (old('verified_user') == 1) ? 'checked=checked' : '' }} name="verified_user">
                                                        Verified </label>
                                                </fieldset>
                                                <select id="status_user" name="status_user" class="form-control"
                                                    data-original-title="" title="" required>
                                                    <option value="">Select Status</option>
                                                    <option {{ (old('status_user') == 1) ? 'selected=selected' : '' }} value="1">Active</option>
                                                    <option {{ (old('status_user') == 0) ? 'selected=selected' : '' }} value="0">In-Active</option>
                                                </select>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class=""></i> Add Vendor
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
        <!--main card end-->
    </div>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$("#delivery_method").select2({
    placeholder: 'Select Delivery Method',
    allowClear: true,
});
$("#payment_term").select2({
    placeholder: 'Select Payment Term',
    allowClear: true,
});
$("#payment_method").select2({
    placeholder: 'Select Payment Method',
});

 $('#products_supplied').select2({
        placeholder: 'Select Products Supplied',
        multiple: true,
        ajax: {
          url: base_url + 'admin/getroducts',
          dataType: 'json',
          delay: 250,
          
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
      });

//For same address
$(".same_addresses").change(function () {
    var checked = $(this).is(':checked');
    if (checked) {
        $('.shipping_address').val($('.billing_address').val()).attr("readonly", "readonly");
        $('.shipping_street').val($('.billing_street').val()).attr("readonly", "readonly");
        $('.shipping_pobox').val($('.billing_pobox').val()).attr("readonly", "readonly");
        $('.shipping_country_id').val($('.billing_country_id').val()).trigger('change.select2').attr(
            "readonly", "readonly");
        $('.shipping_city_id').html($('.billing_city_id').html());
        $('.shipping_city_id').val($('.billing_city_id').val()).trigger('change.select2').attr(
            "readonly", "readonly");
    } else {
        $('.shipping_address').val('').removeAttr("readonly");
        $('.shipping_street').val('').removeAttr("readonly");
        $('.shipping_pobox').val('').removeAttr("readonly");
        $('.shipping_country_id').val('').trigger('change').trigger('change.select2').removeAttr(
            "readonly");
        $('.shipping_city_id').val('').trigger('change.select2').removeAttr("readonly");
    }
}); // same_addresses onchange :: END

$('#next-tab').click(function (e) {
    //Initialize the validation object which will be called on form submit.
    var validobj = $('#ClientAddForm').validate({
        onkeyup: false,
        errorClass: 'error',
        // wrapper: 'div',
        messages: {
            "name": {
                required: 'Name of the company is mandatory.'
            },
            "first_name": {
                required: 'Please type in First name.'
            },
            "email": {
                required: 'Enter email address.'
            },
            "password": {
                required: 'Enter password.'
            },
        },

        //put error message behind each form element
        errorPlacement: function (error, element) {
            var elem = $(element);
            error.insertAfter(element);
        },

    }); // validobj :: END

    if ($('#SupplierAddForm').valid()) { // Checks if form is valid with all validations pass.
        // e.preventDefault();
        $('#tab1').removeClass('active');
        $('#tab1').removeClass('show');
        $('#tab2').addClass('show');
        $('#tab2').addClass('in');
        $('#tab2').addClass('active');
        $('#base-tab1').removeClass('active');
        $('#base-tab2').addClass('active in');
        return false;
    } else {
        console.log("I'm not valid");
    }
}); // next-tab :: END

$(".shipping_country_id").change(function(){
        var parent_id   = $(this).val();
        $.ajax({
            url: '{{ url('cities/getCitiesByCountryId') }}',
            type: 'post',
            data: {country_id: parent_id},
            dataType: 'json',
            cache: false,
            success: function (response) {
              if(response.status == 200){
                var citiesDropDown  =   $('.shipping_city_id');
                citiesDropDown.empty();
                citiesDropDown.append('<option> Select your City </option>');
                $.each(response.cities, function(key, value){
                        citiesDropDown.append('<option value="'+value.id+'">'+value.name+'</option>');
                });
                citiesDropDown.trigger('change.select2')
              }
              else 
                console.log('no results found');
              // var data = $.parseJSON(json);
              //  console.log(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // alert(textStatus + " - " + errorThrown + " -> Unable to fetch data.");
                console.log('no country id found something get went wrong');
            }
        });
    });



    $(".billing_country_id").change(function(){
         var parent_id   = $(this).val();
         $.ajax({
            url: '{{ url('cities/getCitiesByCountryId') }}',
            type: 'post',
            data: {country_id: parent_id},
            dataType: 'json',
            cache: false,
            success: function (response) {
               if(response.status == 200){
                var citiesDropDown  =   $('.billing_city_id');
                citiesDropDown.empty();
                citiesDropDown.append('<option> Select your City </option>');
                $.each(response.cities, function(key, value){
                        citiesDropDown.append('<option value="'+value.id+'">'+value.name+'</option>');
                });
                citiesDropDown.trigger('change.select2')
              }
              else 
                console.log('no results found');
              // var data = $.parseJSON(json);
              //  console.log(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // alert(textStatus + " - " + errorThrown + " -> Unable to fetch data.");
                console.log('no country id found something get went wrong');
            }
        });
    });

  </script>
    @endsection
