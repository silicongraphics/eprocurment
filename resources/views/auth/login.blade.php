@extends('layout.app')

@section('content')

    <div class="login-form">
		<div class="container">
			<h1>Welcome to E-Procurement</h1>
            <span>Please login to your account</span>
            @isset($url)
                <form class="form-login-panel" method="POST" action='{{ url("login/$url") }}'>
            @else
                <form class="form-login-panel" method="POST" action="{{ route('login') }}">
            @endisset
                @csrf
                <input type="hidden" name="type" value={{ Request::get('type') }}>   
                <p>Login</p>
                @if ($errors->has('password'))
                        <span class="alert alert-danger invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif 
                @if ($errors->has('email'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                @endif                   

				<div class="form-group">
					<input type="email" class="form-control username" id="email" name="email"
                           placeholder="Email" value="{{old('email')}}" required>
                        
				</div>
				<div class="form-group">
					<input type="password" class="form-control password" id="password" name="password" placeholder="Password"
						   required>

                </div>
				<div class="checkbox">
					@if(@$url == 'client')
					<label style="padding: 0;">
                   			<a href='{{ url("register/client") }}' class="login-forget">Don't have an account? Signup here </a>
                    </label>
                    @elseif(@$url == 'vendor')
                    	<label style="padding: 0;">
                           <a href='{{ url("register/vendor") }}' class="login-forget">Don't have an account? Signup here </a> 
                    	</label>
                    @endif
					<label>
						<a href="{{ url(@$url.'/password/reset') }}" class="login-forget">Forgot Password</a>
					</label>
                </div>
                <div class="text-center">
    				<button style="float:none" type="submit" id="login" name="submit" class="btn-subs">Login</button>
                </div>
			</form>
		</div>
@endsection
