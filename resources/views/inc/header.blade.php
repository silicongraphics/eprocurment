<header>
	<div class="overlay-form"></div>
	<div class="menu-btn col-md-3 col-sm-3 col-xs-3 no-padding-left">
		@if(Auth::check())
		<div class="menu-ico">
		<img src="{{ asset('images/menu-header-ico.png') }}" alt="Header Ico">
		<h3>Menu</h3>
		</div>
		@endif
	</div>
	<div class="logo login col-md-6 col-sm-6 col-xs-6">
		<img src="{{ asset('images/logo.png') }}" alt="Logo">
	</div>
	<div class="login-n-drop col-md-3 col-sm-3 col-xs-3">
			@if(Auth::check())
			<div class="login-ico">
			<img src="{{asset('images/login-photo.jpg')}}" alt="Header Ico">
			<h4>{{ Auth::guard()->user()->first_name }}<i class="fa fa-angle-down"></i> </h4>
			<ul>
<!--				<li><a href="#"><i class="fa fa-user"></i> My Profile</a></li>-->
				<li><a onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="javascript:;"><i class="fa fa-sign-out"></i> Logout</a></li>
			</ul>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
			</form>			
			</div>
			@endif			
	</div>
</header>

