@if(Auth::guard('client')->check())
<div class="menu-main-has">
	<div class="header-menu-main">
		<img src="{{asset('images/menu-c.png')}}" class="menu-close-has">
		<img src="{{asset('images/e-logo.png')}}" class="menu-logo-has">
	</div>
	<div class="menu-ul">
		<ul>
           <!--  <li><a href="{{ route('clienthome')}} ">Dashboard</a></li> -->
            <li><a href="{{ route('createrfq')}}  ">Create RFQ</a></li>
            <li><a href="{{ route('client-my-rfq')}}">My RFQs</a></li>
            <li><a href="{{ route('client-my-estimates')}}">My Estimates</a></li>
            <li><a href="{{ route('client-my-saved-rfq')}}">Saved Comparison Tables</a></li>
            <li><a href="{{route('client-po')}}">My Purchase Orders</a></li>
		</ul>
	</div>
</div>
@elseif(Auth::guard('vendor')->check())
<div class="menu-main-has">
	<div class="header-menu-main">
		<img src="{{asset('images/menu-c.png')}}" class="menu-close-has">
		<img src="{{asset('images/e-logo.png')}}" class="menu-logo-has">
	</div>
	<div class="menu-ul">
		<ul>
           <!--  <li><a href="{{ route('vendorHome')}} ">Dashboard</a></li> -->
            <li><a href="{{ route('vendor-my-rfq')}}">Received RFQs</a></li>
            <li><a href="{{ route('vendor-my-estimated')}}">Sent Estimates</a></li>
                        <li><a href="{{ route('vendor-po')}}">Received Purchase Orders</a></li>
            <li><a href="{{ route('vendor-my-products')}}">My Products</a></li>
		</ul>
	</div>
</div>
@else
    <div class="left-panel">
        Home
    </div> 
@endif
